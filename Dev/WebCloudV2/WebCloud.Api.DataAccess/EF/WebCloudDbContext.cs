using System.Data.Entity;
using WebCloud.Api.DataAccess.EF.Mapping;
using WebCloud.Entity;

namespace WebCloud.Api.DataAccess.EF
{
    public partial class WebCloudDbContext : DbContext
    {
        static WebCloudDbContext()
        {
            Database.SetInitializer<WebCloudDbContext>(null);
        }

        public WebCloudDbContext()
            : base("Name=WebCloudV2Context")
        {
        }

        public DbSet<Application> Applications { get; set; }
        public DbSet<Article> Articles { get; set; }
        public DbSet<ArticleCategory> ArticleCategories { get; set; }
        public DbSet<ArticleCategoryContent> ArticleCategoryContents { get; set; }
        public DbSet<ArticleCategoryRelationship> ArticleCategoryRelationships { get; set; }
        public DbSet<ArticleContent> ArticleContents { get; set; }
        public DbSet<ArticleInCategory> ArticleInCategories { get; set; }
        public DbSet<Config> Configs { get; set; }
        public DbSet<ConfigInWebsite> ConfigInWebsites { get; set; }
        public DbSet<Discussion> Discussions { get; set; }
        public DbSet<DiscussionGroup> DiscussionGroups { get; set; }
        public DbSet<Language> Languages { get; set; }
        public DbSet<LanguageInWebsite> LanguageInWebsites { get; set; }
        public DbSet<Layout> Layouts { get; set; }
        public DbSet<Log> Logs { get; set; }
        public DbSet<LogGroup> LogGroups { get; set; }
        public DbSet<MenuItem> MenuItems { get; set; }
        public DbSet<MenuItemContent> MenuItemContents { get; set; }
        public DbSet<Module> Modules { get; set; }
        public DbSet<ModuleBase> ModuleBases { get; set; }
        public DbSet<ModuleBaseConfig> ModuleBaseConfigs { get; set; }
        public DbSet<ModuleBaseInWebsite> ModuleBaseInWebsites { get; set; }
        public DbSet<ModuleConfig> ModuleConfigs { get; set; }
        public DbSet<OAuthProvider> OAuthProviders { get; set; }
        public DbSet<Page> Pages { get; set; }
        public DbSet<Product> Products { get; set; }
        public DbSet<ProductCategory> ProductCategories { get; set; }
        public DbSet<ProductCategoryContent> ProductCategoryContents { get; set; }
        public DbSet<ProductCategoryRelationship> ProductCategoryRelationships { get; set; }
        public DbSet<ProductContent> ProductContents { get; set; }
        public DbSet<ProductFilter> ProductFilters { get; set; }
        public DbSet<ProductFilterRange> ProductFilterRanges { get; set; }
        public DbSet<ProductInCategory> ProductInCategories { get; set; }
        public DbSet<ProductProperty> ProductProperties { get; set; }
        public DbSet<ProductPropertyContent> ProductPropertyContents { get; set; }
        public DbSet<ProductPropertyValue> ProductPropertyValues { get; set; }
        public DbSet<RefreshToken> RefreshTokens { get; set; }
        public DbSet<Resource> Resources { get; set; }
        public DbSet<ResourceValue> ResourceValues { get; set; }
        public DbSet<Statistic> Statistics { get; set; }
        public DbSet<Tag> Tags { get; set; }
        public DbSet<Text> Texts { get; set; }
        public DbSet<Theme> Themes { get; set; }
        public DbSet<ThemeCategory> ThemeCategories { get; set; }
        public DbSet<User> Users { get; set; }
        public DbSet<UserGroup> UserGroups { get; set; }
        public DbSet<UserInGroup> UserInGroups { get; set; }
        public DbSet<UserOAuthMembership> UserOAuthMemberships { get; set; }
        public DbSet<UserRole> UserRoles { get; set; }
        public DbSet<WebGroup> WebGroups { get; set; }
        public DbSet<WebPlan> WebPlans { get; set; }
        public DbSet<Website> Websites { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Configurations.Add(new ApplicationMap());
            modelBuilder.Configurations.Add(new ArticleMap());
            modelBuilder.Configurations.Add(new ArticleCategoryMap());
            modelBuilder.Configurations.Add(new ArticleCategoryContentMap());
            modelBuilder.Configurations.Add(new ArticleCategoryRelationshipMap());
            modelBuilder.Configurations.Add(new ArticleContentMap());
            modelBuilder.Configurations.Add(new ArticleInCategoryMap());
            modelBuilder.Configurations.Add(new ConfigMap());
            modelBuilder.Configurations.Add(new ConfigInWebsiteMap());
            modelBuilder.Configurations.Add(new DiscussionMap());
            modelBuilder.Configurations.Add(new DiscussionGroupMap());
            modelBuilder.Configurations.Add(new LanguageMap());
            modelBuilder.Configurations.Add(new LanguageInWebsiteMap());
            modelBuilder.Configurations.Add(new LayoutMap());
            modelBuilder.Configurations.Add(new LogMap());
            modelBuilder.Configurations.Add(new LogGroupMap());
            modelBuilder.Configurations.Add(new MenuItemMap());
            modelBuilder.Configurations.Add(new MenuItemContentMap());
            modelBuilder.Configurations.Add(new ModuleMap());
            modelBuilder.Configurations.Add(new ModuleBaseMap());
            modelBuilder.Configurations.Add(new ModuleBaseConfigMap());
            modelBuilder.Configurations.Add(new ModuleBaseInWebsiteMap());
            modelBuilder.Configurations.Add(new ModuleConfigMap());
            modelBuilder.Configurations.Add(new OAuthProviderMap());
            modelBuilder.Configurations.Add(new PageMap());
            modelBuilder.Configurations.Add(new ProductMap());
            modelBuilder.Configurations.Add(new ProductCategoryMap());
            modelBuilder.Configurations.Add(new ProductCategoryContentMap());
            modelBuilder.Configurations.Add(new ProductCategoryRelationshipMap());
            modelBuilder.Configurations.Add(new ProductContentMap());
            modelBuilder.Configurations.Add(new ProductFilterMap());
            modelBuilder.Configurations.Add(new ProductFilterRangeMap());
            modelBuilder.Configurations.Add(new ProductInCategoryMap());
            modelBuilder.Configurations.Add(new ProductPropertyMap());
            modelBuilder.Configurations.Add(new ProductPropertyContentMap());
            modelBuilder.Configurations.Add(new ProductPropertyValueMap());
            modelBuilder.Configurations.Add(new RefreshTokenMap());
            modelBuilder.Configurations.Add(new ResourceMap());
            modelBuilder.Configurations.Add(new ResourceValueMap());
            modelBuilder.Configurations.Add(new StatisticMap());
            modelBuilder.Configurations.Add(new TagMap());
            modelBuilder.Configurations.Add(new TextMap());
            modelBuilder.Configurations.Add(new ThemeMap());
            modelBuilder.Configurations.Add(new ThemeCategoryMap());
            modelBuilder.Configurations.Add(new UserMap());
            modelBuilder.Configurations.Add(new UserGroupMap());
            modelBuilder.Configurations.Add(new UserInGroupMap());
            modelBuilder.Configurations.Add(new UserOAuthMembershipMap());
            modelBuilder.Configurations.Add(new UserRoleMap());
            modelBuilder.Configurations.Add(new WebGroupMap());
            modelBuilder.Configurations.Add(new WebPlanMap());
            modelBuilder.Configurations.Add(new WebsiteMap());
        }
    }
}
