using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;using WebCloud.Entity;

namespace WebCloud.Api.DataAccess.EF.Mapping
{
    public class ModuleConfigMap : EntityTypeConfiguration<ModuleConfig>
    {
        public ModuleConfigMap()
        {
            // Primary Key
            this.HasKey(t => new { t.WebsiteID, t.ModuleID, t.ModuleBaseConfigID, t.ModuleBaseID });

            // Properties
            this.Property(t => t.WebsiteID)
                .IsRequired()
                .HasMaxLength(50);

            this.Property(t => t.ModuleID)
                .IsRequired()
                .HasMaxLength(50);

            this.Property(t => t.ModuleBaseConfigID)
                .IsRequired()
                .HasMaxLength(50);

            this.Property(t => t.ModuleBaseID)
                .IsRequired()
                .HasMaxLength(50);

            this.Property(t => t.Value)
                .IsRequired();

            // Table & Column Mappings
            this.ToTable("ModuleConfig");
            this.Property(t => t.WebsiteID).HasColumnName("WebsiteID");
            this.Property(t => t.ModuleID).HasColumnName("ModuleID");
            this.Property(t => t.ModuleBaseConfigID).HasColumnName("ModuleBaseConfigID");
            this.Property(t => t.ModuleBaseID).HasColumnName("ModuleBaseID");
            this.Property(t => t.Value).HasColumnName("Value");

            // Relationships
            this.HasRequired(t => t.Module)
                .WithMany(t => t.ModuleConfigs)
                .HasForeignKey(d => new { d.WebsiteID, d.ModuleID });
            this.HasRequired(t => t.ModuleBaseConfig)
                .WithMany(t => t.ModuleConfigs)
                .HasForeignKey(d => new { d.ModuleBaseConfigID, d.ModuleBaseID });

        }
    }
}
