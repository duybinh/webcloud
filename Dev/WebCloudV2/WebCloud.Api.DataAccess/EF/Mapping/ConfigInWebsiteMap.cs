using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;using WebCloud.Entity;

namespace WebCloud.Api.DataAccess.EF.Mapping
{
    public class ConfigInWebsiteMap : EntityTypeConfiguration<ConfigInWebsite>
    {
        public ConfigInWebsiteMap()
        {
            // Primary Key
            this.HasKey(t => new { t.WebsiteID, t.ConfigID });

            // Properties
            this.Property(t => t.WebsiteID)
                .IsRequired()
                .HasMaxLength(50);

            this.Property(t => t.ConfigID)
                .IsRequired()
                .HasMaxLength(50);

            this.Property(t => t.Value)
                .IsRequired();

            // Table & Column Mappings
            this.ToTable("ConfigInWebsite");
            this.Property(t => t.WebsiteID).HasColumnName("WebsiteID");
            this.Property(t => t.ConfigID).HasColumnName("ConfigID");
            this.Property(t => t.Value).HasColumnName("Value");

            // Relationships
            this.HasRequired(t => t.Config)
                .WithMany(t => t.ConfigInWebsites)
                .HasForeignKey(d => d.ConfigID);
            this.HasRequired(t => t.Website)
                .WithMany(t => t.ConfigInWebsites)
                .HasForeignKey(d => d.WebsiteID);

        }
    }
}
