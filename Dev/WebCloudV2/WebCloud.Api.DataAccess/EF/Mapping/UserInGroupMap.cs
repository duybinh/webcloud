using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;using WebCloud.Entity;

namespace WebCloud.Api.DataAccess.EF.Mapping
{
    public class UserInGroupMap : EntityTypeConfiguration<UserInGroup>
    {
        public UserInGroupMap()
        {
            // Primary Key
            this.HasKey(t => new { t.WebsiteID, t.UserGroupID, t.UserID });

            // Properties
            this.Property(t => t.WebsiteID)
                .IsRequired()
                .HasMaxLength(50);

            this.Property(t => t.UserGroupID)
                .IsRequired()
                .HasMaxLength(50);

            this.Property(t => t.UserID)
                .IsRequired()
                .HasMaxLength(50);

            // Table & Column Mappings
            this.ToTable("UserInGroup");
            this.Property(t => t.WebsiteID).HasColumnName("WebsiteID");
            this.Property(t => t.UserGroupID).HasColumnName("UserGroupID");
            this.Property(t => t.UserID).HasColumnName("UserID");

            // Relationships
            this.HasRequired(t => t.User)
                .WithMany(t => t.UserInGroups)
                .HasForeignKey(d => new { d.WebsiteID, d.UserID });
            this.HasRequired(t => t.UserGroup)
                .WithMany(t => t.UserInGroups)
                .HasForeignKey(d => new { d.WebsiteID, d.UserGroupID });

        }
    }
}
