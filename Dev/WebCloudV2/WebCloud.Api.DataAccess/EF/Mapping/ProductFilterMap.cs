using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;using WebCloud.Entity;

namespace WebCloud.Api.DataAccess.EF.Mapping
{
    public class ProductFilterMap : EntityTypeConfiguration<ProductFilter>
    {
        public ProductFilterMap()
        {
            // Primary Key
            this.HasKey(t => new { t.WebsiteID, t.ProductFilterID });

            // Properties
            this.Property(t => t.WebsiteID)
                .IsRequired()
                .HasMaxLength(50);

            this.Property(t => t.ProductFilterID)
                .IsRequired()
                .HasMaxLength(50);

            this.Property(t => t.ProductPropertyID)
                .IsRequired()
                .HasMaxLength(50);

            // Table & Column Mappings
            this.ToTable("ProductFilter");
            this.Property(t => t.WebsiteID).HasColumnName("WebsiteID");
            this.Property(t => t.ProductFilterID).HasColumnName("ProductFilterID");
            this.Property(t => t.ProductPropertyID).HasColumnName("ProductPropertyID");
            this.Property(t => t.AllowMultyChoice).HasColumnName("AllowMultyChoice");
            this.Property(t => t.SortOrder).HasColumnName("SortOrder");
            this.Property(t => t.IsEnabled).HasColumnName("IsEnabled");
            this.Property(t => t.IsDeleted).HasColumnName("IsDeleted");

            // Relationships
            this.HasRequired(t => t.Website)
                .WithMany(t => t.ProductFilters)
                .HasForeignKey(d => d.WebsiteID);

        }
    }
}
