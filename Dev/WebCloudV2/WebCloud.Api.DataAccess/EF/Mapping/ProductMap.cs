using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;using WebCloud.Entity;

namespace WebCloud.Api.DataAccess.EF.Mapping
{
    public class ProductMap : EntityTypeConfiguration<Product>
    {
        public ProductMap()
        {
            // Primary Key
            this.HasKey(t => new { t.WebsiteID, t.ProductID });

            // Properties
            this.Property(t => t.WebsiteID)
                .IsRequired()
                .HasMaxLength(50);

            this.Property(t => t.ProductID)
                .IsRequired()
                .HasMaxLength(50);

            this.Property(t => t.GlobalID)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);

            this.Property(t => t.ThumbImage)
                .IsRequired()
                .HasMaxLength(300);

            this.Property(t => t.CreatedUserName)
                .HasMaxLength(50);

            this.Property(t => t.LastEditedUserName)
                .HasMaxLength(50);

            // Table & Column Mappings
            this.ToTable("Product");
            this.Property(t => t.WebsiteID).HasColumnName("WebsiteID");
            this.Property(t => t.ProductID).HasColumnName("ProductID");
            this.Property(t => t.GlobalID).HasColumnName("GlobalID");
            this.Property(t => t.ThumbImage).HasColumnName("ThumbImage");
            this.Property(t => t.Price).HasColumnName("Price");
            this.Property(t => t.OldPrice).HasColumnName("OldPrice");
            this.Property(t => t.IsNew).HasColumnName("IsNew");
            this.Property(t => t.IsHot).HasColumnName("IsHot");
            this.Property(t => t.IsDiscount).HasColumnName("IsDiscount");
            this.Property(t => t.IsAvailable).HasColumnName("IsAvailable");
            this.Property(t => t.AllowOrder).HasColumnName("AllowOrder");
            this.Property(t => t.AllowComment).HasColumnName("AllowComment");
            this.Property(t => t.CallToPrice).HasColumnName("CallToPrice");
            this.Property(t => t.DisplayDatetime).HasColumnName("DisplayDatetime");
            this.Property(t => t.SortOrder).HasColumnName("SortOrder");
            this.Property(t => t.IsEnabled).HasColumnName("IsEnabled");
            this.Property(t => t.IsDeleted).HasColumnName("IsDeleted");
            this.Property(t => t.DiscussionGroupID).HasColumnName("DiscussionGroupID");
            this.Property(t => t.StatisticID).HasColumnName("StatisticID");
            this.Property(t => t.LogGroupID).HasColumnName("LogGroupID");
            this.Property(t => t.CreatedDatetime).HasColumnName("CreatedDatetime");
            this.Property(t => t.LastEditedDatetime).HasColumnName("LastEditedDatetime");
            this.Property(t => t.CreatedUserName).HasColumnName("CreatedUserName");
            this.Property(t => t.LastEditedUserName).HasColumnName("LastEditedUserName");
            this.Property(t => t.CreatedUserID).HasColumnName("CreatedUserID");
            this.Property(t => t.LastEditedUserID).HasColumnName("LastEditedUserID");

            // Relationships
            this.HasMany(t => t.Tags)
                .WithMany(t => t.Products)
                .Map(m =>
                    {
                        m.ToTable("TagInProduct");
                        m.MapLeftKey("WebsiteID", "ProductID");
                        m.MapRightKey("TagID");
                    });

            this.HasOptional(t => t.DiscussionGroup)
                .WithMany(t => t.Products)
                .HasForeignKey(d => d.DiscussionGroupID);
            this.HasOptional(t => t.LogGroup)
                .WithMany(t => t.Products)
                .HasForeignKey(d => d.LogGroupID);
            this.HasOptional(t => t.Statistic)
                .WithMany(t => t.Products)
                .HasForeignKey(d => d.StatisticID);
            this.HasRequired(t => t.Website)
                .WithMany(t => t.Products)
                .HasForeignKey(d => d.WebsiteID);

        }
    }
}
