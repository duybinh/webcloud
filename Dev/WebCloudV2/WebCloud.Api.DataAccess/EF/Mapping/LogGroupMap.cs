using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;using WebCloud.Entity;

namespace WebCloud.Api.DataAccess.EF.Mapping
{
    public class LogGroupMap : EntityTypeConfiguration<LogGroup>
    {
        public LogGroupMap()
        {
            // Primary Key
            this.HasKey(t => t.LogGroupID);

            // Properties
            this.Property(t => t.ReferenceTable)
                .HasMaxLength(100);

            this.Property(t => t.ItemID)
                .HasMaxLength(50);

            this.Property(t => t.WebsiteID)
                .HasMaxLength(50);

            // Table & Column Mappings
            this.ToTable("LogGroup");
            this.Property(t => t.LogGroupID).HasColumnName("LogGroupID");
            this.Property(t => t.ReferenceTable).HasColumnName("ReferenceTable");
            this.Property(t => t.ItemID).HasColumnName("ItemID");
            this.Property(t => t.WebsiteID).HasColumnName("WebsiteID");
        }
    }
}
