using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;using WebCloud.Entity;

namespace WebCloud.Api.DataAccess.EF.Mapping
{
    public class ProductPropertyContentMap : EntityTypeConfiguration<ProductPropertyContent>
    {
        public ProductPropertyContentMap()
        {
            // Primary Key
            this.HasKey(t => new { t.WebsiteID, t.LanguageID, t.ProductPropertyID });

            // Properties
            this.Property(t => t.WebsiteID)
                .IsRequired()
                .HasMaxLength(50);

            this.Property(t => t.LanguageID)
                .IsRequired()
                .HasMaxLength(10);

            this.Property(t => t.ProductPropertyID)
                .IsRequired()
                .HasMaxLength(50);

            this.Property(t => t.Name)
                .IsRequired()
                .HasMaxLength(250);

            this.Property(t => t.Unit)
                .HasMaxLength(100);

            this.Property(t => t.DefaultTextValue)
                .HasMaxLength(500);

            // Table & Column Mappings
            this.ToTable("ProductPropertyContent");
            this.Property(t => t.WebsiteID).HasColumnName("WebsiteID");
            this.Property(t => t.LanguageID).HasColumnName("LanguageID");
            this.Property(t => t.ProductPropertyID).HasColumnName("ProductPropertyID");
            this.Property(t => t.Name).HasColumnName("Name");
            this.Property(t => t.Unit).HasColumnName("Unit");
            this.Property(t => t.DefaultTextValue).HasColumnName("DefaultTextValue");
            this.Property(t => t.ValueRange).HasColumnName("ValueRange");

            // Relationships
            this.HasRequired(t => t.Language)
                .WithMany(t => t.ProductPropertyContents)
                .HasForeignKey(d => d.LanguageID);
            this.HasRequired(t => t.ProductProperty)
                .WithMany(t => t.ProductPropertyContents)
                .HasForeignKey(d => new { d.WebsiteID, d.ProductPropertyID });

        }
    }
}
