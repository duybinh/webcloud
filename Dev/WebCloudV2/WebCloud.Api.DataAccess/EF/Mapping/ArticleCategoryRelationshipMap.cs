using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;
using WebCloud.Entity;

namespace WebCloud.Api.DataAccess.EF.Mapping
{
    public class ArticleCategoryRelationshipMap : EntityTypeConfiguration<ArticleCategoryRelationship>
    {
        public ArticleCategoryRelationshipMap()
        {
            // Primary Key
            this.HasKey(t => new { t.WebsiteID, t.ParentID });

            // Properties
            this.Property(t => t.WebsiteID)
                .IsRequired()
                .HasMaxLength(50);

            this.Property(t => t.ParentID)
                .IsRequired()
                .HasMaxLength(50);

            this.Property(t => t.ChildID)
                .IsRequired()
                .HasMaxLength(50);

            // Table & Column Mappings
            this.ToTable("ArticleCategoryRelationship");
            this.Property(t => t.WebsiteID).HasColumnName("WebsiteID");
            this.Property(t => t.ParentID).HasColumnName("ParentID");
            this.Property(t => t.ChildID).HasColumnName("ChildID");
            this.Property(t => t.Level).HasColumnName("Level");

            // Relationships
            this.HasRequired(t => t.ArticleCategory)
                .WithOptional(t => t.ArticleCategoryRelationship);

        }
    }
}
