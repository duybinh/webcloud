using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;using WebCloud.Entity;

namespace WebCloud.Api.DataAccess.EF.Mapping
{
    public class ProductFilterRangeMap : EntityTypeConfiguration<ProductFilterRange>
    {
        public ProductFilterRangeMap()
        {
            // Primary Key
            this.HasKey(t => new { t.WebsiteID, t.ProductFilterRangeID });

            // Properties
            this.Property(t => t.WebsiteID)
                .IsRequired()
                .HasMaxLength(50);

            this.Property(t => t.ProductFilterRangeID)
                .IsRequired()
                .HasMaxLength(50);

            this.Property(t => t.ProductFilterID)
                .IsRequired()
                .HasMaxLength(50);

            this.Property(t => t.TextValueEqual)
                .HasMaxLength(500);

            // Table & Column Mappings
            this.ToTable("ProductFilterRange");
            this.Property(t => t.WebsiteID).HasColumnName("WebsiteID");
            this.Property(t => t.ProductFilterRangeID).HasColumnName("ProductFilterRangeID");
            this.Property(t => t.ProductFilterID).HasColumnName("ProductFilterID");
            this.Property(t => t.TextValueEqual).HasColumnName("TextValueEqual");
            this.Property(t => t.NumberValueEqual).HasColumnName("NumberValueEqual");
            this.Property(t => t.BooleanValueEqual).HasColumnName("BooleanValueEqual");
            this.Property(t => t.DatetimeValueEqual).HasColumnName("DatetimeValueEqual");
            this.Property(t => t.NumberValueMin).HasColumnName("NumberValueMin");
            this.Property(t => t.DatetimeValueMin).HasColumnName("DatetimeValueMin");
            this.Property(t => t.NumberValueMax).HasColumnName("NumberValueMax");
            this.Property(t => t.DatetimeValueMax).HasColumnName("DatetimeValueMax");
            this.Property(t => t.SortOrder).HasColumnName("SortOrder");

            // Relationships
            this.HasRequired(t => t.ProductFilter)
                .WithMany(t => t.ProductFilterRanges)
                .HasForeignKey(d => new { d.WebsiteID, d.ProductFilterID });

        }
    }
}
