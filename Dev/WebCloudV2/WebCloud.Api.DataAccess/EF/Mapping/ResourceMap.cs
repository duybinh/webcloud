using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;
using WebCloud.Entity;

namespace WebCloud.Api.DataAccess.EF.Mapping
{
    public class ResourceMap : EntityTypeConfiguration<Resource>
    {
        public ResourceMap()
        {
            // Primary Key
            this.HasKey(t => new { t.ResourceID, t.WebsiteID });

            // Properties
            this.Property(t => t.ResourceID)
                .IsRequired()
                .HasMaxLength(50);

            this.Property(t => t.WebsiteID)
                .IsRequired()
                .HasMaxLength(50);

            this.Property(t => t.Group)
                .HasMaxLength(250);

            this.Property(t => t.Name)
                .HasMaxLength(250);

            this.Property(t => t.Description)
                .HasMaxLength(500);

            // Table & Column Mappings
            this.ToTable("Resource");
            this.Property(t => t.ResourceID).HasColumnName("ResourceID");
            this.Property(t => t.WebsiteID).HasColumnName("WebsiteID");
            this.Property(t => t.Group).HasColumnName("Group");
            this.Property(t => t.Name).HasColumnName("Name");
            this.Property(t => t.Description).HasColumnName("Description");
            this.Property(t => t.DisplayAsHtml).HasColumnName("DisplayAsHtml");
            this.Property(t => t.RolesToView).HasColumnName("RolesToView");
            this.Property(t => t.RolesToEdit).HasColumnName("RolesToEdit");
            this.Property(t => t.IsEnabled).HasColumnName("IsEnabled");
            this.Property(t => t.SortOrder).HasColumnName("SortOrder");

            // Relationships
            this.HasRequired(t => t.Website)
                .WithMany(t => t.Resources)
                .HasForeignKey(d => d.WebsiteID);

        }
    }
}
