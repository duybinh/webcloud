using System.Data.Entity.ModelConfiguration;
using WebCloud.Entity;

namespace WebCloud.Api.DataAccess.EF.Mapping
{
    public class TextMap : EntityTypeConfiguration<Text>
    {
        public TextMap()
        {
            // Primary Key
            this.HasKey(t => new { t.WebsiteID, t.TextID, t.LanguageID });

            // Properties
            this.Property(t => t.WebsiteID)
                .IsRequired()
                .HasMaxLength(50);

            this.Property(t => t.TextID)
                .IsRequired()
                .HasMaxLength(250);

            this.Property(t => t.LanguageID)
                .IsRequired()
                .HasMaxLength(10);

            this.Property(t => t.ReferenceTable)
                .HasMaxLength(250);

            this.Property(t => t.ReferenceField)
                .HasMaxLength(250);

            this.Property(t => t.ReferenceObject)
                .HasMaxLength(250);

            // Table & Column Mappings
            this.ToTable("Text");
            this.Property(t => t.WebsiteID).HasColumnName("WebsiteID");
            this.Property(t => t.TextID).HasColumnName("TextID");
            this.Property(t => t.LanguageID).HasColumnName("LanguageID");
            this.Property(t => t.Value).HasColumnName("Value");
            this.Property(t => t.ReferenceTable).HasColumnName("ReferenceTable");
            this.Property(t => t.ReferenceField).HasColumnName("ReferenceField");
            this.Property(t => t.ReferenceObject).HasColumnName("ReferenceObject");

            // Relationships
            this.HasRequired(t => t.Language)
                .WithMany(t => t.Texts)
                .HasForeignKey(d => d.LanguageID);
            this.HasRequired(t => t.Website)
                .WithMany(t => t.Texts)
                .HasForeignKey(d => d.WebsiteID);

        }
    }
}
