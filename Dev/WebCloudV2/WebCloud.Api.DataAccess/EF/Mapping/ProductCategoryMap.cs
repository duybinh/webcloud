using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;using WebCloud.Entity;

namespace WebCloud.Api.DataAccess.EF.Mapping
{
    public class ProductCategoryMap : EntityTypeConfiguration<ProductCategory>
    {
        public ProductCategoryMap()
        {
            // Primary Key
            this.HasKey(t => new { t.WebsiteID, t.ProductCategoryID });

            // Properties
            this.Property(t => t.WebsiteID)
                .IsRequired()
                .HasMaxLength(50);

            this.Property(t => t.ProductCategoryID)
                .IsRequired()
                .HasMaxLength(50);

            this.Property(t => t.ParentID)
                .HasMaxLength(50);

            this.Property(t => t.ThumbImage)
                .HasMaxLength(250);

            this.Property(t => t.CreatingUser)
                .HasMaxLength(50);

            this.Property(t => t.LastEditedUser)
                .HasMaxLength(50);

            // Table & Column Mappings
            this.ToTable("ProductCategory");
            this.Property(t => t.WebsiteID).HasColumnName("WebsiteID");
            this.Property(t => t.ProductCategoryID).HasColumnName("ProductCategoryID");
            this.Property(t => t.ParentID).HasColumnName("ParentID");
            this.Property(t => t.ThumbImage).HasColumnName("ThumbImage");
            this.Property(t => t.DisplayProductThumb).HasColumnName("DisplayProductThumb");
            this.Property(t => t.IsHot).HasColumnName("IsHot");
            this.Property(t => t.AllowComment).HasColumnName("AllowComment");
            this.Property(t => t.IsEnabled).HasColumnName("IsEnabled");
            this.Property(t => t.IsDeleted).HasColumnName("IsDeleted");
            this.Property(t => t.SortOrder).HasColumnName("SortOrder");
            this.Property(t => t.IsLeaf).HasColumnName("IsLeaf");
            this.Property(t => t.LogGroupID).HasColumnName("LogGroupID");
            this.Property(t => t.CreatedDatetime).HasColumnName("CreatedDatetime");
            this.Property(t => t.LastEditedDatetime).HasColumnName("LastEditedDatetime");
            this.Property(t => t.CreatingUser).HasColumnName("CreatingUser");
            this.Property(t => t.LastEditedUser).HasColumnName("LastEditedUser");

            // Relationships
            this.HasRequired(t => t.Website)
                .WithMany(t => t.ProductCategories)
                .HasForeignKey(d => d.WebsiteID);

        }
    }
}
