using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;
using WebCloud.Entity;

namespace WebCloud.Api.DataAccess.EF.Mapping
{
    public class LayoutMap : EntityTypeConfiguration<Layout>
    {
        public LayoutMap()
        {
            // Primary Key
            this.HasKey(t => new { t.LayoutID, t.WebsiteID });

            // Properties
            this.Property(t => t.LayoutID)
                .IsRequired()
                .HasMaxLength(50);

            this.Property(t => t.WebsiteID)
                .IsRequired()
                .HasMaxLength(50);

            this.Property(t => t.Name)
                .IsRequired()
                .HasMaxLength(250);

            this.Property(t => t.Description)
                .HasMaxLength(500);

            this.Property(t => t.Content)
                .IsRequired();

            // Table & Column Mappings
            this.ToTable("Layout");
            this.Property(t => t.LayoutID).HasColumnName("LayoutID");
            this.Property(t => t.WebsiteID).HasColumnName("WebsiteID");
            this.Property(t => t.Name).HasColumnName("Name");
            this.Property(t => t.Description).HasColumnName("Description");
            this.Property(t => t.Content).HasColumnName("Content");
            this.Property(t => t.SortOrder).HasColumnName("SortOrder");
            this.Property(t => t.IsEnabled).HasColumnName("IsEnabled");
            this.Property(t => t.Type).HasColumnName("Type");

            // Relationships
            this.HasRequired(t => t.Website)
                .WithMany(t => t.Layouts)
                .HasForeignKey(d => d.WebsiteID);

        }
    }
}
