using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;using WebCloud.Entity;

namespace WebCloud.Api.DataAccess.EF.Mapping
{
    public class LogMap : EntityTypeConfiguration<Log>
    {
        public LogMap()
        {
            // Primary Key
            this.HasKey(t => t.LogID);

            // Properties
            this.Property(t => t.Action)
                .IsRequired()
                .HasMaxLength(50);

            this.Property(t => t.ActionDetail)
                .HasMaxLength(250);

            // Table & Column Mappings
            this.ToTable("Log");
            this.Property(t => t.LogID).HasColumnName("LogID");
            this.Property(t => t.LogGroupID).HasColumnName("LogGroupID");
            this.Property(t => t.ActionDatetime).HasColumnName("ActionDatetime");
            this.Property(t => t.Action).HasColumnName("Action");
            this.Property(t => t.ActionDetail).HasColumnName("ActionDetail");
            this.Property(t => t.Actor).HasColumnName("Actor");

            // Relationships
            this.HasRequired(t => t.LogGroup)
                .WithMany(t => t.Logs)
                .HasForeignKey(d => d.LogGroupID);

        }
    }
}
