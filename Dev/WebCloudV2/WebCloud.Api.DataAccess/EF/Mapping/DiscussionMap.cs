using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;using WebCloud.Entity;

namespace WebCloud.Api.DataAccess.EF.Mapping
{
    public class DiscussionMap : EntityTypeConfiguration<Discussion>
    {
        public DiscussionMap()
        {
            // Primary Key
            this.HasKey(t => t.DiscussionID);

            // Properties
            this.Property(t => t.Title)
                .HasMaxLength(200);

            this.Property(t => t.Avarta)
                .HasMaxLength(250);

            this.Property(t => t.Name)
                .HasMaxLength(100);

            this.Property(t => t.Email)
                .HasMaxLength(200);

            this.Property(t => t.CreatingUser)
                .HasMaxLength(50);

            this.Property(t => t.LastEditedUser)
                .HasMaxLength(50);

            // Table & Column Mappings
            this.ToTable("Discussion");
            this.Property(t => t.DiscussionID).HasColumnName("DiscussionID");
            this.Property(t => t.DiscussionGroupID).HasColumnName("DiscussionGroupID");
            this.Property(t => t.Title).HasColumnName("Title");
            this.Property(t => t.Content).HasColumnName("Content");
            this.Property(t => t.Avarta).HasColumnName("Avarta");
            this.Property(t => t.Name).HasColumnName("Name");
            this.Property(t => t.Email).HasColumnName("Email");
            this.Property(t => t.IsReplyOf).HasColumnName("IsReplyOf");
            this.Property(t => t.Rate).HasColumnName("Rate");
            this.Property(t => t.LikeCount).HasColumnName("LikeCount");
            this.Property(t => t.DislikeCount).HasColumnName("DislikeCount");
            this.Property(t => t.IsApproved).HasColumnName("IsApproved");
            this.Property(t => t.IsEnabled).HasColumnName("IsEnabled");
            this.Property(t => t.IsDeleted).HasColumnName("IsDeleted");
            this.Property(t => t.CreatedDatetime).HasColumnName("CreatedDatetime");
            this.Property(t => t.LastEditedDatetime).HasColumnName("LastEditedDatetime");
            this.Property(t => t.CreatingUser).HasColumnName("CreatingUser");
            this.Property(t => t.LastEditedUser).HasColumnName("LastEditedUser");

            // Relationships
            this.HasRequired(t => t.DiscussionGroup)
                .WithMany(t => t.Discussions)
                .HasForeignKey(d => d.DiscussionGroupID);

        }
    }
}
