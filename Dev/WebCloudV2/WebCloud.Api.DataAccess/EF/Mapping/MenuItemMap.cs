using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;using WebCloud.Entity;

namespace WebCloud.Api.DataAccess.EF.Mapping
{
    public class MenuItemMap : EntityTypeConfiguration<MenuItem>
    {
        public MenuItemMap()
        {
            // Primary Key
            this.HasKey(t => new { t.WebsiteID, t.MenuItemID });

            // Properties
            this.Property(t => t.WebsiteID)
                .IsRequired()
                .HasMaxLength(50);

            this.Property(t => t.MenuItemID)
                .IsRequired()
                .HasMaxLength(50);

            this.Property(t => t.ParentID)
                .HasMaxLength(50);

            this.Property(t => t.Icon)
                .HasMaxLength(200);

            this.Property(t => t.TargetUrl)
                .HasMaxLength(300);

            this.Property(t => t.SpecialClass)
                .HasMaxLength(50);

            // Table & Column Mappings
            this.ToTable("MenuItem");
            this.Property(t => t.WebsiteID).HasColumnName("WebsiteID");
            this.Property(t => t.MenuItemID).HasColumnName("MenuItemID");
            this.Property(t => t.ParentID).HasColumnName("ParentID");
            this.Property(t => t.Icon).HasColumnName("Icon");
            this.Property(t => t.TargetUrl).HasColumnName("TargetUrl");
            this.Property(t => t.OpenMode).HasColumnName("OpenMode");
            this.Property(t => t.SortOrder).HasColumnName("SortOrder");
            this.Property(t => t.IsEnabled).HasColumnName("IsEnabled");
            this.Property(t => t.SpecialClass).HasColumnName("SpecialClass");
            this.Property(t => t.IsDeleted).HasColumnName("IsDeleted");

            // Relationships
            this.HasRequired(t => t.Website)
                .WithMany(t => t.MenuItems)
                .HasForeignKey(d => d.WebsiteID);

        }
    }
}
