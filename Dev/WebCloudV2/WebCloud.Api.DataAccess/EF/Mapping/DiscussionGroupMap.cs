using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;using WebCloud.Entity;

namespace WebCloud.Api.DataAccess.EF.Mapping
{
    public class DiscussionGroupMap : EntityTypeConfiguration<DiscussionGroup>
    {
        public DiscussionGroupMap()
        {
            // Primary Key
            this.HasKey(t => t.DiscussionGroupID);

            // Properties
            this.Property(t => t.ReferenceTable)
                .HasMaxLength(100);

            this.Property(t => t.ItemID)
                .HasMaxLength(50);

            this.Property(t => t.WebsiteID)
                .HasMaxLength(50);

            // Table & Column Mappings
            this.ToTable("DiscussionGroup");
            this.Property(t => t.DiscussionGroupID).HasColumnName("DiscussionGroupID");
            this.Property(t => t.DatetimeCreated).HasColumnName("DatetimeCreated");
            this.Property(t => t.DiscussionCount).HasColumnName("DiscussionCount");
            this.Property(t => t.TotalRateValue).HasColumnName("TotalRateValue");
            this.Property(t => t.RateCount).HasColumnName("RateCount");
            this.Property(t => t.RateAverage).HasColumnName("RateAverage");
            this.Property(t => t.IsClosed).HasColumnName("IsClosed");
            this.Property(t => t.DatetimeClosed).HasColumnName("DatetimeClosed");
            this.Property(t => t.ReferenceTable).HasColumnName("ReferenceTable");
            this.Property(t => t.ItemID).HasColumnName("ItemID");
            this.Property(t => t.WebsiteID).HasColumnName("WebsiteID");
        }
    }
}
