using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;using WebCloud.Entity;

namespace WebCloud.Api.DataAccess.EF.Mapping
{
    public class ProductContentMap : EntityTypeConfiguration<ProductContent>
    {
        public ProductContentMap()
        {
            // Primary Key
            this.HasKey(t => new { t.WebsiteID, t.LanguageID, t.ProductID });

            // Properties
            this.Property(t => t.WebsiteID)
                .IsRequired()
                .HasMaxLength(50);

            this.Property(t => t.LanguageID)
                .IsRequired()
                .HasMaxLength(10);

            this.Property(t => t.ProductID)
                .IsRequired()
                .HasMaxLength(50);

            this.Property(t => t.Name)
                .IsRequired()
                .HasMaxLength(250);

            this.Property(t => t.SeoName)
                .IsRequired()
                .HasMaxLength(250);

            this.Property(t => t.Description)
                .HasMaxLength(500);

            this.Property(t => t.MetaTitle)
                .HasMaxLength(250);

            this.Property(t => t.MetaKeyword)
                .HasMaxLength(500);

            this.Property(t => t.MetaDescription)
                .HasMaxLength(500);

            // Table & Column Mappings
            this.ToTable("ProductContent");
            this.Property(t => t.WebsiteID).HasColumnName("WebsiteID");
            this.Property(t => t.LanguageID).HasColumnName("LanguageID");
            this.Property(t => t.ProductID).HasColumnName("ProductID");
            this.Property(t => t.Name).HasColumnName("Name");
            this.Property(t => t.SeoName).HasColumnName("SeoName");
            this.Property(t => t.Description).HasColumnName("Description");
            this.Property(t => t.Detail).HasColumnName("Detail");
            this.Property(t => t.MetaTitle).HasColumnName("MetaTitle");
            this.Property(t => t.MetaKeyword).HasColumnName("MetaKeyword");
            this.Property(t => t.MetaDescription).HasColumnName("MetaDescription");

            // Relationships
            this.HasRequired(t => t.Language)
                .WithMany(t => t.ProductContents)
                .HasForeignKey(d => d.LanguageID);
            this.HasRequired(t => t.Product)
                .WithMany(t => t.ProductContents)
                .HasForeignKey(d => new { d.WebsiteID, d.ProductID });

        }
    }
}
