using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;using WebCloud.Entity;

namespace WebCloud.Api.DataAccess.EF.Mapping
{
    public class ProductCategoryContentMap : EntityTypeConfiguration<ProductCategoryContent>
    {
        public ProductCategoryContentMap()
        {
            // Primary Key
            this.HasKey(t => new { t.WebsiteID, t.LanguageID, t.ProductCategoryID });

            // Properties
            this.Property(t => t.WebsiteID)
                .IsRequired()
                .HasMaxLength(50);

            this.Property(t => t.LanguageID)
                .IsRequired()
                .HasMaxLength(10);

            this.Property(t => t.ProductCategoryID)
                .IsRequired()
                .HasMaxLength(50);

            this.Property(t => t.Name)
                .IsRequired()
                .HasMaxLength(250);

            this.Property(t => t.SeoName)
                .HasMaxLength(250);

            this.Property(t => t.Description)
                .HasMaxLength(500);

            this.Property(t => t.MetaTitle)
                .HasMaxLength(250);

            this.Property(t => t.MetaKeyword)
                .HasMaxLength(500);

            this.Property(t => t.MetaDescription)
                .HasMaxLength(500);

            // Table & Column Mappings
            this.ToTable("ProductCategoryContent");
            this.Property(t => t.WebsiteID).HasColumnName("WebsiteID");
            this.Property(t => t.LanguageID).HasColumnName("LanguageID");
            this.Property(t => t.ProductCategoryID).HasColumnName("ProductCategoryID");
            this.Property(t => t.Name).HasColumnName("Name");
            this.Property(t => t.SeoName).HasColumnName("SeoName");
            this.Property(t => t.Description).HasColumnName("Description");
            this.Property(t => t.MetaTitle).HasColumnName("MetaTitle");
            this.Property(t => t.MetaKeyword).HasColumnName("MetaKeyword");
            this.Property(t => t.MetaDescription).HasColumnName("MetaDescription");

            // Relationships
            this.HasRequired(t => t.Language)
                .WithMany(t => t.ProductCategoryContents)
                .HasForeignKey(d => d.LanguageID);
            this.HasRequired(t => t.ProductCategory)
                .WithMany(t => t.ProductCategoryContents)
                .HasForeignKey(d => new { d.WebsiteID, d.ProductCategoryID });

        }
    }
}
