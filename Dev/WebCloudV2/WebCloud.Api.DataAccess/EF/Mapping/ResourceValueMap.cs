using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;
using WebCloud.Entity;

namespace WebCloud.Api.DataAccess.EF.Mapping
{
    public class ResourceValueMap : EntityTypeConfiguration<ResourceValue>
    {
        public ResourceValueMap()
        {
            // Primary Key
            this.HasKey(t => t.ResourceValueID);

            // Properties
            this.Property(t => t.ResourceID)
                .IsRequired()
                .HasMaxLength(50);

            this.Property(t => t.WebsiteID)
                .IsRequired()
                .HasMaxLength(50);

            this.Property(t => t.LanguageID)
                .HasMaxLength(10);

            this.Property(t => t.Value)
                .IsRequired();

            // Table & Column Mappings
            this.ToTable("ResourceValue");
            this.Property(t => t.ResourceValueID).HasColumnName("ResourceValueID");
            this.Property(t => t.ResourceID).HasColumnName("ResourceID");
            this.Property(t => t.WebsiteID).HasColumnName("WebsiteID");
            this.Property(t => t.LanguageID).HasColumnName("LanguageID");
            this.Property(t => t.Value).HasColumnName("Value");

            // Relationships
            this.HasOptional(t => t.Language)
                .WithMany(t => t.ResourceValues)
                .HasForeignKey(d => d.LanguageID);

        }
    }
}
