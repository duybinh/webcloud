using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;using WebCloud.Entity;

namespace WebCloud.Api.DataAccess.EF.Mapping
{
    public class MenuItemContentMap : EntityTypeConfiguration<MenuItemContent>
    {
        public MenuItemContentMap()
        {
            // Primary Key
            this.HasKey(t => new { t.WebsiteID, t.LanguageID, t.MenuItemID });

            // Properties
            this.Property(t => t.WebsiteID)
                .IsRequired()
                .HasMaxLength(50);

            this.Property(t => t.LanguageID)
                .IsRequired()
                .HasMaxLength(10);

            this.Property(t => t.MenuItemID)
                .IsRequired()
                .HasMaxLength(50);

            this.Property(t => t.Title)
                .IsRequired()
                .HasMaxLength(250);

            // Table & Column Mappings
            this.ToTable("MenuItemContent");
            this.Property(t => t.WebsiteID).HasColumnName("WebsiteID");
            this.Property(t => t.LanguageID).HasColumnName("LanguageID");
            this.Property(t => t.MenuItemID).HasColumnName("MenuItemID");
            this.Property(t => t.Title).HasColumnName("Title");
            this.Property(t => t.Content).HasColumnName("Content");

            // Relationships
            this.HasRequired(t => t.Language)
                .WithMany(t => t.MenuItemContents)
                .HasForeignKey(d => d.LanguageID);
            this.HasRequired(t => t.MenuItem)
                .WithMany(t => t.MenuItemContents)
                .HasForeignKey(d => new { d.WebsiteID, d.MenuItemID });

        }
    }
}
