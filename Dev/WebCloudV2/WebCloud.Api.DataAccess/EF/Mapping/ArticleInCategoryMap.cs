using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;using WebCloud.Entity;

namespace WebCloud.Api.DataAccess.EF.Mapping
{
    public class ArticleInCategoryMap : EntityTypeConfiguration<ArticleInCategory>
    {
        public ArticleInCategoryMap()
        {
            // Primary Key
            this.HasKey(t => new { t.WebsiteID, t.ArticleCategoryID, t.ArticleID });

            // Properties
            this.Property(t => t.WebsiteID)
                .IsRequired()
                .HasMaxLength(50);

            this.Property(t => t.ArticleCategoryID)
                .IsRequired()
                .HasMaxLength(50);

            this.Property(t => t.ArticleID)
                .IsRequired()
                .HasMaxLength(50);

            // Table & Column Mappings
            this.ToTable("ArticleInCategory");
            this.Property(t => t.WebsiteID).HasColumnName("WebsiteID");
            this.Property(t => t.ArticleCategoryID).HasColumnName("ArticleCategoryID");
            this.Property(t => t.ArticleID).HasColumnName("ArticleID");

            // Relationships
            this.HasRequired(t => t.Article)
                .WithMany(t => t.ArticleInCategories)
                .HasForeignKey(d => new { d.WebsiteID, d.ArticleID });
            this.HasRequired(t => t.ArticleCategory)
                .WithMany(t => t.ArticleInCategories)
                .HasForeignKey(d => new { d.WebsiteID, d.ArticleCategoryID });

        }
    }
}
