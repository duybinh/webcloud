using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;using WebCloud.Entity;

namespace WebCloud.Api.DataAccess.EF.Mapping
{
    public class ModuleBaseConfigMap : EntityTypeConfiguration<ModuleBaseConfig>
    {
        public ModuleBaseConfigMap()
        {
            // Primary Key
            this.HasKey(t => new { t.ModuleBaseConfigID, t.ModuleBaseID });

            // Properties
            this.Property(t => t.ModuleBaseConfigID)
                .IsRequired()
                .HasMaxLength(50);

            this.Property(t => t.ModuleBaseID)
                .IsRequired()
                .HasMaxLength(50);

            this.Property(t => t.Name)
                .IsRequired()
                .HasMaxLength(250);

            this.Property(t => t.Description)
                .HasMaxLength(500);

            this.Property(t => t.DataType)
                .IsRequired()
                .HasMaxLength(20);

            this.Property(t => t.RolesToConfig)
                .HasMaxLength(500);

            // Table & Column Mappings
            this.ToTable("ModuleBaseConfig");
            this.Property(t => t.ModuleBaseConfigID).HasColumnName("ModuleBaseConfigID");
            this.Property(t => t.ModuleBaseID).HasColumnName("ModuleBaseID");
            this.Property(t => t.Name).HasColumnName("Name");
            this.Property(t => t.Description).HasColumnName("Description");
            this.Property(t => t.Hint).HasColumnName("Hint");
            this.Property(t => t.DataType).HasColumnName("DataType");
            this.Property(t => t.DefaultValue).HasColumnName("DefaultValue");
            this.Property(t => t.RolesToConfig).HasColumnName("RolesToConfig");
            this.Property(t => t.SortOrder).HasColumnName("SortOrder");
            this.Property(t => t.IsEnabled).HasColumnName("IsEnabled");
            this.Property(t => t.AllLanguage).HasColumnName("AllLanguage");

            // Relationships
            this.HasRequired(t => t.ModuleBase)
                .WithMany(t => t.ModuleBaseConfigs)
                .HasForeignKey(d => d.ModuleBaseID);

        }
    }
}
