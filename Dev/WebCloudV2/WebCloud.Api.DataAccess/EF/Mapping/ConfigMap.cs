using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;using WebCloud.Entity;

namespace WebCloud.Api.DataAccess.EF.Mapping
{
    public class ConfigMap : EntityTypeConfiguration<Config>
    {
        public ConfigMap()
        {
            // Primary Key
            this.HasKey(t => t.ConfigID);

            // Properties
            this.Property(t => t.ConfigID)
                .IsRequired()
                .HasMaxLength(50);

            this.Property(t => t.Name)
                .IsRequired()
                .HasMaxLength(250);

            this.Property(t => t.Description)
                .HasMaxLength(500);

            this.Property(t => t.DataType)
                .IsRequired()
                .HasMaxLength(20);

            this.Property(t => t.DefaultValue)
                .IsRequired();

            this.Property(t => t.Group)
                .HasMaxLength(250);

            // Table & Column Mappings
            this.ToTable("Config");
            this.Property(t => t.ConfigID).HasColumnName("ConfigID");
            this.Property(t => t.Name).HasColumnName("Name");
            this.Property(t => t.Description).HasColumnName("Description");
            this.Property(t => t.DataType).HasColumnName("DataType");
            this.Property(t => t.DefaultValue).HasColumnName("DefaultValue");
            this.Property(t => t.Group).HasColumnName("Group");
            this.Property(t => t.RolesToView).HasColumnName("RolesToView");
            this.Property(t => t.RolesToEdit).HasColumnName("RolesToEdit");
            this.Property(t => t.EditingControl).HasColumnName("EditingControl");
            this.Property(t => t.IsEnabled).HasColumnName("IsEnabled");
            this.Property(t => t.SortOrder).HasColumnName("SortOrder");
        }
    }
}
