using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;using WebCloud.Entity;

namespace WebCloud.Api.DataAccess.EF.Mapping
{
    public class UserOAuthMembershipMap : EntityTypeConfiguration<UserOAuthMembership>
    {
        public UserOAuthMembershipMap()
        {
            // Primary Key
            this.HasKey(t => t.ID);

            // Properties
            this.Property(t => t.WebsiteID)
                .IsRequired()
                .HasMaxLength(50);

            this.Property(t => t.UserID)
                .IsRequired()
                .HasMaxLength(50);

            this.Property(t => t.ProviderUserID)
                .IsRequired()
                .HasMaxLength(250);

            // Table & Column Mappings
            this.ToTable("UserOAuthMembership");
            this.Property(t => t.ID).HasColumnName("ID");
            this.Property(t => t.WebsiteID).HasColumnName("WebsiteID");
            this.Property(t => t.UserID).HasColumnName("UserID");
            this.Property(t => t.OAuthProviderID).HasColumnName("OAuthProviderID");
            this.Property(t => t.ProviderUserID).HasColumnName("ProviderUserID");

            // Relationships
            this.HasRequired(t => t.OAuthProvider)
                .WithMany(t => t.UserOAuthMemberships)
                .HasForeignKey(d => d.OAuthProviderID);
            this.HasRequired(t => t.User)
                .WithMany(t => t.UserOAuthMemberships)
                .HasForeignKey(d => new { d.WebsiteID, d.UserID });

        }
    }
}
