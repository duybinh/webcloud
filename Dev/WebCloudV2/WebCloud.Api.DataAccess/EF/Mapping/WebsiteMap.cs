using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;using WebCloud.Entity;

namespace WebCloud.Api.DataAccess.EF.Mapping
{
    public class WebsiteMap : EntityTypeConfiguration<Website>
    {
        public WebsiteMap()
        {
            // Primary Key
            this.HasKey(t => t.WebsiteID);

            // Properties
            this.Property(t => t.WebsiteID)
                .IsRequired()
                .HasMaxLength(50);

            this.Property(t => t.WebsiteName)
                .IsRequired()
                .HasMaxLength(200);

            this.Property(t => t.Description)
                .HasMaxLength(500);

            this.Property(t => t.AdminEmail)
                .IsRequired()
                .HasMaxLength(200);

            this.Property(t => t.DefaultTheme)
                .HasMaxLength(100);

            this.Property(t => t.DefaultLanguage)
                .HasMaxLength(10);

            this.Property(t => t.ContactName)
                .HasMaxLength(200);

            this.Property(t => t.ContactPhone)
                .HasMaxLength(50);

            this.Property(t => t.City)
                .HasMaxLength(50);

            this.Property(t => t.Address)
                .HasMaxLength(200);

            // Table & Column Mappings
            this.ToTable("Website");
            this.Property(t => t.WebsiteID).HasColumnName("WebsiteID");
            this.Property(t => t.PlanID).HasColumnName("PlanID");
            this.Property(t => t.GroupID).HasColumnName("GroupID");
            this.Property(t => t.WebsiteName).HasColumnName("WebsiteName");
            this.Property(t => t.Description).HasColumnName("Description");
            this.Property(t => t.AdminEmail).HasColumnName("AdminEmail");
            this.Property(t => t.DefaultTheme).HasColumnName("DefaultTheme");
            this.Property(t => t.DefaultLanguage).HasColumnName("DefaultLanguage");
            this.Property(t => t.ContactName).HasColumnName("ContactName");
            this.Property(t => t.ContactPhone).HasColumnName("ContactPhone");
            this.Property(t => t.City).HasColumnName("City");
            this.Property(t => t.Address).HasColumnName("Address");
            this.Property(t => t.IsApproved).HasColumnName("IsApproved");
            this.Property(t => t.IsEnabled).HasColumnName("IsEnabled");
            this.Property(t => t.IsDeleted).HasColumnName("IsDeleted");
            this.Property(t => t.IsDemo).HasColumnName("IsDemo");
            this.Property(t => t.IsTrial).HasColumnName("IsTrial");
            this.Property(t => t.ExpiredDatetime).HasColumnName("ExpiredDatetime");
            this.Property(t => t.CreatedDatetime).HasColumnName("CreatedDatetime");

            // Relationships
            this.HasOptional(t => t.WebGroup)
                .WithMany(t => t.Websites)
                .HasForeignKey(d => d.GroupID);
            this.HasOptional(t => t.WebPlan)
                .WithMany(t => t.Websites)
                .HasForeignKey(d => d.PlanID);

        }
    }
}
