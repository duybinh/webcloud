using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;using WebCloud.Entity;

namespace WebCloud.Api.DataAccess.EF.Mapping
{
    public class ProductPropertyValueMap : EntityTypeConfiguration<ProductPropertyValue>
    {
        public ProductPropertyValueMap()
        {
            // Primary Key
            this.HasKey(t => new { t.WebsiteID, t.LanguageID, t.ProductID, t.ProductPropertyID });

            // Properties
            this.Property(t => t.WebsiteID)
                .IsRequired()
                .HasMaxLength(50);

            this.Property(t => t.LanguageID)
                .IsRequired()
                .HasMaxLength(10);

            this.Property(t => t.ProductID)
                .IsRequired()
                .HasMaxLength(50);

            this.Property(t => t.ProductPropertyID)
                .IsRequired()
                .HasMaxLength(50);

            this.Property(t => t.TextValue)
                .HasMaxLength(500);

            // Table & Column Mappings
            this.ToTable("ProductPropertyValue");
            this.Property(t => t.WebsiteID).HasColumnName("WebsiteID");
            this.Property(t => t.LanguageID).HasColumnName("LanguageID");
            this.Property(t => t.ProductID).HasColumnName("ProductID");
            this.Property(t => t.ProductPropertyID).HasColumnName("ProductPropertyID");
            this.Property(t => t.TextValue).HasColumnName("TextValue");
            this.Property(t => t.NumberValue).HasColumnName("NumberValue");
            this.Property(t => t.BooleanValue).HasColumnName("BooleanValue");
            this.Property(t => t.DatetimeValue).HasColumnName("DatetimeValue");

            // Relationships
            this.HasRequired(t => t.Language)
                .WithMany(t => t.ProductPropertyValues)
                .HasForeignKey(d => d.LanguageID);
            this.HasRequired(t => t.Product)
                .WithMany(t => t.ProductPropertyValues)
                .HasForeignKey(d => new { d.WebsiteID, d.ProductID });
            this.HasRequired(t => t.ProductProperty)
                .WithMany(t => t.ProductPropertyValues)
                .HasForeignKey(d => new { d.WebsiteID, d.ProductPropertyID });

        }
    }
}
