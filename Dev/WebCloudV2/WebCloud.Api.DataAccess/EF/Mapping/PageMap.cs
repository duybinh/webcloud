using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;using WebCloud.Entity;

namespace WebCloud.Api.DataAccess.EF.Mapping
{
    public class PageMap : EntityTypeConfiguration<Page>
    {
        public PageMap()
        {
            // Primary Key
            this.HasKey(t => new { t.PageID, t.WebsiteID });

            // Properties
            this.Property(t => t.PageID)
                .IsRequired()
                .HasMaxLength(50);

            this.Property(t => t.WebsiteID)
                .IsRequired()
                .HasMaxLength(50);

            this.Property(t => t.ParentID)
                .HasMaxLength(50);

            this.Property(t => t.LayoutID)
                .IsRequired()
                .HasMaxLength(50);

            this.Property(t => t.Group)
                .HasMaxLength(200);

            this.Property(t => t.Name)
                .IsRequired()
                .HasMaxLength(250);

            this.Property(t => t.SeoName)
                .IsRequired()
                .HasMaxLength(250);

            this.Property(t => t.MetaTitle)
                .HasMaxLength(250);

            this.Property(t => t.MetaKeyword)
                .HasMaxLength(500);

            this.Property(t => t.MetaDescription)
                .HasMaxLength(500);

            // Table & Column Mappings
            this.ToTable("Page");
            this.Property(t => t.PageID).HasColumnName("PageID");
            this.Property(t => t.WebsiteID).HasColumnName("WebsiteID");
            this.Property(t => t.ParentID).HasColumnName("ParentID");
            this.Property(t => t.LayoutID).HasColumnName("LayoutID");
            this.Property(t => t.Group).HasColumnName("Group");
            this.Property(t => t.Name).HasColumnName("Name");
            this.Property(t => t.SeoName).HasColumnName("SeoName");
            this.Property(t => t.MetaTitle).HasColumnName("MetaTitle");
            this.Property(t => t.MetaKeyword).HasColumnName("MetaKeyword");
            this.Property(t => t.MetaDescription).HasColumnName("MetaDescription");
            this.Property(t => t.SortOrder).HasColumnName("SortOrder");
            this.Property(t => t.IsEnabled).HasColumnName("IsEnabled");
            this.Property(t => t.IsGroup).HasColumnName("IsGroup");
            this.Property(t => t.IsDeleted).HasColumnName("IsDeleted");

            // Relationships
            this.HasRequired(t => t.Website)
                .WithMany(t => t.Pages)
                .HasForeignKey(d => d.WebsiteID);

        }
    }
}
