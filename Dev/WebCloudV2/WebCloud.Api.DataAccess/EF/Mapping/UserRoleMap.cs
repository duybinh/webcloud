using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;using WebCloud.Entity;

namespace WebCloud.Api.DataAccess.EF.Mapping
{
    public class UserRoleMap : EntityTypeConfiguration<UserRole>
    {
        public UserRoleMap()
        {
            // Primary Key
            this.HasKey(t => t.UserRoleID);

            // Properties
            this.Property(t => t.UserRoleID)
                .IsRequired()
                .HasMaxLength(50);

            this.Property(t => t.Name)
                .IsRequired()
                .HasMaxLength(250);

            this.Property(t => t.Description)
                .IsRequired()
                .HasMaxLength(500);

            // Table & Column Mappings
            this.ToTable("UserRole");
            this.Property(t => t.UserRoleID).HasColumnName("UserRoleID");
            this.Property(t => t.Name).HasColumnName("Name");
            this.Property(t => t.Description).HasColumnName("Description");
            this.Property(t => t.SortOrder).HasColumnName("SortOrder");
            this.Property(t => t.IsEnabled).HasColumnName("IsEnabled");

            // Relationships
            this.HasMany(t => t.Websites)
                .WithMany(t => t.UserRoles)
                .Map(m =>
                    {
                        m.ToTable("UserRoleInWebsite");
                        m.MapLeftKey("UserRoleID");
                        m.MapRightKey("WebsiteID");
                    });


        }
    }
}
