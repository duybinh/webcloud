using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;using WebCloud.Entity;

namespace WebCloud.Api.DataAccess.EF.Mapping
{
    public class WebGroupMap : EntityTypeConfiguration<WebGroup>
    {
        public WebGroupMap()
        {
            // Primary Key
            this.HasKey(t => t.WebGroupID);

            // Properties
            this.Property(t => t.WebGroupID)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.Name)
                .IsRequired()
                .HasMaxLength(200);

            this.Property(t => t.Description)
                .IsRequired()
                .HasMaxLength(500);

            this.Property(t => t.Name_Lang)
                .IsRequired()
                .HasMaxLength(50);

            this.Property(t => t.Description_Lang)
                .IsRequired()
                .HasMaxLength(50);

            // Table & Column Mappings
            this.ToTable("WebGroup");
            this.Property(t => t.WebGroupID).HasColumnName("WebGroupID");
            this.Property(t => t.Name).HasColumnName("Name");
            this.Property(t => t.Description).HasColumnName("Description");
            this.Property(t => t.Name_Lang).HasColumnName("Name_Lang");
            this.Property(t => t.Description_Lang).HasColumnName("Description_Lang");
            this.Property(t => t.IsEnabled).HasColumnName("IsEnabled");
            this.Property(t => t.SortOrder).HasColumnName("SortOrder");
        }
    }
}
