using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;
using WebCloud.Entity;

namespace WebCloud.Api.DataAccess.EF.Mapping
{
    public class ModuleBaseInWebsiteMap : EntityTypeConfiguration<ModuleBaseInWebsite>
    {
        public ModuleBaseInWebsiteMap()
        {
            // Primary Key
            this.HasKey(t => new { t.ModuleBaseID, t.WebsiteID });

            // Properties
            this.Property(t => t.ModuleBaseID)
                .IsRequired()
                .HasMaxLength(50);

            this.Property(t => t.WebsiteID)
                .IsRequired()
                .HasMaxLength(50);

            this.Property(t => t.InstalledUserName)
                .HasMaxLength(50);

            // Table & Column Mappings
            this.ToTable("ModuleBaseInWebsite");
            this.Property(t => t.ModuleBaseID).HasColumnName("ModuleBaseID");
            this.Property(t => t.WebsiteID).HasColumnName("WebsiteID");
            this.Property(t => t.InstalledDate).HasColumnName("InstalledDate");
            this.Property(t => t.InstalledUserName).HasColumnName("InstalledUserName");
            this.Property(t => t.InstalledUserID).HasColumnName("InstalledUserID");

            // Relationships
            this.HasRequired(t => t.ModuleBase)
                .WithMany(t => t.ModuleBaseInWebsites)
                .HasForeignKey(d => d.ModuleBaseID);
            this.HasRequired(t => t.Website)
                .WithMany(t => t.ModuleBaseInWebsites)
                .HasForeignKey(d => d.WebsiteID);

        }
    }
}
