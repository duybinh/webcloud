using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;using WebCloud.Entity;

namespace WebCloud.Api.DataAccess.EF.Mapping
{
    public class ThemeCategoryMap : EntityTypeConfiguration<ThemeCategory>
    {
        public ThemeCategoryMap()
        {
            // Primary Key
            this.HasKey(t => t.ThemeCategoryID);

            // Properties
            this.Property(t => t.ThemeCategoryID)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.Name)
                .IsRequired()
                .HasMaxLength(200);

            this.Property(t => t.Description)
                .HasMaxLength(500);

            // Table & Column Mappings
            this.ToTable("ThemeCategory");
            this.Property(t => t.ThemeCategoryID).HasColumnName("ThemeCategoryID");
            this.Property(t => t.Name).HasColumnName("Name");
            this.Property(t => t.Description).HasColumnName("Description");
            this.Property(t => t.IsEnable).HasColumnName("IsEnable");
            this.Property(t => t.SortOrder).HasColumnName("SortOrder");
        }
    }
}
