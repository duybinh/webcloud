using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;using WebCloud.Entity;

namespace WebCloud.Api.DataAccess.EF.Mapping
{
    public class StatisticMap : EntityTypeConfiguration<Statistic>
    {
        public StatisticMap()
        {
            // Primary Key
            this.HasKey(t => t.StatisticID);

            // Properties
            this.Property(t => t.ReferenceTable)
                .HasMaxLength(100);

            this.Property(t => t.ItemID)
                .HasMaxLength(50);

            this.Property(t => t.WebsiteID)
                .HasMaxLength(50);

            // Table & Column Mappings
            this.ToTable("Statistic");
            this.Property(t => t.StatisticID).HasColumnName("StatisticID");
            this.Property(t => t.ViewCount).HasColumnName("ViewCount");
            this.Property(t => t.LikeCount).HasColumnName("LikeCount");
            this.Property(t => t.DislikeCount).HasColumnName("DislikeCount");
            this.Property(t => t.LastUpdatedDatetime).HasColumnName("LastUpdatedDatetime");
            this.Property(t => t.LikeUserIDs).HasColumnName("LikeUserIDs");
            this.Property(t => t.DislikeUserIDs).HasColumnName("DislikeUserIDs");
            this.Property(t => t.ViewUserIDs).HasColumnName("ViewUserIDs");
            this.Property(t => t.ViewUserIPs).HasColumnName("ViewUserIPs");
            this.Property(t => t.ReferenceTable).HasColumnName("ReferenceTable");
            this.Property(t => t.ItemID).HasColumnName("ItemID");
            this.Property(t => t.WebsiteID).HasColumnName("WebsiteID");
        }
    }
}
