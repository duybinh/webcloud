using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;using WebCloud.Entity;

namespace WebCloud.Api.DataAccess.EF.Mapping
{
    public class TagMap : EntityTypeConfiguration<Tag>
    {
        public TagMap()
        {
            // Primary Key
            this.HasKey(t => t.TagID);

            // Properties
            this.Property(t => t.Value)
                .IsRequired()
                .HasMaxLength(250);

            this.Property(t => t.SeoValue)
                .IsRequired()
                .HasMaxLength(250);

            this.Property(t => t.LanguageID)
                .IsRequired()
                .HasMaxLength(10);

            // Table & Column Mappings
            this.ToTable("Tag");
            this.Property(t => t.TagID).HasColumnName("TagID");
            this.Property(t => t.Value).HasColumnName("Value");
            this.Property(t => t.SeoValue).HasColumnName("SeoValue");
            this.Property(t => t.SearchCount).HasColumnName("SearchCount");
            this.Property(t => t.LastSearchDatetime).HasColumnName("LastSearchDatetime");
            this.Property(t => t.SortOrder).HasColumnName("SortOrder");
            this.Property(t => t.LanguageID).HasColumnName("LanguageID");

            // Relationships
            this.HasRequired(t => t.Language)
                .WithMany(t => t.Tags)
                .HasForeignKey(d => d.LanguageID);

        }
    }
}
