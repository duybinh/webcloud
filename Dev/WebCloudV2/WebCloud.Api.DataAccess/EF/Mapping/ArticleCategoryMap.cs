using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;using WebCloud.Entity;

namespace WebCloud.Api.DataAccess.EF.Mapping
{
    public class ArticleCategoryMap : EntityTypeConfiguration<ArticleCategory>
    {
        public ArticleCategoryMap()
        {
            // Primary Key
            this.HasKey(t => new { t.WebsiteID, t.ArticleCategoryID });

            // Properties
            this.Property(t => t.WebsiteID)
                .IsRequired()
                .HasMaxLength(50);

            this.Property(t => t.ArticleCategoryID)
                .IsRequired()
                .HasMaxLength(50);

            this.Property(t => t.GlobalID)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);

            this.Property(t => t.ParentID)
                .HasMaxLength(50);

            this.Property(t => t.ThumbImage)
                .HasMaxLength(250);

            this.Property(t => t.CreatedUserName)
               .HasMaxLength(50);

            this.Property(t => t.LastEditedUserName)
                .HasMaxLength(50);

            // Table & Column Mappings
            this.ToTable("ArticleCategory");
            this.Property(t => t.WebsiteID).HasColumnName("WebsiteID");
            this.Property(t => t.ArticleCategoryID).HasColumnName("ArticleCategoryID");
            this.Property(t => t.GlobalID).HasColumnName("GlobalID"); 
            this.Property(t => t.ParentID).HasColumnName("ParentID");
            this.Property(t => t.ThumbImage).HasColumnName("ThumbImage");
            this.Property(t => t.DisplayArticleThumb).HasColumnName("DisplayArticleThumb");
            this.Property(t => t.IsHot).HasColumnName("IsHot");
            this.Property(t => t.AllowComment).HasColumnName("AllowComment");
            this.Property(t => t.IsEnabled).HasColumnName("IsEnabled");
            this.Property(t => t.IsDeleted).HasColumnName("IsDeleted");
            this.Property(t => t.SortOrder).HasColumnName("SortOrder");
            this.Property(t => t.IsLeaf).HasColumnName("IsLeaf");
            this.Property(t => t.LogGroupID).HasColumnName("LogGroupID");
            this.Property(t => t.CreatedDatetime).HasColumnName("CreatedDatetime");
            this.Property(t => t.LastEditedDatetime).HasColumnName("LastEditedDatetime");
            this.Property(t => t.CreatedUserName).HasColumnName("CreatedUserName");
            this.Property(t => t.LastEditedUserName).HasColumnName("LastEditedUserName");
            this.Property(t => t.CreatedUserID).HasColumnName("CreatedUserID");
            this.Property(t => t.LastEditedUserID).HasColumnName("LastEditedUserID");

            // Relationships
            this.HasOptional(t => t.LogGroup)
                .WithMany(t => t.ArticleCategories)
                .HasForeignKey(d => d.LogGroupID);
            this.HasRequired(t => t.Website)
                .WithMany(t => t.ArticleCategories)
                .HasForeignKey(d => d.WebsiteID);

        }
    }
}
