using System.Data.Entity.ModelConfiguration;
using WebCloud.Entity;

namespace WebCloud.Api.DataAccess.EF.Mapping
{
    public class RefreshTokenMap : EntityTypeConfiguration<RefreshToken>
    {
        public RefreshTokenMap()
        {
            // Primary Key
            this.HasKey(t => t.RefreshTokenID);

            // Properties
            this.Property(t => t.RefreshTokenID)
                .IsRequired()
                .HasMaxLength(50);

            this.Property(t => t.UserID)
                .IsRequired()
                .HasMaxLength(50);

            this.Property(t => t.WebsiteID)
                .IsRequired()
                .HasMaxLength(50);

            this.Property(t => t.Client)
                .IsRequired()
                .HasMaxLength(50);

            this.Property(t => t.Audience)
                .IsRequired()
                .HasMaxLength(50);

            // Table & Column Mappings
            this.ToTable("RefreshToken");
            this.Property(t => t.RefreshTokenID).HasColumnName("RefreshTokenID");
            this.Property(t => t.UserID).HasColumnName("UserID");
            this.Property(t => t.WebsiteID).HasColumnName("WebsiteID");
            this.Property(t => t.Client).HasColumnName("Client");
            this.Property(t => t.Audience).HasColumnName("Audience");
            this.Property(t => t.IssuedUtc).HasColumnName("IssuedUtc");
            this.Property(t => t.ExpiredUtc).HasColumnName("ExpiredUtc");
            this.Property(t => t.ProtectedTicket).HasColumnName("ProtectedTicket");
        }
    }
}
