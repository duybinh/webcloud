using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;using WebCloud.Entity;

namespace WebCloud.Api.DataAccess.EF.Mapping
{
    public class ModuleMap : EntityTypeConfiguration<Module>
    {
        public ModuleMap()
        {
            // Primary Key
            this.HasKey(t => new { t.WebsiteID, t.ModuleID });

            // Properties
            this.Property(t => t.WebsiteID)
                .IsRequired()
                .HasMaxLength(50);

            this.Property(t => t.ModuleID)
                .IsRequired()
                .HasMaxLength(50);

            this.Property(t => t.ModuleBaseID)
                .IsRequired()
                .HasMaxLength(50);

            this.Property(t => t.PageID)
                .IsRequired()
                .HasMaxLength(50);

            this.Property(t => t.Section)
                .IsRequired()
                .HasMaxLength(100);

            this.Property(t => t.Name)
                .IsRequired()
                .HasMaxLength(250);

            // Table & Column Mappings
            this.ToTable("Module");
            this.Property(t => t.WebsiteID).HasColumnName("WebsiteID");
            this.Property(t => t.ModuleID).HasColumnName("ModuleID");
            this.Property(t => t.ModuleBaseID).HasColumnName("ModuleBaseID");
            this.Property(t => t.PageID).HasColumnName("PageID");
            this.Property(t => t.Section).HasColumnName("Section");
            this.Property(t => t.Name).HasColumnName("Name");
            this.Property(t => t.OnAllPage).HasColumnName("OnAllPage");
            this.Property(t => t.IsBasic).HasColumnName("IsBasic");
            this.Property(t => t.IsEnabled).HasColumnName("IsEnabled");
            this.Property(t => t.SortOrder).HasColumnName("SortOrder");

            // Relationships
            this.HasRequired(t => t.ModuleBase)
                .WithMany(t => t.Modules)
                .HasForeignKey(d => d.ModuleBaseID);
            this.HasRequired(t => t.Page)
                .WithMany(t => t.Modules)
                .HasForeignKey(d => new { d.PageID, d.WebsiteID });
            this.HasRequired(t => t.Website)
                .WithMany(t => t.Modules)
                .HasForeignKey(d => d.WebsiteID);

        }
    }
}
