using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;using WebCloud.Entity;

namespace WebCloud.Api.DataAccess.EF.Mapping
{
    public class ThemeMap : EntityTypeConfiguration<Theme>
    {
        public ThemeMap()
        {
            // Primary Key
            this.HasKey(t => t.ThemeCode);

            // Properties
            this.Property(t => t.ThemeCode)
                .IsRequired()
                .HasMaxLength(50);

            this.Property(t => t.Name)
                .IsRequired()
                .HasMaxLength(250);

            this.Property(t => t.BasedTheme)
                .HasMaxLength(50);

            this.Property(t => t.Author)
                .HasMaxLength(250);

            // Table & Column Mappings
            this.ToTable("Theme");
            this.Property(t => t.ThemeCode).HasColumnName("ThemeCode");
            this.Property(t => t.Name).HasColumnName("Name");
            this.Property(t => t.Description).HasColumnName("Description");
            this.Property(t => t.ThemeCategoryID).HasColumnName("ThemeCategoryID");
            this.Property(t => t.BasedTheme).HasColumnName("BasedTheme");
            this.Property(t => t.Author).HasColumnName("Author");
            this.Property(t => t.RolesToView).HasColumnName("RolesToView");
            this.Property(t => t.RolesToInstall).HasColumnName("RolesToInstall");
            this.Property(t => t.IsEnabled).HasColumnName("IsEnabled");
            this.Property(t => t.SortOrder).HasColumnName("SortOrder");

            // Relationships
            this.HasOptional(t => t.ThemeCategory)
                .WithMany(t => t.Themes)
                .HasForeignKey(d => d.ThemeCategoryID);

        }
    }
}
