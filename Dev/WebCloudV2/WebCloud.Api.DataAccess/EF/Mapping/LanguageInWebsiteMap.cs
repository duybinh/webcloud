using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;
using WebCloud.Entity;

namespace WebCloud.Api.DataAccess.EF.Mapping
{
    public class LanguageInWebsiteMap : EntityTypeConfiguration<LanguageInWebsite>
    {
        public LanguageInWebsiteMap()
        {
            // Primary Key
            this.HasKey(t => new { t.LanguageID, t.WebsiteID });

            // Properties
            this.Property(t => t.LanguageID)
                .IsRequired()
                .HasMaxLength(10);

            this.Property(t => t.WebsiteID)
                .IsRequired()
                .HasMaxLength(50);

            this.Property(t => t.InstalledUserName)
                .HasMaxLength(50);

            // Table & Column Mappings
            this.ToTable("LanguageInWebsite");
            this.Property(t => t.LanguageID).HasColumnName("LanguageID");
            this.Property(t => t.WebsiteID).HasColumnName("WebsiteID");
            this.Property(t => t.IsEnabled).HasColumnName("IsEnabled");
            this.Property(t => t.SortOrder).HasColumnName("SortOrder");
            this.Property(t => t.InstalledDate).HasColumnName("InstalledDate");
            this.Property(t => t.InstalledUserName).HasColumnName("InstalledUserName");
            this.Property(t => t.InstalledUserID).HasColumnName("InstalledUserID");

            // Relationships
            this.HasRequired(t => t.Language)
                .WithMany(t => t.LanguageInWebsites)
                .HasForeignKey(d => d.LanguageID);
            this.HasRequired(t => t.Website)
                .WithMany(t => t.LanguageInWebsites)
                .HasForeignKey(d => d.WebsiteID);

        }
    }
}
