using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;using WebCloud.Entity;

namespace WebCloud.Api.DataAccess.EF.Mapping
{
    public class ProductPropertyMap : EntityTypeConfiguration<ProductProperty>
    {
        public ProductPropertyMap()
        {
            // Primary Key
            this.HasKey(t => new { t.WebsiteID, t.ProductPropertyID });

            // Properties
            this.Property(t => t.WebsiteID)
                .IsRequired()
                .HasMaxLength(50);

            this.Property(t => t.ProductPropertyID)
                .IsRequired()
                .HasMaxLength(50);

            this.Property(t => t.DataType)
                .IsRequired()
                .HasMaxLength(50);

            this.Property(t => t.AppliedCategoryIDs)
                .HasMaxLength(250);

            // Table & Column Mappings
            this.ToTable("ProductProperty");
            this.Property(t => t.WebsiteID).HasColumnName("WebsiteID");
            this.Property(t => t.ProductPropertyID).HasColumnName("ProductPropertyID");
            this.Property(t => t.DataType).HasColumnName("DataType");
            this.Property(t => t.DefaultNumberValue).HasColumnName("DefaultNumberValue");
            this.Property(t => t.DefaultBooleanValue).HasColumnName("DefaultBooleanValue");
            this.Property(t => t.DefaultDatetimeValue).HasColumnName("DefaultDatetimeValue");
            this.Property(t => t.AppliedCategoryIDs).HasColumnName("AppliedCategoryIDs");
            this.Property(t => t.ApplyToAllProduct).HasColumnName("ApplyToAllProduct");
            this.Property(t => t.AllowAdvanceSearch).HasColumnName("AllowAdvanceSearch");
            this.Property(t => t.AllowFilter).HasColumnName("AllowFilter");
            this.Property(t => t.SortOrder).HasColumnName("SortOrder");
            this.Property(t => t.IsEnabled).HasColumnName("IsEnabled");
            this.Property(t => t.AllLanguage).HasColumnName("AllLanguage");

            // Relationships
            this.HasRequired(t => t.Website)
                .WithMany(t => t.ProductProperties)
                .HasForeignKey(d => d.WebsiteID);

        }
    }
}
