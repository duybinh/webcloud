using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;using WebCloud.Entity;

namespace WebCloud.Api.DataAccess.EF.Mapping
{
    public class UserMap : EntityTypeConfiguration<User>
    {
        public UserMap()
        {
            // Primary Key
            this.HasKey(t => new { t.WebsiteID, t.UserID });

            // Properties
            this.Property(t => t.WebsiteID)
                .IsRequired()
                .HasMaxLength(50);

            this.Property(t => t.UserID)
                .IsRequired()
                .HasMaxLength(50);

            this.Property(t => t.LanguageID)
                .HasMaxLength(10);

            this.Property(t => t.GlobalID)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);

            this.Property(t => t.Password)
                .HasMaxLength(50);

            this.Property(t => t.Email)
                .IsRequired()
                .HasMaxLength(250);

            this.Property(t => t.DisplayName)
                .HasMaxLength(250);

            this.Property(t => t.FirstName)
                .HasMaxLength(100);

            this.Property(t => t.LastName)
                .HasMaxLength(100);

            this.Property(t => t.Avatar)
                .HasMaxLength(255);

            this.Property(t => t.EmailVerificationKey)
                .IsRequired()
                .HasMaxLength(50);

            // Table & Column Mappings
            this.ToTable("User");
            this.Property(t => t.WebsiteID).HasColumnName("WebsiteID");
            this.Property(t => t.UserID).HasColumnName("UserID");
            this.Property(t => t.LanguageID).HasColumnName("LanguageID");
            this.Property(t => t.GlobalID).HasColumnName("GlobalID");
            this.Property(t => t.Password).HasColumnName("Password");
            this.Property(t => t.Email).HasColumnName("Email");
            this.Property(t => t.DisplayName).HasColumnName("DisplayName");
            this.Property(t => t.FirstName).HasColumnName("FirstName");
            this.Property(t => t.LastName).HasColumnName("LastName");
            this.Property(t => t.Birthday).HasColumnName("Birthday");
            this.Property(t => t.Gender).HasColumnName("Gender");
            this.Property(t => t.Avatar).HasColumnName("Avatar");
            this.Property(t => t.EmailVerificationKey).HasColumnName("EmailVerificationKey");
            this.Property(t => t.IsVerifiedEmail).HasColumnName("IsVerifiedEmail");
            this.Property(t => t.EmailVerificationExpried).HasColumnName("EmailVerificationExpried");
            this.Property(t => t.IsLocked).HasColumnName("IsLocked");
            this.Property(t => t.LockExpried).HasColumnName("LockExpried");
            this.Property(t => t.IsDeleted).HasColumnName("IsDeleted");
            this.Property(t => t.CreatedDateTime).HasColumnName("CreatedDateTime");

            // Relationships
            this.HasMany(t => t.UserRoles)
                .WithMany(t => t.Users)
                .Map(m =>
                    {
                        m.ToTable("UserInRole");
                        m.MapLeftKey("WebsiteID", "UserID");
                        m.MapRightKey("UserRoleID");
                    });

            this.HasOptional(t => t.Language)
                .WithMany(t => t.Users)
                .HasForeignKey(d => d.LanguageID);
            this.HasRequired(t => t.Website)
                .WithMany(t => t.Users)
                .HasForeignKey(d => d.WebsiteID);

        }
    }
}
