using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;using WebCloud.Entity;

namespace WebCloud.Api.DataAccess.EF.Mapping
{
    public class ArticleMap : EntityTypeConfiguration<Article>
    {
        public ArticleMap()
        {
            // Primary Key
            this.HasKey(t => new { t.WebsiteID, t.ArticleID });

            // Properties
            this.Property(t => t.WebsiteID)
                .IsRequired()
                .HasMaxLength(50);

            this.Property(t => t.ArticleID)
                .IsRequired()
                .HasMaxLength(50);

            this.Property(t => t.GlobalID)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);

            this.Property(t => t.ThumbImage)
                .HasMaxLength(250);

            this.Property(t => t.CreatedUserName)
                .HasMaxLength(50);

            this.Property(t => t.LastEditedUserName)
                .HasMaxLength(50);

            // Table & Column Mappings
            this.ToTable("Article");
            this.Property(t => t.WebsiteID).HasColumnName("WebsiteID");
            this.Property(t => t.ArticleID).HasColumnName("ArticleID");
            this.Property(t => t.GlobalID).HasColumnName("GlobalID");
            this.Property(t => t.ThumbImage).HasColumnName("ThumbImage");
            this.Property(t => t.SortOrder).HasColumnName("SortOrder");
            this.Property(t => t.HaveVideo).HasColumnName("HaveVideo");
            this.Property(t => t.HavePhoto).HasColumnName("HavePhoto");
            this.Property(t => t.HaveAttachFile).HasColumnName("HaveAttachFile");
            this.Property(t => t.IsHot).HasColumnName("IsHot");
            this.Property(t => t.IsApproved).HasColumnName("IsApproved");
            this.Property(t => t.AllowComment).HasColumnName("AllowComment");
            this.Property(t => t.DisplayDatetime).HasColumnName("DisplayDatetime");
            this.Property(t => t.IsGlobalDisplay).HasColumnName("IsGlobalDisplay");
            this.Property(t => t.IsEnabled).HasColumnName("IsEnabled");
            this.Property(t => t.IsDeleted).HasColumnName("IsDeleted");
            this.Property(t => t.DiscussionGroupID).HasColumnName("DiscussionGroupID");
            this.Property(t => t.StatisticID).HasColumnName("StatisticID");
            this.Property(t => t.LogGroupID).HasColumnName("LogGroupID");
            this.Property(t => t.CreatedDatetime).HasColumnName("CreatedDatetime");
            this.Property(t => t.LastEditedDatetime).HasColumnName("LastEditedDatetime");
            this.Property(t => t.CreatedUserName).HasColumnName("CreatedUserName");
            this.Property(t => t.LastEditedUserName).HasColumnName("LastEditedUserName");
            this.Property(t => t.CreatedUserID).HasColumnName("CreatedUserID");
            this.Property(t => t.LastEditedUserID).HasColumnName("LastEditedUserID");

            // Relationships
            this.HasMany(t => t.Tags)
                .WithMany(t => t.Articles)
                .Map(m =>
                    {
                        m.ToTable("TagInArticle");
                        m.MapLeftKey("WebsiteID", "ArticleID");
                        m.MapRightKey("TagID");
                    });

            this.HasOptional(t => t.DiscussionGroup)
                .WithMany(t => t.Articles)
                .HasForeignKey(d => d.DiscussionGroupID);
            this.HasOptional(t => t.LogGroup)
                .WithMany(t => t.Articles)
                .HasForeignKey(d => d.LogGroupID);
            this.HasOptional(t => t.Statistic)
                .WithMany(t => t.Articles)
                .HasForeignKey(d => d.StatisticID);
            this.HasRequired(t => t.Website)
                .WithMany(t => t.Articles)
                .HasForeignKey(d => d.WebsiteID);

        }
    }
}
