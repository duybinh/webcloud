using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;using WebCloud.Entity;

namespace WebCloud.Api.DataAccess.EF.Mapping
{
    public class ProductInCategoryMap : EntityTypeConfiguration<ProductInCategory>
    {
        public ProductInCategoryMap()
        {
            // Primary Key
            this.HasKey(t => new { t.WebsiteID, t.ProductCategoryID, t.ProductID });

            // Properties
            this.Property(t => t.WebsiteID)
                .IsRequired()
                .HasMaxLength(50);

            this.Property(t => t.ProductCategoryID)
                .IsRequired()
                .HasMaxLength(50);

            this.Property(t => t.ProductID)
                .IsRequired()
                .HasMaxLength(50);

            // Table & Column Mappings
            this.ToTable("ProductInCategory");
            this.Property(t => t.WebsiteID).HasColumnName("WebsiteID");
            this.Property(t => t.ProductCategoryID).HasColumnName("ProductCategoryID");
            this.Property(t => t.ProductID).HasColumnName("ProductID");

            // Relationships
            this.HasRequired(t => t.Product)
                .WithMany(t => t.ProductInCategories)
                .HasForeignKey(d => new { d.WebsiteID, d.ProductID });
            this.HasRequired(t => t.ProductCategory)
                .WithMany(t => t.ProductInCategories)
                .HasForeignKey(d => new { d.WebsiteID, d.ProductCategoryID });

        }
    }
}
