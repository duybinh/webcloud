using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;using WebCloud.Entity;

namespace WebCloud.Api.DataAccess.EF.Mapping
{
    public class OAuthProviderMap : EntityTypeConfiguration<OAuthProvider>
    {
        public OAuthProviderMap()
        {
            // Primary Key
            this.HasKey(t => t.OAuthProviderID);

            // Properties
            this.Property(t => t.OAuthProviderID)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.Name)
                .IsRequired()
                .HasMaxLength(250);

            this.Property(t => t.Description)
                .HasMaxLength(500);

            this.Property(t => t.Website)
                .HasMaxLength(250);

            // Table & Column Mappings
            this.ToTable("OAuthProvider");
            this.Property(t => t.OAuthProviderID).HasColumnName("OAuthProviderID");
            this.Property(t => t.Name).HasColumnName("Name");
            this.Property(t => t.Description).HasColumnName("Description");
            this.Property(t => t.Website).HasColumnName("Website");
        }
    }
}
