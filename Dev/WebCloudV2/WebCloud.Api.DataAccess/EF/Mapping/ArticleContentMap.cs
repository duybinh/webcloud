using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;using WebCloud.Entity;

namespace WebCloud.Api.DataAccess.EF.Mapping
{
    public class ArticleContentMap : EntityTypeConfiguration<ArticleContent>
    {
        public ArticleContentMap()
        {
            // Primary Key
            this.HasKey(t => new { t.WebsiteID, t.ArticleID, t.LanguageID });

            // Properties
            this.Property(t => t.WebsiteID)
                .IsRequired()
                .HasMaxLength(50);

            this.Property(t => t.ArticleID)
                .IsRequired()
                .HasMaxLength(50);

            this.Property(t => t.LanguageID)
                .IsRequired()
                .HasMaxLength(10);

            this.Property(t => t.Title)
                .IsRequired()
                .HasMaxLength(250);

            this.Property(t => t.SeoTitle)
                .IsRequired()
                .HasMaxLength(250);

            this.Property(t => t.Description)
                .HasMaxLength(500);

            this.Property(t => t.MetaTitle)
                .HasMaxLength(250);

            this.Property(t => t.MetaKeyword)
                .HasMaxLength(500);

            this.Property(t => t.MetaDescription)
                .HasMaxLength(500);

            // Table & Column Mappings
            this.ToTable("ArticleContent");
            this.Property(t => t.WebsiteID).HasColumnName("WebsiteID");
            this.Property(t => t.ArticleID).HasColumnName("ArticleID");
            this.Property(t => t.LanguageID).HasColumnName("LanguageID");
            this.Property(t => t.Title).HasColumnName("Title");
            this.Property(t => t.SeoTitle).HasColumnName("SeoTitle");
            this.Property(t => t.Description).HasColumnName("Description");
            this.Property(t => t.Content).HasColumnName("Content");
            this.Property(t => t.MetaTitle).HasColumnName("MetaTitle");
            this.Property(t => t.MetaKeyword).HasColumnName("MetaKeyword");
            this.Property(t => t.MetaDescription).HasColumnName("MetaDescription");

            // Relationships
            this.HasRequired(t => t.Article)
                .WithMany(t => t.ArticleContents)
                .HasForeignKey(d => new { d.WebsiteID, d.ArticleID });
            this.HasRequired(t => t.Language)
                .WithMany(t => t.ArticleContents)
                .HasForeignKey(d => d.LanguageID);

        }
    }
}
