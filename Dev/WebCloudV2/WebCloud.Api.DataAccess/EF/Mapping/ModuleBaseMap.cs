using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;using WebCloud.Entity;

namespace WebCloud.Api.DataAccess.EF.Mapping
{
    public class ModuleBaseMap : EntityTypeConfiguration<ModuleBase>
    {
        public ModuleBaseMap()
        {
            // Primary Key
            this.HasKey(t => t.ModuleBaseID);

            // Properties
            this.Property(t => t.ModuleBaseID)
                .IsRequired()
                .HasMaxLength(50);

            this.Property(t => t.DisplayControl)
                .HasMaxLength(500);

            this.Property(t => t.ManageControl)
                .HasMaxLength(500);

            this.Property(t => t.Name)
                .IsRequired()
                .HasMaxLength(200);

            this.Property(t => t.Description)
                .HasMaxLength(500);

            this.Property(t => t.LayoutID)
                .HasMaxLength(50);

            this.Property(t => t.ScriptID)
             .HasMaxLength(50);

            this.Property(t => t.RolesToInstall)
                .HasMaxLength(500);

            this.Property(t => t.RolesToCreate)
                .HasMaxLength(500);

            this.Property(t => t.RolesToEdit)
                .HasMaxLength(500);

            this.Property(t => t.RolesToView)
                .HasMaxLength(500);

            this.Property(t => t.Group)
                .HasMaxLength(200);

            this.Property(t => t.Thumbnail)
                .HasMaxLength(200);

            this.Property(t => t.Version)
                .HasMaxLength(10);

            this.Property(t => t.Author)
                .HasMaxLength(100);

            // Table & Column Mappings
            this.ToTable("ModuleBase");
            this.Property(t => t.ModuleBaseID).HasColumnName("ModuleBaseID");
            this.Property(t => t.DisplayControl).HasColumnName("DisplayControl");
            this.Property(t => t.ManageControl).HasColumnName("ManageControl");
            this.Property(t => t.Name).HasColumnName("Name");
            this.Property(t => t.Description).HasColumnName("Description");
            this.Property(t => t.LayoutID).HasColumnName("LayoutID");
            this.Property(t => t.ScriptID).HasColumnName("ScriptID");
            this.Property(t => t.RolesToInstall).HasColumnName("RolesToInstall");
            this.Property(t => t.RolesToCreate).HasColumnName("RolesToCreate");
            this.Property(t => t.RolesToEdit).HasColumnName("RolesToEdit");
            this.Property(t => t.RolesToView).HasColumnName("RolesToView");
            this.Property(t => t.Group).HasColumnName("Group");
            this.Property(t => t.Thumbnail).HasColumnName("Thumbnail");
            this.Property(t => t.Version).HasColumnName("Version");
            this.Property(t => t.Author).HasColumnName("Author");
            this.Property(t => t.SortOrder).HasColumnName("SortOrder");
            this.Property(t => t.IsEnabled).HasColumnName("IsEnabled");
        }
    }
}
