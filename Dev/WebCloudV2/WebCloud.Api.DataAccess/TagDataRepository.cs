﻿using System;
using System.Collections.Generic;
using DHSoft.Utility.Sql;
using WebCloud.Api.DataAccess.SQL;
using WebCloud.Entity;

namespace WebCloud.Api.DataAccess
{
    public interface ITagDataRepository
    {
        List<Tag> GetAll(string languageId);
    }
    public class TagDataRepository : ITagDataRepository
    {
        public List<Tag> GetAll(string languageId)
        {

            using (var sqlConn = WebCloudSqlConnection.CreateSqlConnection())
            {
                var result = new List<Tag>();
                sqlConn.Open();
                var command = sqlConn.CreateProcedureCommand(
                                                "[dbo].[Tag_GetAll]",
                                                new Dictionary<string, object>
                                                    {
                                                            {"@LanguageId", languageId}
                                                    });
                var reader = command.ExecuteReader();
                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        var item = new Tag();
                        item.TagID = (long)reader["TagID"];
                        item.Value = reader.ReadString("Value");
                        item.SeoValue = reader.ReadString("SeoValue");
                        item.SearchCount = (long)reader["SearchCount"];
                        item.LastSearchDatetime = reader.ReadNullable<DateTime>("LastSearchDatetime");
                        item.SortOrder = (long)reader["SortOrder"];
                        item.LanguageID = languageId;
                        result.Add(item);
                    }
                }
                return result;
            }
        }
    }
}
