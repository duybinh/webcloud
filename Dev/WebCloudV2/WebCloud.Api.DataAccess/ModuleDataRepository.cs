﻿using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using WebCloud.Api.DataAccess.EF;
using WebCloud.Common.Constant;
using WebCloud.Entity;

namespace WebCloud.Api.DataAccess
{
    public interface IModuleDataRepository
    {
        List<Module> GetAll(string websiteId, bool includeConfig = false, bool? isEnabled = null);
        List<Module> GetModuleInPage(string websiteId, string pageId, bool includeOnAllPage = true, bool includeConfig = false);
        Module GetById(string websiteId, string id, bool includeConfig = false, string languageId = null);
        bool Add(Module obj);
        bool Edit(Module obj, string languageId);
        bool Delete(string websiteId, string id);
        bool IsExist(string websiteId, string id);
        bool UpdatePosition(string websiteId, string moduleId, string section, string belowTo);
        bool UpdateFields(string websiteId, string id, string languageId, Dictionary<string, object> fields);
    }

    public class ModuleDataRepository : IModuleDataRepository
    {
        public List<Module> GetAll(string websiteId, bool includeConfig = false, bool? isEnabled = null)
        {
            using (var dataContext = new WebCloudDbContext())
            {
                dataContext.Configuration.ProxyCreationEnabled = false;

                if (!includeConfig)
                {
                    var result = dataContext.Modules.Where(o => o.WebsiteID == websiteId && (isEnabled == null || o.IsEnabled == isEnabled)).ToList();
                    return result;
                }

                var modules = dataContext.Modules.Where(o => o.WebsiteID == websiteId && (isEnabled == null || o.IsEnabled == isEnabled))
                                                 .Include(o => o.ModuleConfigs)
                                                 .ToList();

                foreach (var module in modules)
                {
                    FillTextToConfig(dataContext, module);
                }

                return modules;
            }
        }

        public List<Module> GetModuleInPage(string websiteId, string pageId, bool includeOnAllPage = true, bool includeConfig = false)
        {
            using (var dataContext = new WebCloudDbContext())
            {
                dataContext.Configuration.ProxyCreationEnabled = false;

                if (!includeConfig)
                {
                    var tmp = dataContext.Modules.Where(o => o.WebsiteID == websiteId && (o.PageID == pageId || (includeOnAllPage && o.OnAllPage)))
                                                 .Include(o => o.ModuleBase).OrderByDescending(o => o.SortOrder).ToList();
                    var result = (from module in tmp select module.DeepCopy()).ToList();
                    return result;
                }

                var modules = dataContext.Modules.Where(o => o.WebsiteID == websiteId && (o.PageID == pageId || (includeOnAllPage && o.OnAllPage)))
                                                 .Include(o => o.ModuleConfigs).OrderByDescending(o => o.SortOrder).ToList();

                foreach (var module in modules)
                {
                    FillTextToConfig(dataContext, module);
                }
                return modules;
            }
        }

        public Module GetById(string websiteId, string id, bool includeConfig = false, string languageId = null)
        {

            using (var dataContext = new WebCloudDbContext())
            {
                dataContext.Configuration.ProxyCreationEnabled = false;
                if (!includeConfig)
                    return dataContext.Modules.SingleOrDefault(o => o.WebsiteID == websiteId && o.ModuleID == id);

                var module = dataContext.Modules.Where(o => o.WebsiteID == websiteId && o.ModuleID == id)
                                                .Include(o => o.ModuleConfigs)
                                                .FirstOrDefault();
                if (module == null)
                    return null;

                module.ModuleConfigs = dataContext.ModuleConfigs.Where(o => o.WebsiteID == websiteId && o.ModuleID == id).ToList();

                if (languageId == null)
                    FillTextToConfig(dataContext, module);
                else
                    FillTextToConfig(dataContext, module, languageId);

                return module;
            }
        }

        public bool Add(Module obj)
        {
            using (var dataContext = new WebCloudDbContext())
            {
                dataContext.Modules.Add(obj);
                UpdateConfigValueInLang(dataContext, obj, Default.LanguageId);
                return dataContext.SaveChanges() > 0;
            }
        }

        public bool Edit(Module obj, string languageId)
        {
            using (var dataContext = new WebCloudDbContext())
            {
                var current = dataContext.Modules.SingleOrDefault(o => o.WebsiteID == obj.WebsiteID && o.ModuleID == obj.ModuleID);
                if (current == null) return false;

                current.IsBasic = obj.IsBasic;
                current.IsEnabled = obj.IsEnabled;
                current.Name = obj.Name;
                current.OnAllPage = obj.OnAllPage;
                current.Section = obj.Section;
                current.SortOrder = obj.SortOrder;

                // update config
                foreach (var config in current.ModuleConfigs.ToList())
                {
                    var updateConfig = obj.ModuleConfigs.SingleOrDefault(o => o.ModuleBaseConfigID == config.ModuleBaseConfigID);

                    if (updateConfig == null)
                    {
                        // remove config
                        dataContext.ModuleConfigs.Remove(config);
                    }
                    else
                    {
                        // update exist config
                        config.Value = updateConfig.Value;
                    }
                }

                // add new config
                foreach (var config in obj.ModuleConfigs)
                {
                    if (current.ModuleConfigs.All(o => o.ModuleBaseConfigID != config.ModuleBaseConfigID))
                    {
                        config.Module = current;
                        dataContext.ModuleConfigs.Add(config);
                    }
                }

                // update config value by language
                TextDataHelper.DeleteByCondition(dataContext, obj.WebsiteID, languageId, "Module", obj.ModuleID);
                UpdateConfigValueInLang(dataContext, obj, languageId);

                dataContext.SaveChanges();
                return true;
            }
        }

        public bool Delete(string websiteId, string id)
        {
            using (var dataContext = new WebCloudDbContext())
            {
                var objDelete = dataContext.Modules.SingleOrDefault(o => o.WebsiteID == websiteId && o.ModuleID == id);
                if (objDelete == null) return false;

                dataContext.Modules.Remove(objDelete);

                // delete config value in lang
                TextDataHelper.DeleteByCondition(dataContext, Default.WebsiteId, null, "Module", id);

                dataContext.SaveChanges();
                return true;
            }
        }

        public bool IsExist(string websiteId, string id)
        {
            using (var dataContext = new WebCloudDbContext())
            {
                return dataContext.Modules.Any(o => o.WebsiteID == websiteId && o.ModuleID == id);
            }
        }

        public bool UpdatePosition(string websiteId, string moduleId, string section, string belowTo)
        {
            using (var dataContext = new WebCloudDbContext())
            {
                var module = dataContext.Modules.SingleOrDefault(o => o.WebsiteID == websiteId && o.ModuleID == moduleId);
                if (module == null) return false;
                module.Section = section;
                var modulesInSection = dataContext.Modules.Where(o => o.WebsiteID == websiteId
                                                                      && (o.PageID == module.PageID || o.OnAllPage)
                                                                      && o.Section == module.Section
                                                                      && o.ModuleID != moduleId)
                                                          .OrderBy(o => o.SortOrder).ToList();
                if (modulesInSection.Count > 0)
                {
                    if (string.IsNullOrEmpty(belowTo))
                    {
                        module.SortOrder = modulesInSection.Last().SortOrder + 10;
                    }
                    else
                    {
                        for (int i = 0; i < modulesInSection.Count; i++)
                        {
                            var m = modulesInSection[i];
                            if (m.ModuleID != belowTo)
                            {
                                module.SortOrder = m.SortOrder + 1;
                                continue;
                            }

                            var prevOrder = i > 0 ? modulesInSection[i - 1].SortOrder : 0;
                            module.SortOrder = prevOrder + 1;

                            if (module.SortOrder >= m.SortOrder)
                                InscreaseSortOrder(modulesInSection, i, module.SortOrder + 1);

                            break;
                        }
                    }
                }

                dataContext.SaveChanges();
                return true;
            }
        }

        public bool UpdateFields(string websiteId, string id, string languageId, Dictionary<string, object> fields)
        {
            using (var dataContext = new WebCloudDbContext())
            {
                var current = dataContext.Modules.SingleOrDefault(o => o.WebsiteID == websiteId && o.ModuleID == id);
                if (current == null) return false;

                var baseConfigs = current.ModuleBase.ModuleBaseConfigs.ToList();
                foreach (var field in fields)
                {
                    var baseConfig = baseConfigs.SingleOrDefault(o => o.ModuleBaseConfigID == field.Key);
                    if (baseConfig == null)
                        continue;

                    var config = current.ModuleConfigs.SingleOrDefault(o => o.ModuleBaseConfigID == field.Key);
                    if (config == null)
                    {
                        config = new ModuleConfig()
                        {
                            ModuleBaseConfigID = field.Key,
                            ModuleID = id,
                        };
                        current.ModuleConfigs.Add(config);
                    }
                    config.Value = field.Value.ToString();
                    if (!baseConfig.AllLanguage)
                    {
                        string textId = TextDataHelper.CombineTextId("Module", id, "Config_" + baseConfig.ModuleBaseConfigID);
                        TextDataHelper.UpdateText(dataContext, websiteId, languageId, textId, config.Value);
                    }
                }
                dataContext.SaveChanges();
                return true;
            }
        }

        private void InscreaseSortOrder(List<Module> modules, int position, int newSortOrder)
        {
            modules[position].SortOrder = newSortOrder;
            for (int i = position; i < modules.Count - 1; i++)
            {
                var m1 = modules[i];
                var m2 = modules[i + 1];
                if (m2.SortOrder <= m1.SortOrder)
                {
                    m2.SortOrder = m1.SortOrder + 1;
                }
            }
        }

        #region helper methods

        private void FillTextToConfig(WebCloudDbContext dataContext, Module obj, string languageId)
        {
            var moduleBaseConfigs = dataContext.ModuleBaseConfigs.Where(o => o.ModuleBaseID == obj.ModuleBaseID).ToList();

            // get texts
            var textsForModuleBase = TextDataHelper.GetByCondition(dataContext, Default.WebsiteId, languageId, "ModuleBase", obj.ModuleBaseID);
            var textsForModule = TextDataHelper.GetByCondition(dataContext, obj.WebsiteID, languageId, "Module", obj.ModuleID);


            // set value for module's config
            foreach (var baseConfig in moduleBaseConfigs)
            {
                var config = obj.ModuleConfigs.SingleOrDefault(o => o.ModuleBaseConfigID == baseConfig.ModuleBaseConfigID);
                if (config == null)
                {
                    if (!baseConfig.AllLanguage)
                    {
                        // get base config's value in this lang
                        string textId = TextDataHelper.CombineTextId("ModuleBase", obj.ModuleBaseID,
                            "Config_" + baseConfig.ModuleBaseConfigID);
                        var text = textsForModuleBase.FirstOrDefault(o => o.TextID == textId);
                        if (text != null)
                            baseConfig.DefaultValue = text.Value;
                    }
                    // add missing config
                    config = new ModuleConfig
                    {
                        ModuleID = obj.ModuleID,
                        ModuleBaseID = baseConfig.ModuleBaseID,
                        ModuleBaseConfigID = baseConfig.ModuleBaseConfigID,
                        WebsiteID = obj.WebsiteID,
                        Value = baseConfig.DefaultValue
                    };
                    obj.ModuleConfigs.Add(config);
                }
                else if (!baseConfig.AllLanguage)
                {
                    // get config's value in this lang
                    string textId = TextDataHelper.CombineTextId("Module", obj.ModuleID, "Config_" + baseConfig.ModuleBaseConfigID);
                    var text = textsForModule.FirstOrDefault(o => o.TextID == textId);
                    if (text != null)
                        config.Value = text.Value;
                }
            }
        }

        private void FillTextToConfig(WebCloudDbContext dataContext, Module obj)
        {
            var moduleBaseConfigs = dataContext.ModuleBaseConfigs.Where(o => o.ModuleBaseID == obj.ModuleBaseID).ToList();

            // get texts in all lang
            var textsForModuleBase = TextDataHelper.GetByCondition(dataContext, Default.WebsiteId, null, "ModuleBase", obj.ModuleBaseID);
            var textsForModule = TextDataHelper.GetByCondition(dataContext, obj.WebsiteID, null, "Module", obj.ModuleBaseID);

            // set value for config
            foreach (var baseConfig in moduleBaseConfigs)
            {
                var config = obj.ModuleConfigs.SingleOrDefault(o => o.ModuleBaseConfigID == baseConfig.ModuleBaseConfigID);
                if (config == null)
                {
                    if (!baseConfig.AllLanguage)
                    {
                        // get base config's value in this lang
                        baseConfig.DefaultValueString = new LocalizationString();
                        string textId = TextDataHelper.CombineTextId("ModuleBase", obj.ModuleBaseID, "Config_" + baseConfig.ModuleBaseConfigID);
                        foreach (var text in textsForModuleBase.Where(o => o.TextID == textId))
                        {
                            baseConfig.DefaultValueString.ValueInLanguages[text.LanguageID] = text.Value;
                        }
                    }
                    // add missing config
                    config = new ModuleConfig
                    {
                        ModuleID = obj.ModuleID,
                        ModuleBaseID = baseConfig.ModuleBaseID,
                        ModuleBaseConfigID = baseConfig.ModuleBaseConfigID,
                        WebsiteID = obj.WebsiteID,
                        Value = baseConfig.DefaultValue,
                        ValueString = baseConfig.DefaultValueString
                    };
                    obj.ModuleConfigs.Add(config);
                }
                else if (!baseConfig.AllLanguage)
                {
                    // get default config value from base module
                    config.ValueString = new LocalizationString();
                    string textId1 = TextDataHelper.CombineTextId("ModuleBase", obj.ModuleBaseID, "Config_" + baseConfig.ModuleBaseConfigID);
                    foreach (var text in textsForModuleBase.Where(o => o.TextID == textId1))
                    {
                        config.ValueString.ValueInLanguages[text.LanguageID] = text.Value;
                    }
                    // override config value of module
                    string textId2 = TextDataHelper.CombineTextId("Module", obj.ModuleID, "Config_" + baseConfig.ModuleBaseConfigID);
                    foreach (var text in textsForModule.Where(o => o.TextID == textId2))
                    {
                        config.ValueString.ValueInLanguages[text.LanguageID] = text.Value;
                    }
                }
            }
        }

        private void UpdateConfigValueInLang(WebCloudDbContext dataContext, Module module, string languageId)
        {
            var moduleBase = dataContext.ModuleBases.Where(o => o.ModuleBaseID == module.ModuleBaseID).Include(o => o.ModuleBaseConfigs).FirstOrDefault();
            if (moduleBase == null) return;

            foreach (var baseConfig in moduleBase.ModuleBaseConfigs.Where(o => !o.AllLanguage))
            {
                var config = module.ModuleConfigs.SingleOrDefault(o => o.ModuleBaseConfigID == baseConfig.ModuleBaseConfigID);
                if (config == null) continue;

                string textId = TextDataHelper.CombineTextId("Module", module.ModuleID, "Config_" + baseConfig.ModuleBaseConfigID);
                TextDataHelper.UpdateText(dataContext, module.WebsiteID, languageId, textId, config.Value);
            }
        }

        #endregion
    }
}
