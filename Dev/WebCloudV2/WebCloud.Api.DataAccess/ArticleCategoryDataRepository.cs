﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DHSoft.Utility.Sql;
using WebCloud.Api.DataAccess.EF;
using WebCloud.Api.DataAccess.SQL;
using WebCloud.Common.Constant;
using WebCloud.Entity;

namespace WebCloud.Api.DataAccess
{
    public interface IArticleCategoryDataRepository
    {
        List<ArticleCategory> GetAll(string websiteId, string languageId, string defaultLangId = Default.LanguageId, bool isDeleted = false);
        ArticleCategory GetById(string websiteId, string id, string languageId, string defaultLangId = Default.LanguageId);
        bool IsExist(string websiteId, string id);
        bool Add(ArticleCategory obj);
        bool Edit(ArticleCategory obj);
        bool Delete(ArticleCategory obj, bool deleteChilds = false);
        bool Remove(ArticleCategory obj);
        bool Restore(ArticleCategory obj);
        bool EmptyTrash(string websiteId);
        bool HasChild(string websiteId, string parentId, string childId);
    }
    public class ArticleCategoryDataRepository : IArticleCategoryDataRepository
    {
        public List<ArticleCategory> GetAll(string websiteId, string languageId, string defaultLangId, bool isDeleted)
        {

            using (var sqlConn = WebCloudSqlConnection.CreateSqlConnection())
            {
                var result = new List<ArticleCategory>();
                sqlConn.Open();
                var command = sqlConn.CreateProcedureCommand(
                                                "[dbo].[ArticleCategory_GetAll]",
                                                new Dictionary<string, object>
                                                    {
                                                            {"@WebsiteId", websiteId},
                                                            {"@LanguageId", languageId},
                                                            {"@DefaultLanguageId", defaultLangId},
                                                            {"@IsDeleted", isDeleted}
                                                    });
                var reader = command.ExecuteReader();
                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        var item = new ArticleCategory();
                        item.WebsiteID = (string)reader["WebsiteID"];
                        item.ArticleCategoryID = (string)reader["ArticleCategoryID"];
                        item.GlobalID = (long)reader["GlobalID"];
                        item.ParentID = reader.ReadString("ParentID");
                        item.ThumbImage = reader.ReadString("ThumbImage");
                        item.DisplayArticleThumb = (bool)reader["DisplayArticleThumb"];
                        item.IsHot = (bool)reader["IsHot"];
                        item.AllowComment = (bool)reader["AllowComment"];
                        item.IsEnabled = (bool)reader["IsEnabled"];
                        item.IsLeaf = (bool)reader["IsLeaf"];
                        item.IsDeleted = (bool)reader["IsDeleted"];
                        item.SortOrder = (int)reader["SortOrder"];
                        item.LogGroupID = reader.ReadNullable<long>("LogGroupID");

                        item.CreatedDatetime = (DateTime)reader["CreatedDatetime"];
                        item.LastEditedDatetime = (DateTime)reader["CreatedDatetime"];
                        item.CreatedUserName = reader.ReadString("CreatedUserName");
                        item.LastEditedUserName = reader.ReadString("LastEditedUserName");
                        item.CreatedUserID = reader.ReadNullable<long>("CreatedUserID");
                        item.LastEditedUserID = reader.ReadNullable<long>("LastEditedUserID");

                        item.Content = new ArticleCategoryContent();
                        item.Content.LanguageID = (string)reader["LanguageID"];
                        item.Content.Name = (string)reader["Name"];
                        item.Content.SeoName = reader.ReadString("SeoName");
                        item.Content.Description = reader.ReadString("Description");
                        item.Content.MetaTitle = reader.ReadString("MetaTitle");
                        item.Content.MetaKeyword = reader.ReadString("MetaKeyword");
                        item.Content.MetaDescription = reader.ReadString("MetaDescription");

                        item.ArticleCount = (int)reader["ArticleCount"];
                        result.Add(item);
                    }
                }
                return result;
            }
        }

        public ArticleCategory GetById(string websiteId, string id, string languageId, string defaultLang)
        {
            using (var dataContext = new WebCloudDbContext())
            {
                dataContext.Configuration.ProxyCreationEnabled = false;
                var category =
                    dataContext.ArticleCategories.SingleOrDefault(
                        obj => obj.WebsiteID == websiteId
                            && obj.ArticleCategoryID == id);

                if (category == null) return null;

                var content =
                    dataContext.ArticleCategoryContents.SingleOrDefault(
                        obj => obj.WebsiteID == websiteId
                            && obj.ArticleCategoryID == id
                            && obj.LanguageID == languageId);

                if (content == null)
                {
                    content = dataContext.ArticleCategoryContents.SingleOrDefault(
                        obj => obj.WebsiteID == websiteId
                            && obj.ArticleCategoryID == id
                            && obj.LanguageID == defaultLang);
                }

                if (content == null) return null;

                category.Content = content;

                return category;
            }
        }

        public bool IsExist(string websiteId, string id)
        {
            throw new NotImplementedException();
        }

        public bool Add(ArticleCategory obj)
        {
            using (var sqlConn = WebCloudSqlConnection.CreateSqlConnection())
            {
                sqlConn.Open();
                var command = sqlConn.CreateProcedureCommand(
                                                "[dbo].[ArticleCategory_Insert]",
                                                new Dictionary<string, object>
                                                    {
                                                            {"@WebsiteId", obj.WebsiteID},
                                                            {"@ArticleCategoryID", obj.ArticleCategoryID},
                                                            {"@LanguageID", obj.Content.LanguageID},
                                                            {"@Name", obj.Content.Name},
                                                            {"@SeoName", obj.Content.SeoName},
                                                            {"@Description", obj.Content.Description},
                                                            {"@MetaTitle", obj.Content.MetaTitle},
                                                            {"@MetaKeyword", obj.Content.MetaKeyword},
                                                            {"@MetaDescription", obj.Content.MetaDescription},
                                                            {"@ParentID", obj.ParentID},
                                                            {"@ThumbImage", obj.ThumbImage},
                                                            {"@DisplayArticleThumb", obj.DisplayArticleThumb},
                                                            {"@IsHot", obj.IsHot},
                                                            {"@AllowComment", obj.AllowComment},
                                                            {"@IsEnabled", obj.IsEnabled},
                                                            {"@SortOrder", obj.SortOrder},
                                                            {"@CreatedUserName", obj.CreatedUserName},
                                                            {"@LastEditedUserName", obj.LastEditedUserName},
                                                            {"@CreatedUserID", obj.CreatedUserID},
                                                            {"@LastEditedUserID", obj.LastEditedUserID}
                                                    });
                return command.ExecuteNonQuery() != 0;
            }
        }

        public bool Edit(ArticleCategory obj)
        {
            using (var sqlConn = WebCloudSqlConnection.CreateSqlConnection())
            {
                sqlConn.Open();
                var command = sqlConn.CreateProcedureCommand(
                                                "[dbo].[ArticleCategory_Update]",
                                                new Dictionary<string, object>
                                                    {
                                                            {"@WebsiteId", obj.WebsiteID},
                                                            {"@ArticleCategoryID", obj.ArticleCategoryID},
                                                            {"@LanguageID", obj.Content.LanguageID},
                                                            {"@Name", obj.Content.Name},
                                                            {"@SeoName", obj.Content.SeoName},
                                                            {"@Description", obj.Content.Description},
                                                            {"@MetaTitle", obj.Content.MetaTitle},
                                                            {"@MetaKeyword", obj.Content.MetaKeyword},
                                                            {"@MetaDescription", obj.Content.MetaDescription},
                                                            {"@ParentID", obj.ParentID},
                                                            {"@ThumbImage", obj.ThumbImage},
                                                            {"@DisplayArticleThumb", obj.DisplayArticleThumb},
                                                            {"@IsHot", obj.IsHot},
                                                            {"@AllowComment", obj.AllowComment},
                                                            {"@IsEnabled", obj.IsEnabled},
                                                            {"@SortOrder", obj.SortOrder},
                                                            {"@LastEditedUserName", obj.LastEditedUserName},
                                                            {"@LastEditedUserID", obj.LastEditedUserID}
                                                    });
                return command.ExecuteNonQuery() != 0;
            }
        }

        public bool Delete(ArticleCategory objDelete, bool deleteChilds)
        {
            using (var sqlConn = WebCloudSqlConnection.CreateSqlConnection())
            {
                sqlConn.Open();
                var command = sqlConn.CreateProcedureCommand(
                                                "[dbo].[ArticleCategory_Delete]",
                                                new Dictionary<string, object>
                                                    {
                                                            {"@WebsiteId", objDelete.WebsiteID},
                                                            {"@ArticleCategoryID", objDelete.ArticleCategoryID},
                                                            {"@LastEditedUserName", objDelete.LastEditedUserName},
                                                            {"@LastEditedUserID", objDelete.LastEditedUserID},
                                                            {"@DeleteChild", deleteChilds}
                                                    });
                return command.ExecuteNonQuery() != 0;
            }
        }

        public bool Remove(ArticleCategory objDelete)
        {
            using (var sqlConn = WebCloudSqlConnection.CreateSqlConnection())
            {
                sqlConn.Open();
                var command = sqlConn.CreateProcedureCommand(
                                                "[dbo].[ArticleCategory_Remove]",
                                                new Dictionary<string, object>
                                                    {
                                                            {"@WebsiteId", objDelete.WebsiteID},
                                                            {"@ArticleCategoryID", objDelete.ArticleCategoryID}
                                                    });
                return command.ExecuteNonQuery() != 0;
            }
        }

        public bool Restore(ArticleCategory objDelete)
        {
            using (var sqlConn = WebCloudSqlConnection.CreateSqlConnection())
            {
                sqlConn.Open();
                var command = sqlConn.CreateProcedureCommand(
                                                "[dbo].[ArticleCategory_Restore]",
                                                new Dictionary<string, object>
                                                    {
                                                            {"@WebsiteId", objDelete.WebsiteID},
                                                            {"@ArticleCategoryID", objDelete.ArticleCategoryID},
                                                            {"@LastEditedUserName", objDelete.LastEditedUserName},
                                                            {"@LastEditedUserID", objDelete.LastEditedUserID}
                                                    });
                return command.ExecuteNonQuery() != 0;
            }
        }

        public bool EmptyTrash(string websiteId)
        {
            using (var sqlConn = WebCloudSqlConnection.CreateSqlConnection())
            {
                sqlConn.Open();
                var command = sqlConn.CreateProcedureCommand(
                                                "[dbo].[ArticleCategory_EmptyTrash]",
                                                new Dictionary<string, object>
                                                    {
                                                            {"@WebsiteId", websiteId}
                                                    });
                return command.ExecuteNonQuery() != 0;
            }
        }

        public bool HasChild(string websiteId, string parentId, string childId)
        {
            using (var dataContext = new WebCloudDbContext())
            {
                return
                    dataContext.ArticleCategoryRelationships.Any(
                        obj => obj.WebsiteID == websiteId && obj.ParentID == parentId && obj.ChildID == childId);
            }
        }
    }
}
