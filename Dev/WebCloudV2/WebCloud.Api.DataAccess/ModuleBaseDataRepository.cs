﻿using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using WebCloud.Api.DataAccess.EF;
using WebCloud.Common.Constant;
using WebCloud.Entity;

namespace WebCloud.Api.DataAccess
{
    public interface IModuleBaseDataRepository
    {
        List<ModuleBase> GetAll(bool? isEnabled = null, bool includeConfig = false);
        List<ModuleBaseInWebsite> GetInstalled(string websiteId);
        ModuleBase GetById(string id, bool includeConfig = false, string languageId = null);
        bool Add(ModuleBase obj);
        bool Edit(ModuleBase obj, string languageId);
        bool Delete(string id);
        bool IsExist(string id);
        bool Install(ModuleBaseInWebsite obj);
        bool Uninstall(string websiteId, string moduleBaseId);
    }

    public class ModuleBaseDataRepository : IModuleBaseDataRepository
    {
        public List<ModuleBase> GetAll(bool? isEnabled = null, bool includeConfig = false)
        {
            using (var dataContext = new WebCloudDbContext())
            {
                dataContext.Configuration.ProxyCreationEnabled = false;
                var modules = dataContext.ModuleBases.Where(o => (isEnabled == null || o.IsEnabled == isEnabled));
                if (!includeConfig)
                    return modules.ToList();

                var result = modules.Include(o => o.ModuleBaseConfigs).ToList();
                foreach (var moduleBase in result)
                {
                    FillTextToConfig(dataContext, moduleBase);
                }
                return result;
            }
        }

        public List<ModuleBaseInWebsite> GetInstalled(string websiteId)
        {
            using (var dataContext = new WebCloudDbContext())
            {
                dataContext.Configuration.ProxyCreationEnabled = false;
                var modules = dataContext.ModuleBaseInWebsites.Where(o => o.WebsiteID == websiteId).ToList();
                return modules;
            }
        }

        public ModuleBase GetById(string id, bool includeConfig = false, string languageId = null)
        {
            using (var dataContext = new WebCloudDbContext())
            {
                dataContext.Configuration.ProxyCreationEnabled = false;
                if (!includeConfig) return dataContext.ModuleBases.SingleOrDefault(o => o.ModuleBaseID == id);

                var obj = dataContext.ModuleBases.Where(o => o.ModuleBaseID == id).Include(o => o.ModuleBaseConfigs).FirstOrDefault();
                if (obj == null) return null;

                obj.ModuleBaseConfigs = obj.ModuleBaseConfigs.OrderByDescending(o => o.SortOrder).ThenBy(o => o.ModuleBaseConfigID).ToList();
                if (languageId == null)
                    FillTextToConfig(dataContext, obj);
                else
                    FillTextToConfig(dataContext, obj, languageId);

                return obj;
            }
        }

        private void FillTextToConfig(WebCloudDbContext dataContext, ModuleBase obj, string languageId)
        {
            // get texts
            var texts = TextDataHelper.GetByCondition(dataContext, Default.WebsiteId, languageId, "ModuleBase", obj.ModuleBaseID);
            // set value for config
            foreach (var config in obj.ModuleBaseConfigs.Where(o => !o.AllLanguage))
            {
                string textId = TextDataHelper.CombineTextId("ModuleBase", obj.ModuleBaseID, "Config_" + config.ModuleBaseConfigID);
                var text = texts.FirstOrDefault(o => o.TextID == textId);
                if (text != null)
                    config.DefaultValue = text.Value;
            }
        }

        private void FillTextToConfig(WebCloudDbContext dataContext, ModuleBase obj)
        {
            // get texts in all lang
            var texts = TextDataHelper.GetByCondition(dataContext, Default.WebsiteId, null, "ModuleBase", obj.ModuleBaseID);
            // set value for config
            foreach (var config in obj.ModuleBaseConfigs.Where(o => !o.AllLanguage))
            {
                config.DefaultValueString = new LocalizationString();
                string textId = TextDataHelper.CombineTextId("ModuleBase", obj.ModuleBaseID, "Config_" + config.ModuleBaseConfigID);
                foreach (var text in texts.Where(o => o.TextID == textId))
                {
                    config.DefaultValueString.ValueInLanguages[text.LanguageID] = text.Value;
                }
            }
        }

        public bool Add(ModuleBase obj)
        {
            using (var dataContext = new WebCloudDbContext())
            {
                dataContext.ModuleBases.Add(obj);
                UpdateConfigValueInLang(dataContext, obj, Default.LanguageId);
                return dataContext.SaveChanges() > 0;
            }
        }


        public bool Edit(ModuleBase obj, string languageId)
        {
            using (var dataContext = new WebCloudDbContext())
            {
                var current = dataContext.ModuleBases.SingleOrDefault(o => o.ModuleBaseID == obj.ModuleBaseID);
                if (current == null) return false;

                current.Author = obj.Author;
                current.Description = obj.Description;
                current.DisplayControl = obj.DisplayControl;
                current.Group = obj.Group;
                current.LayoutID = obj.LayoutID;
                current.ScriptID = obj.ScriptID;
                current.ManageControl = obj.ManageControl;
                current.Name = obj.Name;
                current.RolesToCreate = obj.RolesToCreate;
                current.RolesToEdit = obj.RolesToEdit;
                current.RolesToInstall = obj.RolesToInstall;
                current.RolesToView = obj.RolesToView;
                current.Thumbnail = obj.Thumbnail;
                current.Version = obj.Version;

                current.SortOrder = obj.SortOrder;
                current.IsEnabled = obj.IsEnabled;

                // update config
                foreach (var config in current.ModuleBaseConfigs.ToList())
                {
                    var updateConfig = obj.ModuleBaseConfigs.SingleOrDefault(o => o.ModuleBaseConfigID == config.ModuleBaseConfigID);

                    if (updateConfig == null)
                    {
                        // remove config
                        dataContext.ModuleBaseConfigs.Remove(config);
                    }
                    else
                    {
                        // update exist config
                        config.AllLanguage = updateConfig.AllLanguage;
                        config.DataType = updateConfig.DataType;
                        config.DefaultValue = updateConfig.DefaultValue;
                        config.Description = updateConfig.Description;
                        config.Hint = updateConfig.Hint;
                        config.IsEnabled = updateConfig.IsEnabled;
                        config.Name = updateConfig.Name;
                        config.RolesToConfig = updateConfig.RolesToConfig;
                        config.SortOrder = updateConfig.SortOrder;
                    }
                }

                // add new config
                foreach (var config in obj.ModuleBaseConfigs)
                {
                    if (current.ModuleBaseConfigs.All(o => o.ModuleBaseConfigID != config.ModuleBaseConfigID))
                    {
                        config.ModuleBase = current;
                        dataContext.ModuleBaseConfigs.Add(config);
                    }
                }

                // update config value by language
                TextDataHelper.DeleteByCondition(dataContext, Default.WebsiteId, languageId, "ModuleBase", obj.ModuleBaseID);
                UpdateConfigValueInLang(dataContext, obj, languageId);

                dataContext.SaveChanges();
                return true;
            }
        }

        public bool Delete(string id)
        {
            using (var dataContext = new WebCloudDbContext())
            {
                var objDelete = dataContext.ModuleBases.SingleOrDefault(o => o.ModuleBaseID == id);
                if (objDelete == null) return false;

                dataContext.ModuleBases.Remove(objDelete);

                // delete config value in lang
                TextDataHelper.DeleteByCondition(dataContext, Default.WebsiteId, null, "ModuleBase", id);

                dataContext.SaveChanges();
                return true;
            }
        }

        public bool IsExist(string id)
        {
            using (var dataContext = new WebCloudDbContext())
            {
                return dataContext.ModuleBases.Any(o => o.ModuleBaseID == id);
            }
        }

        public bool Install(ModuleBaseInWebsite obj)
        {
            using (var dataContext = new WebCloudDbContext())
            {
                dataContext.ModuleBaseInWebsites.Add(obj);
                dataContext.SaveChanges();
                return true;
            }
        }

        public bool Uninstall(string websiteId, string moduleBaseId)
        {
            using (var dataContext = new WebCloudDbContext())
            {
                var objDelete = dataContext.ModuleBaseInWebsites.SingleOrDefault(o => o.ModuleBaseID == moduleBaseId && o.WebsiteID == websiteId);
                if (objDelete == null) return false;

                dataContext.ModuleBaseInWebsites.Remove(objDelete);

                dataContext.SaveChanges();
                return true;
            }
        }

        private void UpdateConfigValueInLang(WebCloudDbContext dataContext, ModuleBase obj, string languageId)
        {
            if (obj.ModuleBaseConfigs != null)
            {
                foreach (var config in obj.ModuleBaseConfigs.Where(o => !o.AllLanguage))
                {
                    string textId = TextDataHelper.CombineTextId("ModuleBase", obj.ModuleBaseID, "Config_" + config.ModuleBaseConfigID);
                    TextDataHelper.UpdateText(dataContext, Default.WebsiteId, languageId, textId, config.DefaultValue);
                }
            }
        }

    }
}
