﻿using System.Collections.Generic;
using System.Linq;
using WebCloud.Api.DataAccess.EF;
using WebCloud.Entity;

namespace WebCloud.Api.DataAccess
{
    public interface ILayoutDataRepository
    {
        List<Layout> Query(string websiteId, bool? isEnabled = null, int? type = null);
        Layout GetById(string websiteId, string id);
        bool Add(Layout obj);
        bool Edit(Layout obj);
        bool Delete(string websiteId, string id);
        bool IsExist(string websiteId, string id);
    }

    public class LayoutDataRepository : ILayoutDataRepository
    {
        #region Layout
        public List<Layout> Query(string websiteId, bool? isEnabled = null, int? type = null)
        {
            using (var dataContext = new WebCloudDbContext())
            {
                dataContext.Configuration.ProxyCreationEnabled = false;
                return dataContext.Layouts.Where(obj => obj.WebsiteID == websiteId
                                                    && (isEnabled == null || obj.IsEnabled == isEnabled)
                                                    && (type == null || obj.Type == type)
                                                 ).ToList();
            }
        }

        public Layout GetById(string websiteId, string id)
        {
            using (var dataContext = new WebCloudDbContext())
            {
                dataContext.Configuration.ProxyCreationEnabled = false;
                return dataContext.Layouts.SingleOrDefault(obj => obj.WebsiteID == websiteId && obj.LayoutID == id);
            }
        }

        public bool Add(Layout obj)
        {
            using (var dataContext = new WebCloudDbContext())
            {
                dataContext.Layouts.Add(obj);
                return dataContext.SaveChanges() > 0;
            }
        }

        public bool Edit(Layout obj)
        {
            using (var dataContext = new WebCloudDbContext())
            {
                var current = dataContext.Layouts.SingleOrDefault(o => o.WebsiteID == obj.WebsiteID && o.LayoutID == obj.LayoutID);
                if (current == null) return false;

                current.Type = obj.Type;
                current.Name = obj.Name;
                current.Description = obj.Description;
                current.Content = obj.Content;
                current.SortOrder = obj.SortOrder;
                current.IsEnabled = obj.IsEnabled;

                dataContext.SaveChanges();
                return true;
            }
        }

        public bool Delete(string websiteId, string id)
        {
            using (var dataContext = new WebCloudDbContext())
            {
                var objDelete = dataContext.Layouts.SingleOrDefault(o => o.WebsiteID == websiteId && o.LayoutID == id);
                if (objDelete == null) return false;

                dataContext.Layouts.Remove(objDelete);
                dataContext.SaveChanges();
                return true;
            }
        }

        public bool IsExist(string websiteId, string id)
        {
            using (var dataContext = new WebCloudDbContext())
            {
                return dataContext.Layouts.Any(o => o.WebsiteID == websiteId && o.LayoutID == id);
            }
        }

        #endregion
    }
}
