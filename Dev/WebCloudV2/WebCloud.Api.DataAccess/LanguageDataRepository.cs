﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WebCloud.Api.DataAccess.EF;
using WebCloud.Entity;

namespace WebCloud.Api.DataAccess
{
    public interface ILanguageDataRepository
    {
        List<Language> GetAll();
        Language GetById(string langId);
        bool IsExist(string langId);
        bool Add(Language lang);
        bool Edit(Language lang);
        bool Delete(string langId);
    }
    public class LanguageDataRepository : ILanguageDataRepository
    {
        public List<Language> GetAll()
        {
            using (var dataContext = new WebCloudDbContext())
            {
                dataContext.Configuration.ProxyCreationEnabled = false;
                return dataContext.Languages.ToList();
            }
        }

        public Language GetById(string langId)
        {
            using (var dataContext = new WebCloudDbContext())
            {
                dataContext.Configuration.ProxyCreationEnabled = false;
                return dataContext.Languages.SingleOrDefault(obj => obj.LanguageID == langId);
            }
        }

        public bool IsExist(string langId)
        {
            using (var dataContext = new WebCloudDbContext())
            {
                return dataContext.Languages.Any(obj => obj.LanguageID == langId);
            }
        }

        public bool Add(Language lang)
        {
            using (var dataContext = new WebCloudDbContext())
            {
                dataContext.Languages.Add(lang);
                return dataContext.SaveChanges() > 0;
            }
        }

        public bool Edit(Language lang)
        {
            using (var dataContext = new WebCloudDbContext())
            {
                var current = dataContext.Languages.SingleOrDefault(obj => obj.LanguageID == lang.LanguageID);
                if (current == null) return false;

                current.TranslateCode = lang.TranslateCode;
                current.Name = lang.Name;
                current.DisplayName = lang.DisplayName;
                current.Image = lang.Image;
                current.SortOrder = lang.SortOrder;
                current.IsEnabled = lang.IsEnabled;

                dataContext.SaveChanges();
                return true;
            }
        }

        public bool Delete(string langId)
        {
            using (var dataContext = new WebCloudDbContext())
            {
                var lang = dataContext.Languages.SingleOrDefault(obj => obj.LanguageID == langId);
                if (lang == null) return false;

                dataContext.Languages.Remove(lang);
                dataContext.SaveChanges();
                return true;
            }
        }
    }
}
