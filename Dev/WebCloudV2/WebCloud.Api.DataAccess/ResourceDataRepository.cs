﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WebCloud.Api.DataAccess.EF;
using WebCloud.Common.Constant;
using WebCloud.Entity;

namespace WebCloud.Api.DataAccess
{
    public interface IResourceDataRepository
    {
        bool Add(Resource obj);
        bool Edit(Resource obj);
        bool Delete(string websiteId, string id);
        Resource GetById(string websiteId, string id);
        List<Resource> GetAll(string websiteId, bool? isEnabled = null);
        Dictionary<string, string> GetValues(string websiteId, string languageId);
        string GetValue(string id, string websiteId, string languageId);
        bool UpdateValue(string id, string websiteId, string languageId, string value);
        void DeleteValueByCondition(string id, string websiteId = null, string languageId = null);
        List<ResourceValue> GetResourceValues(string websiteId);
        bool IsExist(string websiteId, string id);
    }

    public class ResourceDataRepository : IResourceDataRepository
    {
        public bool Add(Resource obj)
        {
            using (var dataContext = new WebCloudDbContext())
            {
                dataContext.Resources.Add(obj);
                return dataContext.SaveChanges() > 0;
            }
        }

        public bool Edit(Resource obj)
        {
            using (var dataContext = new WebCloudDbContext())
            {
                var current = dataContext.Resources.SingleOrDefault(o => o.ResourceID == obj.ResourceID);
                if (current == null) return false;

                current.Group = obj.Group;
                current.Name = obj.Name;
                current.Description = obj.Description;
                current.RolesToEdit = obj.RolesToEdit;
                current.RolesToView = obj.RolesToView;
                current.DisplayAsHtml = obj.DisplayAsHtml;
                current.IsEnabled = obj.IsEnabled;
                current.SortOrder = obj.SortOrder;
                //current.Value = obj.Value;

                dataContext.SaveChanges();
                return true;
            }
        }

        public bool Delete(string websiteId, string id)
        {
            using (var dataContext = new WebCloudDbContext())
            {
                var objDelete = dataContext.Resources.SingleOrDefault(o => o.WebsiteID == websiteId && o.ResourceID == id);
                if (objDelete == null) return false;

                dataContext.Resources.Remove(objDelete);
                dataContext.SaveChanges();
                return true;
            }
        }

        public Resource GetById(string websiteId, string id)
        {
            using (var dataContext = new WebCloudDbContext())
            {
                dataContext.Configuration.ProxyCreationEnabled = false;
                return dataContext.Resources.SingleOrDefault(obj => obj.WebsiteID == websiteId && obj.ResourceID == id);
            }
        }


        public List<Resource> GetAll(string websiteId, bool? isEnabled = null)
        {
            using (var dataContext = new WebCloudDbContext())
            {
                dataContext.Configuration.ProxyCreationEnabled = false;
                return dataContext.Resources.Where(obj => obj.WebsiteID == websiteId
                                                      && (isEnabled == null || obj.IsEnabled == isEnabled)).ToList();
            }
        }

        public Dictionary<string, string> GetValues(string websiteId, string languageId)
        {
            using (var dataContext = new WebCloudDbContext())
            {
                dataContext.Configuration.ProxyCreationEnabled = false;
                return dataContext.ResourceValues.Where(obj => obj.WebsiteID == websiteId && obj.LanguageID == languageId)
                                                 .ToDictionary(obj => obj.ResourceID, obj => obj.Value);
            }
        }

        public string GetValue(string id, string websiteId, string languageId)
        {
            using (var dataContext = new WebCloudDbContext())
            {
                dataContext.Configuration.ProxyCreationEnabled = false;
                var resourceValue = dataContext.ResourceValues.SingleOrDefault(obj => obj.ResourceID == id && obj.WebsiteID == websiteId && obj.LanguageID == languageId);
                return resourceValue != null ? resourceValue.Value : null;
            }
        }

        public bool UpdateValue(string id, string websiteId, string languageId, string value)
        {
            using (var dataContext = new WebCloudDbContext())
            {
                var current = dataContext.ResourceValues.SingleOrDefault(o => o.ResourceID == id && o.WebsiteID == websiteId && o.LanguageID == languageId);
                if (current == null)
                {
                    current = new ResourceValue
                                  {
                                      ResourceID = id,
                                      WebsiteID = websiteId,
                                      LanguageID = languageId
                                  };
                    dataContext.ResourceValues.Add(current);

                }
                current.Value = value;

                dataContext.SaveChanges();
                return true;
            }
        }

        public void DeleteValueByCondition(string id, string websiteId = null, string languageId = null)
        {
            using (var dataContext = new WebCloudDbContext())
            {

                var resources = dataContext.ResourceValues.Where(o => o.ResourceID == id
                                                                   && (websiteId == null || o.WebsiteID == websiteId)
                                                                   && (languageId == null || o.LanguageID == languageId)
                                                                );

                dataContext.ResourceValues.RemoveRange(resources);
                dataContext.SaveChanges();
            }
        }

        public List<ResourceValue> GetResourceValues(string websiteId)
        {
            using (var dataContext = new WebCloudDbContext())
            {
                return dataContext.ResourceValues.Where(o => o.WebsiteID == Default.WebsiteId || o.WebsiteID == websiteId).ToList();
            }
        }

        public bool IsExist(string websiteId, string id)
        {
            using (var dataContext = new WebCloudDbContext())
            {
                return dataContext.Resources.Any(obj => obj.WebsiteID == websiteId && obj.ResourceID == id);
            }
        }
    }
}
