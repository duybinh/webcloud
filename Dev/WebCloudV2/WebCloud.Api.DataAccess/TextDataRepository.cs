﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using WebCloud.Api.DataAccess.EF;
using WebCloud.Entity;

namespace WebCloud.Api.DataAccess
{
    public interface ITextDataRepository
    {
        List<Text> GetByCondition(string websiteId, string languageId = null, string table = null, string objectId = null, string field = null);
        void DeleteByCondition(string websiteId, string languageId = null, string table = null, string objectId = null, string field = null);
        void GetText(string websiteId, string languageId, Dictionary<string, string> values);
        void UpdateText(string websiteId, string languageId, Dictionary<string, string> values);
        Text GetById(string websiteId, string id, string languageId);
        bool Add(Text obj);
        bool Edit(Text obj);
        bool Delete(string websiteId, string id, string languageId);
        bool IsExist(string websiteId, string id, string languageId);
    }

    public class TextDataRepository : ITextDataRepository
    {
        public List<Text> GetByCondition(string websiteId, string languageId = null, string table = null, string objectId = null, string field = null)
        {
            using (var dataContext = new WebCloudDbContext())
            {
                dataContext.Configuration.ProxyCreationEnabled = false;
                return dataContext.Texts.Where(o => o.WebsiteID == websiteId
                                                    && (languageId == null || o.LanguageID == languageId)
                                                    && (table == null || o.ReferenceTable == table)
                                                    && (field == null || o.ReferenceField == field)
                                                    && (objectId == null || o.ReferenceObject == objectId)
                                               ).ToList();
            }
        }

        public void DeleteByCondition(string websiteId, string languageId = null, string table = null, string objectId = null, string field = null)
        {
            using (var dataContext = new WebCloudDbContext())
            {
                var list = dataContext.Texts.Where(o => o.WebsiteID == websiteId
                                                    && (languageId == null || o.LanguageID == languageId)
                                                    && (table == null || o.ReferenceTable == table)
                                                    && (field == null || o.ReferenceField == field)
                                                    && (objectId == null || o.ReferenceObject == objectId)
                                               ).ToList();

                foreach (var text in list)
                {
                    dataContext.Texts.Remove(text);
                }

                dataContext.SaveChanges();
            }
        }

        public void GetText(string websiteId, string languageId, Dictionary<string, string> values)
        {
            using (var dataContext = new WebCloudDbContext())
            {
                foreach (var id in values.Keys.ToArray())
                {
                    var text =
                        dataContext.Texts.SingleOrDefault(
                            o => o.WebsiteID == websiteId && o.LanguageID == languageId && o.TextID == id);

                    if (text != null)
                        values[id] = text.Value;
                }
            }
        }

        public void UpdateText(string websiteId, string languageId, Dictionary<string, string> values)
        {
            using (var dataContext = new WebCloudDbContext())
            {
                foreach (var id in values.Keys)
                {
                    var text = dataContext.Texts.SingleOrDefault(o => o.WebsiteID == websiteId && o.LanguageID == languageId && o.TextID == id);

                    if (text == null)
                    {
                        text = new Text { WebsiteID = websiteId, LanguageID = languageId, TextID = id };
                        if (id.Contains("__"))
                        {
                            string[] strs = id.Split(new[] { "__" }, StringSplitOptions.RemoveEmptyEntries);
                            if (strs.Length == 3)
                            {
                                text.ReferenceTable = strs[0];
                                text.ReferenceObject = strs[1];
                                text.ReferenceField = strs[2];
                            }
                        }

                        dataContext.Texts.Add(text);
                    }

                    text.Value = values[id];
                }

                dataContext.SaveChanges();
            }
        }

        public Text GetById(string websiteId, string id, string languageId)
        {
            using (var dataContext = new WebCloudDbContext())
            {
                dataContext.Configuration.ProxyCreationEnabled = false;
                return dataContext.Texts.SingleOrDefault(o => o.WebsiteID == websiteId && o.LanguageID == languageId && o.TextID == id);
            }
        }

        public bool Add(Text obj)
        {
            using (var dataContext = new WebCloudDbContext())
            {
                dataContext.Texts.Add(obj);
                return dataContext.SaveChanges() > 0;
            }
        }

        public bool Edit(Text obj)
        {
            using (var dataContext = new WebCloudDbContext())
            {
                var current = dataContext.Texts.SingleOrDefault(o => o.WebsiteID == obj.WebsiteID && o.LanguageID == obj.LanguageID && o.TextID == obj.TextID);
                if (current == null) return false;

                current.Value = obj.Value;
                current.ReferenceTable = obj.ReferenceTable;
                current.ReferenceField = obj.ReferenceField;
                current.ReferenceObject = obj.ReferenceObject;

                dataContext.SaveChanges();
                return true;
            }
        }

        public bool Delete(string websiteId, string id, string languageId)
        {
            using (var dataContext = new WebCloudDbContext())
            {
                var objDelete = dataContext.Texts.SingleOrDefault(o => o.WebsiteID == websiteId && o.LanguageID == languageId && o.TextID == id);
                if (objDelete == null) return false;

                dataContext.Texts.Remove(objDelete);
                dataContext.SaveChanges();
                return true;
            }
        }

        public bool IsExist(string websiteId, string id, string languageId)
        {
            using (var dataContext = new WebCloudDbContext())
            {
                return dataContext.Texts.Any(o => o.WebsiteID == websiteId && o.LanguageID == languageId && o.TextID == id);
            }
        }
    }

    public class TextDataHelper
    {
        #region Combine Extract Text Id
        /// <summary>
        ///  return "Table_Id_Field"
        /// </summary>
        /// <param name="table"></param>
        /// <param name="objectId"></param>
        /// <param name="field"></param>
        /// <returns></returns>
        public static string CombineTextId(string table, string objectId, string field)
        {
            return string.Format("{0}__{1}__{2}", table, objectId, field);
        }

        public static bool ExtractTextId(string textId, out string table, out string objectId, out string field)
        {
            table = field = objectId = null;

            if (textId.Contains("__"))
            {
                string[] strs = textId.Split(new[] { "__" }, StringSplitOptions.RemoveEmptyEntries);
                if (strs.Length == 3)
                {
                    table = strs[0];
                    objectId = strs[1];
                    field = strs[2];
                    return true;
                }
            }

            return false;
        }
        #endregion

        #region Get, Delete By Condition
        public static List<Text> GetByCondition(WebCloudDbContext dataContext, string websiteId, string languageId = null, string table = null, string objectId = null, string field = null)
        {
            dataContext.Configuration.ProxyCreationEnabled = false;
            return dataContext.Texts.Where(o => o.WebsiteID == websiteId
                                                && (languageId == null || o.LanguageID == languageId)
                                                && (table == null || o.ReferenceTable == table)
                                                && (field == null || o.ReferenceField == field)
                                                && (objectId == null || o.ReferenceObject == objectId)
                                           ).ToList();
        }

        public static void DeleteByCondition(WebCloudDbContext dataContext, string websiteId, string languageId = null, string table = null, string objectId = null, string field = null)
        {
            var list = dataContext.Texts.Where(o => o.WebsiteID == websiteId
                                                && (languageId == null || o.LanguageID == languageId)
                                                && (table == null || o.ReferenceTable == table)
                                                && (field == null || o.ReferenceField == field)
                                                && (objectId == null || o.ReferenceObject == objectId)
                                           ).ToList();

            foreach (var text in list)
            {
                dataContext.Texts.Remove(text);
            }

        }
        #endregion

        #region Fill, Update, Delete Object Content
        public static void FillObjectContent(WebCloudDbContext dataContext, object obj, string objectId, string websiteId)
        {
            var texts = GetByCondition(dataContext, websiteId, null, obj.GetType().Name, objectId);
            foreach (var text in texts)
            {
                var field = obj.GetType().GetProperty(text.ReferenceField + "String");
                if (field != null && field.PropertyType == typeof(string))
                {
                    var fieldValue = (LocalizationString)field.GetValue(obj);
                    if (fieldValue == null)
                    {
                        fieldValue = new LocalizationString(text.Value);
                        field.SetValue(obj, fieldValue);
                    }
                    fieldValue.ValueInLanguages[text.LanguageID] = text.Value;
                }
            }
        }

        public static void FillObjectContent(WebCloudDbContext dataContext, object obj, string objectId, string websiteId, string languageId)
        {
            var texts = GetByCondition(dataContext, websiteId, languageId, obj.GetType().Name, objectId);
            foreach (var text in texts)
            {
                var field = obj.GetType().GetProperty(text.ReferenceField);
                if (field != null && field.PropertyType == typeof(string))
                {
                    field.SetValue(obj, text.Value);
                }
            }
        }

        public static void UpdateObjectContent(WebCloudDbContext dataContext, object obj, string objectId, string websiteId, string languageId)
        {
            string refTable = obj.GetType().Name;
            foreach (var property in obj.GetType().GetProperties())
            {
                if (property.PropertyType == typeof(string)
                    && property.CustomAttributes.Any(a => a.AttributeType == typeof(LocalizationField)))
                {
                    string textId = CombineTextId(refTable, objectId, property.Name);
                    var textObj = dataContext.Texts.SingleOrDefault(o => o.WebsiteID == websiteId && o.LanguageID == languageId && o.TextID == textId);
                    if (textObj == null)
                    {
                        textObj = new Text
                        {
                            WebsiteID = websiteId,
                            LanguageID = languageId,
                            TextID = textId,
                            ReferenceTable = refTable,
                            ReferenceObject = objectId,
                            ReferenceField = property.Name
                        };
                        dataContext.Texts.Add(textObj);
                    }

                    textObj.Value = (string)property.GetValue(obj);
                    //property.SetValue(obj, textId);
                }
            }
        }

        public static void DeleteObjectContent(WebCloudDbContext dataContext, object obj, string objectId, string websiteId)
        {
            DeleteByCondition(dataContext, websiteId, null, obj.GetType().Name, objectId);
        }
        #endregion

        #region

        public static void UpdateText(WebCloudDbContext dataContext, string websiteId, string languageId, string textId, string value)
        {
            var c = dataContext.Texts.SingleOrDefault(o => o.WebsiteID == websiteId && o.LanguageID ==languageId && o.TextID == textId);
            if (c == null)
            {
                c = new Text
                {
                    WebsiteID = websiteId,
                    LanguageID = languageId,
                    TextID = textId,
                    Value = value
                };
                dataContext.Texts.Add(c);
            }
            else
            {
                c.Value = value;
            }
        }

        #endregion

        #region Text CRUD - Unused

        //public static void GetText(WebCloudDbContext dataContext, string websiteId, string languageId, Dictionary<string, string> values)
        //{
        //    foreach (var id in values.Keys.ToArray())
        //    {
        //        var text =
        //            dataContext.Texts.SingleOrDefault(
        //                o => o.WebsiteID == websiteId && o.LanguageID == languageId && o.TextID == id);

        //        if (text != null)
        //            values[id] = text.Value;
        //    }
        //}

        //public static void UpdateText(WebCloudDbContext dataContext, string websiteId, string languageId, Dictionary<string, string> values)
        //{
        //    foreach (var id in values.Keys)
        //    {
        //        var text = dataContext.Texts.SingleOrDefault(o => o.WebsiteID == websiteId && o.LanguageID == languageId && o.TextID == id);

        //        if (text == null)
        //        {
        //            text = new Text { WebsiteID = websiteId, LanguageID = languageId, TextID = id };
        //            if (id.Contains("__"))
        //            {
        //                string[] strs = id.Split(new[] { "__" }, StringSplitOptions.RemoveEmptyEntries);
        //                if (strs.Length == 3)
        //                {
        //                    text.ReferenceTable = strs[0];
        //                    text.ReferenceObject = strs[1];
        //                    text.ReferenceField = strs[2];
        //                }
        //            }

        //            dataContext.Texts.Add(text);
        //        }

        //        text.Value = values[id];
        //    }

        //}

        //public Text GetById(WebCloudDbContext dataContext, string websiteId, string id, string languageId)
        //{
        //    dataContext.Configuration.ProxyCreationEnabled = false;
        //    return dataContext.Texts.SingleOrDefault(o => o.WebsiteID == websiteId && o.LanguageID == languageId && o.TextID == id);
        //}

        //public void Add(WebCloudDbContext dataContext, Text obj)
        //{
        //    dataContext.Texts.Add(obj);
        //}

        //public bool Edit(WebCloudDbContext dataContext, Text obj)
        //{
        //    var current = dataContext.Texts.SingleOrDefault(o => o.WebsiteID == obj.WebsiteID && o.LanguageID == obj.LanguageID && o.TextID == obj.TextID);
        //    if (current == null) return false;

        //    current.Value = obj.Value;
        //    current.ReferenceTable = obj.ReferenceTable;
        //    current.ReferenceField = obj.ReferenceField;
        //    current.ReferenceObject = obj.ReferenceObject;

        //    return true;
        //}

        //public bool Delete(WebCloudDbContext dataContext, string websiteId, string id, string languageId)
        //{
        //    var objDelete = dataContext.Texts.SingleOrDefault(o => o.WebsiteID == websiteId && o.LanguageID == languageId && o.TextID == id);
        //    if (objDelete == null) return false;

        //    dataContext.Texts.Remove(objDelete);
        //    return true;
        //}

        #endregion
    }
}
