﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using WebCloud.Entity;

namespace WebCloud.Api.DataAccess
{
    public interface IWebsiteDataRepository
    {
        bool Insert(Website obj);
        bool Update(Website obj);
        bool Delete(Website obj);
        Website GetById(String id);
        List<Website> GetAll();
    }

    public class WebsiteDataRepository : IWebsiteDataRepository
    {
        public bool Insert(Website obj)
        {
            throw new NotImplementedException();
        }

        public bool Update(Website obj)
        {
            throw new NotImplementedException();
        }

        public bool Delete(Website obj)
        {
            throw new NotImplementedException();
        }

        public Website GetById(string id)
        {
            throw new NotImplementedException();
        }

        public List<Website> GetAll()
        {
            throw new NotImplementedException();
        }
    }
}
