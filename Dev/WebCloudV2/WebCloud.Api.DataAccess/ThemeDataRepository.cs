﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WebCloud.Api.DataAccess.EF;
using WebCloud.Entity;

namespace WebCloud.Api.DataAccess
{
    public interface IThemeDataRepository
    {
        List<Theme> GetAll();
        Theme GetById(string id);
        bool IsExist(string id);
        bool Add(Theme entity);
        bool Edit(Theme entity);
        bool Delete(string id);
    }
    public class ThemeDataRepository : IThemeDataRepository
    {
        public List<Theme> GetAll()
        {
            using (var dataContext = new WebCloudDbContext())
            {
                dataContext.Configuration.ProxyCreationEnabled = false;
                return dataContext.Themes.ToList();
            }
        }

        public Theme GetById(string id)
        {
            using (var dataContext = new WebCloudDbContext())
            {
                dataContext.Configuration.ProxyCreationEnabled = false;
                return dataContext.Themes.SingleOrDefault(obj => obj.ThemeCode == id);
            }
        }

        public bool IsExist(string id)
        {
            using (var dataContext = new WebCloudDbContext())
            {
                return dataContext.Themes.Any(obj => obj.ThemeCode == id);
            }
        }

        public bool Add(Theme entity)
        {
            using (var dataContext = new WebCloudDbContext())
            {
                dataContext.Themes.Add(entity);
                return dataContext.SaveChanges() > 0;
            }
        }

        public bool Edit(Theme entity)
        {
            using (var dataContext = new WebCloudDbContext())
            {
                var current = dataContext.Themes.SingleOrDefault(obj => obj.ThemeCode == entity.ThemeCode);
                if (current == null) return false;

                current.Name = entity.Name;
                current.Description = entity.Description;
                current.ThemeCategoryID = entity.ThemeCategoryID;
                current.RolesToView = entity.RolesToView;
                current.RolesToInstall = entity.RolesToInstall;
                current.BasedTheme = entity.BasedTheme;
                current.Author = entity.Author;
                current.SortOrder = entity.SortOrder;
                current.IsEnabled = entity.IsEnabled;

                dataContext.SaveChanges();
                return true;
            }
        }

        public bool Delete(string id)
        {
            using (var dataContext = new WebCloudDbContext())
            {
                var entity = dataContext.Themes.SingleOrDefault(obj => obj.ThemeCode == id);
                if (entity == null) return false;

                dataContext.Themes.Remove(entity);
                dataContext.SaveChanges();
                return true;
            }
        }
    }
}
