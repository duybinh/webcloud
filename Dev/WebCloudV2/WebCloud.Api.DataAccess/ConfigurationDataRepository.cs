﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using WebCloud.Api.DataAccess.EF;
using WebCloud.Entity;

namespace WebCloud.Api.DataAccess
{
    public interface IConfigurationDataRepository
    {
        //Dictionary<string, Config> GetConfigsInWebsite(string websiteId, string languageId);
        //Object GetWebsiteConfigValue(string key, string websiteId, string languageId);
        //bool UpdateWebsiteConfigValue(string key, string websiteId, string languageId, string value);

        #region Management
        List<Config> GetAll(bool? isEnabled = null);
        bool IsExist(string id);
        bool Add(Config entity);
        bool Edit(Config entity);
        bool Delete(string id);
        Config GetById(string id);
        #endregion

        Dictionary<string, string> GetConfigsInWebsite(string websiteId);
        string GetConfigInWebsite(string websiteId, string key);
        void UpdateWebsiteConfigs(string websiteId, Dictionary<string, string> configs);
    }

    public class ConfigurationDataRepository : IConfigurationDataRepository
    {
        //#region Website configuration

        //public Dictionary<string, Config> GetConfigsInWebsite(string websiteId, string languageId)
        //{
        //    using (var dbContext = new WebCloudDbContext())
        //    {
        //        var dictConfig = dbContext.Configs.ToDictionary(obj => obj.ConfigID, obj => obj);
        //        var dictConfigInWebsite = dbContext.ConfigInWebsites.Where(obj => obj.WebsiteID == websiteId)
        //                                                            .ToDictionary(obj => obj.ConfigID, obj => obj.Value);
        //        var dictConfigInLang = dbContext.ConfigInLanguages.Where(obj => obj.WebsiteID == websiteId && obj.LanguageID == languageId)
        //                                                          .ToDictionary(obj => obj.ConfigID, obj => obj.Value);
        //        foreach (var keyValuePair in dictConfig)
        //        {
        //            string value = null;
        //            if (!keyValuePair.Value.IsGlobalConfig)
        //            {
        //                if (dictConfigInLang.ContainsKey(keyValuePair.Key))
        //                    value = dictConfigInLang[keyValuePair.Key];
        //            }

        //            if (value == null)
        //            {
        //                if (dictConfigInWebsite.ContainsKey(keyValuePair.Key))
        //                    value = dictConfigInWebsite[keyValuePair.Key];
        //            }

        //            if (value != null)
        //            {
        //                // default value
        //                keyValuePair.Value.Value = value;
        //            }
        //            else
        //            {
        //                keyValuePair.Value.Value = keyValuePair.Value.DefaultValue;
        //            }

        //        }
        //        return dictConfig;
        //    }
        //}

        //public object GetWebsiteConfigValue(string key, string websiteId, string languageId)
        //{
        //    using (var dbContext = new WebCloudDbContext())
        //    {
        //        var config = dbContext.Configs.SingleOrDefault(obj => obj.ConfigID == key);
        //        if (config != null)
        //        {
        //            if (!config.IsGlobalConfig)
        //            {
        //                var configInLanguage =
        //                    config.ConfigInLanguages.SingleOrDefault(
        //                        obj => obj.WebsiteID == websiteId && obj.LanguageID == languageId);
        //                if (configInLanguage != null)
        //                    return configInLanguage.Value;
        //            }

        //            var configInWebsite = config.ConfigInWebsites.SingleOrDefault(obj => obj.WebsiteID == websiteId);
        //            if (configInWebsite != null)
        //                return configInWebsite.Value;

        //            // return default
        //            return config.DefaultValue;
        //        }

        //        return null;
        //    }
        //}

        //public bool UpdateWebsiteConfigValue(string key, string websiteId, string languageId, string value)
        //{
        //    using (var dbContext = new WebCloudDbContext())
        //    {
        //        var config = dbContext.Configs.SingleOrDefault(obj => obj.ConfigID == key);
        //        if (config != null)
        //        {
        //            if (!config.IsGlobalConfig)
        //            {
        //                var configInLanguage =
        //                    config.ConfigInLanguages.SingleOrDefault(
        //                        obj => obj.WebsiteID == websiteId && obj.LanguageID == languageId);

        //                if (configInLanguage == null)
        //                {
        //                    configInLanguage = new ConfigInLanguage
        //                                           {
        //                                               ConfigID = key,
        //                                               WebsiteID = websiteId,
        //                                               LanguageID = languageId,
        //                                               Value = value
        //                                           };
        //                    dbContext.ConfigInLanguages.Add(configInLanguage);
        //                }
        //                else
        //                    configInLanguage.Value = value;
        //            }
        //            else
        //            {
        //                var configInWebsite = config.ConfigInWebsites.SingleOrDefault(obj => obj.WebsiteID == websiteId);
        //                if (configInWebsite == null)
        //                {
        //                    configInWebsite = new ConfigInWebsite
        //                                         {
        //                                             ConfigID = key,
        //                                             WebsiteID = websiteId,
        //                                             Value = value
        //                                         };
        //                    dbContext.ConfigInWebsites.Add(configInWebsite);
        //                }
        //                else
        //                    configInWebsite.Value = value;
        //            }
        //            // save changes
        //            return dbContext.SaveChanges() > 0;
        //        }

        //        return false;
        //    }
        //}
        //#endregion

        #region Management
        public List<Config> GetAll(bool? isEnabled = null)
        {
            using (var dataContext = new WebCloudDbContext())
            {
                dataContext.Configuration.ProxyCreationEnabled = false;
                return dataContext.Configs.Where(o => isEnabled == null || o.IsEnabled == isEnabled).ToList();
            }
        }

        public bool IsExist(string id)
        {
            using (var dataContext = new WebCloudDbContext())
            {
                return dataContext.Configs.Any(obj => obj.ConfigID == id);
            }
        }

        public bool Add(Config entity)
        {
            using (var dataContext = new WebCloudDbContext())
            {
                dataContext.Configs.Add(entity);
                return dataContext.SaveChanges() > 0;
            }
        }

        public bool Edit(Config entity)
        {
            using (var dataContext = new WebCloudDbContext())
            {
                var current = dataContext.Configs.SingleOrDefault(obj => obj.ConfigID == entity.ConfigID);
                if (current == null) return false;

                current.DataType = entity.DataType;
                current.DefaultValue = entity.DefaultValue;
                current.Description = entity.Description;
                current.EditingControl = entity.EditingControl;
                current.Group = entity.Group;
                current.IsEnabled = entity.IsEnabled;
                current.Name = entity.Name;
                current.RolesToEdit = entity.RolesToEdit;
                current.RolesToView = entity.RolesToView;
                current.SortOrder = entity.SortOrder;

                dataContext.SaveChanges();
                return true;
            }
        }

        public bool Delete(string id)
        {
            using (var dataContext = new WebCloudDbContext())
            {
                var entity = dataContext.Configs.SingleOrDefault(obj => obj.ConfigID == id);
                if (entity == null) return false;

                dataContext.Configs.Remove(entity);
                dataContext.SaveChanges();
                return true;
            }
        }

        public Config GetById(string id)
        {
            using (var dataContext = new WebCloudDbContext())
            {
                dataContext.Configuration.ProxyCreationEnabled = false;
                return dataContext.Configs.SingleOrDefault(obj => obj.ConfigID == id);
            }
        }
        #endregion

        public Dictionary<string, string> GetConfigsInWebsite(string websiteId)
        {
            using (var dataContext = new WebCloudDbContext())
            {
                dataContext.Configuration.ProxyCreationEnabled = false;
                return dataContext.ConfigInWebsites.Where(o => o.WebsiteID == websiteId).ToDictionary(o => o.ConfigID, o => o.Value);
            }
        }

        public string GetConfigInWebsite(string websiteId, string key)
        {
            using (var dataContext = new WebCloudDbContext())
            {
                dataContext.Configuration.ProxyCreationEnabled = false;
                var config = dataContext.ConfigInWebsites.SingleOrDefault(o => o.WebsiteID == websiteId && o.ConfigID == key);
                return config != null ? config.Value : null;
            }
        }

        public void UpdateWebsiteConfigs(string websiteId, Dictionary<string, string> configs)
        {
            using (var dataContext = new WebCloudDbContext())
            {
                foreach (var pair in configs)
                {
                    var config = dataContext.ConfigInWebsites.SingleOrDefault(o => o.WebsiteID == websiteId && o.ConfigID == pair.Key);
                    if (config == null)
                    {
                        config = new ConfigInWebsite
                        {
                            WebsiteID = websiteId,
                            ConfigID = pair.Key,
                            Value = pair.Value
                        };
                        dataContext.ConfigInWebsites.Add(config);
                    }
                    else
                    {
                        config.Value = pair.Value;
                    }
                }

                dataContext.SaveChanges();
            }
        }

    }
}
