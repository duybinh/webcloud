﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using DHSoft.Utility.Sql;
using WebCloud.Api.DataAccess.EF;
using WebCloud.Api.DataAccess.SQL;
using WebCloud.Entity;

namespace WebCloud.Api.DataAccess
{
    public interface IMenuDataRepository
    {
        List<MenuItem> GetMenuItems(string websiteId, string languageId, string defaultLangId = null, bool isDeleted = false);
        MenuItem GetById(string websiteId, string menuItemId, string languageId, string defaultLangId = null);
        string Add(MenuItem item);
        bool Edit(MenuItem item);
        bool EditContent(MenuItemContent content);
        bool DeleteContent(MenuItemContent content);
        bool Delete(string websiteId, string menuItemId);
        bool UnDelete(string websiteId, string menuItemId);
        bool Remove(string websiteId, string menuItemId);
        bool RemoveAllDeleted(string websiteId);
        bool UpdateField(MenuItem item, params string[] fields);
        bool UpdateField(MenuItem[] items, params string[] fields);
    }

    public class MenuDataRepository : IMenuDataRepository
    {
        #region Implementation of IMenuDataRepository

        public List<MenuItem> GetMenuItems(string websiteId, string languageId, string defaultLangId = null, bool isDeleted = false)
        {
            using (var sqlConn = WebCloudSqlConnection.CreateSqlConnection())
            {
                var result = new List<MenuItem>();
                sqlConn.Open();
                var command = sqlConn.CreateProcedureCommand(
                                                "[dbo].[Menu_GetMenuItems]",
                                                new Dictionary<string, object>
                                                    {
                                                            {"@WebsiteId", websiteId},
                                                            {"@LanguageId", languageId},
                                                            {"@DefaultLanguageId", defaultLangId},
                                                            {"@IsDeleted", isDeleted}
                                                    });
                var reader = command.ExecuteReader();
                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        var item = new MenuItem();
                        item.WebsiteID = (string)reader["WebsiteID"];
                        item.MenuItemID = (string)reader["MenuItemID"];
                        item.ParentID = reader.ReadString("ParentID");
                        item.Icon = reader.ReadString("Icon");
                        item.TargetUrl = reader.ReadString("TargetUrl");
                        item.OpenMode = (int)reader["OpenMode"];
                        item.SortOrder = (int)reader["SortOrder"];
                        item.IsEnabled = (bool)reader["IsEnabled"];
                        item.SpecialClass = reader.ReadString("SpecialClass");
                        item.IsDeleted = (bool)reader["IsDeleted"];

                        item.Content = new MenuItemContent();
                        item.Content.LanguageID = (string)reader["LanguageID"];
                        item.Content.Title = (string)reader["Title"];
                        item.Content.Content = reader.ReadString("Content");

                        result.Add(item);
                    }
                }
                return result;
            }
        }

        public MenuItem GetById(string websiteId, string menuItemId, string languageId, string defaultLangId = null)
        {
            using (var dbContext = new WebCloudDbContext())
            {
                var item = dbContext.MenuItems.SingleOrDefault(
                                obj => obj.WebsiteID == websiteId && obj.MenuItemID == menuItemId);
                if (item != null)
                {
                    var content = item.MenuItemContents.SingleOrDefault(obj => obj.LanguageID == languageId);

                    if (content == null && defaultLangId != null)
                    {
                        content = item.MenuItemContents.SingleOrDefault(obj => obj.LanguageID == defaultLangId);
                    }

                    if (content != null)
                    {
                        item.Content = new MenuItemContent
                                           {
                                               LanguageID = content.LanguageID,
                                               Title = content.Title,
                                               Content = content.Content
                                           };
                    }
                    return item;
                }
                return null;
            }
        }

        public string Add(MenuItem item)
        {
            throw new NotImplementedException();
        }

        public bool Edit(MenuItem item)
        {
            throw new NotImplementedException();
        }

        public bool EditContent(MenuItemContent content)
        {
            throw new NotImplementedException();
        }

        public bool DeleteContent(MenuItemContent content)
        {
            throw new NotImplementedException();
        }

        public bool Delete(string websiteId, string menuItemId)
        {
            throw new NotImplementedException();
        }

        public bool UnDelete(string websiteId, string menuItemId)
        {
            throw new NotImplementedException();
        }

        public bool Remove(string websiteId, string menuItemId)
        {
            throw new NotImplementedException();
        }

        public bool RemoveAllDeleted(string websiteId)
        {
            throw new NotImplementedException();
        }

        public bool UpdateField(MenuItem item, params string[] fields)
        {
            using (var dbContext = new WebCloudDbContext())
            {
                var current =
                    dbContext.MenuItems.SingleOrDefault(
                        obj => obj.WebsiteID == item.WebsiteID && obj.MenuItemID == item.MenuItemID);

                if (current == null) return false;

                foreach (var field in fields)
                {
                    var property = typeof(MenuItem).GetProperty(field);
                    if (property == null || !property.CanRead || !property.CanWrite) return false;
                    var value = typeof(MenuItem).GetProperty(field).GetValue(item);
                    property.SetValue(current, value);
                }

                dbContext.SaveChanges();
                return true;
            }
        }

        public bool UpdateField(MenuItem[] items, params string[] fields)
        {
            using (var dbContext = new WebCloudDbContext())
            {
                var properties = new List<PropertyInfo>();

                foreach (var field in fields)
                {
                    var property = typeof(MenuItem).GetProperty(field);
                    if (property == null || !property.CanRead || !property.CanWrite) return false;
                    properties.Add(property);
                }

                foreach (var item in items)
                {
                    var current = dbContext.MenuItems.SingleOrDefault(
                                        obj => obj.WebsiteID == item.WebsiteID && obj.MenuItemID == item.MenuItemID);

                    if (current == null) return false;

                    foreach (var property in properties)
                    {
                        var value = property.GetValue(item);
                        property.SetValue(current, value);
                    }
                }

                dbContext.SaveChanges();
                return true;
            }
        }

        #endregion
    }
}
