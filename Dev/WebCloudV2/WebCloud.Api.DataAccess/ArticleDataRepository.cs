﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.SqlClient;
using System.Linq;
using DHSoft.Utility.Common;
using DHSoft.Utility.Sql;
using WebCloud.Api.DataAccess.EF;
using WebCloud.Api.DataAccess.SQL;
using WebCloud.Common.Constant;
using WebCloud.Entity;

namespace WebCloud.Api.DataAccess
{
    public interface IArticleDataRepository
    {
        List<Article> Query(out int total, ArticleQuery query, string websiteId, string languageId, string defaultLangId = Default.LanguageId);
        List<Article> GetAll(string websiteId, string languageId, string defaultLangId = Default.LanguageId, bool isDeleted = false);
        Article GetById(string websiteId, string id, string languageId, string defaultLangId = Default.LanguageId);
        bool IsExist(string websiteId, string id);
        bool Add(Article obj);
        bool Edit(Article obj);
        bool Delete(Article obj);
        bool Remove(Article obj);
        bool Restore(Article obj);
        bool EmptyTrash(string websiteId);
        bool UpdateFields(string websiteId, string id, string languageId, Dictionary<string, object> fields);

    }
    public class ArticleDataRepository : IArticleDataRepository
    {
        public List<Article> Query(out int total, ArticleQuery query, string websiteId, string languageId, string defaultLangId = Default.LanguageId)
        {
            using (var sqlConn = WebCloudSqlConnection.CreateSqlConnection())
            {
                var result = new List<Article>();
                sqlConn.Open();
                var command = sqlConn.CreateProcedureCommand(
                                                "[dbo].[Article_Query]",
                                                new Dictionary<string, object>
                                                    {
                                                            {"@WebsiteId", websiteId},
                                                            {"@LanguageId", languageId},
                                                            {"@DefaultLanguageId", defaultLangId},
                                                            {"@Title", query.Title},
                                                            {"@SeoTitle", query.Title.ToSeoString()},
                                                            {"@IsHot", query.IsHot},
                                                            {"@IsApproved", query.IsApproved},
                                                            {"@IsEnabled", query.IsEnabled},
                                                            {"@IsGlobalDisplay", query.IsGlobalDisplay},
                                                            {"@CategoryId", query.CategoryIds != null && query.CategoryIds.Count > 0},
                                                            {"@CategoryIds", query.CategoryIds.ToListStringTable()},
                                                            {"@SortField", query.SortField},
                                                            {"@SortDirection", query.SortDirection},
                                                            {"@Skip", query.PageSize * (query.Page - 1)},
                                                            {"@Limit", query.PageSize }
                                                    });
                var reader = command.ExecuteReader();
                BindDataReaderToList(reader, result, out total, true);
                return result;
            }
        }

        public List<Article> GetAll(string websiteId, string languageId, string defaultLangId, bool isDeleted)
        {

            using (var sqlConn = WebCloudSqlConnection.CreateSqlConnection())
            {
                var result = new List<Article>();
                sqlConn.Open();
                var command = sqlConn.CreateProcedureCommand(
                                                "[dbo].[Article_GetAll]",
                                                new Dictionary<string, object>
                                                    {
                                                            {"@WebsiteId", websiteId},
                                                            {"@LanguageId", languageId},
                                                            {"@DefaultLanguageId", defaultLangId},
                                                            {"@IsDeleted", isDeleted}
                                                    });
                var reader = command.ExecuteReader();
                int total;
                BindDataReaderToList(reader, result, out total, false);
                return result;
            }
        }

        private void BindDataReaderToList(SqlDataReader reader, List<Article> result, out int total, bool readTotal)
        {
            total = 0;
            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    var item = new Article();
                    if (readTotal && total == 0)
                        total = (int)reader["TotalCount"];

                    item.WebsiteID = (string)reader["WebsiteID"];
                    item.ArticleID = (string)reader["ArticleID"];
                    item.GlobalID = (long)reader["GlobalID"];
                    item.ThumbImage = reader.ReadString("ThumbImage");
                    item.SortOrder = (long)reader["SortOrder"];
                    item.HaveVideo = (bool)reader["HaveVideo"];
                    item.HavePhoto = (bool)reader["HavePhoto"];
                    item.HaveAttachFile = (bool)reader["HaveAttachFile"];
                    item.IsHot = (bool)reader["IsHot"];
                    item.IsApproved = (bool)reader["IsApproved"];
                    item.AllowComment = (bool)reader["AllowComment"];
                    item.DisplayDatetime = (DateTime)reader["DisplayDatetime"];
                    item.IsGlobalDisplay = (bool)reader["IsGlobalDisplay"];
                    item.IsEnabled = (bool)reader["IsEnabled"];
                    item.IsDeleted = (bool)reader["IsDeleted"];

                    item.DiscussionGroupID = reader.ReadNullable<long>("DiscussionGroupID");
                    item.StatisticID = reader.ReadNullable<long>("StatisticID");
                    item.LogGroupID = reader.ReadNullable<long>("LogGroupID");

                    item.CreatedDatetime = (DateTime)reader["CreatedDatetime"];
                    item.LastEditedDatetime = (DateTime)reader["CreatedDatetime"];
                    item.CreatedUserName = reader.ReadString("CreatedUserName");
                    item.LastEditedUserName = reader.ReadString("LastEditedUserName");
                    item.CreatedUserID = reader.ReadNullable<long>("CreatedUserID");
                    item.LastEditedUserID = reader.ReadNullable<long>("LastEditedUserID");

                    item.Content = new ArticleContent();
                    item.Content.LanguageID = (string)reader["LanguageID"];
                    item.Content.Title = (string)reader["Title"];
                    item.Content.SeoTitle = reader.ReadString("SeoTitle");
                    item.Content.Description = reader.ReadString("Description");
                    //item.Content.Content = reader.ReadString("Content");
                    item.Content.MetaTitle = reader.ReadString("MetaTitle");
                    item.Content.MetaKeyword = reader.ReadString("MetaKeyword");
                    item.Content.MetaDescription = reader.ReadString("MetaDescription");

                    item.DiscussionGroup = new DiscussionGroup();
                    item.DiscussionGroup.DiscussionCount = (long)reader["DiscussionCount"];
                    item.DiscussionGroup.RateAverage = (decimal)reader["RateAverage"];

                    item.Statistic = new Statistic();
                    item.Statistic.ViewCount = (long)reader["ViewCount"];
                    item.Statistic.LikeCount = (long)reader["LikeCount"];
                    item.Statistic.DislikeCount = (long)reader["DislikeCount"];

                    result.Add(item);
                }
            }
        }

        public Article GetById(string websiteId, string id, string languageId, string defaultLang)
        {
            using (var dataContext = new WebCloudDbContext())
            {
                dataContext.Configuration.ProxyCreationEnabled = false;
                var record = dataContext.Articles.Where(obj => obj.WebsiteID == websiteId && obj.ArticleID == id)
                                        .Include("Tags")
                                        .Include("DiscussionGroup")
                                        .Include("Statistic")
                                        .FirstOrDefault();

                if (record == null) return null;

                var content = dataContext.ArticleContents.SingleOrDefault(obj => obj.WebsiteID == websiteId
                                                                              && obj.ArticleID == id
                                                                              && obj.LanguageID == languageId);

                if (content == null)
                {
                    content = dataContext.ArticleContents.SingleOrDefault(
                        obj => obj.WebsiteID == websiteId
                            && obj.ArticleID == id
                            && obj.LanguageID == defaultLang);
                }

                if (content == null) return null;

                record.Content = content;
                // categories
                record.CategoryIds = dataContext.ArticleInCategories
                                                .Where(obj => obj.WebsiteID == websiteId && obj.ArticleID == id)
                                                .Select(obj => obj.ArticleCategoryID)
                                                .ToList();

                // tags
                record.TagValues = record.Tags.Where(obj => obj.LanguageID == languageId).Select(obj => obj.Value).ToList();
                return record;
            }
        }

        public bool IsExist(string websiteId, string id)
        {
            throw new NotImplementedException();
        }

        public bool Add(Article obj)
        {
            using (var sqlConn = WebCloudSqlConnection.CreateSqlConnection())
            {
                sqlConn.Open();
                var command = sqlConn.CreateProcedureCommand(
                                                "[dbo].[Article_Insert]",
                                                new Dictionary<string, object>
                                                    {
                                                            {"@CategoryIds", obj.CategoryIds.ToListStringTable()},
                                                            {"@TagValues", obj.TagValues.ToListStringTable()},
                                                            {"@WebsiteId", obj.WebsiteID},
                                                            {"@ArticleID", obj.ArticleID},
                                                            {"@LanguageID", obj.Content.LanguageID},
                                                            {"@Title", obj.Content.Title},
                                                            {"@SeoTitle", obj.Content.SeoTitle},
                                                            {"@Description", obj.Content.Description},
                                                            {"@Content", obj.Content.Content},
                                                            {"@MetaTitle", obj.Content.MetaTitle},
                                                            {"@MetaKeyword", obj.Content.MetaKeyword},
                                                            {"@MetaDescription", obj.Content.MetaDescription},
                                                            {"@ThumbImage", obj.ThumbImage},
                                                            {"@SortOrder", obj.SortOrder},
                                                            {"@HaveVideo", obj.HaveVideo},
                                                            {"@HavePhoto", obj.HavePhoto},
                                                            {"@HaveAttachFile", obj.HaveAttachFile},
                                                            {"@IsHot", obj.IsHot},
                                                            {"@IsApproved", obj.IsApproved},
                                                            {"@AllowComment", obj.AllowComment},
                                                            {"@DisplayDatetime", obj.DisplayDatetime},
                                                            {"@IsGlobalDisplay", obj.IsGlobalDisplay},
                                                            {"@IsEnabled", obj.IsEnabled},
                                                            {"@CreatedUserName", obj.CreatedUserName},
                                                            {"@CreatedUserID", obj.CreatedUserID}
                                                    });
                return command.ExecuteNonQuery() != 0;
            }
        }

        public bool Edit(Article obj)
        {
            using (var sqlConn = WebCloudSqlConnection.CreateSqlConnection())
            {
                sqlConn.Open();
                var command = sqlConn.CreateProcedureCommand(
                                                "[dbo].[Article_Update]",
                                                new Dictionary<string, object>
                                                    {
                                                            {"@CategoryIds", obj.CategoryIds.ToListStringTable()},
                                                            {"@TagValues", obj.TagValues.ToListStringTable()},
                                                            {"@WebsiteId", obj.WebsiteID},
                                                            {"@ArticleID", obj.ArticleID},
                                                            {"@LanguageID", obj.Content.LanguageID},
                                                            {"@Title", obj.Content.Title},
                                                            {"@SeoTitle", obj.Content.SeoTitle},
                                                            {"@Description", obj.Content.Description},
                                                            {"@Content", obj.Content.Content},
                                                            {"@MetaTitle", obj.Content.MetaTitle},
                                                            {"@MetaKeyword", obj.Content.MetaKeyword},
                                                            {"@MetaDescription", obj.Content.MetaDescription},
                                                            {"@ThumbImage", obj.ThumbImage},
                                                            {"@SortOrder", obj.SortOrder},
                                                            {"@HaveVideo", obj.HaveVideo},
                                                            {"@HavePhoto", obj.HavePhoto},
                                                            {"@HaveAttachFile", obj.HaveAttachFile},
                                                            {"@IsHot", obj.IsHot},
                                                            {"@IsApproved", obj.IsApproved},
                                                            {"@AllowComment", obj.AllowComment},
                                                            {"@DisplayDatetime", obj.DisplayDatetime},
                                                            {"@IsGlobalDisplay", obj.IsGlobalDisplay},
                                                            {"@IsEnabled", obj.IsEnabled},
                                                            {"@LastEditedUserName", obj.LastEditedUserName},
                                                            {"@LastEditedUserID", obj.LastEditedUserID}
                                                    });
                return command.ExecuteNonQuery() != 0;
            }
        }

        public bool Delete(Article objDelete)
        {
            using (var sqlConn = WebCloudSqlConnection.CreateSqlConnection())
            {
                sqlConn.Open();
                var command = sqlConn.CreateProcedureCommand(
                                                "[dbo].[Article_Delete]",
                                                new Dictionary<string, object>
                                                    {
                                                            {"@WebsiteId", objDelete.WebsiteID},
                                                            {"@ArticleID", objDelete.ArticleID},
                                                            {"@LastEditedUserName", objDelete.LastEditedUserName},
                                                            {"@LastEditedUserID", objDelete.LastEditedUserID}
                                                    });
                return command.ExecuteNonQuery() != 0;
            }
        }

        public bool Remove(Article objDelete)
        {
            using (var sqlConn = WebCloudSqlConnection.CreateSqlConnection())
            {
                sqlConn.Open();
                var command = sqlConn.CreateProcedureCommand(
                                                "[dbo].[Article_Remove]",
                                                new Dictionary<string, object>
                                                    {
                                                            {"@WebsiteId", objDelete.WebsiteID},
                                                            {"@ArticleID", objDelete.ArticleID}
                                                    });
                return command.ExecuteNonQuery() != 0;
            }
        }

        public bool Restore(Article objDelete)
        {
            using (var sqlConn = WebCloudSqlConnection.CreateSqlConnection())
            {
                sqlConn.Open();
                var command = sqlConn.CreateProcedureCommand(
                                                "[dbo].[Article_Restore]",
                                                new Dictionary<string, object>
                                                    {
                                                            {"@WebsiteId", objDelete.WebsiteID},
                                                            {"@ArticleID", objDelete.ArticleID},
                                                            {"@LastEditedUserName", objDelete.LastEditedUserName},
                                                            {"@LastEditedUserID", objDelete.LastEditedUserID}
                                                    });
                return command.ExecuteNonQuery() != 0;
            }
        }

        public bool EmptyTrash(string websiteId)
        {
            using (var sqlConn = WebCloudSqlConnection.CreateSqlConnection())
            {
                sqlConn.Open();
                var command = sqlConn.CreateProcedureCommand(
                                                "[dbo].[Article_EmptyTrash]",
                                                new Dictionary<string, object>
                                                    {
                                                            {"@WebsiteId", websiteId}
                                                    });
                return command.ExecuteNonQuery() != 0;
            }
        }

        public bool UpdateFields(string websiteId, string id, string languageId, Dictionary<string, object> fields)
        {
            using (var dataContext = new WebCloudDbContext())
            {
                var record = dataContext.Articles.SingleOrDefault(obj => obj.WebsiteID == websiteId && obj.ArticleID == id);
                if (record == null) return false;

                var content = record.ArticleContents.SingleOrDefault(o => o.LanguageID == languageId);
                if (content == null) return false;

                foreach (var field in fields)
                {
                    switch (field.Key)
                    {
                        case "Content.Title":
                            content.Title = (string)field.Value;
                            break;
                        case "Content.Description":
                            content.Description = (string)field.Value;
                            break;
                        case "Content.Content":
                            content.Content = (string)field.Value;
                            break;
                        case "LastEditedUserName":
                            record.LastEditedUserName = (string)field.Value;
                            break;
                        case "LastEditedUserID":
                            record.LastEditedUserID = (long)field.Value;
                            break;
                        case "LastEditedDatetime":
                            record.LastEditedDatetime = (DateTime)field.Value;
                            break;
                    }
                }

                dataContext.SaveChanges();
                return true;
            }
        }
    }
}
