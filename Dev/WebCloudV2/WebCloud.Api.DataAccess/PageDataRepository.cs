﻿using System.Collections.Generic;
using System.Linq;
using WebCloud.Api.DataAccess.EF;
using WebCloud.Api.DataAccess.SQL;
using WebCloud.Common.Constant;
using WebCloud.Entity;
using DHSoft.Utility.Sql;
namespace WebCloud.Api.DataAccess
{
    public interface IPageDataRepository
    {
        List<Page> GetAll(string websiteId, bool? isDeleted = null, bool? isEnabled = null, bool withFullContent = false);
        Page GetById(string websiteId, string id);
        Page GetById(string websiteId, string id, string languageId);
        Page GetBySeoName(string websiteId, string seoName);
        bool Add(Page obj);
        bool Edit(Page obj, string languageId);
        bool Delete(string websiteId, string id);
        bool IsExist(string websiteId, string id);
        bool Install(string fromWebsiteId, string pageId, string toWebsiteId);
    }

    public class PageDataRepository : IPageDataRepository
    {
        public List<Page> GetAll(string websiteId, bool? isDeleted = null, bool? isEnabled = null, bool withFullContent = false)
        {
            using (var dataContext = new WebCloudDbContext())
            {
                dataContext.Configuration.ProxyCreationEnabled = false;
                var result = dataContext.Pages.Where(o => o.WebsiteID == websiteId 
                                                       && (isDeleted == null || o.IsDeleted == isDeleted)
                                                       && (isEnabled == null || o.IsEnabled == isEnabled)).ToList();

                if (!withFullContent)
                    return result;

                foreach (var page in result)
                {
                    TextDataHelper.FillObjectContent(dataContext, page, page.PageID, websiteId);
                }
                return result;
            }
        }

        public Page GetById(string websiteId, string id)
        {
            using (var dataContext = new WebCloudDbContext())
            {
                dataContext.Configuration.ProxyCreationEnabled = false;
                var page = dataContext.Pages.SingleOrDefault(o => o.WebsiteID == websiteId && o.PageID == id);

                if (page == null)
                    return null;

                TextDataHelper.FillObjectContent(dataContext, page, id, websiteId);
                return page;
            }
        }

        public Page GetById(string websiteId, string id, string languageId)
        {
            using (var dataContext = new WebCloudDbContext())
            {
                dataContext.Configuration.ProxyCreationEnabled = false;
                var page = dataContext.Pages.SingleOrDefault(o => o.WebsiteID == websiteId && o.PageID == id);

                if (page == null)
                    return null;

                TextDataHelper.FillObjectContent(dataContext, page, id, websiteId, languageId);
                return page;
            }
        }

        public Page GetBySeoName(string websiteId, string seoName)
        {
            using (var dataContext = new WebCloudDbContext())
            {
                dataContext.Configuration.ProxyCreationEnabled = false;
                return dataContext.Pages.FirstOrDefault(o => o.WebsiteID == websiteId && o.SeoName == seoName);
            }
        }

        public bool Add(Page obj)
        {
            using (var dataContext = new WebCloudDbContext())
            {
                TextDataHelper.UpdateObjectContent(dataContext, obj, obj.PageID, obj.WebsiteID, Default.LanguageId);
                dataContext.Pages.Add(obj);
                return dataContext.SaveChanges() > 0;
            }
        }

        public bool Edit(Page obj, string languageId)
        {
            using (var dataContext = new WebCloudDbContext())
            {
                var current = dataContext.Pages.SingleOrDefault(o => o.WebsiteID == obj.WebsiteID && o.PageID == obj.PageID);
                if (current == null) return false;

                current.ParentID = obj.ParentID;
                current.LayoutID = obj.LayoutID;
                current.Group = obj.Group;

                if (languageId == Default.LanguageId)
                {
                    current.Name = obj.Name;
                    current.SeoName = obj.SeoName;
                    current.MetaTitle = obj.MetaTitle;
                    current.MetaKeyword = obj.MetaKeyword;
                    current.MetaDescription = obj.MetaDescription;
                }

                current.SortOrder = obj.SortOrder;
                current.IsEnabled = obj.IsEnabled;
                current.IsGroup = obj.IsGroup;

                // update text content
                TextDataHelper.UpdateObjectContent(dataContext, obj, obj.PageID, obj.WebsiteID, languageId);

                dataContext.SaveChanges();
                return true;
            }
        }

        public bool Delete(string websiteId, string id)
        {
            using (var dataContext = new WebCloudDbContext())
            {
                var objDelete = dataContext.Pages.SingleOrDefault(o => o.WebsiteID == websiteId && o.PageID == id);
                if (objDelete == null) return false;

                TextDataHelper.DeleteObjectContent(dataContext, objDelete, id, websiteId);
                dataContext.Pages.Remove(objDelete);
                dataContext.SaveChanges();
                return true;
            }
        }

        public bool IsExist(string websiteId, string id)
        {
            using (var dataContext = new WebCloudDbContext())
            {
                return dataContext.Pages.Any(o => o.WebsiteID == websiteId && o.PageID == id);
            }
        }

        public bool Install(string fromWebsiteId, string pageId, string toWebsiteId)
        {
            using (var sqlConn = WebCloudSqlConnection.CreateSqlConnection())
            {
                sqlConn.Open();
                var command = sqlConn.CreateProcedureCommand(
                                                "[dbo].[Page_Copy]",
                                                new Dictionary<string, object>
                                                    {
                                                            {"@PageId", pageId},
                                                            {"@FromWebsiteId",fromWebsiteId},
                                                            {"@ToWebsiteId", toWebsiteId}
                                                    });
                return command.ExecuteNonQuery() != 0;
            }
        }

    }
}
