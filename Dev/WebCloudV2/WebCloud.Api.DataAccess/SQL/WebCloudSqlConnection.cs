﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WebCloud.Api.DataAccess.SQL
{
    public class WebCloudSqlConnection : IDisposable
    {
        public static readonly string ConnectionString = ConfigurationManager.ConnectionStrings["WebCloudV2Context"].ConnectionString;
        //public static readonly string ConnectionString = @"Data Source=BINHPRO\SQLEXPRESS;Initial Catalog=WebCloudV2;Persist Security Info=True;User ID=sa;Password=1234567;MultipleActiveResultSets=True";

        private readonly SqlConnection _sqlConnection;

        public WebCloudSqlConnection()
        {
            _sqlConnection = new SqlConnection(ConnectionString);
        }
        #region Implementation of IDisposable

        public void Dispose()
        {
            _sqlConnection.Dispose();
        }

        public SqlDataReader ExecuteReader(string storedProcedure, Dictionary<string,object> inParameters)
        {

            var command = new SqlCommand(storedProcedure, _sqlConnection)
            {
                CommandType = CommandType.StoredProcedure
            };

            if (_sqlConnection.State == ConnectionState.Closed)
            {
                _sqlConnection.Open();
            }

            // put inParameters
            if (inParameters.Count > 0)
            {
                foreach (var pair in inParameters)
                {
                    command.Parameters.AddWithValue(pair.Key, pair.Value);
                }
            }

            var reader = command.ExecuteReader();
            return reader;
        }

        #endregion
    
        public static SqlConnection CreateSqlConnection()
        {
            return  new SqlConnection(ConnectionString);
        }
    }
}
