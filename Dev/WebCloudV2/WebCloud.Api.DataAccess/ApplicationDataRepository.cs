﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using WebCloud.Api.DataAccess.EF;
using WebCloud.Entity;

namespace WebCloud.Api.DataAccess
{
    public interface IApplicationDataRepository
    {
        List<Application> GetAll();
    }

    public class ApplicationDataRepository : IApplicationDataRepository
    {
        #region Implementation of IApplicationDataRepository

        public List<Application> GetAll()
        {
            using (var dbContext = new WebCloudDbContext())
            {
                return dbContext.Applications.ToList();
            }
        }

        #endregion
    }
}
