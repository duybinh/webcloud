﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WebCloud.Api.DataAccess.EF;
using WebCloud.Entity;

namespace WebCloud.Api.DataAccess
{
    public interface IRoleDataRepository
    {
        List<UserRole> GetAll();
        UserRole GetById(string roleId);
        bool IsExist(string roleId);
        bool Add(UserRole role);
        bool Edit(UserRole role);
        bool Delete(string roleId);
    }
    public class RoleDataRepository : IRoleDataRepository
    {
        public List<UserRole> GetAll()
        {
            using (var dataContext = new WebCloudDbContext())
            {
                dataContext.Configuration.ProxyCreationEnabled = false;
                return dataContext.UserRoles.ToList();
            }
        }

        public UserRole GetById(string roleId)
        {
            using (var dataContext = new WebCloudDbContext())
            {
                dataContext.Configuration.ProxyCreationEnabled = false;
                return dataContext.UserRoles.SingleOrDefault(obj => obj.UserRoleID == roleId);
            }
        }

        public bool IsExist(string roleId)
        {
            using (var dataContext = new WebCloudDbContext())
            {
                return dataContext.UserRoles.Any(obj => obj.UserRoleID == roleId);
            }
        }

        public bool Add(UserRole role)
        {
            using (var dataContext = new WebCloudDbContext())
            {
                dataContext.UserRoles.Add(role);
                return dataContext.SaveChanges() > 0;
            }
        }

        public bool Edit(UserRole role)
        {
            using (var dataContext = new WebCloudDbContext())
            {
                var current = dataContext.UserRoles.SingleOrDefault(obj => obj.UserRoleID == role.UserRoleID);
                if (current == null) return false;

                current.Name = role.Name;
                current.Description = role.Description;
                current.SortOrder = role.SortOrder;
                current.IsEnabled = role.IsEnabled;

                dataContext.SaveChanges();
                return true;
            }
        }

        public bool Delete(string roleId)
        {
            using (var dataContext = new WebCloudDbContext())
            {
                var role = dataContext.UserRoles.SingleOrDefault(obj => obj.UserRoleID == roleId);
                if (role == null) return false;

                dataContext.UserRoles.Remove(role);
                dataContext.SaveChanges();
                return true;
            }
        }
    }
}
