﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using WebCloud.Common;
using WebCloud.Api.DataAccess.EF;
using WebCloud.Entity;

namespace WebCloud.Api.DataAccess
{
    public interface IUserDataRepository
    {
        bool Insert(User obj);
        bool Update(User obj);
        bool Delete(User obj);
        bool DeleteDirectly(User obj);
        List<User> GetAll();
        // Role
        List<User> GetUsersInRole(string role);
        bool IsUserInRole(User user, string role);
        List<UserRole> GetRolesOfUser(User user);
        List<string> GetRoleIdsOfUser(User user);

        // Group
        List<User> GetUsersInGroup(string groupId);
        bool IsUserInGroup(User user, string groupId);
        List<UserGroup> GetGroupsOfUser(User user);
        List<string> GetGroupIdsOfUser(User user);

        User GetUserById(string websiteId, string userId);
        User GetUserByGlobalId(long globalId);

        AuthenticationResult Authenticate(string websiteId, string userId, string password);

        bool ChangePassword(User user, string newPassword);
    }

    public enum AuthenticationResult
    {
        Success,
        UserNotExist,
        PasswordFailed
    }

    public class UserDataRepository : IUserDataRepository
    {
        public bool Insert(User obj)
        {
            throw new NotImplementedException();
        }

        public bool Update(User obj)
        {
            throw new NotImplementedException();
        }

        public bool Delete(User obj)
        {
            throw new NotImplementedException();
        }

        public bool DeleteDirectly(User obj)
        {
            throw new NotImplementedException();
        }

        public List<User> GetAll()
        {
            throw new NotImplementedException();
        }

        public List<User> GetUsersInRole(string role)
        {
            throw new NotImplementedException();
        }

        public bool IsUserInRole(User user, string role)
        {
            throw new NotImplementedException();
        }

        public List<UserRole> GetRolesOfUser(User user)
        {
            using (var dbContext = new WebCloudDbContext())
            {
                var u = dbContext.Users.SingleOrDefault(obj => obj.WebsiteID == user.WebsiteID
                                                               && obj.UserID == user.UserID);
                if (u == null) return null;

                return u.UserRoles.ToList();
            }
        }

        public List<string> GetRoleIdsOfUser(User user)
        {
            using (var dbContext = new WebCloudDbContext())
            {
                var u = dbContext.Users.SingleOrDefault(obj => obj.WebsiteID == user.WebsiteID
                                                               && obj.UserID == user.UserID);
                if (u == null) return null;

                return u.UserRoles.Select(obj => obj.UserRoleID).ToList();
            }
        }

        public List<User> GetUsersInGroup(string groupId)
        {
            throw new NotImplementedException();
        }

        public bool IsUserInGroup(User user, string groupId)
        {
            throw new NotImplementedException();
        }

        public List<UserGroup> GetGroupsOfUser(User user)
        {
            throw new NotImplementedException();
        }

        public List<string> GetGroupIdsOfUser(User user)
        {
            throw new NotImplementedException();
        }

        public User GetUserById(string websiteId, string userId)
        {
            using (var dbContext = new WebCloudDbContext())
            {
                dbContext.Configuration.LazyLoadingEnabled = false;
                var u = dbContext.Users.SingleOrDefault(obj => obj.WebsiteID == websiteId
                                                               && obj.UserID == userId);
                return u;
            }
        }

        public User GetUserByGlobalId(long globalId)
        {
            using (var dbContext = new WebCloudDbContext())
            {
                dbContext.Configuration.LazyLoadingEnabled = false;
                var u = dbContext.Users.SingleOrDefault(obj => obj.GlobalID == globalId);
                return u;
            }
        }

        public AuthenticationResult Authenticate(string websiteId, string userId, string password)
        {
            using (var dbContext = new WebCloudDbContext())
            {
                var user = dbContext.Users.SingleOrDefault(obj => obj.WebsiteID == websiteId && obj.UserID == userId);
                if (user == null)
                {
                    return AuthenticationResult.UserNotExist;
                }
                if (user.Password != password)
                {
                    return AuthenticationResult.PasswordFailed;
                }
                return AuthenticationResult.Success;
            }
        }

        public bool ChangePassword(User user, string newPassword)
        {
            throw new NotImplementedException();
        }
    }
}
