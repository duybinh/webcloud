﻿using System.Configuration;
using DHSoft.Utility.Common;

namespace WebCloud.Auth.Helper
{
    public class ApplicationSettings
    {
        private static readonly ApplicationSettings Instance = new ApplicationSettings();
        private readonly string _tokenEndpointPath;
        private readonly int _accessTokenExpireMinute;

        private ApplicationSettings()
        {
            _tokenEndpointPath = ConfigurationManager.AppSettings["TokenEndpointPath"];
            _accessTokenExpireMinute =  ConfigurationManager.AppSettings["AccessTokenExpireMinute"].ToInt32TryParse();
        }
        public static string TokenEndpointPath
        {
            get
            {
                return Instance._tokenEndpointPath;
            }
        }

        public static int AccessTokenExpireMinute
        {
            get
            {
                return Instance._accessTokenExpireMinute;
            }
        }


    }
}