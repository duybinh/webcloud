﻿using System.Threading.Tasks;
using System.Web.Http;

namespace WebCloud.Auth.Controllers
{
    public class HomeController : ApiController
    {
        [Route("")]
        [HttpGet]
        public string Index()
        {
            return "Welcome to WebCloud Authentication System! Copyright @2015 by Dong Hanh.";
        }
	}
}