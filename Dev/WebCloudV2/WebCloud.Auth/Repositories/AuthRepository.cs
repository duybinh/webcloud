﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WebCloud.Api.DataAccess.EF;
using WebCloud.Auth.Entities;
using WebCloud.Entity;

namespace WebCloud.Auth.Repositories
{
    public class AuthRepository
    {
        #region Simple inmem Clients and Audiences store
        public static Dictionary<string, Client> ClientsStore = new Dictionary<string, Client>()
        {
            {"WebCloudManagementApp", new Client()
            {
                ClientId = "WebCloudManagementApp",
                Name = "Webcloud management web application",
                SecretKey = "aww2a1LnyaKxDYWwzqcQ5a86haUnhKFAjIIVCDEPrFo",
                ApplicationType = ApplicationType.Javascript,
                IsActive = true,
                AllowedOrigin = "*",
                RefreshTokenLifeTime = 43200 // minutes = 30 days
            }},
            {"WebCloudFrontendSite", new Client()
            {
                ClientId = "WebCloudFrontendSite",
                Name = "Webcloud frontend website",
                SecretKey = "xQhMekeAgqeFttAQvJppPfJ54cInI5g_0R4VBJnvOac",
                ApplicationType = ApplicationType.Javascript,
                IsActive = true,
                AllowedOrigin = "*",
                RefreshTokenLifeTime = 43200 // minutes = 30 days
            }}
        };

        public static Dictionary<string, Audience> AudiencesStore = new Dictionary<string, Audience>()
        {
            {"WebCloudBackend", new Audience()
            {
                AudienceId = "WebCloudBackend",
                SecretKey = "NV4lntfJwxZsFq8ovLvbksUu4zYuDm3qTpO1urH1i6E",
                Name = "WebCloud Api and Resource server"
            }} // audiences for both WebCloud.Api server and WebCloud.Resource server
        };
        #endregion

        public Client FindClient(string clientId)
        {
            if (ClientsStore.ContainsKey(clientId))
                return ClientsStore[clientId];

            return null;
        }

        public Audience FindAudience(string audienceId)
        {
            if (AudiencesStore.ContainsKey(audienceId))
                return AudiencesStore[audienceId];

            return null;
        }

        #region Manage refresh tokens

        public bool AddRefreshToken(RefreshToken token)
        {
            using (var dbContext = new WebCloudDbContext())
            {
                var existToken = dbContext.RefreshTokens.SingleOrDefault(o => o.WebsiteID == token.WebsiteID && o.UserID == token.UserID
                                                                           && o.Client == token.Client && o.Audience == token.Audience);
                if (existToken != null)
                {
                    dbContext.RefreshTokens.Remove(existToken);
                }
                dbContext.RefreshTokens.Add(token);
                return dbContext.SaveChanges() > 0;
            }
        }

        public bool RemoveRefreshToken(string tokenId)
        {
            using (var dbContext = new WebCloudDbContext())
            {
                var existToken = dbContext.RefreshTokens.SingleOrDefault(o => o.RefreshTokenID == tokenId);
                if (existToken == null)
                {
                    return false;
                }

                dbContext.RefreshTokens.Remove(existToken);
                return dbContext.SaveChanges() > 0;
            }
        }

        public RefreshToken FindRefreshToken(string tokenId)
        {
            using (var dbContext = new WebCloudDbContext())
            {
                dbContext.Configuration.LazyLoadingEnabled = false;
                dbContext.Configuration.ProxyCreationEnabled = false;
                return dbContext.RefreshTokens.SingleOrDefault(o => o.RefreshTokenID == tokenId);
            }
        }
        #endregion

    }
}