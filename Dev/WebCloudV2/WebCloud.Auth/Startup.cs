﻿using System;
using System.Web.Http;
using Microsoft.Owin;
using Microsoft.Owin.Security.OAuth;
using Owin;
using WebCloud.Auth;
using WebCloud.Auth.Format;
using WebCloud.Auth.Helper;
using WebCloud.Auth.Providers;

[assembly: OwinStartup(typeof(Startup))]
namespace WebCloud.Auth
{
    public class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureOAuth(app);

            var config = new HttpConfiguration();
            WebApiConfig.Register(config);
            
            app.UseCors(Microsoft.Owin.Cors.CorsOptions.AllowAll);
            app.UseWebApi(config);
        }

        public void ConfigureOAuth(IAppBuilder app)
        {
            var oAuthServerOptions = new OAuthAuthorizationServerOptions()
            {
                AllowInsecureHttp = true,
                TokenEndpointPath = new PathString(ApplicationSettings.TokenEndpointPath),
                AccessTokenExpireTimeSpan = TimeSpan.FromMinutes(ApplicationSettings.AccessTokenExpireMinute),
                Provider = new WebCloudAuthServerProvider(),
                RefreshTokenProvider = new WebCloudRefreshTokenProvider(),
                AccessTokenFormat = new WebCloudJwtFormat("WebCloud.Auth")
            };
            var oAuthBearerOptions =new OAuthBearerAuthenticationOptions();
            
            // Token Generation
            app.UseOAuthAuthorizationServer(oAuthServerOptions);
            app.UseOAuthBearerAuthentication(oAuthBearerOptions);

        }

    }
}