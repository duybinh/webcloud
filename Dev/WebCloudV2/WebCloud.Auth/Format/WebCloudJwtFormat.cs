﻿using System;
using System.IdentityModel.Tokens;
using Microsoft.Owin.Security;
using Microsoft.Owin.Security.DataHandler.Encoder;
using Thinktecture.IdentityModel.Tokens;
using WebCloud.Auth.Entities;
using WebCloud.Auth.Repositories;

namespace WebCloud.Auth.Format
{
    public class WebCloudJwtFormat : ISecureDataFormat<AuthenticationTicket>
    {
        private readonly AuthRepository _authRepository = new AuthRepository();

        private readonly string _issuer = string.Empty;

        public WebCloudJwtFormat(string issuer)
        {
            _issuer = issuer;
        }

        public string Protect(AuthenticationTicket data)
        {
            if (data == null)
                throw new ArgumentNullException("data");

            string audienceId = data.Properties.Dictionary.ContainsKey("audience_id") ? data.Properties.Dictionary["audience_id"] : null;
            if (string.IsNullOrWhiteSpace(audienceId)) 
                throw new InvalidOperationException("Authentication ticket properties does not include audience");

            Audience audience = _authRepository.FindAudience(audienceId);
            if(audience == null)
                throw new InvalidOperationException(string.Format("Audience {0} not exist", audienceId));

            string symmetricKeyAsBase64 = audience.SecretKey;
            var keyByteArray = TextEncodings.Base64Url.Decode(symmetricKeyAsBase64);
            var signingKey = new HmacSigningCredentials(keyByteArray);

            var issued = data.Properties.IssuedUtc;
            var expires = data.Properties.ExpiresUtc;

            var token = new JwtSecurityToken(_issuer, audienceId, data.Identity.Claims, issued.Value.UtcDateTime, expires.Value.UtcDateTime, signingKey);
            var handler = new JwtSecurityTokenHandler();
            var jwt = handler.WriteToken(token);

            return jwt;
        }

        public AuthenticationTicket Unprotect(string protectedText)
        {
            throw new NotImplementedException();
        }
    }
}