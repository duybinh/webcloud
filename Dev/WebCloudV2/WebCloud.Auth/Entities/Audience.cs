﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebCloud.Auth.Entities
{
    public class Audience
    {
        public string AudienceId { set; get; }
        public string SecretKey { set; get; }
        public string Name { set; get; }
    }
}