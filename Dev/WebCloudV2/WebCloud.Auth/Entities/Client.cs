﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebCloud.Auth.Entities
{
    public enum ApplicationType
    {
        Javascript = 0,
        NativeConfidential = 1
    }
    public class Client
    {
        public string ClientId { set; get; }
        public string SecretKey { set; get; }
        public string Name { set; get; }
        public ApplicationType ApplicationType { set; get; }
        public bool IsActive { set; get; }
        public long RefreshTokenLifeTime { set; get; }
        public string AllowedOrigin { set; get; }
    }
}