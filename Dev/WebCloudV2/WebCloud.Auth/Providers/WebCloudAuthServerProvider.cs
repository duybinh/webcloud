﻿using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using DHSoft.Utility.Security;
using Microsoft.Owin.Security;
using Microsoft.Owin.Security.OAuth;
using WebCloud.Api.DataAccess.EF;
using WebCloud.Auth.Entities;
using WebCloud.Auth.Repositories;
using WebCloud.Common.Constant;

namespace WebCloud.Auth.Providers
{
    public class WebCloudAuthServerProvider : OAuthAuthorizationServerProvider
    {
        private readonly AuthRepository _authRepository = new AuthRepository();
        public override Task ValidateClientAuthentication(OAuthValidateClientAuthenticationContext context)
        {
            string clientId = string.Empty;
            string clientSecret = string.Empty;

            if (!context.TryGetBasicCredentials(out clientId, out clientSecret))
            {
                context.TryGetFormCredentials(out clientId, out clientSecret);
            }

            if (context.ClientId == null)
            {
                context.SetError("Invalid_Client", "Client Id is required, set it to 'client_id' request header.");
                return Task.FromResult<object>(null);
            }

            // validate client id
            Client client = _authRepository.FindClient(clientId);
            if (client == null)
            {
                context.SetError("Invalid_Client", string.Format("Client {0} is not valid client of system.", clientId));
                return Task.FromResult<object>(null);
            }
            // validate client secret key
            if (client.ApplicationType == ApplicationType.NativeConfidential)
            {
                if (clientSecret != client.SecretKey)
                {
                    context.SetError("Invalid_Client", "Secret key provided in 'client_secret' for client {0} is not valid.", clientId);
                    return Task.FromResult<object>(null);
                }
            }
            // validate client is active
            if (!client.IsActive)
            {
                context.SetError("Invalid_Client", "Client {0} is not active.", clientId);
                return Task.FromResult<object>(null);
            }

            context.OwinContext.Set("as:clientAllowedOrigin", client.AllowedOrigin);
            context.OwinContext.Set("as:clientRefreshTokenLifeTime", client.RefreshTokenLifeTime.ToString());

            context.Validated();
            return Task.FromResult<object>(null);
        }

        public override async Task GrantResourceOwnerCredentials(OAuthGrantResourceOwnerCredentialsContext context)
        {

            var allowedOrigin = context.OwinContext.Get<string>("as:clientAllowedOrigin");
            if (allowedOrigin == null) allowedOrigin = "*";

            context.OwinContext.Response.Headers.Add("Access-Control-Allow-Origin", new[] { allowedOrigin });

            string websiteId = context.Request.Headers["WebsiteId"];
            if (string.IsNullOrEmpty(websiteId))
                websiteId = Default.WebsiteId;
            
            string audienceId = context.Request.Headers["AudienceId"];
            if (string.IsNullOrEmpty(audienceId))
                audienceId = "WebCloudBackend";

            using (var dbContext = new WebCloudDbContext())
            {
                string password = context.Password.GetMd5Hash();
                var user = await dbContext.Users.SingleOrDefaultAsync(obj => obj.WebsiteID == websiteId && !obj.IsDeleted
                                                                          && obj.UserID == context.UserName && obj.Password == password);
                if (user == null && websiteId != Default.WebsiteId)
                {
                    user = await dbContext.Users.SingleOrDefaultAsync(obj => obj.WebsiteID == Default.WebsiteId && !obj.IsDeleted
                                                                          && obj.UserID == context.UserName && obj.Password == password);
                }
                if (user == null)
                {
                    context.SetError("invalid_credential", string.Format("The user name or password for website {0} is incorrect.", websiteId));
                    return;
                }

                if (user.IsLocked)
                {
                    context.SetError("locked_user", string.Format("The user {0} is locked in website {1}.", context.UserName, websiteId));
                    return;
                }

                var identity = new ClaimsIdentity(context.Options.AuthenticationType);

                identity.AddClaim(new Claim(ClaimTypes.Name, user.UserID));
                identity.AddClaim(new Claim("global_id", user.GlobalID.ToString()));
                identity.AddClaim(new Claim("user_website", user.WebsiteID));
                identity.AddClaim(new Claim("target_website", websiteId));
                identity.AddClaim(new Claim("email", user.Email));
                identity.AddClaim(new Claim("is_verified_email", user.IsVerifiedEmail.ToString()));

                string userRoles = string.Join(",", user.UserRoles.Select(o => o.UserRoleID));
                identity.AddClaim(new Claim(ClaimTypes.Role, userRoles));

                string userGroups = string.Join(",", user.UserInGroups.Select(o => o.UserGroupID));
                identity.AddClaim(new Claim("groups", userGroups));

                var props = new AuthenticationProperties(new Dictionary<string, string>
                {
                    {"client_id", context.ClientId ?? string.Empty},
                    {"audience_id", audienceId},
                    {"website_id", websiteId},
                });

                var ticket = new AuthenticationTicket(identity, props);
                context.Validated(ticket);
            }
        }

        public override Task GrantRefreshToken(OAuthGrantRefreshTokenContext context)
        {
            var originalClient = context.Ticket.Properties.Dictionary["client_id"];
            var currentClient = context.ClientId;

            if (originalClient != currentClient)
            {
                context.SetError("invalid_clientid", "Refresh token is issued to a different clientId.");
                return Task.FromResult<object>(null);
            }

            var websiteIdClaim = context.Ticket.Identity.Claims.SingleOrDefault(o => o.Type == "user_website");
            var userIdClaim = context.Ticket.Identity.Claims.SingleOrDefault(o => o.Type == ClaimTypes.Name);
            if (websiteIdClaim == null)
            {
                context.SetError("invalid_website_claim", "user_website claim is invalid.");
                return Task.FromResult<object>(null);
            }
            if (userIdClaim == null)
            {
                context.SetError("invalid_userid_claim", "UserId claim is invalid.");
                return Task.FromResult<object>(null);
            }

            string userRoles = "";
            string userGroups = "";
            using (var dbContext = new WebCloudDbContext())
            {
                string websiteId = websiteIdClaim.Value;
                string userId = userIdClaim.Value;
                var user = dbContext.Users.SingleOrDefault(obj => !obj.IsDeleted && obj.WebsiteID == websiteId && obj.UserID == userId);
                if (user == null)
                {
                    context.SetError("invalid_user", "User is not exist.");
                    return Task.FromResult<object>(null);
                }
                if (user.IsLocked)
                {
                    context.SetError("locked_user", "User is not active.");
                    return Task.FromResult<object>(null);
                }
                // update roles & group
                userRoles = string.Join(",", user.UserRoles.Select(o => o.UserRoleID));
                userGroups = string.Join(",", user.UserInGroups.Select(o => o.UserGroupID));
            }

            // Change auth ticket for refresh token requests
            var newIdentity = new ClaimsIdentity(context.Ticket.Identity);
            var roleClaim = newIdentity.Claims.FirstOrDefault(c => c.Type == ClaimTypes.Role);
            if (roleClaim != null)
            {
                newIdentity.RemoveClaim(roleClaim);
            }
            newIdentity.AddClaim(new Claim(ClaimTypes.Role, userRoles));

            var groupClaim = newIdentity.Claims.FirstOrDefault(c => c.Type == "groups");
            if (groupClaim != null)
            {
                newIdentity.RemoveClaim(groupClaim);
            }
            newIdentity.AddClaim(new Claim("groups", userGroups));

            var newTicket = new AuthenticationTicket(newIdentity, context.Ticket.Properties);
            context.Validated(newTicket);

            return Task.FromResult<object>(null);
        }

        public override Task TokenEndpoint(OAuthTokenEndpointContext context)
        {
            foreach (KeyValuePair<string, string> property in context.Properties.Dictionary)
            {
                context.AdditionalResponseParameters.Add(property.Key, property.Value);
            }

            return Task.FromResult<object>(null);
        }

        public override Task MatchEndpoint(OAuthMatchEndpointContext context)
        {
            if (context.IsTokenEndpoint && context.Request.Method == "OPTIONS")
            {
                context.OwinContext.Response.Headers.Add("Access-Control-Allow-Origin", new[] { "*" });
                context.OwinContext.Response.Headers.Add("Access-Control-Allow-Headers", new[] { "WebsiteId, AudienceId" });
                context.OwinContext.Response.Headers.Add("Access-Control-Allow-Methods", new[] { "GET, POST, OPTIONS, PUT, DELETE" });
                context.RequestCompleted();
                return Task.FromResult(0);
            }

            return base.MatchEndpoint(context);
        }
    }
}