﻿using System;
using System.Threading.Tasks;
using DHSoft.Utility.Security;
using Microsoft.Owin.Security.Infrastructure;
using WebCloud.Auth.Repositories;
using WebCloud.Entity;

namespace WebCloud.Auth.Providers
{
    public class WebCloudRefreshTokenProvider : IAuthenticationTokenProvider
    {
        private readonly AuthRepository _authRepository = new AuthRepository();

        public async Task CreateAsync(AuthenticationTokenCreateContext context)
        {
            var clientid = context.Ticket.Properties.Dictionary["client_id"];
            var audienceId = context.Ticket.Properties.Dictionary["audience_id"];
            var websiteId = context.Ticket.Properties.Dictionary["website_id"];

            if (string.IsNullOrEmpty(clientid))
            {
                return;
            }

            var refreshTokenId = Guid.NewGuid().ToString("n");
            var refreshTokenLifeTime = context.OwinContext.Get<string>("as:clientRefreshTokenLifeTime");

            var token = new RefreshToken()
            {
                RefreshTokenID = refreshTokenId.GetMd5Hash(),
                WebsiteID = websiteId,
                UserID = context.Ticket.Identity.Name,
                Client = clientid,
                Audience = audienceId,
                IssuedUtc = DateTime.UtcNow,
                ExpiredUtc = DateTime.UtcNow.AddMinutes(Convert.ToDouble(refreshTokenLifeTime))
            };

            context.Ticket.Properties.IssuedUtc = token.IssuedUtc;
            context.Ticket.Properties.ExpiresUtc = token.ExpiredUtc;

            token.ProtectedTicket = context.SerializeTicket();

            var result = _authRepository.AddRefreshToken(token);

            if (result)
            {
                context.SetToken(refreshTokenId);
            }
        }

        public async Task ReceiveAsync(AuthenticationTokenReceiveContext context)
        {

            var allowedOrigin = context.OwinContext.Get<string>("as:clientAllowedOrigin");
            context.OwinContext.Response.Headers.Add("Access-Control-Allow-Origin", new[] { allowedOrigin });

            string hashedTokenId = context.Token.GetMd5Hash();

            var refreshToken = _authRepository.FindRefreshToken(hashedTokenId);

            if (refreshToken != null)
            {
                context.DeserializeTicket(refreshToken.ProtectedTicket);
                // remove refresh token in db 
                _authRepository.RemoveRefreshToken(hashedTokenId);
            }
        }

        public void Create(AuthenticationTokenCreateContext context)
        {
            throw new NotImplementedException();
        }

        public void Receive(AuthenticationTokenReceiveContext context)
        {
            throw new NotImplementedException();
        }
    }
}