﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Routing;
using System.Web.Security;
using System.Web.SessionState;
using WebCloud.Site.Infrastructure.Provider;
using WebCloud.Site.Infrastructure.Render;

namespace WebCloud.Site
{
    public class Global : System.Web.HttpApplication
    {
        void RegisterCustomRoutes(RouteCollection routes)
        {
            routes.MapPageRoute(
              "ListArticles",
              "a/{ArticleCategorySeoName}/{ArticleCategoryId}",
              "~/Default.aspx", true,
              new RouteValueDictionary { { "PageId", "article_list" } }
          );

            routes.MapPageRoute(
               "ArticleDetail",
               "ad/{ArticleSeoName}/{ArticleId}",
               "~/Default.aspx", true,
               new RouteValueDictionary { { "PageId", "article_detail" } }
           );

            routes.MapPageRoute(
                "Page",
                "{PageName}",
                "~/Default.aspx"
            );

        }

        protected void Application_Start(object sender, EventArgs e)
        {
            // Code that runs on application startup
            //RouteConfig.RegisterRoutes(RouteTable.Routes);
            //BundleConfig.RegisterBundles(BundleTable.Bundles);

            // Add Routes.
            RegisterCustomRoutes(RouteTable.Routes);

            TemplateRender.InitRender();
        }

        protected void Session_Start(object sender, EventArgs e)
        {

        }

        protected void Application_BeginRequest(object sender, EventArgs e)
        {
            ContextProvider.LoadWebsiteContext(this.Context);
        }

        protected void Application_AuthenticateRequest(object sender, EventArgs e)
        {

        }

        protected void Application_Error(object sender, EventArgs e)
        {

        }

        protected void Session_End(object sender, EventArgs e)
        {

        }

        protected void Application_End(object sender, EventArgs e)
        {

        }
    }
}