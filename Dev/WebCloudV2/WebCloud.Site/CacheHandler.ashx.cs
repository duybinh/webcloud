﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Newtonsoft.Json;
using WebCloud.Common;
using WebCloud.Site.Infrastructure.Base;
using WebCloud.Site.Infrastructure.Provider;

namespace WebCloud.Site
{
    /// <summary>
    /// Handle clear cache request
    /// </summary>
    public class CacheHandler : BaseHttpHandler
    {

        public override void ProcessRequest(HttpContext context)
        {
            base.ProcessRequest(context);

            string cacheKey = context.Request["CacheKey"];
            if (string.IsNullOrEmpty(cacheKey))
            {
                // response
                ResponseJson(context, new ResultBase { IsSuccess = false, Message = new Message("CacheKey_Invalid") });
                return;
            }

            bool allWebsite = !string.IsNullOrEmpty(context.Request["AllWebsite"]);
            bool allLanguage = !string.IsNullOrEmpty(context.Request["AllLanguage"]);
            string website = allWebsite ? null : ContextProvider.WebsiteContext.WebsiteId;
            string language = allLanguage ? null : ContextProvider.WebsiteContext.LanguageId;
            // delete cache items
            CachingProvider.Instance.DeleteCache(cacheKey, website, language);

            // response
            ResponseJson(context, new ResultObject<dynamic> { IsSuccess = true, Data = new { CacheKey = cacheKey, Website = website, Language = language } });
        }

        public override bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}