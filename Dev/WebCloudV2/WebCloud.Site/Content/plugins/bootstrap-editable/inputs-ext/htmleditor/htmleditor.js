
(function ($) {
    "use strict";

    var HtmlEditor = function (options) {
        this.init('htmleditor', options, HtmlEditor.defaults);

        //extend htmleditor manually as $.extend not recursive 
        this.options.htmleditor = $.extend({}, HtmlEditor.defaults.htmleditor, options.htmleditor);
    };

    $.fn.editableutils.inherit(HtmlEditor, $.fn.editabletypes.abstractinput);

    $.extend(HtmlEditor.prototype, {
        render: function () {
            var deferred = $.Deferred();

            //generate unique id as it required for htmleditor
            //this.$input.attr('id', 'textarea_' + (new Date()).getTime());

            this.setClass();
            this.setAttr('placeholder');

            //resolve deffered when widget loaded
            $.extend(this.options.htmleditor, {
                onInit: function () {
                    deferred.resolve();
                }
            });

            this.$input.parents('.editable-input').css('display', 'block').css('width', '100%');
            this.$input.parents('.editable-container').addClass('editable-html-container');
            this.$input.summernote(this.options.htmleditor);

            return deferred.promise();
        },

        value2html: function (value, element) {
            $(element).html(value);
        },

        html2value: function (html) {
            return html;
        },

        value2input: function (value) {
            this.$input.code(value);
        },

        input2value: function () {
            var result = this.$input.code();
            this.$input.destroy();
            return result;
        },

        activate: function () {
            this.$input.focus();
        },

        isEmpty: function ($element) {
            if ($.trim($element.html()) === '') {
                return true;
            } else if ($.trim($element.text()) !== '') {
                return false;
            } else {
                //e.g. '<img>', '<br>', '<p></p>'
                return !$element.height() || !$element.width();
            }
        }
    });

    HtmlEditor.defaults = $.extend({}, $.fn.editabletypes.abstractinput.defaults, {
        tpl: '<div></div>',
        inputclass: 'inline-html-editor',
        htmleditor: {
            airMode: true,
        }
    });

    $.fn.editabletypes.htmleditor = HtmlEditor;

}(window.jQuery));
