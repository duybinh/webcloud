﻿(function () {
    $.cookie.raw = true;

    // editable field
    $.fn.editable.defaults.mode = 'inline';
    $('[data-edit-obj]').each(function () {
        $(this).editable({
            url: 'http://localhost:14469/api/update-data',
            onblur: $(this).attr('data-edit-type') == 'htmleditor' ? 'ignore' : 'cancel',
            type: $(this).attr('data-edit-type'),
            pk: $(this).attr('data-edit-key'),
            send: 'always',
            ajaxOptions: {
                type: 'post',
                dataType: 'json',
                contentType: "application/json",
                headers: {
                    'WebsiteId': $.cookie('WebsiteId'),
                    'LanguageId': $.cookie('LanguageId'),
                    'AuthToken': $.cookie('AuthToken')
                }
            },
            params: function (params) {
                console.debug('params');
                console.debug(params);
                var postParams = {
                    Obj: $(this).attr('data-edit-obj'),
                    Key: $(this).attr('data-edit-key'),
                    Fields: {}
                };
                postParams.Fields[$(this).attr('data-edit-field')] = params.value;
                return JSON.stringify(postParams);
            },
            // unsavedclass: null,
            success: function (response, newValue) {
                console.debug('post update-data success');
                console.debug(response);
                if (response.IsSuccess) {
                    return { newValue: newValue };
                } else {
                    return response.Message.Code;
                }
            }
        });
    });
}());