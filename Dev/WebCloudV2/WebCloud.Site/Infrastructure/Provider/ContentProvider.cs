﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Routing;
using WebCloud.Api.Core.Helper;
using WebCloud.Common;
using WebCloud.Entity;
using WebCloud.Site.Infrastructure.Helper;

namespace WebCloud.Site.Infrastructure.Provider
{
    public class ContentProvider
    {
        public static readonly ContentProvider Instance = new ContentProvider();

        private ContentProvider()
        {

        }

        public ResultObject<Object> GetData(string dataSource, Dictionary<string, string> dataSourceParams)
        {
            switch (dataSource)
            {
                case "CurrentArticle":
                    return new ResultObject<object>
                        {
                            IsSuccess = true,
                            Data = GetCurrentArticle()
                        };

                case "ListArticles":
                    return new ResultObject<object>
                    {
                        IsSuccess = true,
                        Data = GetListArticles(dataSourceParams)
                    };

                case "CurrentArticleCategory":
                    return new ResultObject<object>
                    {
                        IsSuccess = true,
                        Data = GetCurrentArticleCategory()
                    };

                default:
                    return new ResultObject<object>
                        {
                            IsSuccess = false,
                            Message = new Message { Code = "DataSource_Invalid" }
                        };
            }
        }

        #region Article
        private Article GetCurrentArticle()
        {
            var articleId = (string)HttpHelper.GetUrlParam("ArticleId");
            if (string.IsNullOrEmpty(articleId))
                return null;
            var result = ApiAdapter.GetArticleById(ContextProvider.WebsiteContext.WebsiteId, ContextProvider.WebsiteContext.LanguageId, articleId);
            return result.Data;
        }

        private ArticleCategory GetCurrentArticleCategory()
        {
            var categoryId = (string)HttpHelper.GetUrlParam("ArticleCategoryId");
            if (string.IsNullOrEmpty(categoryId))
                return null;
            var result = ApiAdapter.GetArticleCategoryById(ContextProvider.WebsiteContext.WebsiteId, ContextProvider.WebsiteContext.LanguageId, categoryId);
            return result.Data;

        }

        private ResultArray GetListArticles(Dictionary<string, string> dataSourceParams)
        {
            var query = new ArticleQuery
            {
                IsEnabled = true,
                IsApproved = true,
                PageSize = 10
            };
            var categoryId = (string)HttpHelper.GetUrlParam("ArticleCategoryId");
            if (!string.IsNullOrEmpty(categoryId))
                query.CategoryIds.Add(categoryId);

            var result = ApiAdapter.QueryArticles(ContextProvider.WebsiteContext.WebsiteId, ContextProvider.WebsiteContext.LanguageId, query);
            return new ResultArray()
            {
                Total = result.Total,
                Count = result.Data.Count,
                Data = result.Data.ToArray()
            };
        }
        #endregion
    }
}