﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WebCloud.Api.Core.Helper;
using WebCloud.Common;
using WebCloud.Entity;

namespace WebCloud.Site.Infrastructure.Provider
{
    public class ResourceProvider
    {
        public static readonly ResourceProvider Instance = new ResourceProvider();

        private const string ResourceCacheKey = "Resource";

        public Dictionary<string, LocalizationString> ListResources(string websiteId)
        {
            var result = (Dictionary<string, LocalizationString>)CachingProvider.Instance.GetFromCache(ResourceCacheKey, websiteId);
            if (result == null)
            {
                result = ApiAdapter.GetResources(websiteId).Data;
                CachingProvider.Instance.InsertToCache(result, ResourceCacheKey, websiteId);
            }
            return result;
        }

        public string ResourceById(string key, string websiteId, string languageId)
        {
            var resources = ListResources(websiteId);
            if (resources == null || !resources.ContainsKey(key)) return null;
            return resources[key].GetValue(languageId);
        }

    }
}