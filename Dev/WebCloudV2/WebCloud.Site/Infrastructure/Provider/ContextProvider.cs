﻿using System.Web;
using WebCloud.Entity;

namespace WebCloud.Site.Infrastructure.Provider
{
    public class ContextProvider
    {
        public static object GetItem(string key)
        {
            var obj = HttpContext.Current.Items[key];
            return obj;
        }

        public static void SetItem(string key, object obj)
        {
            HttpContext.Current.Items[key] = obj;
        }

        public static WebsiteContext WebsiteContext
        {
            get
            {
                var obj = (WebsiteContext)GetItem("WebsiteContext");
                if (obj == null)
                {
                    obj = new WebsiteContext();
                    SetItem("WebsiteContext", obj);
                }
                return obj;
            }
        }

        public static void LoadWebsiteContext(HttpContext context)
        {
            // QueryString, Form, Cookies, or ServerVariables
            ContextProvider.WebsiteContext.WebsiteId = context.Request["WebsiteId"];

            if (string.IsNullOrEmpty(ContextProvider.WebsiteContext.WebsiteId))
                ContextProvider.WebsiteContext.WebsiteId = context.Request.Headers["WebsiteId"];

            if (string.IsNullOrEmpty(ContextProvider.WebsiteContext.WebsiteId))
                ContextProvider.WebsiteContext.WebsiteId = Common.Constant.Default.WebsiteId;

            // QueryString, Form, Cookies, or ServerVariables
            ContextProvider.WebsiteContext.LanguageId = context.Request["LanguageId"];

            if (string.IsNullOrEmpty(ContextProvider.WebsiteContext.LanguageId))
                ContextProvider.WebsiteContext.LanguageId = context.Request.Headers["LanguageId"];

            if (string.IsNullOrEmpty(ContextProvider.WebsiteContext.LanguageId))
                ContextProvider.WebsiteContext.LanguageId = Common.Constant.Default.LanguageId;

        }

    }

    public class WebsiteContext
    {
        public string WebsiteId { set; get; }
        public string LanguageId { set; get; }

        private Website _currentWebsite;
        private Language _currentLanguage;

        public Website CurrentWebsite
        {
            get
            {
                if (_currentWebsite == null)
                {
                    _currentWebsite = new Website();
                }
                return _currentWebsite;
            }
        }

        public Language CurrentLanguage
        {
            get
            {
                if (_currentLanguage == null)
                {
                    _currentLanguage = new Language();
                }
                return _currentLanguage;
            }
        }
    }

}