﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Caching;
using WebCloud.Common.Wrapper;

namespace WebCloud.Site.Infrastructure.Provider
{
    public class CachingProvider
    {
        public static readonly CachingProvider Instance = new CachingProvider();

        private CachingProvider()
        {

        }

        // methods
        public void InsertToCache(object data, string key, string websiteId = null, string languageId = null)
        {
            key = BuildCacheKey(key, websiteId, languageId);
            Cache cache = HttpContext.Current.Cache;
            cache.Insert(key, data, null, DateTime.Now.AddDays(CommonConfiguration.CacheTime), Cache.NoSlidingExpiration);
        }

        public object GetFromCache(string key, string websiteId = null, string languageId = null)
        {
            key = BuildCacheKey(key, websiteId, languageId);
            Cache cache = HttpContext.Current.Cache;
            return cache[key];
        }

        public object DeleteCache(string key, string websiteId = null, string languageId = null)
        {
            key = BuildCacheKey(key, websiteId, languageId);
            Cache cache = HttpContext.Current.Cache;
            return cache.Remove(key);
        }

        /// <summary>
        /// webcloud__cachekey__(default|websiteId)[__languageId]
        /// </summary>
        /// <param name="key"></param>
        /// <param name="websiteId"></param>
        /// <param name="languageId"></param>
        /// <returns></returns>
        public string BuildCacheKey(string key, string websiteId = null, string languageId = null)
        {
            key = "webcloud__" + key;
            key += "__" + (websiteId ?? Common.Constant.Default.WebsiteId);
            if (languageId != null) key += "__" + languageId;
            return key;
        }

        /// <summary>
        /// clear all cache items of this system
        /// </summary>
        public void ClearSystemCache()
        {
            Cache cache = HttpContext.Current.Cache;
            IDictionaryEnumerator enumerator = cache.GetEnumerator();
            while (enumerator.MoveNext())
            {
                string cacheKey = enumerator.Key.ToString();
                if (IsCacheOfSystem(cacheKey))
                    cache.Remove(cacheKey);
            }
        }

        /// <summary>
        /// clear all cache items in a website
        /// </summary>
        /// <param name="websiteId"></param>
        public void ClearCacheMatchWebsite(string websiteId)
        {
            Cache cache = HttpContext.Current.Cache;
            IDictionaryEnumerator enumerator = cache.GetEnumerator();
            while (enumerator.MoveNext())
            {
                string cacheKey = enumerator.Key.ToString();
                if (IsCacheOfWebsite(cacheKey, websiteId))
                    cache.Remove(cacheKey);
            }
        }

        /// <summary>
        /// clear all cache items of a key
        /// </summary>
        /// <param name="key"></param>
        public void ClearCacheMatchKey(string key)
        {
            Cache cache = HttpContext.Current.Cache;
            IDictionaryEnumerator enumerator = cache.GetEnumerator();
            while (enumerator.MoveNext())
            {
                string cacheKey = enumerator.Key.ToString();
                if (IsCacheOfKey(cacheKey, key))
                    cache.Remove(cacheKey);
            }
        }

        /// <summary>
        /// clear all cache items of a key in special language
        /// </summary>
        /// <param name="key"></param>
        /// <param name="languageId"></param>
        public void ClearCacheMatchKeyAndLanguage(string key, string languageId)
        {
            Cache cache = HttpContext.Current.Cache;
            IDictionaryEnumerator enumerator = cache.GetEnumerator();
            while (enumerator.MoveNext())
            {
                string cacheKey = enumerator.Key.ToString();
                if (IsCacheOfKeyAndLanguage(cacheKey, key, languageId))
                    cache.Remove(cacheKey);
            }
        }

        #region private method
        private bool IsCacheOfSystem(string cacheKey)
        {
            return cacheKey != null && cacheKey.StartsWith("webcloud__");
        }

        private bool IsCacheOfWebsite(string cacheKey, string websiteId)
        {
            if (cacheKey != null && cacheKey.Contains("__"))
            {
                string[] strs = cacheKey.Split(new[] { "__" }, StringSplitOptions.None);
                return strs.Length > 2 && strs[2].Equals(websiteId);
            }
            return false;
        }

        private bool IsCacheOfKey(string cacheKey, string key)
        {
            if (cacheKey != null && cacheKey.Contains("__"))
            {
                string[] strs = cacheKey.Split(new[] { "__" }, StringSplitOptions.None);
                return strs.Length > 1 && strs[1].Equals(key);
            }
            return false;
        }

        private bool IsCacheOfKeyAndLanguage(string cacheKey, string key, string languageId)
        {
            if (cacheKey != null && cacheKey.Contains("__"))
            {
                string[] strs = cacheKey.Split(new[] { "__" }, StringSplitOptions.None);
                return strs.Length > 3 && strs[1].Equals(key) && strs[3].Equals(languageId);
            }
            return false;
        }
        #endregion
    }
}