﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WebCloud.Api.Core.Helper;
using WebCloud.Common;
using WebCloud.Entity;

namespace WebCloud.Site.Infrastructure.Provider
{
    public class ConfigurationProvider
    {
        public static readonly ConfigurationProvider Instance = new ConfigurationProvider();

        private const string ConfigCacheKey = "Config";

        public Dictionary<string, object> ListConfigs(string websiteId)
        {
            var result = (Dictionary<string, object>)CachingProvider.Instance.GetFromCache(ConfigCacheKey, websiteId);
            if (result == null)
            {
                result = ApiAdapter.GetConfigs(websiteId).Data;
                CachingProvider.Instance.InsertToCache(result, ConfigCacheKey, websiteId);
            }
            return result;
        }

        public object ConfigById(string websiteId, string key)
        {
            var configs = ListConfigs(websiteId);
            if (configs == null || !configs.ContainsKey(key)) return null;
            return configs[key];
        }
    }
}