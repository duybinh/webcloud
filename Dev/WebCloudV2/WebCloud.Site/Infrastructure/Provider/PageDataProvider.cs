﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;
using WebCloud.Api.Core.Helper;
using WebCloud.Entity;

namespace WebCloud.Site.Infrastructure.Provider
{
    public class PageDataProvider
    {
        public static readonly PageDataProvider Instance = new PageDataProvider();

        private const string ModuleBaseCacheKey = "ModuleBase";
        private const string ModuleCacheKey = "Module";
        private const string PageCacheKey = "Page";
        private const string LayoutCacheKey = "Layout";

        private PageDataProvider()
        {

        }

        #region ModuleBase
        public List<ModuleBase> ListModuleBases()
        {
            var result = (List<ModuleBase>)CachingProvider.Instance.GetFromCache(ModuleBaseCacheKey);
            if (result == null)
            {
                result = ApiAdapter.GetModuleBases().Data;
                CachingProvider.Instance.InsertToCache(result, ModuleBaseCacheKey);
            }
            return result;
        }

        public ModuleBase ModuleBaseById(string id)
        {
            var moduleBases = ListModuleBases();
            if (moduleBases == null) return null;
            return moduleBases.FirstOrDefault(o => o.ModuleBaseID == id);
        }
        #endregion

        #region Module
        public List<Module> ListModules(string websiteId)
        {
            var result = (List<Module>)CachingProvider.Instance.GetFromCache(ModuleCacheKey, websiteId);
            if (result == null)
            {
                result = ApiAdapter.GetModules(websiteId).Data;
                CachingProvider.Instance.InsertToCache(result, ModuleCacheKey, websiteId);
            }
            return result;
        }

        public List<Module> ModulesInPage(string websiteId, string pageId)
        {
            var modules = ListModules(websiteId);
            var modulesInPage = modules.Where(o => o.OnAllPage || o.PageID == pageId).OrderByDescending(o => o.SortOrder).ToList();
            return modulesInPage;
        }
        #endregion

        #region Page
        public List<Page> ListPages(string websiteId)
        {
            var result = (List<Page>)CachingProvider.Instance.GetFromCache(PageCacheKey, websiteId);
            if (result == null)
            {
                result = ApiAdapter.GetPages(websiteId).Data;
                CachingProvider.Instance.InsertToCache(result, PageCacheKey, websiteId);
            }
            return result;
        }

        public Page PageBySeoName(string websiteId, string languageId, string seoName)
        {
            var pages = ListPages(websiteId);
            if (pages == null) return null;

            var page = pages.FirstOrDefault(o => o.SeoName.Equals(seoName, StringComparison.CurrentCultureIgnoreCase));
            if (page != null)
            {
                ModulesForPage(page, websiteId);
                return page;
            }

            // find in system page
            if (websiteId != Common.Constant.Default.WebsiteId)
            {
                return PageBySeoName(Common.Constant.Default.WebsiteId, languageId, seoName);
            }

            return null;
        }

        public Page PageById(string websiteId, string languageId, string id)
        {
            var pages = ListPages(websiteId);
            if (pages == null) return null;

            var page = pages.FirstOrDefault(o => o.PageID == id);
            if (page != null)
            {
                ModulesForPage(page, websiteId);
                return page;
            }

            // find in system page
            if (websiteId != Common.Constant.Default.WebsiteId)
            {
                return PageById(Common.Constant.Default.WebsiteId, languageId, id);
            }

            return null;
        }

        private void ModulesForPage(Page page, string websiteId)
        {
            page.Modules = ModulesInPage(websiteId, page.PageID);
            if (page.Modules.Count > 0)
            {
                // get module base
                var moduleBases = ListModuleBases();
                foreach (var module in page.Modules)
                {
                    module.ModuleBase = moduleBases.FirstOrDefault(o => o.ModuleBaseID == module.ModuleBaseID);
                }
            }
        }
        #endregion

        #region Layout
        public List<Layout> ListLayouts(string websiteId)
        {
            var result = (List<Layout>)CachingProvider.Instance.GetFromCache(LayoutCacheKey, websiteId);
            if (result == null)
            {
                result = ApiAdapter.GetLayouts(websiteId).Data;
                CachingProvider.Instance.InsertToCache(result, LayoutCacheKey, websiteId);
            }
            return result;
        }

        public Layout LayoutById(string websiteId, string id)
        {
            var layouts = ListLayouts(websiteId);
            var layout = layouts.FirstOrDefault(o => o.LayoutID == id);
            if (layout == null)
            {
                var baseLayouts = ListLayouts(Common.Constant.Default.WebsiteId);
                layout = baseLayouts.SingleOrDefault(o => o.LayoutID == id);

                if (layout == null)
                {
                    // return default layout
                    return new Layout();
                }
            }

            return layout;
        }
        #endregion

    }
}