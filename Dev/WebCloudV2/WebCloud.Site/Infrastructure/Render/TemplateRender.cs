﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using DotLiquid;
using DotLiquid.NamingConventions;
using WebCloud.Common;
using WebCloud.Entity;
using WebCloud.Site.Infrastructure.Helper;

namespace WebCloud.Site.Infrastructure.Render
{
    public class TemplateRender
    {
        public static void InitRender()
        {
            Liquid.UseRubyDateFormat = true;
            Template.RegisterTag<ModuleTag>("module");
            Template.RegisterTag<ThemeTag>("theme");
            Template.RegisterTag<FileHandlerTag>("resource");
            Template.RegisterTag<FileTag>("file");
            Template.RegisterTag<BlockTag>("snippet");
            Template.RegisterSafeType(typeof(ResultArray), new[] { "Total", "Data" });
            #region Register Article
            Template.RegisterSafeType(typeof(Article), new[] {
                "Content", "ThumbImage", "ArticleID", "GlobalID", "SortOrder", "IsHot", "AllowComment",
                "DisplayDatetime", "DiscussionGroup", "Statistic", "CreatedUserName", "TagValues"});
            Template.RegisterSafeType(typeof(ArticleContent), new[] { "Title", "Description", "Content", "SeoTitle", "MetaTitle", "MetaKeyword", "MetaDescription" });
            Template.RegisterSafeType(typeof(DiscussionGroup), new[] { "DiscussionCount", "IsClosed" });
            Template.RegisterSafeType(typeof(Statistic), new[] { "ViewCount", "LikeCount", "DislikeCount" });

            Template.RegisterSafeType(typeof(ArticleCategory), new[] {
                "GlobalID", "ParentID", "ThumbImage", "DisplayArticleThumb", "IsHot", "AllowComment", "SortOrder",
                "IsLeaf", "Content", "ArticleCount", "CreatedUserName"});
            Template.RegisterSafeType(typeof(ArticleCategoryContent), new[] { "Name", "SeoName", "Description", "MetaTitle", "MetaKeyword", "MetaDescription" });

            #endregion

            Template.NamingConvention = new CSharpNamingConvention();
        }

        public static string RenderContent(string content, Dictionary<string, dynamic> bindings = null)
        {
            if (bindings == null)
            {
                bindings = new Dictionary<string, dynamic>();
            }
            bindings["config"] = new ConfigRender();
            bindings["text"] = new ResourceRender();
            bindings["param"] = new ParamsRender();
            bindings["setting"] = new
            {
                ResourceUrl = ApplicationSettings.ResourceUrl,
                ResourceHandlerUrl = ApplicationSettings.ResourceHandlerUrl,
            };

            Template template = Template.Parse(content);
            string str = template.Render(Hash.FromDictionary(bindings));
            return str;
        }

    }
}