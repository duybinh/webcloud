﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using DotLiquid;
using WebCloud.Site.Infrastructure.Helper;
using WebCloud.Site.Infrastructure.Provider;

namespace WebCloud.Site.Infrastructure.Render
{
    public class ThemeTag : DotLiquid.Tag
    {
        private string _path;
        public override void Initialize(string tagName, string markup, List<string> tokens)
        {
            base.Initialize(tagName, markup, tokens);
            _path = markup;
        }

        public override void Render(Context context, TextWriter result)
        {
            string currentTheme = (string)ConfigurationProvider.Instance.ConfigById(ContextProvider.WebsiteContext.WebsiteId, "theme");
            result.Write(ApplicationSettings.ResourceUrl + "themes/" + currentTheme + "/" + _path);
        }
    }

    public class FileTag : DotLiquid.Tag
    {
        private string _path;
        public override void Initialize(string tagName, string markup, List<string> tokens)
        {
            base.Initialize(tagName, markup, tokens);
            _path = markup;
        }

        public override void Render(Context context, TextWriter result)
        {
            result.Write(ApplicationSettings.ResourceUrl + _path);
        }
    }



    public class FileHandlerTag : DotLiquid.Tag
    {
        private string _path;
        public override void Initialize(string tagName, string markup, List<string> tokens)
        {
            base.Initialize(tagName, markup, tokens);
            _path = markup;
        }

        public override void Render(Context context, TextWriter result)
        {
            result.Write(ApplicationSettings.ResourceHandlerUrl +
                        "?WebsiteId=" + ContextProvider.WebsiteContext.WebsiteId +
                        "&LanguageId=" + ContextProvider.WebsiteContext.LanguageId +
                        "&" + _path);
        }
    }
}