﻿using DotLiquid;
using WebCloud.Site.Infrastructure.Helper;

namespace WebCloud.Site.Infrastructure.Render
{
    public class ParamsRender : Drop
    {
        public override object BeforeMethod(string method)
        {
            object param = HttpHelper.GetUrlParam(method);
            if (param != null) return param;
            return "";
        }
    }
}