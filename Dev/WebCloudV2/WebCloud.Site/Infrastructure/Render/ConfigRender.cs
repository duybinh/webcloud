﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using DotLiquid;
using WebCloud.Site.Infrastructure.Base;
using WebCloud.Site.Infrastructure.Provider;

namespace WebCloud.Site.Infrastructure.Render
{
    public class ConfigRender : Drop
    {
        public override object BeforeMethod(string method)
        {
            var value = ConfigurationProvider.Instance.ConfigById(ContextProvider.WebsiteContext.WebsiteId, method);
            return value;
        }
    }
}