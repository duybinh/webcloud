﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using DotLiquid;
using WebCloud.Site.Infrastructure.Base;
using WebCloud.Site.Infrastructure.Provider;

namespace WebCloud.Site.Infrastructure.Render
{
    public class ResourceRender : Drop
    {
        public override object BeforeMethod(string method)
        {
            var value = ResourceProvider.Instance.ResourceById(method, ContextProvider.WebsiteContext.WebsiteId, ContextProvider.WebsiteContext.LanguageId);
            return value;
        }
    }
}