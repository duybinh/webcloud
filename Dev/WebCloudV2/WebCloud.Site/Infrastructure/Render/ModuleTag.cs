﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using DotLiquid;
using Newtonsoft.Json;
using WebCloud.Entity;
using WebCloud.Site.Infrastructure.Provider;

namespace WebCloud.Site.Infrastructure.Render
{
    public class ModuleTag : DotLiquid.Tag
    {
        private ModuleBase _module;
        private Dictionary<string, string> _configs;
        public override void Initialize(string tagName, string markup, List<string> tokens)
        {
            base.Initialize(tagName, markup, tokens);

            string moduleId;
            if (markup.Contains("{"))
            {
                moduleId = markup.Substring(0, markup.IndexOf("{"));
                string moduleConfigValue = markup.Substring(markup.IndexOf("{"));
                _configs = JsonConvert.DeserializeObject<Dictionary<string, string>>(moduleConfigValue);
            }
            else
            {
                moduleId = markup.Trim();
            }

            _module = PageDataProvider.Instance.ModuleBaseById(moduleId);
        }

        public override void Render(Context context, TextWriter result)
        {
            if (_module != null)
            {
                result.Write(RenderModuleContent(_module,_configs));
            }
            else
            {
                result.Write("{unknown module}");
            }
        }

        public static string RenderModuleContent(ModuleBase moduleBase, Dictionary<string, string> configs)
        {
            Layout layout = PageDataProvider.Instance.LayoutById(ContextProvider.WebsiteContext.WebsiteId, moduleBase.LayoutID);
            if (layout == null) return "{module empty layout}";

            var configValues = new Dictionary<string, object>();
            foreach (var moduleBaseConfig in moduleBase.ModuleBaseConfigs)
            {
                configValues[moduleBaseConfig.ModuleBaseConfigID] = moduleBaseConfig.DefaultValue;
            }
            if (configs != null)
            {
                foreach (var config in configs)
                {
                    configValues[config.Key] = config.Value;
                }
            }

            Template template = Template.Parse(layout.Content);
            string str = template.Render(Hash.FromDictionary(configValues));
            return str;
        }

    }
}