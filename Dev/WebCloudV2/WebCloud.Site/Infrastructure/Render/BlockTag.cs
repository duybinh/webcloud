﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using DotLiquid;
using Newtonsoft.Json;
using WebCloud.Entity;
using WebCloud.Site.Infrastructure.Provider;

namespace WebCloud.Site.Infrastructure.Render
{
    public class BlockTag : DotLiquid.Tag
    {
        private Layout _block;
        public override void Initialize(string tagName, string markup, List<string> tokens)
        {
            base.Initialize(tagName, markup, tokens);

            _block = PageDataProvider.Instance.LayoutById(ContextProvider.WebsiteContext.WebsiteId, markup.Trim());
        }

        public override void Render(Context context, TextWriter result)
        {
            if (_block != null)
            {
                result.Write(_block.Content);
            }
        }
    }
}