﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using Newtonsoft.Json;
using WebCloud.Entity;
using WebCloud.Site.Infrastructure.Provider;
using WebCloud.Site.Infrastructure.Render;

namespace WebCloud.Site.Infrastructure.Base
{
    public class BaseModule
    {
        public BasePage CurrentPage { private set; get; }
        public Module CurrentModule { private set; get; }
        public Layout CurrentLayout { private set; get; }
        protected Dictionary<string, dynamic> Bindings;
        public BaseModule(BasePage currentPage, Module currentModule)
        {
            CurrentPage = currentPage;
            CurrentModule = currentModule;
            CurrentLayout = PageDataProvider.Instance.LayoutById(ContextProvider.WebsiteContext.WebsiteId, CurrentModule.ModuleBase.LayoutID);
        }

        public virtual string Render()
        {
            if (CurrentLayout == null)
                return "";

            Bindings = CurrentModule.ModuleConfigs.ToDictionary(o => o.ModuleBaseConfigID, o => (dynamic)o.Value);
            Bindings["CurrentModule"] = new
            {
                Id = CurrentModule.ModuleID
            };
            string content = HandleRequiredData(CurrentLayout.Content);
            content = TemplateRender.RenderContent(content, Bindings);
            content = TemplateRender.RenderContent(content, CurrentPage.DataContext);
            return content;
        }

        private string HandleRequiredData(string content)
        {
            var r = new Regex(@"\%\%\s*data\s+(?<data>.+)\%\%");
            var matches = r.Matches(content);
            foreach (Match match in matches)
            {
                string dataToken = match.Groups["data"].Value.Trim();
                string[] subs = dataToken.Split(new[] { ' ' }, 2, StringSplitOptions.RemoveEmptyEntries);
                string dataKey = "", dataSource = "";
                Dictionary<string, string> dataSourceParams = null;
                if (subs.Length == 1)
                {
                    GetKeyAndParams(subs[0], out dataSource, out dataSourceParams);
                    dataKey = dataSource;
                }
                else if (subs.Length == 2)
                {
                    dataKey = subs[0].Trim();
                    GetKeyAndParams(subs[1], out dataSource, out dataSourceParams);
                }
                if (PrepareData(dataKey, dataSource, dataSourceParams))
                {
                    content = content.Replace(match.Value, "");
                }
            }

            return content;
        }

        private bool PrepareData(string dataKey, string dataSource, Dictionary<string, string> dataSourceParams)
        {
            if (string.IsNullOrEmpty(dataKey) || string.IsNullOrEmpty(dataSource) || dataSourceParams == null)
                return false;

            if (CurrentPage.DataContext.ContainsKey(dataKey))
            {
                Bindings[dataKey] = CurrentPage.DataContext[dataKey];
                return true;
            }

            var data = ContentProvider.Instance.GetData(dataSource, dataSourceParams);
            if (data.IsSuccess)
            {
                CurrentPage.DataContext.Add(dataKey, data.Data);
                Bindings[dataKey] = data.Data;
                return true;
            }

            return false;
        }

        private void GetKeyAndParams(string markup, out string key, out Dictionary<string, string> parameters)
        {
            if (markup.Contains("{"))
            {
                key = markup.Substring(0, markup.IndexOf("{")).Trim();
                string moduleConfigValue = markup.Substring(markup.IndexOf("{")).Trim();
                parameters = JsonConvert.DeserializeObject<Dictionary<string, string>>(moduleConfigValue);
            }
            else
            {
                key = markup.Trim();
                parameters = new Dictionary<string, string>();
            }
        }
    }
}