﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using WebCloud.Entity;
using WebCloud.Site;
using WebCloud.Site.Infrastructure.Modules;
using WebCloud.Site.Infrastructure.Provider;
using WebCloud.Site.Infrastructure.Render;

namespace WebCloud.Site.Infrastructure.Base
{
    public class BasePage : System.Web.UI.Page
    {
        public Dictionary<string, Object> DataContext { private set; get; }
        public Page CurrentPage { private set; get; }

        public BasePage()
        {
            DataContext = new Dictionary<string, object>();
        }

        protected void Page_PreInit(object sender, EventArgs e)
        {
            LoadPageContent();
        }

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);
            RenderPage();
        }

        protected virtual void LoadPageContent()
        {
            string pageId = Request["PageId"];
            if (string.IsNullOrEmpty(pageId))
                pageId = (string)Page.RouteData.Values["PageId"];

            string pageName = Request["PageName"];
            if (string.IsNullOrEmpty(pageName))
                pageName = (string)Page.RouteData.Values["PageName"];

            if (string.IsNullOrEmpty(pageId) && string.IsNullOrEmpty(pageName))
                pageId = "default";

            if (!string.IsNullOrEmpty(pageId))
            {
                CurrentPage = PageDataProvider.Instance.PageById(ContextProvider.WebsiteContext.WebsiteId, ContextProvider.WebsiteContext.LanguageId, pageId);
            }

            if (CurrentPage == null && !string.IsNullOrEmpty(pageName))
            {
                CurrentPage = PageDataProvider.Instance.PageBySeoName(ContextProvider.WebsiteContext.WebsiteId, ContextProvider.WebsiteContext.LanguageId, pageName);
            }

            if (CurrentPage == null)
            {
                // TODO: handle 404 page not found
                throw new Exception();
            }
        }

        protected virtual void RenderPage()
        {
            Layout pageLayout = PageDataProvider.Instance.LayoutById(ContextProvider.WebsiteContext.WebsiteId, CurrentPage.LayoutID);
            string content = TemplateRender.RenderContent(pageLayout.Content);

            // render section content
            // find sections
            var sectionContent = new Dictionary<string, string>();
            Regex r = new Regex(@"\%\%\s*section\s+(?<section>\w+)\s*\%\%");
            var matches = r.Matches(content);
            foreach (Match match in matches)
            {
                sectionContent[match.Groups["section"].Value] = "";
            }

            // render modules in section
            foreach (var module in CurrentPage.Modules)
            {
                if (!string.IsNullOrEmpty(module.Section) && sectionContent.ContainsKey(module.Section))
                {
                    var m = ModuleFactory.NewModule(this, module);
                    sectionContent[module.Section] += m.Render() + "\n";
                }
            }

            // replace section by section's content
            foreach (Match match in matches)
            {
                content = content.Replace(match.Value, sectionContent[match.Groups["section"].Value]);
            }

            content = TemplateRender.RenderContent(content);
            Response.Write(content);
        }
    }

}