﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WebCloud.Entity;
using WebCloud.Site.Infrastructure.Base;

namespace WebCloud.Site.Infrastructure.Modules
{
    public class ModuleFactory
    {
        public static BaseModule NewModule(BasePage currentPage, Module currentModule)
        {
            switch (currentModule.ModuleBaseID)
            {
                case "ProductList":
                case "ProductDetail":
                case "ProductScroller":
                default:
                    return new BaseModule(currentPage, currentModule);
            }
        }
    }
}