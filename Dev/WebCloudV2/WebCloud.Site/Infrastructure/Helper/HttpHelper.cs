﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Routing;

namespace WebCloud.Site.Infrastructure.Helper
{
    public static class HttpHelper
    {
        public static object GetRouteParam(string name)
        {
            var routeData = RouteTable.Routes.GetRouteData(new HttpContextWrapper(HttpContext.Current));
            if (routeData != null)
                return routeData.Values[name];

            return null;
        }

        public static object GetUrlParam(string name)
        {
            if (HttpContext.Current == null) return null;
            string categoryId = HttpContext.Current.Request[name];
            if (!string.IsNullOrEmpty(categoryId))
                return categoryId;

            return GetRouteParam(name);
        }
    }
}