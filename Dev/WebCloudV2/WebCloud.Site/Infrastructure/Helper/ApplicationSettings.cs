﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;

namespace WebCloud.Site.Infrastructure.Helper
{
    public class ApplicationSettings
    {
        private static readonly ApplicationSettings Instance = new ApplicationSettings();
        private readonly string _resourceUrl;
        private readonly string _resourceHandlerUrl;

        private ApplicationSettings()
        {
            _resourceUrl = ConfigurationManager.AppSettings["ResourceUrl"];
            _resourceHandlerUrl = ConfigurationManager.AppSettings["ResourceHandlerUrl"];
        }
        public static string ResourceUrl
        {
            get
            {
                return Instance._resourceUrl;
            }
        }

        public static string ResourceHandlerUrl
        {
            get
            {
                return Instance._resourceHandlerUrl;
            }
        }


    }
}