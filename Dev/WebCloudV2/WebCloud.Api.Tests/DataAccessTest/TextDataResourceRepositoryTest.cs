﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using WebCloud.Api.DataAccess;
using WebCloud.Entity;

namespace WebCloud.Api.Tests.DataAccessTest
{
    [TestClass]
    public class TextDataResourceRepositoryTest
    {
        //ITextResourceDataRepository _textResourceDataRepository = new TextResourceDataRepository();

        //[TestMethod]
        //public void GetTextsInWebsite()
        //{
        //    var result1 = _textResourceDataRepository.GetTextsDefault();
        //    var result2 = _textResourceDataRepository.GetTextsInLanguage("vi-VN");
        //    var result3 = _textResourceDataRepository.GetTextsInWebsite("default", "vi-VN");
        //    Assert.AreEqual(result1.Count, result2.Count);
        //    Assert.AreEqual(result1.Count, result3.Count);
        //}

        IPageDataRepository _pageDataRepository = new PageDataRepository();

        [TestMethod]
        public void TestPageContent()
        {
            Page page = _pageDataRepository.GetById("default", "product", "vi");
            Assert.IsNotNull(page.NameString);
            Assert.AreEqual(page.NameString.ValueInLanguages.Count,1);
        }

        [TestMethod]
        public void TestAddPage()
        {
            Page page = new Page()
            {
                WebsiteID = "default",
                PageID = "product",
                LayoutID = "product.layout",
                Name = "Sản phẩm", 
                SeoName = "san-pham",
                MetaTitle = "San pham",
                MetaKeyword = "Danh sach san pham",
                MetaDescription = "Trang san pham"
            };
           Assert.IsTrue(_pageDataRepository.Add(page));

           page = new Page()
           {
               WebsiteID = "default",
               PageID = "product",
               LayoutID = "product.layout",
               Name = "Product Page",
               SeoName = "product",
               MetaTitle = "Product page title",
               MetaKeyword = "Product page keyword",
               MetaDescription = "Product page description"
           };
           Assert.IsTrue(_pageDataRepository.Edit(page, "en"));
        }
    }
}
