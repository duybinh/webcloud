﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using WebCloud.Api.DataAccess;
using WebCloud.Entity;

namespace WebCloud.Api.Tests.DataAccessTest
{
    [TestClass]
    public class MenuDataRepositoryTest
    {
        private readonly IMenuDataRepository _menuDataRepository = new MenuDataRepository();

        [TestMethod]
        public void ValidateFolderName()
        {
            Regex r = new Regex("["+ Regex.Escape(new string(System.IO.Path.GetInvalidPathChars())) + "]");

            Assert.AreEqual(r.IsMatch("sddssdfd"), false);
            Assert.AreEqual(r.IsMatch("sdd/"), true);
        }

        [TestMethod]
        public void GetMenuItems()
        {
            var result = _menuDataRepository.GetMenuItems("default", "en-US", "vi-VN", false);
            Assert.AreEqual(result.Count, 3);
            Assert.AreEqual(result[0].IsDeleted, false);
            Assert.AreEqual(result[0].Content.LanguageID, "en-US");
            Assert.AreEqual(result[1].Content.LanguageID, "vi-VN");
            Assert.AreEqual(result[2].Content.LanguageID, "vi-VN");

            result = _menuDataRepository.GetMenuItems("default", "en-US", null, false);
            Assert.AreEqual(result.Count, 1);
            Assert.AreEqual(result[0].Content.LanguageID, "en-US");

            result = _menuDataRepository.GetMenuItems("default", "en-US", "en-US", false);
            Assert.AreEqual(result.Count, 1);
            Assert.AreEqual(result[0].Content.LanguageID, "en-US");
        }

        [TestMethod]
        public void UpdateField()
        {
            var menuItem = new MenuItem
                               {
                                   WebsiteID = "default",
                                   MenuItemID = "1",
                                   SortOrder = 5,
                                   SpecialClass = "class"
                               };

            var result = _menuDataRepository.UpdateField(menuItem, "SortOrder", "SpecialClass");
            Assert.AreEqual(result, true);
            var updatedItem = _menuDataRepository.GetById("default", "1", "vi-VN");
            Assert.AreNotEqual(updatedItem, null);
            Assert.AreEqual(updatedItem.SortOrder, 5);
            Assert.AreEqual(updatedItem.SpecialClass, "class");
        }

        [TestMethod]
        public void UpdateFieldForMultyItem()
        {
            var menuItem1 = new MenuItem
            {
                WebsiteID = "default",
                MenuItemID = "1",
                SortOrder = 1,
                SpecialClass = "class1"
            };

            var menuItem2 = new MenuItem
            {
                WebsiteID = "default",
                MenuItemID = "2",
                SortOrder = 2,
                SpecialClass = "class2"
            };

            var result = _menuDataRepository.UpdateField(new[] { menuItem1, menuItem2 }, "SortOrder", "SpecialClass");

            Assert.AreEqual(result, true);

            var updatedItem1 = _menuDataRepository.GetById("default", "1", "vi-VN");
            Assert.AreNotEqual(updatedItem1, null);
            Assert.AreEqual(updatedItem1.SortOrder, 1);
            Assert.AreEqual(updatedItem1.SpecialClass, "class1");

            var updatedItem2 = _menuDataRepository.GetById("default", "2", "vi-VN");
            Assert.AreNotEqual(updatedItem2, null);
            Assert.AreEqual(updatedItem2.SortOrder, 2);
            Assert.AreEqual(updatedItem2.SpecialClass, "class2");
        }
    
        [TestMethod]
        public void GenerateID()
        {
            Console.WriteLine(DateTime.Now.Ticks);
            var id = Guid.NewGuid();
            Console.WriteLine(id.ToString("N"));
            Console.WriteLine(id.ToString().Length);
            Console.WriteLine(id.GetHashCode());
            Console.WriteLine(DateTime.Now.ToFileTime());
            Console.WriteLine(DateTime.Now.Ticks);
            Console.WriteLine(DateTime.Now.Ticks.ToString().Length);
        }
    }
}
