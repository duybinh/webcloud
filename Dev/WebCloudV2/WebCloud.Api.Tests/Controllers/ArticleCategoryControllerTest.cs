﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using WebCloud.Api.Core.Helper;
using WebCloud.Common;
using WebCloud.Common.Auth;
using WebCloud.Entity;

namespace WebCloud.Api.Tests.Controllers
{
    [TestClass]
    public class ArticleCategoryControllerTest
    {
        private string _token = "fVOKOZdRMSBMBYcetyX+2sqsfMqD7yjHENFVjAyyILLgkUrVAdEeUEjd8QJastWK6jLIiA2wpRtu9MrTIa4an6Iq8i27HK2y7+xoaRQAd2kMEPhmNGJLQay/7o8uiRERwgxSIAvpVVA2A8fXLvomZk+txSjqpbdu/rWyXuNWElselPdy+GvPjnd6slastfTOqXpiyqkpgdOF2iTcp88Ra9d7G2qdOnq9y8LyLti4ibBfD417sF7TP9klNfoVV9w/Uydf0lz5XvsyvMxtRy01eWAKk1/iKDUAAvFN3CI/NSEgNu7NOyOq3PmtLWQ2EQDspuK/BzS8izGdwZDRrEXCf48KedyumYi1DhIelnr5GTpp6k51wtdTFMXvXLffYHNo";

        [TestMethod]
        public void GetAll()
        {
            HttpResponseMessage response = ApiHelper.Get(ApiHelper.ArticleCategoryUrl, "default", "vi", _token);
            response.EnsureSuccessStatusCode();
            var result = response.Content.ReadAsAsync<ResultObject<List<ArticleCategory>>>().Result;
            Assert.IsNotNull(result);
            Assert.AreEqual(result.IsSuccess, true);
            Assert.IsNotNull(result.Data);
            Console.WriteLine(result.Data.Count);
        }

        [TestMethod]
        public void Add()
        {
            var obj = new ArticleCategory()
                       {
                           Content = new ArticleCategoryContent()
                           {
                               Name = "Tin tức",
                               SeoName = "Tin-tuc",
                               Description = "Mô tả",
                           },
                           IsHot = true
                       };
            HttpResponseMessage response = ApiHelper.Post(ApiHelper.ArticleCategoryAddUrl, obj, "default", "vi", _token);
            response.EnsureSuccessStatusCode();
            var result = response.Content.ReadAsAsync<ResultObject<string>>().Result;
            Assert.IsNotNull(result);
            if (result.Message != null)
            {
                Console.WriteLine(result.Message.Code);
                Console.WriteLine(result.Message.Title);
                Console.WriteLine(result.Message.Detail);
            }
            Assert.AreEqual(result.IsSuccess, true);
            Assert.IsNotNull(result.Data);
            Console.WriteLine(result.Data);
        }

        [TestMethod]
        public void GetById()
        {
            HttpResponseMessage response = ApiHelper.Get(ApiHelper.ArticleCategoryGetByIdUrl.Replace("{id}", "XoTKTN5eP"), "default", "vi", _token);
            response.EnsureSuccessStatusCode();
            var result = response.Content.ReadAsAsync<ResultObject<ArticleCategory>>().Result;
            Assert.IsNotNull(result);
            Assert.AreEqual(result.IsSuccess, true);
            Assert.IsNotNull(result.Data);
            Assert.AreEqual(result.Data.ArticleCategoryID, "XoTKTN5eP");
            Assert.AreEqual(result.Data.Content.Name, "Bản tin");
            Assert.AreEqual(result.Data.Content.Description, "Mô tả 1");
            Assert.AreEqual(result.Data.SortOrder, 1);
            Assert.AreEqual(result.Data.IsEnabled, false);
        }

        [TestMethod]
        public void Edit()
        {
            var obj = new ArticleCategory()
            {
                ArticleCategoryID = "XoTKTN5eP",
                Content = new ArticleCategoryContent()
                {
                    Name = "Bản tin",
                    SeoName = "ban-tin",
                    Description = "Mô tả 1",
                    MetaTitle = "Bản tin",
                    MetaDescription = "Bản tin",
                    MetaKeyword = "Bản tin",
                },
                IsEnabled = false,
                SortOrder = 1
            };
            HttpResponseMessage response = ApiHelper.Post(ApiHelper.ArticleCategoryEditUrl, obj, "default", "vi", _token);
            response.EnsureSuccessStatusCode();
            var result1 = response.Content.ReadAsAsync<ResultBase>().Result;
            Assert.IsNotNull(result1);
            Assert.AreEqual(result1.IsSuccess, true);

            response = ApiHelper.Get(ApiHelper.ArticleCategoryGetByIdUrl.Replace("{id}", "XoTKTN5eP"), "default", "vi", _token);
            response.EnsureSuccessStatusCode();
            var result = response.Content.ReadAsAsync<ResultObject<ArticleCategory>>().Result;
            Assert.IsNotNull(result);
            Assert.AreEqual(result.IsSuccess, true);
            Assert.IsNotNull(result.Data);
            Assert.AreEqual(result.Data.ArticleCategoryID, "XoTKTN5eP");
            Assert.AreEqual(result.Data.Content.Name, "Bản tin");
            Assert.AreEqual(result.Data.Content.Description, "Mô tả 1");
            Assert.AreEqual(result.Data.SortOrder, 1);
            Assert.AreEqual(result.Data.IsEnabled, false);
        }

        [TestMethod]
        public void Delete()
        {
            //HttpResponseMessage response = ApiHelper.Get(ApiHelper.RoleDeleteUrl.Replace("{id}", "TestID"), "default", "vi-VN", _token);
            //response.EnsureSuccessStatusCode();
            //var result = response.Content.ReadAsAsync<ResultBase>().Result;
            //Assert.IsNotNull(result);
            //Assert.AreEqual(result.IsSuccess, true);
        }
    }
}
