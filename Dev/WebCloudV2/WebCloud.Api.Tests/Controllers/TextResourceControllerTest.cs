﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using WebCloud.Api.Core.Helper;
using WebCloud.Api.Core.Models;
using WebCloud.Common;
using WebCloud.Entity;

namespace WebCloud.Api.Tests.Controllers
{
    [TestClass]
    public class TextResourceControllerTest
    {
        private string _token =
"fVOKOZdRMSBMBYcetyX+2sqsfMqD7yjHENFVjAyyILLgkUrVAdEeUEjd8QJastWK6jLIiA2wpRtu9MrTIa4an386ESNImfAHYxE+Q/V3swj2cg0SVfmnat/HsEF6pAX7avYaoi5MOTWTOMXwIx5gj3EAeJVjYJKCumIMzeuBjqpSyRkhqFAaXhe77JGruT2BWCQv068vO7xCmEgSvKT7DYr1Cqxz86mkAPgYJUFJLcUmAD2PpwOT/TWKa+dE7SsQEAH4WmsHusnGuE4mVYYbnn/nX2s2pidvnxSt3yZDmtt20wzAuAcI7+T7ZZv+CLrFaaF2ETIJqvvxQj8TCHz02zez8NWZff3jP57EMVD62fQ+xaZzH2wgjRwR+ODFzfN9";

        [TestMethod]
        public void GetTextInLangs()
        {
            var model = new GetTextInLangsModel()
            {
                RefreshCache = false,
                Languages = new[] { "vi-VN", "en-US" }
            };

            HttpResponseMessage response = ApiHelper.Post(ApiHelper.TextsInLanguagesUrl, model, "default", "vi-VN", _token);
            response.EnsureSuccessStatusCode();
            var result = response.Content.ReadAsAsync<ResultObject<DataTable>>().Result;
            Assert.IsNotNull(result);
            Assert.AreEqual(result.IsSuccess, true);
        }
    }
}
