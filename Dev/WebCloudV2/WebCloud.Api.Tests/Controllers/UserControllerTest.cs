﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using WebCloud.Api.Core.Helper;
using WebCloud.Api.Core.Models;
using WebCloud.Common;
using WebCloud.Common.Auth;

namespace WebCloud.Api.Tests.Controllers
{
    [TestClass]
    public class UserControllerTest
    {
        private string _token =
            "fVOKOZdRMSBMBYcetyX+2sqsfMqD7yjHENFVjAyyILLgkUrVAdEeUEjd8QJastWK6jLIiA2wpRtu9MrTIa4an386ESNImfAHYxE+Q/V3swj2cg0SVfmnat/HsEF6pAX7avYaoi5MOTWTOMXwIx5gj3EAeJVjYJKCumIMzeuBjqpSyRkhqFAaXhe77JGruT2BK2mm6jxDoaTkd1dwc2vsNOjY4Dq8g0EFGY+98mi98BoR382MMr3rt/qDBVVWV4Z6tRw5A4MYam02Ti7iHLRd7I1k8VJ0CKSWaOUEQulAmCQabUjXBX65pCnI/SH7XGG88M54IPcF7jyT1FeTkJnmO6OfU3wbvn1BL4MsCSVAdPlCjcsT0jXoogUeZYlJeWjYfS1ncfaQfwS0sGBDSi1UMw==";

        [TestMethod]
        public void GetMyProfile()
        {
            HttpResponseMessage response = ApiHelper.Get(ApiHelper.UserGetMyProfileUrl, "default", "vi-VN", _token);
            response.EnsureSuccessStatusCode();
            var result = response.Content.ReadAsAsync<ResultObject<WebsitePrincipalModel>>().Result;
            Assert.IsNotNull(result);
            Assert.AreEqual(result.IsSuccess, true);
            Assert.IsNotNull(result.Data);
            Assert.AreEqual(result.Data.UserID, "admin");
            Console.WriteLine(result.Data);
        }
    }
}
