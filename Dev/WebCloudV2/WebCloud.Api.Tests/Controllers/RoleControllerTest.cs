﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using WebCloud.Api.Core.Helper;
using WebCloud.Common;
using WebCloud.Common.Auth;
using WebCloud.Entity;

namespace WebCloud.Api.Tests.Controllers
{
    [TestClass]
    public class RoleControllerTest
    {
        private string _token =
    "fVOKOZdRMSBMBYcetyX+2sqsfMqD7yjHENFVjAyyILLgkUrVAdEeUEjd8QJastWK6jLIiA2wpRtu9MrTIa4an386ESNImfAHYxE+Q/V3swj2cg0SVfmnat/HsEF6pAX7avYaoi5MOTWTOMXwIx5gj3EAeJVjYJKCumIMzeuBjqpSyRkhqFAaXhe77JGruT2BWCQv068vO7xCmEgSvKT7DYr1Cqxz86mkAPgYJUFJLcUmAD2PpwOT/TWKa+dE7SsQEAH4WmsHusnGuE4mVYYbnn/nX2s2pidvnxSt3yZDmtt20wzAuAcI7+T7ZZv+CLrFaaF2ETIJqvvxQj8TCHz02zez8NWZff3jP57EMVD62fQ+xaZzH2wgjRwR+ODFzfN9";

        [TestMethod]
        public void GetAll()
        {
            HttpResponseMessage response = ApiHelper.Get(ApiHelper.RoleGetAllUrl, "default", "vi-VN", _token);
            response.EnsureSuccessStatusCode();
            var result = response.Content.ReadAsAsync<ResultObject<List<UserRole>>>().Result;
            Assert.IsNotNull(result);
            Assert.AreEqual(result.IsSuccess, true);
            Assert.IsNotNull(result.Data);
            Console.WriteLine(result.Data.Count);
        }

        [TestMethod]
        public void Add()
        {
            var role = new UserRole()
                       {
                           UserRoleID = "TestID",
                           Name = "Test name",
                           Description = "Test description",
                           IsEnabled = true,
                           SortOrder = 0
                       };
            HttpResponseMessage response = ApiHelper.Post(ApiHelper.RoleAddUrl, role, "default", "vi-VN", _token);
            response.EnsureSuccessStatusCode();
            var result = response.Content.ReadAsAsync<ResultBase>().Result;
            Assert.IsNotNull(result);
            Assert.AreEqual(result.IsSuccess, true);
        }

        [TestMethod]
        public void GetById()
        {
            HttpResponseMessage response = ApiHelper.Get(ApiHelper.RoleGetByIdUrl.Replace("{id}", "TestID"), "default", "vi-VN", _token);
            response.EnsureSuccessStatusCode();
            var result = response.Content.ReadAsAsync<ResultObject<UserRole>>().Result;
            Assert.IsNotNull(result);
            Assert.AreEqual(result.IsSuccess, true);
            Assert.IsNotNull(result.Data);
            Assert.AreEqual(result.Data.UserRoleID, "TestID");
            Assert.AreEqual(result.Data.Name, "Test name");
            Assert.AreEqual(result.Data.Description, "Test description");
            Assert.AreEqual(result.Data.SortOrder, 0);
            Assert.AreEqual(result.Data.IsEnabled, true);
        }

        [TestMethod]
        public void Edit()
        {
            var role = new UserRole()
            {
                UserRoleID = "TestID",
                Name = "Test name 1",
                Description = "Test description 1",
                IsEnabled = false,
                SortOrder = 1
            };
            HttpResponseMessage response = ApiHelper.Post(ApiHelper.RoleEditUrl, role, "default", "vi-VN", _token);
            response.EnsureSuccessStatusCode();
            var result1 = response.Content.ReadAsAsync<ResultBase>().Result;
            Assert.IsNotNull(result1);
            Assert.AreEqual(result1.IsSuccess, true);

            response = ApiHelper.Get(ApiHelper.RoleGetByIdUrl.Replace("{id}", "TestID"), "default", "vi-VN", _token);
            response.EnsureSuccessStatusCode();
            var result = response.Content.ReadAsAsync<ResultObject<UserRole>>().Result;
            Assert.IsNotNull(result);
            Assert.AreEqual(result.IsSuccess, true);
            Assert.IsNotNull(result.Data);
            Assert.AreEqual(result.Data.UserRoleID, "TestID");
            Assert.AreEqual(result.Data.Name, "Test name 1");
            Assert.AreEqual(result.Data.Description, "Test description 1");
            Assert.AreEqual(result.Data.SortOrder, 1);
            Assert.AreEqual(result.Data.IsEnabled, false);
        }

        [TestMethod]
        public void Delete()
        {
            HttpResponseMessage response = ApiHelper.Get(ApiHelper.RoleDeleteUrl.Replace("{id}", "TestID"), "default", "vi-VN", _token);
            response.EnsureSuccessStatusCode();
            var result = response.Content.ReadAsAsync<ResultBase>().Result;
            Assert.IsNotNull(result);
            Assert.AreEqual(result.IsSuccess, true);
        }
    }
}
