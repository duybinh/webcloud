﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using DHSoft.Utility.Security;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using WebCloud.Api.Controllers;
using WebCloud.Api.Core.Helper;
using WebCloud.Api.Core.Models;
using WebCloud.Api.Business;
using WebCloud.Api.Business.Auth;
using WebCloud.Common;
using WebCloud.Common.Auth;
using WebCloud.Common.Constant;
using WebCloud.Api.DataAccess;

namespace WebCloud.Api.Tests.Controllers
{
    [TestClass]
    public class AuthenticationControllerTest
    {
        HttpClient client = new HttpClient
                                {
                                    BaseAddress = new Uri("http://localhost:14469/")
                                };

        private string token =
            "fVOKOZdRMSBMBYcetyX+2sqsfMqD7yjHENFVjAyyILLgkUrVAdEeUEjd8QJastWK6jLIiA2wpRtu9MrTIa4an386ESNImfAHYxE+Q/V3swj2cg0SVfmnat/HsEF6pAX7avYaoi5MOTWTOMXwIx5gj3EAeJVjYJKCumIMzeuBjqpSyRkhqFAaXhe77JGruT2BWCQv068vO7xCmEgSvKT7DYr1Cqxz86mkAPgYJUFJLcUmAD2PpwOT/TWKa+dE7SsQEAH4WmsHusnGuE4mVYYbnn/nX2s2pidvnxSt3yZDmtt20wzAuAcI7+T7ZZv+CLrFaaF2ETIJqvvxQj8TCHz02zez8NWZff3jP57EMVD62fQ+xaZzH2wgjRwR+ODFzfN9";

        [TestMethod]
        public void AuthorizeSuccess()
        {
            // Add an Accept header for JSON format.
            // client.DefaultRequestHeaders.Accept.Add(
            // new MediaTypeWithQualityHeaderValue("application/json"));

            client.DefaultRequestHeaders.Add("AuthToken", token);
            client.DefaultRequestHeaders.Add("WebsiteId", "default");
            client.DefaultRequestHeaders.Add("LanguageId", "vi-VN");
            client.DefaultRequestHeaders.Add("ApplicationId", "api-test");
            client.DefaultRequestHeaders.Add("ApplicationSecret", "1234567");

            HttpResponseMessage response = client.GetAsync("api/values/test").Result;
            response.EnsureSuccessStatusCode();
            var result = response.Content.ReadAsAsync<ResultObject<BasePrincipalModel>>().Result;
            Assert.IsNotNull(result);
            Assert.AreEqual(result.IsSuccess, true);
            Assert.AreEqual(result.Data.UserID, "admin");
        }

        [TestMethod]
        public void AuthorizeTokenNotValid()
        {
            // Add an Accept header for JSON format.
            // client.DefaultRequestHeaders.Accept.Add(
            // new MediaTypeWithQualityHeaderValue("application/json"));

            client.DefaultRequestHeaders.Add("AuthToken", "NotValid_fVOKOZdRMSBMBYcetyX+2sqsfMqD7yjHENFVjAyyILLgkUrVAdEeUEjd8QJastWK6jLIiA2wpRtu9MrTIa4an386ESNImfAHYxE+Q/V3swj2cg0SVfmnat/HsEF6pAX7avYaoi5MOTWTOMXwIx5gj3EAeJVjYJKCumIMzeuBjqpSyRkhqFAaXhe77JGruT2BjIX0i7E6jahS9CF+teoxpheHhZ0OFuTKuC6DUmqKMbgiRJPLlKR44cJPfa36HmsObrF2YjDmUU3tyhHrsIgimMggZl7thsOHU8efuY3U34p5ehiGgMB0obEZNHkNF/saD4Hn3+yfs4y+57dJKypE/Q==");
            client.DefaultRequestHeaders.Add("WebsiteId", "default");
            client.DefaultRequestHeaders.Add("LanguageId", "vi-VN");
            client.DefaultRequestHeaders.Add("ApplicationId", "api-test");
            client.DefaultRequestHeaders.Add("ApplicationSecret", "1234567");

            HttpResponseMessage response = client.GetAsync("api/values/test").Result;
            Assert.AreEqual(response.StatusCode, HttpStatusCode.Unauthorized);
            var result = response.Content.ReadAsAsync<ResultBase>().Result;
            Assert.IsNotNull(result);
            Assert.AreEqual(result.IsSuccess, false);
            Assert.AreEqual(result.Message.Code, MessageCode.Auth_TokenNotValid);

        }

        [TestMethod]
        public void AuthorizeTokenEmpty()
        {
            // Add an Accept header for JSON format.
            // client.DefaultRequestHeaders.Accept.Add(
            // new MediaTypeWithQualityHeaderValue("application/json"));

            HttpResponseMessage response = client.GetAsync("api/values/test").Result;
            Assert.AreEqual(response.StatusCode, HttpStatusCode.Unauthorized);
            var result = response.Content.ReadAsAsync<ResultBase>().Result;
            Assert.IsNotNull(result);
            Assert.AreEqual(result.IsSuccess, false);
            Assert.AreEqual(result.Message.Code, MessageCode.Auth_TokenEmpty);
        }

        [TestMethod]
        public void AuthorizeUserNotHasRole()
        {
            // Add an Accept header for JSON format.
            // client.DefaultRequestHeaders.Accept.Add(
            // new MediaTypeWithQualityHeaderValue("application/json"));

            client.DefaultRequestHeaders.Add("AuthToken", token);
            client.DefaultRequestHeaders.Add("WebsiteId", "default");
            client.DefaultRequestHeaders.Add("LanguageId", "vi-VN");
            client.DefaultRequestHeaders.Add("ApplicationId", "api-test");
            client.DefaultRequestHeaders.Add("ApplicationSecret", "1234567");

            HttpResponseMessage response = client.GetAsync("api/values/testrole").Result;
            Assert.AreEqual(response.StatusCode, HttpStatusCode.Unauthorized);
            var result = response.Content.ReadAsAsync<ResultBase>().Result;
            Assert.IsNotNull(result);
            Assert.AreEqual(result.IsSuccess, false);
            Assert.AreEqual(result.Message.Code, MessageCode.Auth_NotAuthorize);
        }

        [TestMethod]
        public void AuthenticateSuccess()
        {
            // Test success case
            var postData = new
            {
                UserId = "admin",
                Password = "1234567"
            };

            HttpResponseMessage response = ApiHelper.Post(ApiHelper.AuthenticationUrl, postData, "default");
            response.EnsureSuccessStatusCode();
            var result = response.Content.ReadAsAsync<ResultObject<string>>().Result;
            Assert.IsNotNull(result);
            Assert.AreEqual(result.IsSuccess, true);
            Assert.IsNotNull(result.Data);
            Console.WriteLine(result.Data);
        }

        [TestMethod]
        public void AuthenticateNoUser()
        {

            // Add an Accept header for JSON format.
            // client.DefaultRequestHeaders.Accept.Add(
            // new MediaTypeWithQualityHeaderValue("application/json"));

            client.DefaultRequestHeaders.Add("WebsiteId", "default");
            client.DefaultRequestHeaders.Add("LanguageId", "en-US");

            // Test success case
            var postData = new AuthenticationModel()
            {
                UserId = "admin1",
                Password = "1234567"
            };

            HttpResponseMessage response = client.PostAsJsonAsync("api/auth", postData).Result;
            response.EnsureSuccessStatusCode();
            var result = response.Content.ReadAsAsync<ResultObject<string>>().Result;
            Assert.IsNotNull(result);
            Assert.AreEqual(result.IsSuccess, false);
            Assert.AreEqual(result.Message.Code, MessageCode.Auth_UserNotExist);
            Console.WriteLine(result.Message.Title);
        }

        [TestMethod]
        public void AuthenticatePasswordFail()
        {
            // Add an Accept header for JSON format.
            // client.DefaultRequestHeaders.Accept.Add(
            // new MediaTypeWithQualityHeaderValue("application/json"));

            client.DefaultRequestHeaders.Add("WebsiteId", "default");
            client.DefaultRequestHeaders.Add("LanguageId", "vi-VN");

            // Test success case
            var postData = new AuthenticationModel()
            {
                UserId = "admin",
                Password = "123456"
            };

            HttpResponseMessage response = client.PostAsJsonAsync("api/auth", postData).Result;
            response.EnsureSuccessStatusCode();
            var result = response.Content.ReadAsAsync<ResultObject<string>>().Result;
            Assert.IsNotNull(result);
            Assert.AreEqual(result.IsSuccess, false);
            Assert.AreEqual(result.Message.Code, MessageCode.Auth_PasswordFailed);
            Console.WriteLine(result.Message.Title);
        }

        [TestMethod]
        public void AuthenticateAppFail()
        {
            // Add an Accept header for JSON format.
            // client.DefaultRequestHeaders.Accept.Add(
            // new MediaTypeWithQualityHeaderValue("application/json"));

            client.DefaultRequestHeaders.Add("ApplicationId", "api-test");
            client.DefaultRequestHeaders.Add("ApplicationSecret", "1234567");

            HttpResponseMessage response = client.GetAsync("api/values/testauthapp").Result;
            var result = response.Content.ReadAsAsync<ResultBase>().Result;
            Assert.AreEqual(response.StatusCode, HttpStatusCode.Unauthorized);
            Assert.AreEqual(result.IsSuccess, false);
            Assert.AreEqual(result.Message.Code, MessageCode.Auth_UnAuthenticatedApplication);
        }

        [TestMethod]
        public void AuthenticateAppSuccess()
        {
            // Add an Accept header for JSON format.
            // client.DefaultRequestHeaders.Accept.Add(
            // new MediaTypeWithQualityHeaderValue("application/json"));

            client.DefaultRequestHeaders.Add("ApplicationId", "WebCloud.Admin");
            client.DefaultRequestHeaders.Add("ApplicationSecret", "1234567");

            HttpResponseMessage response = client.GetAsync("api/values/testauthapp").Result;
            response.EnsureSuccessStatusCode();
            var result = response.Content.ReadAsAsync<string>().Result;
            Assert.AreEqual(result, "OK");
        }
    }
}
