﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using WebCloud.Common;
using WebCloud.Entity;

namespace WebCloud.Api.Core.Helper
{
    public class ApiAdapter
    {
        public static ResultCollection<ModuleBase> GetModuleBases()
        {
            HttpResponseMessage response = ApiHelper.Get(ApiHelper.ModuleBaseForSiteUrl);
            response.EnsureSuccessStatusCode();
            var result = response.Content.ReadAsAsync<ResultCollection<ModuleBase>>().Result;
            return result;
        }

        public static ResultCollection<Page> GetPages(string websiteId)
        {
            HttpResponseMessage response = ApiHelper.Get(ApiHelper.PageForSiteUrl, websiteId);
            response.EnsureSuccessStatusCode();
            var result = response.Content.ReadAsAsync<ResultCollection<Page>>().Result;
            return result;
        }

        public static ResultCollection<Layout> GetLayouts(string websiteId)
        {
            HttpResponseMessage response = ApiHelper.Get(ApiHelper.LayoutForSiteUrl, websiteId);
            response.EnsureSuccessStatusCode();
            var result = response.Content.ReadAsAsync<ResultCollection<Layout>>().Result;
            return result;
        }

        public static ResultObject<Dictionary<string, object>> GetConfigs(string websiteId)
        {
            HttpResponseMessage response = ApiHelper.Get(ApiHelper.ConfigGetForSiteUrl, websiteId);
            response.EnsureSuccessStatusCode();
            var result = response.Content.ReadAsAsync<ResultObject<Dictionary<string, object>>>().Result;
            return result;
        }

        public static ResultObject<Dictionary<string, LocalizationString>> GetResources(string websiteId)
        {
            HttpResponseMessage response = ApiHelper.Get(ApiHelper.ResourceGetForSiteUrl, websiteId);
            response.EnsureSuccessStatusCode();
            var result = response.Content.ReadAsAsync<ResultObject<Dictionary<string, LocalizationString>>>().Result;
            return result;
        }

        public static ResultCollection<Module> GetModules(string websiteId)
        {
            HttpResponseMessage response = ApiHelper.Get(ApiHelper.ModuleForSite, websiteId);
            response.EnsureSuccessStatusCode();
            var result = response.Content.ReadAsAsync<ResultCollection<Module>>().Result;
            return result;
        }

        #region Article
        public static ResultObject<Article> GetArticleById(string websiteId, string languageId, string id)
        {
            HttpResponseMessage response = ApiHelper.Get(ApiHelper.ArticleGetByIdUrl.Replace("{id}", id), websiteId, languageId);
            response.EnsureSuccessStatusCode();
            var result = response.Content.ReadAsAsync<ResultObject<Article>>().Result;
            return result;
        }

        public static ResultPagging<Article> QueryArticles(string websiteId, string languageId, ArticleQuery query)
        {
            HttpResponseMessage response = ApiHelper.Post(ApiHelper.ArticleQueryUrl,query, websiteId, languageId);
            response.EnsureSuccessStatusCode();
            var result = response.Content.ReadAsAsync<ResultPagging<Article>>().Result;
            return result;
        }

        public static ResultObject<ArticleCategory> GetArticleCategoryById(string websiteId, string languageId, string id)
        {
            HttpResponseMessage response = ApiHelper.Get(ApiHelper.ArticleCategoryGetByIdUrl.Replace("{id}", id), websiteId, languageId);
            response.EnsureSuccessStatusCode();
            var result = response.Content.ReadAsAsync<ResultObject<ArticleCategory>>().Result;
            return result;
        }
        #endregion
    }
}
