﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using WebCloud.Common.Constant;

namespace WebCloud.Api.Core.Helper
{
    public static class ApiHelper
    {
        #region URL constants
        /// <summary>
        /// ConfigurationManager.AppSettings["ApiBaseUrl"]
        /// </summary>
        public static readonly string ApiBaseUrl = ConfigurationManager.AppSettings["ApiBaseUrl"];

        /// <summary>
        /// "api/auth"
        /// </summary>
        public const string AuthenticationUrl = "api/auth";

        /// <summary>
        /// "api/user/myprofile"
        /// </summary>
        public const string UserGetMyProfileUrl = "api/user/myprofile";

        /// <summary>
        /// "api/update-data"
        /// </summary>
        public const string DataUpdateUrl = "api/update-data";

        #region Role
        /// <summary>
        /// "api/role"
        /// </summary>
        public const string RoleGetAllUrl = "api/role";

        /// <summary>
        /// "api/role/add"
        /// </summary>
        public const string RoleAddUrl = "api/role/add";

        /// <summary>
        /// "api/role/edit"
        /// </summary>
        public const string RoleEditUrl = "api/role/edit";

        /// <summary>
        /// "api/role/delete/{id}"
        /// </summary>
        public const string RoleDeleteUrl = "api/role/delete/{id}";

        /// <summary>
        /// "api/role/{id}"
        /// </summary>
        public const string RoleGetByIdUrl = "api/role/{id}";
        #endregion

        #region Language
        /// <summary>
        /// "api/lang"
        /// </summary>
        public const string LanguageGetAllUrl = "api/lang";

        /// <summary>
        /// "api/lang/add"
        /// </summary>
        public const string LanguageAddUrl = "api/lang/add";

        /// <summary>
        /// "api/lang/edit"
        /// </summary>
        public const string LanguageEditUrl = "api/lang/edit";

        /// <summary>
        /// "api/lang/delete/{id}"
        /// </summary>
        public const string LanguageDeleteUrl = "api/lang/delete/{id}";

        /// <summary>
        /// "api/lang/{id}"
        /// </summary>
        public const string LanguageGetByIdUrl = "api/lang/{id}";
        #endregion

        #region Text
        /// <summary>
        /// "api/text/langs"
        /// </summary>
        public const string TextsInLanguagesUrl = "api/text/langs";

        /// <summary>
        /// "api/text/update"
        /// </summary>
        public const string TextsInLanguagesUpdateUrl = "api/text/update";

        /// <summary>
        /// "api/text/add"
        /// </summary>
        public const string TextsInLanguagesAddUrl = "api/text/add";

        /// <summary>
        /// "api/text/delete"
        /// </summary>
        public const string TextResourceDeleteUrl = "api/text/delete";
        #endregion

        #region ArticleCategory
        /// <summary>
        /// "api/article-category"
        /// </summary>
        public const string ArticleCategoryUrl = "api/article-category";

        /// <summary>
        /// "api/article-category/add"
        /// </summary>
        public const string ArticleCategoryAddUrl = "api/article-category/add";

        /// <summary>
        /// "api/article-category/edit"
        /// </summary>
        public const string ArticleCategoryEditUrl = "api/article-category/edit";

        /// <summary>
        /// "api/article-category/{id}"
        /// </summary>
        public const string ArticleCategoryGetByIdUrl = "api/article-category/{id}";

        /// <summary>
        /// "api/article-category/delete/{id}"
        /// </summary>
        public const string ArticleCategoryDeleteUrl = "api/article-category/delete/{id}";

        /// <summary>
        /// "api/article-category/delete/withchilds/{id}"
        /// </summary>
        public const string ArticleCategoryDeleteWithChildsUrl = "api/article-category/delete/withchilds/{id}";

        /// <summary>
        /// "api/article-category/trash"
        /// </summary>
        public const string ArticleCategoryTrashUrl = "api/article-category/trash";

        /// <summary>
        /// "api/article-category/remove/{id}"
        /// </summary>
        public const string ArticleCategoryRemoveUrl = "api/article-category/remove/{id}";

        /// <summary>
        /// "api/article-category/restore/{id}"
        /// </summary>
        public const string ArticleCategoryRestoreUrl = "api/article-category/restore/{id}";

        /// <summary>
        /// "api/article-category/empty-trash"
        /// </summary>
        public const string ArticleCategoryEmptyTrashUrl = "api/article-category/empty-trash";
        #endregion

        #region Article
        /// <summary>
        /// "api/article"
        /// </summary>
        public const string ArticleUrl = "api/article";

        /// <summary>
        /// "api/article/query"
        /// </summary>
        public const string ArticleQueryUrl = "api/article/query";

        /// <summary>
        /// "api/article/add"
        /// </summary>
        public const string ArticleAddUrl = "api/article/add";

        /// <summary>
        /// "api/article/edit"
        /// </summary>
        public const string ArticleEditUrl = "api/article/edit";

        /// <summary>
        /// "api/article/{id}"
        /// </summary>
        public const string ArticleGetByIdUrl = "api/article/{id}";

        /// <summary>
        /// "api/article/delete/{id}"
        /// </summary>
        public const string ArticleDeleteUrl = "api/article/delete/{id}";

        /// <summary>
        /// "api/article/trash"
        /// </summary>
        public const string ArticleTrashUrl = "api/article/trash";

        /// <summary>
        /// "api/article/remove/{id}"
        /// </summary>
        public const string ArticleRemoveUrl = "api/article/remove/{id}";

        /// <summary>
        /// "api/article/restore/{id}"
        /// </summary>
        public const string ArticleRestoreUrl = "api/article/restore/{id}";

        /// <summary>
        /// "api/article/empty-trash"
        /// </summary>
        public const string ArticleEmptyTrashUrl = "api/article/empty-trash";
        #endregion

        #region Tag
        /// <summary>
        /// "api/tag"
        /// </summary>
        public const string TagUrl = "api/tag";
        #endregion

        #region Layout
        /// <summary>
        /// "api/layout"
        /// </summary>
        public const string LayoutUrl = "api/layout";

        /// <summary>
        /// "api/layout"
        /// </summary>
        public const string LayoutByTypeUrl = "api/layout/type/{id}";

        /// <summary>
        /// "api/layout/add"
        /// </summary>
        public const string LayoutAddUrl = "api/layout/add";

        /// <summary>
        /// "apilayout/edit"
        /// </summary>
        public const string LayoutEditUrl = "api/layout/edit";

        /// <summary>
        /// "api/layout/{id}"
        /// </summary>
        public const string LayoutGetByIdUrl = "api/layout/{id}";

        /// <summary>
        /// "api/layout/delete/{id}"
        /// </summary>
        public const string LayoutDeleteUrl = "api/layout/delete/{id}";

        /// <summary>
        /// "api/layout/site"
        /// </summary>
        public const string LayoutForSiteUrl = "api/layout/site";
        #endregion

        #region Page
        /// <summary>
        /// "api/page"
        /// </summary>
        public const string PageUrl = "api/page";

        /// <summary>
        /// "api/page/add"
        /// </summary>
        public const string PageAddUrl = "api/page/add";

        /// <summary>
        /// "api/page/edit"
        /// </summary>
        public const string PageEditUrl = "api/page/edit";

        /// <summary>
        /// "api/page/{id}"
        /// </summary>
        public const string PageGetByIdUrl = "api/page/{id}";

        /// <summary>
        /// "api/page/delete/{id}"
        /// </summary>
        public const string PageDeleteUrl = "api/page/delete/{id}";
        
        /// <summary>
        /// "api/page/install/{id}"
        /// </summary>
        public const string PageInstallUrl = "api/page/install/{id}";

        /// <summary>
        /// "api/page/site"
        /// </summary>
        public const string PageForSiteUrl = "api/page/site";

        #endregion

        //#region PageBaseBase
        ///// <summary>
        ///// "api/page-base"
        ///// </summary>
        //public const string PageBaseUrl = "api/page-base";

        ///// <summary>
        ///// "api/page-base/add"
        ///// </summary>
        //public const string PageBaseAddUrl = "api/page-base/add";

        ///// <summary>
        ///// "apipage-base/edit"
        ///// </summary>
        //public const string PageBaseEditUrl = "api/page-base/edit";

        ///// <summary>
        ///// "api/page-base/{id}"
        ///// </summary>
        //public const string PageBaseGetByIdUrl = "api/page-base/{id}";

        ///// <summary>
        ///// "api/page-base/delete/{id}"
        ///// </summary>
        //public const string PageBaseDeleteUrl = "api/page-base/delete/{id}";

        //#endregion

        #region ModuleBase
        /// <summary>
        /// "api/module-base"
        /// </summary>
        public const string ModuleBaseUrl = "api/module-base";

        /// <summary>
        /// "api/module-base/add"
        /// </summary>
        public const string ModuleBaseAddUrl = "api/module-base/add";

        /// <summary>
        /// "apimodule-base/edit"
        /// </summary>
        public const string ModuleBaseEditUrl = "api/module-base/edit";

        /// <summary>
        /// "api/module-base/{id}"
        /// </summary>
        public const string ModuleBaseGetByIdUrl = "api/module-base/get/{id}";

        /// <summary>
        /// "api/module-base/delete/{id}"
        /// </summary>
        public const string ModuleBaseDeleteUrl = "api/module-base/delete/{id}";

        /// <summary>
        /// "api/module-base/site"
        /// </summary>
        public const string ModuleBaseForSiteUrl = "api/module-base/site";

        /// <summary>
        /// "api/module-base/admin"
        /// </summary>
        public const string ModuleBaseForAdminUrl = "api/module-base/admin";

        /// <summary>
        /// "api/module-base/installed"
        /// </summary>
        public const string ModuleBaseGetInstalledUrl = "api/module-base/installed";


        /// <summary>
        /// "api/module-base/install/{id}"
        /// </summary>
        public const string ModuleBaseInstallUrl = "api/module-base/install/{id}";


        /// <summary>
        /// "api/module-base/uninstall/{id}"
        /// </summary>
        public const string ModuleBaseUninstallUrl = "api/module-base/uninstall/{id}";
        #endregion

        #region Module
        /// <summary>
        /// "api/module/page/{pageId}"
        /// </summary>
        public const string ModuleInPageUrl = "api/module/page/{pageId}";

        /// <summary>
        /// "api/module/add"
        /// </summary>
        public const string ModuleAddUrl = "api/module/add";

        /// <summary>
        /// "api/module/edit"
        /// </summary>
        public const string ModuleEditUrl = "api/module/edit";

        /// <summary>
        /// "api/module/{id}"
        /// </summary>
        public const string ModuleGetByIdUrl = "api/module/{id}";

        /// <summary>
        /// "api/module/delete/{id}"
        /// </summary>
        public const string ModuleDeleteUrl = "api/module/delete/{id}";

        /// <summary>
        /// "api/module/edit/posistion"
        /// </summary>
        public const string ModuleUpdatePositionUrl = "api/module/edit/posistion";

        /// <summary>
        /// "api/module/site"
        /// </summary>
        public const string ModuleForSite = "api/module/site";

        #endregion

        #region Theme
        /// <summary>
        /// "api/theme"
        /// </summary>
        public const string ThemeGetAllUrl = "api/theme";

        /// <summary>
        /// "api/theme/add"
        /// </summary>
        public const string ThemeAddUrl = "api/theme/add";

        /// <summary>
        /// "api/theme/edit"
        /// </summary>
        public const string ThemeEditUrl = "api/theme/edit";

        /// <summary>
        /// "api/theme/delete/{id}"
        /// </summary>
        public const string ThemeDeleteUrl = "api/theme/delete/{id}";

        /// <summary>
        /// "api/theme/{id}"
        /// </summary>
        public const string ThemeGetByIdUrl = "api/theme/{id}";
        #endregion

        #region Configuration
        /// <summary>
        /// "api/configuration"
        /// </summary>
        public const string ConfigGetAllUrl = "api/configuration";

        /// <summary>
        /// "api/configuration/add"
        /// </summary>
        public const string ConfigAddUrl = "api/configuration/add";

        /// <summary>
        /// "api/configuration/edit"
        /// </summary>
        public const string ConfigEditUrl = "api/configuration/edit";

        /// <summary>
        /// "api/configuration/delete/{id}"
        /// </summary>
        public const string ConfigDeleteUrl = "api/configuration/delete/{id}";

        /// <summary>
        /// "api/configuration/{id}"
        /// </summary>
        public const string ConfigGetByIdUrl = "api/configuration/{id}";

        /// <summary>
        /// "api/configuration/site"
        /// </summary>
        public const string ConfigGetForSiteUrl = "api/configuration/site";


        /// <summary>
        /// "api/configuration/update"
        /// </summary>
        public const string ConfigUpdateUrl = "api/configuration/update";
        #endregion

        #region Resource
        /// <summary>
        /// "api/resource"
        /// </summary>
        public const string ResourceGetAllUrl = "api/resource";

        /// <summary>
        /// "api/resource/lang/{id}"
        /// </summary>
        public const string ResourceGetAllUrlInLang = "api/resource/lang/{id}";


        /// <summary>
        /// "api/resource/add"
        /// </summary>
        public const string ResourceAddUrl = "api/resource/add";

        /// <summary>
        /// "api/resource/edit"
        /// </summary>
        public const string ResourceEditUrl = "api/resource/edit";

        /// <summary>
        /// "api/resource/update"
        /// </summary>
        public const string ResourceUpdateUrl = "api/resource/update";

        /// <summary>
        /// "api/resource/delete/{id}"
        /// </summary>
        public const string ResourceDeleteUrl = "api/resource/delete/{id}";

        /// <summary>
        /// "api/resource/{id}"
        /// </summary>
        public const string ResourceGetByIdUrl = "api/resource/{id}";

        /// <summary>
        /// "api/resource/site"
        /// </summary>
        public const string ResourceGetForSiteUrl = "api/resource/site";
        #endregion
       
        #endregion

        #region Config
        private static string _applicationId = null;
        private static string _applicationSecret = null;
        private static bool _isConfig = false;

        public static bool Config(string applicationId, string applicationSecret)
        {
            if (!_isConfig)
            {
                _applicationId = applicationId;
                _applicationSecret = applicationSecret;
                return (_isConfig = true);
            }
            return false;
        }
        #endregion

        #region Helper Method
        public static HttpResponseMessage Get(string requestUri, string websiteId = null, string languageId = null, string authToken = null)
        {
            var client = new HttpClient
            {
                BaseAddress = new Uri(ApiBaseUrl)
            };

            if (websiteId != null)
                client.DefaultRequestHeaders.Add("WebsiteId", websiteId);

            if (languageId != null)
                client.DefaultRequestHeaders.Add("LanguageId", languageId);

            if (authToken == null && HttpContext.Current != null)
            {
                var httpCookie = HttpContext.Current.Request.Cookies[HttpConstant.AuthToken];
                if (httpCookie != null)
                    authToken = httpCookie.Value;
            }

            if (authToken != null)
            {
                client.DefaultRequestHeaders.Add("AuthToken", authToken);
            }

            if (_applicationId != null)
                client.DefaultRequestHeaders.Add("ApplicationId", _applicationId);

            if (_applicationSecret != null)
                client.DefaultRequestHeaders.Add("ApplicationSecret", _applicationSecret);

            HttpResponseMessage response = client.GetAsync(requestUri).Result;
            return response;
        }

        public static HttpResponseMessage Post<T>(string requestUri, T data, string websiteId = null, string languageId = null, string authToken = null)
        {
            var client = new HttpClient
            {
                BaseAddress = new Uri(ApiBaseUrl)
            };

            if (websiteId != null)
                client.DefaultRequestHeaders.Add("WebsiteId", websiteId);

            if (languageId != null)
                client.DefaultRequestHeaders.Add("LanguageId", languageId);

            if (authToken != null)
                client.DefaultRequestHeaders.Add("AuthToken", authToken);

            if (_applicationId != null)
                client.DefaultRequestHeaders.Add("ApplicationId", _applicationId);

            if (_applicationSecret != null)
                client.DefaultRequestHeaders.Add("ApplicationSecret", _applicationSecret);

            HttpResponseMessage response = client.PostAsJsonAsync(requestUri, data).Result;
            return response;
        }
        #endregion

    }
}
