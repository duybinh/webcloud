﻿using System.ComponentModel.DataAnnotations;

namespace WebCloud.Api.Core.Models
{
    public class AuthenticationModel
    {
        [Required]
        public string UserId { set; get; }
        
        [Required]
        public string Password { set; get; }
    }
}