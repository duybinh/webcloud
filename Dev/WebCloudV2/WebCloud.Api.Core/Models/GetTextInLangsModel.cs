﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace WebCloud.Api.Core.Models
{
    public class GetTextInLangsModel
    {
        public bool RefreshCache { get; set; }
        public string[] Languages { get; set; }
    }
}
