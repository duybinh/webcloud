﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace WebCloud.Api.Core.Models
{
    public class UserProfileModel
    {
        public long GlobalID { get; set; }
        public string WebsiteID { get; set; }
        public string UserID { get; set; }
        public string LanguageID { get; set; }
        public string Email { get; set; }
        public bool IsVerifiedEmail { get; set; }
        public string[] Roles { get; set; }
        public string[] Groups { get; set; }
        public string DisplayName { get; set; }
        public string Avatar { get; set; }
    }
}
