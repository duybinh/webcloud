﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace WebCloud.Api.Core.Models
{
    public class UpdateModulePositionModel
    {
       public string ModuleId { set; get; }
       public string Section { set; get; }
       public string BelowTo { set; get; }
      
    }
}
