﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace WebCloud.Api.Core.Models
{
    public class ResourceUpdateModel
    {
        public string ResourceId { get; set; }
        public string LanguageId { get; set; }
        public string Value { get; set; }
    }

    public class DataUpdateModel
    {
        public string Key { get; set; }
        public string Obj { get; set; }
        public Dictionary<string, object> Fields { get; set; }
    }
}
