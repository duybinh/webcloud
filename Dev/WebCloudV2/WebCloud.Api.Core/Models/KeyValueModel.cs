﻿using System;

namespace WebCloud.Api.Core.Models
{
    public class KeyValueModel
    {
        public String Key { set; get; }
        public Object Value { set; get; }
    }
}