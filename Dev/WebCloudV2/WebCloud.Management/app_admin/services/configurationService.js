﻿(function () {

    var configurationFactory = function ($http, $q) {
        var factory = {};

        factory.new = function () {
            return $q.when({
                ConfigID: null,
                Name: null,
                Description: null,
                DataType: 'String',
                DefaultValue: null,
                Group: null,
                IsGlobalConfig: true,
                RolesToView: '*',
                RolesToEdit: '*',
                EditingControl: '',
                IsEnabled: true,
                SortOrder: 0,
            });
        };

        factory.getall = function () {
            return $http.get(webcloud.config.apiUrl + 'configuration').then(
              function (results) {
                  if (results.data.IsSuccess) {
                      return results.data.Data;
                  } else {
                      webcloud.error(results.data);
                      return null;
                  }
              });
        };

        factory.get = function (id) {
            return $http.get(webcloud.config.apiUrl + 'configuration/' + id).then(
              function (results) {
                  if (results.data.IsSuccess) {
                      return results.data.Data;
                  } else {
                      webcloud.error(results.data);
                      return null;
                  }
              });
        };

        factory.add = function (obj) {
            console.debug(angular.toJson(obj));
            return $http.post(webcloud.config.apiUrl + 'configuration/add', obj).then(
              function (results) {
                  if (results.data.IsSuccess) {
                      return true;
                  } else {
                      webcloud.error(results.data);
                      return null;
                  }
              });
        };

        factory.edit = function (obj) {
            console.debug(angular.toJson(obj));
            return $http.post(webcloud.config.apiUrl + 'configuration/edit', obj).then(
              function (results) {
                  if (results.data.IsSuccess) {
                      return true;
                  } else {
                      webcloud.error(results.data);
                      return false;
                  }
              });
        };

        factory.delete = function (id) {
            var url = webcloud.config.apiUrl + 'configuration/delete/' + id;
            return $http.get(url).then(
               function (results) {
                   if (results.data.IsSuccess) {
                       return true;
                   } else {
                       webcloud.error(results.data);
                       return false;
                   }
               });
        };
        
        factory.update = function (obj) {
            return $http.post(webcloud.config.apiUrl + 'configuration/update', obj).then(
              function (results) {
                  if (results.data.IsSuccess) {
                      return true;
                  } else {
                      webcloud.error(results.data);
                      return false;
                  }
              });
        };

        return factory;
    };

    configurationFactory.$inject = ['$http', '$q'];

    angular.module('webcloudAdminApp').factory('configurationService', configurationFactory);

}());