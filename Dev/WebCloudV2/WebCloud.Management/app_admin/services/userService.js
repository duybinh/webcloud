﻿(function () {

    var userFactory = function ($http, $q, $rootScope, authService) {
        var factory = {
            currentUser: null
        };

        factory.getCurrentUser = function () {
            if (!authService.isAuthenticated) {
                window.location = '/login';
            }
            // set header AuthToken for all request 
//            $http.defaults.headers.common.AuthToken = $.cookie('AuthToken');
//            $http.defaults.headers.common.AuthToken = $.cookie('AuthToken');
            

            return $http.get(webcloud.config.apiUrl + 'user/myprofile').then(
                function (results) {
                    if (results.data.IsSuccess) {
                        factory.currentUser = results.data.Data;
                    }
                    return results.data;
                });
        };

        return factory;
    };

    userFactory.$inject = ['$http', '$q', '$rootScope', 'authService'];

    angular.module('webcloudAdminApp').factory('userService', userFactory);

}());

