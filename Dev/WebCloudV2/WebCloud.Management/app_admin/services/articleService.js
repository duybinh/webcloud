﻿(function () {

    var articleFactory = function ($http, $q) {
        var factory = {};

        factory.new = function () {
            return {
                CategoryIds: [],
                TagValues: [],
                ArticleID: null,
                ThumbImage: null,
                SortOrder: 0,
                HaveVideo: false,
                HavePhoto: false,
                HaveAttachFile: false,
                IsHot: false,
                IsApproved: false,
                AllowComment: true,
                DisplayDatetime: new Date(),
                IsGlobalDisplay: false,
                IsEnabled: true,
                IsDeleted: false,
                Content: {
                    Title: null,
                    SeoTitle: null,
                    Description: null,
                    Content: null,
                    MetaTitle: null,
                    MetaKeyword: null,
                    MetaDescription: null
                }
            };
        };

        factory.getall = function () {
            return $http.get(webcloud.config.apiUrl + 'article').then(
              function (results) {
                  if (results.data.IsSuccess) {
                      return results.data.Data;
                  } else {
                      webcloud.error(results.data);
                      return null;
                  }
              });
        };


        factory.queryObj = {};
        factory.queryObjDefault = function () {
            return {
                Title: '',
                CategoryIds: [],
                IsEnabled: '',
                IsApproved: '',
                IsHot: '',
                IsGlobalDisplay: '',
                AllowComment: '',
                SortField: 'GlobalID',
                SortDirection: 'DESC',
                Page: 1,
                PageSize: 10,
            };
        };
        factory.defaultQueryObj = function () {
            var df = factory.queryObjDefault();
            factory.queryObj.Title = df.Title;
            factory.queryObj.CategoryIds = df.CategoryIds;
            factory.queryObj.IsEnabled = df.IsEnabled;
            factory.queryObj.IsApproved = df.IsApproved;
            factory.queryObj.IsHot = df.IsHot;
            factory.queryObj.IsGlobalDisplay = df.IsGlobalDisplay;
            factory.queryObj.AllowComment = df.AllowComment;
            factory.queryObj.SortField = df.SortField;
            factory.queryObj.SortDirection = df.SortDirection;
            factory.queryObj.Page = df.Page;
            factory.queryObj.PageSize = df.PageSize;
        };
        factory.defaultQueryObj();
        factory.isDefaultQuery = function () {
            return JSON.stringify(factory.queryObj) === JSON.stringify(factory.queryObjDefault());
        };

        factory.query = function (obj) {
            return $http.post(webcloud.config.apiUrl + 'article/query', obj).then(
              function (results) {
                  if (results.data.IsSuccess) {
                      return results.data;
                  } else {
                      webcloud.error(results.data);
                      return null;
                  }
              });
        };

        factory.trash = function () {
            return $http.get(webcloud.config.apiUrl + 'article/trash').then(
              function (results) {
                  if (results.data.IsSuccess) {
                      return results.data.Data;
                  } else {
                      webcloud.error(results.data);
                      return null;
                  }
              });
        };

        factory.get = function (id) {
            return $http.get(webcloud.config.apiUrl + 'article/' + id).then(
              function (results) {
                  if (results.data.IsSuccess) {
                      return results.data.Data;
                  } else {
                      webcloud.error(results.data);
                      return null;
                  }
              });
        };

        factory.add = function (obj) {
            return $http.post(webcloud.config.apiUrl + 'article/add', obj).then(
              function (results) {
                  if (results.data.IsSuccess) {
                      return results.data.Data;
                  } else {
                      webcloud.error(results.data);
                      return null;
                  }
              });
        };

        factory.edit = function (obj) {
            return $http.post(webcloud.config.apiUrl + 'article/edit', obj).then(
              function (results) {
                  if (results.data.IsSuccess) {
                      return true;
                  } else {
                      webcloud.error(results.data);
                      return false;
                  }
              });
        };

        factory.delete = function (id) {
            return $http.get(webcloud.config.apiUrl + 'article/delete/' + id).then(
               function (results) {
                   if (results.data.IsSuccess) {
                       return true;
                   } else {
                       webcloud.error(results.data);
                       return false;
                   }
               });
        };

        factory.remove = function (id) {
            return $http.get(webcloud.config.apiUrl + 'article/remove/' + id).then(
               function (results) {
                   if (results.data.IsSuccess) {
                       return true;
                   } else {
                       webcloud.error(results.data);
                       return false;
                   }
               });
        };

        factory.restore = function (id) {
            return $http.get(webcloud.config.apiUrl + 'article/restore/' + id).then(
               function (results) {
                   if (results.data.IsSuccess) {
                       return true;
                   } else {
                       webcloud.error(results.data);
                       return false;
                   }
               });
        };

        factory.emptyTrash = function () {
            return $http.get(webcloud.config.apiUrl + 'article/empty-trash/').then(
               function (results) {
                   if (results.data.IsSuccess) {
                       return true;
                   } else {
                       webcloud.error(results.data);
                       return false;
                   }
               });
        };

        return factory;
    };

    articleFactory.$inject = ['$http', '$q'];

    angular.module('webcloudAdminApp').factory('articleService', articleFactory);

}());