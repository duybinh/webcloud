﻿(function () {

    var articleCategoryFactory = function ($http, $q) {
        var factory = {};
        $http.defaults.headers.common.AuthToken = $.cookie('AuthToken');

        factory.new = function () {
            return $q.when({
                ParentID: null,
                ThumbImage: null,
                DisplayArticleThumb: true,
                IsHot: false,
                AllowComment: true,
                IsEnabled: true,
                IsDeleted: false,
                SortOrder: 0,
                Content: {
                    Name: null,
                    SeoName: null,
                    Description: null,
                    MetaTitle: null,
                    MetaKeyword: null,
                    MetaDescription: null
                }
            });
        };

        factory.getall = function () {
            return $http.get(webcloud.config.apiUrl + 'article-category').then(
              function (results) {
                  if (results.data.IsSuccess) {
                      return results.data.Data;
                  } else {
                      webcloud.error(results.data);
                      return null;
                  }
              });
        };
        
        factory.trash = function () {
            return $http.get(webcloud.config.apiUrl + 'article-category/trash').then(
              function (results) {
                  if (results.data.IsSuccess) {
                      return results.data.Data;
                  } else {
                      webcloud.error(results.data);
                      return null;
                  }
              });
        };

        factory.get = function (id) {
            return $http.get(webcloud.config.apiUrl + 'article-category/' + id).then(
              function (results) {
                  if (results.data.IsSuccess) {
                      return results.data.Data;
                  } else {
                      webcloud.error(results.data);
                      return null;
                  }
              });
        };

        factory.add = function (obj) {
            return $http.post(webcloud.config.apiUrl + 'article-category/add', obj).then(
              function (results) {
                  if (results.data.IsSuccess) {
                      return results.data.Data;
                  } else {
                      webcloud.error(results.data);
                      return null;
                  }
              });
        };

        factory.edit = function (obj) {
            return $http.post(webcloud.config.apiUrl + 'article-category/edit', obj).then(
              function (results) {
                  if (results.data.IsSuccess) {
                      return true;
                  } else {
                      webcloud.error(results.data);
                      return false;
                  }
              });
        };
        
        factory.delete = function (id, deleteChilds) {
            var url;
            if (deleteChilds) {
                url = webcloud.config.apiUrl + 'article-category/delete/withchilds/' + id;
            } else {
                url = webcloud.config.apiUrl + 'article-category/delete/' + id;
            }
            return $http.get(url).then(
               function (results) {
                   if (results.data.IsSuccess) {
                       return true;
                   } else {
                       webcloud.error(results.data);
                       return false;
                   }
               });
        };
        
        factory.remove = function (id) {
            return $http.get(webcloud.config.apiUrl + 'article-category/remove/' + id).then(
               function (results) {
                   if (results.data.IsSuccess) {
                       return true;
                   } else {
                       webcloud.error(results.data);
                       return false;
                   }
               });
        };
        
        factory.restore = function (id) {
            return $http.get(webcloud.config.apiUrl + 'article-category/restore/' + id).then(
               function (results) {
                   if (results.data.IsSuccess) {
                       return true;
                   } else {
                       webcloud.error(results.data);
                       return false;
                   }
               });
        };

        factory.emptyTrash = function () {
            return $http.get(webcloud.config.apiUrl + 'article-category/empty-trash/').then(
               function (results) {
                   if (results.data.IsSuccess) {
                       return true;
                   } else {
                       webcloud.error(results.data);
                       return false;
                   }
               });
        };

        return factory;
    };

    articleCategoryFactory.$inject = ['$http', '$q'];

    angular.module('webcloudAdminApp').factory('articleCategoryService', articleCategoryFactory);

}());