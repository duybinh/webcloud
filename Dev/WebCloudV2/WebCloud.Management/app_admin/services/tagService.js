﻿(function () {

    var tagFactory = function ($http, $q) {
        var factory = {};
        factory.data = [];
        factory.firstLoad = true;
        
        $http.defaults.headers.common.AuthToken = $.cookie('AuthToken');

        factory.getall = function () {
            if (factory.firstLoad) {
                return $http.get(webcloud.config.apiUrl + 'tag').then(
                              function (results) {
                                  if (results.data.IsSuccess) {
                                      factory.data = results.data.Data;
                                      factory.firstLoad = false;
                                      return results.data.Data;
                                  } else {
                                      webcloud.error(results.data);
                                      return null;
                                  }
                              });
            } else {
                return $q.when(factory.data);
            }

        };

        factory.add = function (tag) {
            if (tag) {
                if (factory.data.indexOf(tag) < 0) {
                    factory.data.push(tag);
                }
            }
        };


        return factory;
    };

    tagFactory.$inject = ['$http', '$q'];

    angular.module('webcloudAdminApp').factory('tagService', tagFactory);

}());