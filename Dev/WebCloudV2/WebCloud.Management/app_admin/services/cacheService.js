﻿(function () {

    var cacheFactory = function ($http,$q) {
        var factory = {};
        factory.refreshCache = function (cacheKey, allLang) {
            var url = webcloud.config.siteUrl + 'CacheHandler.ashx?CacheKey=' + cacheKey
                + "&WebsiteId=" + $.cookie('WebsiteId')
                + "&LanguageId=" + $.cookie('LanguageId');

            if (allLang)
                url += '&AllLanguage=true';

            $.getJSON(url, function (data) {
                 console.debug(data);
            });
        };

        factory.refreshPageCache = function () {
            factory.refreshCache('Page', true);
        };

        factory.refreshModuleCache = function () {
            factory.refreshCache('Module', true);
        };

        factory.refreshLayoutCache = function () {
            factory.refreshCache('Layout', true);
        };

        factory.refreshResourceCache = function () {
            factory.refreshCache('Resource', true);
        };

        factory.refreshConfigCache = function () {
            factory.refreshCache('Config', true);
        };

        return factory;
    };

    cacheFactory.$inject = ['$http','$q'];

    angular.module('webcloudAdminApp').factory('cacheService', cacheFactory);

}());