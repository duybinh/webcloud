﻿(function () {

    var moduleFactory = function ($http, $q) {
        var factory = {};

        factory.getall = function () {
            return $http.get(webcloud.config.apiUrl + 'module-base/admin').then(
              function (results) {
                  if (results.data.IsSuccess) {
                      return results.data.Data;
                  } else {
                      webcloud.error(results.data);
                      return null;
                  }
              });
        };

        factory.getInstalled = function () {
            return $http.get(webcloud.config.apiUrl + 'module-base/installed').then(
              function (results) {
                  if (results.data.IsSuccess) {
                      return results.data.Data;
                  } else {
                      webcloud.error(results.data);
                      return null;
                  }
              });
        };

        factory.install = function (obj) {
            return $http.get(webcloud.config.apiUrl + 'module-base/install/' + obj).then(
              function (results) {
                  if (results.data.IsSuccess) {
                      return true;
                  } else {
                      webcloud.error(results.data);
                      return false;
                  }
              });
        };

        factory.uninstall = function (obj) {
            return $http.get(webcloud.config.apiUrl + 'module-base/uninstall/' + obj).then(
              function (results) {
                  if (results.data.IsSuccess) {
                      return true;
                  } else {
                      webcloud.error(results.data);
                      return false;
                  }
              });
        };

        factory.get = function (id) {
            return $http.get(webcloud.config.apiUrl + 'module-base/get/' + id).then(
              function (results) {
                  if (results.data.IsSuccess) {
                      return results.data.Data;
                  } else {
                      webcloud.error(results.data);
                      return null;
                  }
              });
        };

        return factory;
    };

    moduleFactory.$inject = ['$http', '$q'];

    angular.module('webcloudAdminApp').factory('moduleService', moduleFactory);

}());