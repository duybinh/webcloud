﻿(function () {

    var app = angular.module('webcloudAdminApp',
        ['ui.router', 'ngRoute', 'ngAnimate', 'ngCookies', 'wc.directives', 'ngSanitize', 'LocalStorageModule']);

    app.config(function ($stateProvider, $urlRouterProvider) {
        // For any unmatched url, redirect to /home
        $urlRouterProvider.otherwise("/home");
        // Now set up the states
        $stateProvider
            .state('home', {
                url: "/home",
                controller: 'homeController',
                templateUrl: '/app_admin/views/home.html'
            })
            // layout management
            .state('ui-layout', {
                url: "/ui/layout",
                controller: 'layoutController',
                templateUrl: '/app_admin/views/layout/layout.html'
            })
            .state('ui-layout-add', {
                url: "/ui/layout/add",
                controller: 'layoutAddeditController',
                templateUrl: '/app_admin/views/layout/layout-addedit.html'
            })
            .state('ui-layout-edit', {
                url: "/ui/layout/edit/:id",
                controller: 'layoutAddeditController',
                templateUrl: '/app_admin/views/layout/layout-addedit.html'
            })
            .state('ui-layout-copy', {
                url: "/ui/layout/copy/:id",
                params: {
                    m: 'copy',
                },
                controller: 'layoutAddeditController',
                templateUrl: '/app_admin/views/layout/layout-addedit.html'
            })
            // theme management
            .state('ui-theme', {
                url: "/ui/theme",
                controller: 'themeController',
                templateUrl: '/app_admin/views/theme/theme.html'
            })
            .state('ui-theme-add', {
                url: "/ui/theme/add",
                controller: 'themeAddeditController',
                templateUrl: '/app_admin/views/theme/theme-addedit.html'
            })
            .state('ui-theme-edit', {
                url: "/ui/theme/edit/:id",
                controller: 'themeAddeditController',
                templateUrl: '/app_admin/views/theme/theme-addedit.html'
            })
            .state('ui-theme-copy', {
                url: "/ui/theme/copy/:id",
                params: {
                    m: 'copy',
                },
                controller: 'themeAddeditController',
                templateUrl: '/app_admin/views/theme/theme-addedit.html'
            })
            .state('ui-theme-resourses', {
                url: "/ui/theme/files/:id",
                controller: 'themeResourcesController',
                templateUrl: '/app_admin/views/theme/theme-resources.html'
            })
            // configuration management
            .state('configuration', {
                url: "/configuration",
                controller: 'configurationController',
                templateUrl: '/app_admin/views/config/configuration.html'
            })
            .state('configuration-add', {
                url: "/configuration/add",
                controller: 'configurationAddeditController',
                templateUrl: '/app_admin/views/config/configuration-addedit.html'
            })
            .state('configuration-edit', {
                url: "/configuration/edit/:id",
                controller: 'configurationAddeditController',
                templateUrl: '/app_admin/views/config/configuration-addedit.html'
            })
            // resource management
            .state('resource', {
                url: "/resource",
                controller: 'resourceController',
                templateUrl: '/app_admin/views/resource/resource.html'
            })
            .state('resource-add', {
                url: "/resource/add",
                controller: 'resourceAddeditController',
                templateUrl: '/app_admin/views/resource/resource-addedit.html'
            })
            .state('resource-edit', {
                url: "/resource/edit/:id",
                controller: 'resourceAddeditController',
                templateUrl: '/app_admin/views/resource/resource-addedit.html'
            })
            // page management
            .state('page', {
                url: "/page",
                controller: 'pageController',
                templateUrl: '/app_admin/views/page/page.html'
            })
            .state('page-add', {
                url: "/page/add",
                controller: 'pageAddeditController',
                templateUrl: '/app_admin/views/page/page-addedit.html'
            })
            .state('page-edit', {
                url: "/page/edit/:id",
                controller: 'pageAddeditController',
                templateUrl: '/app_admin/views/page/page-addedit.html'
            })
            // module in page
            //.state('page-module', {
            //    url: "/page/:pageId/module",
            //    controller: 'moduleInPageController',
            //    templateUrl: '/app_admin/views/page/module-in-page.html'
            //})
            .state('page-module', {
                url: "/page/:pageId/module",
                controller: 'manageModuleInPageController',
                templateUrl: '/app_admin/views/page/manage-module-in-page.html',
                //data: {
                //    onSuccess: null,
                //    onCancel: null,
                //}
            })
            .state('page-module.new', {
                url: "/new",
                params: {
                    id: null,
                    displayAsModal: null,
                    moduleBaseId: null,
                    section: null,
                    onSuccess: null,
                    onCancel: null,
                },
                controller: 'moduleInPageAddeditController',
                templateUrl: '/app_admin/views/page/module-in-page-addedit-form.html'
            })
            .state('page-module-add', {
                url: "/page/:pageId/module/add",
                controller: 'moduleInPageAddeditController',
                templateUrl: '/app_admin/views/page/module-in-page-addedit.html'
            })
            .state('page-module-edit', {
                url: "/page/:pageId/module/edit/:id",
                controller: 'moduleInPageAddeditController',
                templateUrl: '/app_admin/views/page/module-in-page-addedit.html'
            })

            // module-base management
            .state('module-base', {
                url: "/module-base",
                controller: 'moduleController',
                templateUrl: '/app_admin/views/module/module-base.html'
            })
            .state('module-base-add', {
                url: "/module-base/add",
                controller: 'moduleAddeditController',
                templateUrl: '/app_admin/views/module/module-base-addedit.html'
            })
            .state('module-base-edit', {
                url: "/module-base/edit/:id",
                controller: 'moduleAddeditController',
                templateUrl: '/app_admin/views/module/module-base-addedit.html'
            })

            // article management
            .state('article-category', {
                url: "/article/article-category",
                controller: 'articleCategoryController',
                templateUrl: '/app_admin/views/article/article-category.html'
            })
            .state('article-category-add', {
                url: "/article/article-category/add",
                controller: 'articleCategoryAddeditController',
                templateUrl: '/app_admin/views/article/article-category-addedit.html'
            })
            .state('article-category-edit', {
                url: "/article/article-category/edit/:id",
                controller: 'articleCategoryAddeditController',
                templateUrl: '/app_admin/views/article/article-category-addedit.html'
            })
            .state('article-category-trash', {
                url: "/article/article-category/trash",
                controller: 'articleCategoryTrashController',
                templateUrl: '/app_admin/views/article/article-category-trash.html'
            })
            .state('article', {
                url: "/article/article-list",
                controller: 'articleController',
                templateUrl: '/app_admin/views/article/article.html'
            })
            .state('article-add', {
                url: "/article/article/add",
                controller: 'articleAddeditController',
                templateUrl: '/app_admin/views/article/article-addedit.html'
            }).state('article-copy', {
                url: "/article/article/copy/:id",
                params: {
                    m: 'copy',
                },
                controller: 'articleAddeditController',
                templateUrl: '/app_admin/views/article/article-addedit.html'
            })
            .state('article-edit', {
                url: "/article/article/edit/:id",
                controller: 'articleAddeditController',
                templateUrl: '/app_admin/views/article/article-addedit.html'
            })
            .state('article-trash', {
                url: "/article/article/trash",
                controller: 'articleTrashController',
                templateUrl: '/app_admin/views/article/article-trash.html'
            })
        ;
    });

    app.config(function ($httpProvider) {
        $httpProvider.interceptors.push('authInterceptorService');
    });

    app.run(['$http', '$q', '$rootScope', '$location', '$window', 'localStorageService', 'authService',
       function ($http, $q, $rootScope, $location, $window, localStorageService, authService) {
           authService.init();
       }]);

    angular.element(document).ready(
       function () {
           var initInjector = angular.injector(['LocalStorageModule', 'ng']);
           var $http = initInjector.get('$http');
           var localStorageService = initInjector.get('localStorageService');
           var authData = localStorageService.get('authorizationData');
           if (authData) {
               $http.defaults.headers.common.Authorization = 'Bearer ' + authData.token;
               $http.defaults.headers.common.WebsiteId = $.cookie('WebsiteId');
               $http.defaults.headers.common.LanguageId = $.cookie('LanguageId');
               console.debug(authData);
           } else {
               window.location = '/login';
               return;
           }

           var websiteData = {
               WebsiteId: $.cookie('WebsiteId'),
               LanguageId: $.cookie('LanguageId')
           };
           localStorageService.set('websiteData', websiteData);
           webcloud.store.websiteData = websiteData;

           // get logged in user profile
           $http.get(webcloud.config.apiUrl + 'user/myprofile').then(
             function (response) {
                 if (response.data.IsSuccess) {
                     app.constant('currentUser', response.data.Data);
                 } else {
                     alert('Can not get user profile');
                 }
                 return response.data.Data;
             },
             function (response) {
                 window.location = '/login';
             }
           ).then(function (response) {
               // boostrap the application
               angular.bootstrap(document, ['webcloudAdminApp']);
           });

       }
   );

}());

