﻿(function () {

    var app = angular.module('webcloudAdminApp',
        ['ngRoute', 'ngAnimate', 'ngCookies', 'wc.directives', 'LocalStorageModule']);

    app.run(['$http', '$q', '$rootScope', '$location', 'appSettings', 'authService',
        function ($http, $q, $rootScope, $location) {
            $http.defaults.headers.common.WebsiteId = $.cookie('WebsiteId');
            //$http.defaults.headers.common.LanguageId = $.cookie('LanguageId');
        }]);
}());

