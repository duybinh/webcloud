﻿(function () {

    var controller = function ($scope, $location, $routeParams, articleService) {
        $scope.title = 'Bài viết đã xóa';
        $scope.ready = false;
        $scope.source = {
            dataType: "json",
            dataFields: [
                { name: 'GlobalID', type: 'number' },
                { name: 'ArticleID', type: 'string' },
                { name: 'Title', map: 'Content>Title', type: 'string' }
            ],
            id: 'ArticleID',
        };

        init();

        function init() {
            articleService.trash().then(function (obj) {
                if (obj) {
                    $scope.source.localData = obj;
                    $scope.dataAdapter = new $.jqx.dataAdapter($scope.source);

                    $(document).ready(function () {
                        // create Tree Grid
                        $("#articleTrashGrid").jqxGrid(
                        {
                            width: '100%',
                            rowsheight: 36,
                            source: $scope.dataAdapter,
                            columnsResize: true,
                            sortable: true,
                            filterable: true,
                            selectionmode: 'checkbox',
                            altrows: true,
                            ready: function () {
                                $('a.func-restore').die("click");
                                $('a.func-restore').live('click', function () {
                                    var id = $(this).attr('data');
                                    $scope.restore(id);
                                });

                                $('a.func-remove').die("click");
                                $('a.func-remove').live('click', function () {
                                    var id = $(this).attr('data');
                                    $scope.remove(id);
                                });
                            },
                            columns: [
                              { text: 'Tiêu đề', dataField: 'Title' },
                              {
                                  dataField: 'ArticleID', text: '', width: 80,
                                  cellsRenderer: function (row, column, value) {
                                      return '<div class="grid-cell-content-wrap function-cell"><a class="func-restore btn btn-sm green" title="Phục hồi" data="'
                                            + value + '"><span class="glyphicon glyphicon-repeat"></span></a>'
                                            + '&nbsp;<a class="func-remove btn btn-sm red" title="Xóa" data="'
                                            + value + '" ><span class="glyphicon glyphicon-trash" ></span></a></div>';
                                  }
                              }
                            ]
                        });
                    });

                    $scope.ready = true;
                }
            });
        }

        $scope.remove = function (id) {
            webcloud.confirm(webcloud.text.removeConfirmTitle, webcloud.text.removeConfirmContent, function () {
                articleService.remove(id).then(function (result) {
                    if (result) {
                        webcloud.success(webcloud.text.removeSuccess);
                        init();
                    }
                });
            });
        };

        $scope.restore = function (id) {
            articleService.restore(id).then(function (result) {
                if (result) {
                    webcloud.success(webcloud.text.restoreSuccess);
                    init();
                }
            });
        };

        $scope.empty = function () {
            webcloud.confirm(webcloud.text.removeConfirmTitle, webcloud.text.emptyTrashConfirmContent, function () {
                articleService.emptyTrash().then(function (result) {
                    if (result) {
                        webcloud.success(webcloud.text.removeSuccess);
                        init();
                    }
                });
            });
        };

    };

    controller.$inject = ['$scope', '$location', '$routeParams', 'articleService'];

    angular.module('webcloudAdminApp').controller('articleTrashController', controller);

}());
