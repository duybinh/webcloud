﻿(function () {

    var globalSettingController = function ($scope, $location, $window, $state,$route, languageService) {
        $scope.selectedLang = $.cookie('LanguageId');
        $scope.$watch('selectedLang', function (newValue, oldValue) {
            if (newValue && oldValue && newValue != oldValue) {
                console.debug('Select lang: ' + newValue + ' - ' + oldValue);
                languageService.changeLang(newValue);
                $state.reload();
            }
        });
    };

    globalSettingController.$inject = ['$scope', '$location', '$window', '$state','$route', 'languageService'];

    angular.module('webcloudAdminApp').controller('globalSettingController', globalSettingController);

}());
