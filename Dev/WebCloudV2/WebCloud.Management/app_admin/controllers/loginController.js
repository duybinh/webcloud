﻿(function () {

    var loginController = function ($scope, $location, $routeParams, authService, appSettings) {
        $scope.username = null;
        $scope.password = null;
        $scope.rememberMe = true;
        $scope.errorMsg = null;
        $scope.login = function () {
            var loginData = {
                userName: $scope.username,
                password: $scope.password
            };
            authService.login(loginData).then(function (response) {
                // redirect
                var path = '/';
                //var redirect = webcloud.getUrlParam('redirect');
                //if (redirect != '')
                //    path = path + '#' + redirect;

                window.location = path;

            }, function (err) {
                $scope.errorMsg = err.error_description;
                //webcloud.error(err.error_description);
            });
        };
    };

    loginController.$inject = ['$scope', '$location', '$routeParams', 'authService', 'appSettings'];

    angular.module('webcloudAdminApp').controller('loginController', loginController);

}());