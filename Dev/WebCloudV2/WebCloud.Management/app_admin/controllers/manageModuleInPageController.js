﻿(function () {

    var manageModuleInPageController = function ($scope, $location, $stateParams, $state, authService, pageService, moduleService, cacheService) {
        var onAddEvent = null;

        $scope.pageId = $stateParams.pageId;
        $scope.title = "Cài đặt module thuộc trang";
        $scope.listModuleBase = [];
        $scope.listModuleInPage = [];

        $scope.delete = function (id) {
            webcloud.confirm(webcloud.text.deleteConfirmTitle, webcloud.text.deleteConfirmContent, function () {
                doDelete(id, false);
            });
        };

        $scope.newModule = function () {
            doNewModule();
        };

        function init() {
            $('a.func-delete').die("click");
            $('a.func-delete').live('click', function () {
                var id = $(this).attr('data');
                $scope.delete(id);
            });

            $(".section").sortable({
                group: "module",
                scroll: true,
                animation: 200,
                onAdd: function (evt) {
                    var el = $(evt.item);
                    var from = $(evt.from);
                    if (from.hasClass('modulebase-list')) {
                        //el.parentNode.removeChild(el);
                        //alert('Dropped: ' + el.textContent);
                        onAddEvent = evt;
                        el.attr("class", "module-item");
                        var moduleBaseId = el.attr("module");
                        console.debug("moduleBaseId: " + moduleBaseId);
                        var section = el.parent().attr("section");
                        console.debug("section: " + section);
                        doNewModule(moduleBaseId, section);
                    } else {
                        doUpdatePosition(el);
                    }
                },
                onSort: function (evt) {
                    var el = $(evt.item);
                    if (!el.hasClass('modulebase-item'))
                        doUpdatePosition(el);
                },
            });

            $(".modulebase-list").sortable({
                group: {
                    name: 'module',
                    pull: 'clone',
                    put: false
                },
                sort: false,
                scroll: true,
                onStart: function (evt) {
                    console.debug('start event: ' + evt);
                    $(evt.item).tooltip('hide');
                },
            });

            $(document).off('click', '.func-info');
            $(document).on('click', '.func-info', function (e) {
                e.stopPropagation();
                var moduleId = $(this).attr('data');
                var content = $('#popover_' + moduleId).html();
                $(this).popover({
                    html: true,
                    content: content,
                    title: 'Thông tin module',
                    trigger: 'hover',
                    placement: 'auto'
                });
                $(this).popover('toggle');
            });

            $(document).off('click', '.func-edit');
            $(document).on('click', '.func-edit', function (e) {
                e.stopPropagation();
                var moduleId = $(this).attr('data');
                doEditModule(moduleId);
            });

            $(document).off('click', '.func-delete');
            $(document).on('click', '.func-delete', function (e) {
                e.stopPropagation();
                var moduleId = $(this).attr('data');
                webcloud.confirm(webcloud.text.deleteConfirmTitle, webcloud.text.deleteConfirmContent, function () {
                    doDelete(moduleId, false);
                });
            });

            loadBaseModule();
            loadModuleInPage();
        }

        function loadBaseModule() {
            // load list module bases
            moduleService.getInstalled().then(function (obj) {
                if (obj) {
                    $scope.listModuleBase = obj;
                    $.map($scope.listModuleBase, function (item, index) {
                        item.ThumbnailUrl = webcloud.thumbUrl(item.Thumbnail, "sites", 60, 60);
                    });
                }
            });
        }

        function loadModuleInPage() {
            // load list module in page
            pageService.getAllModule($scope.pageId).then(function (obj) {
                if (obj) {
                    $scope.listModuleInPage = obj;
                    // updateList($scope.listModuleInPage, obj);
                    $.map($scope.listModuleInPage, function (item, index) {
                        item.ThumbnailUrl = webcloud.thumbUrl(item.ModuleBase.Thumbnail, "sites", 60, 60);
                    });
                }
            });
        }
        
        function updateList(list1, list2) {
            for (var i = 0; i < list1.length; i++) {
                var moduleId = list1[i].ModuleID;
                var m = $.grep(list2, function (item) { return item.ModuleID == moduleId; });
                if (m.length > 0) {
                    list1[i] = m[0];
                } else {
                    
                }
            }
            
            for (var i2 = 0; i2 < list2.length; i2++) {
                var moduleId2 = list2[i2].ModuleID;
                console.debug('check ' + moduleId2);
                var m2 = $.grep(list1, function (item) { return item.ModuleID == moduleId2; });
                if (m2.length == 0) {
                    console.debug('add ' + moduleId2);
                    list1.push(list2[i2]);
                } else {

                }
            }
        }

        function doDelete(id) {
            pageService.deleteModule(id).then(function (result) {
                if (result) {
                    webcloud.success(webcloud.text.deleteSuccess);
                    loadModuleInPage();
                }
            });
        };

        function doUpdatePosition(el) {
            var modulePosition = {};
            modulePosition.ModuleId = el.attr("module-id");
            modulePosition.Section = el.parent().attr("section");
            if (el.prev())
                modulePosition.BelowTo = el.prev().attr("module-id");

            pageService.updateModulePosition(modulePosition).then(function (result) {
                if (result) {
                    //loadModuleInPage();

                    // refresh cache in frontend site
                    cacheService.refreshModuleCache();
                }
            });
           
        };

        function doNewModule(moduleBaseId, section) {
            $stateParams.displayAsModal = true;
            $stateParams.id = null;
            $stateParams.moduleBaseId = moduleBaseId;
            $stateParams.section = section;
            $stateParams.onSuccess = function () {
                var el = $(onAddEvent.item);
                //$(el).pulsate({
                //    color: '#FF3F3F',                       // set the color of the pulse
                //    reach: 30,                              // how far the pulse goes in px
                //    speed: 1500,                            // how long one pulse takes in ms
                //    pause: 0,                               // how long the pause between pulses is in ms
                //    glow: true,                             // if the glow should be shown too
                //    repeat: 2,                              // will repeat forever if true, if given a number will repeat for that many times
                //    onHover: false                          // if true only pulsate if user hovers over the element
                //});

                el.remove();

                loadModuleInPage();
            };
            $stateParams.onCancel = function () {
                var el = $(onAddEvent.item);
                el.fadeOut(1000, function () { el.remove(); });;
            };
            $state.go('.new', $stateParams);
        };

        function doEditModule(moduleId) {
            $stateParams.displayAsModal = true;
            $stateParams.id = moduleId;
            $stateParams.onSuccess = function () {
                //var el = $('.func-edit[data="' + moduleId + '"]').parent().parent();
                //$(el).pulsate({
                //    color: '#FF3F3F',                       // set the color of the pulse
                //    reach: 30,                              // how far the pulse goes in px
                //    speed: 1500,                            // how long one pulse takes in ms
                //    pause: 0,                               // how long the pause between pulses is in ms
                //    glow: true,                             // if the glow should be shown too
                //    repeat: 2,                              // will repeat forever if true, if given a number will repeat for that many times
                //    onHover: false                          // if true only pulsate if user hovers over the element
                //});
                
                loadModuleInPage();
            };
            $stateParams.onCancel = null;
            $state.go('.new', $stateParams);
        };

        init();

    };


    manageModuleInPageController.$inject = ['$scope', '$location', '$stateParams', '$state', 'authService', 'pageService', 'moduleService', 'cacheService'];

    angular.module('webcloudAdminApp').controller('manageModuleInPageController', manageModuleInPageController);

}());
