﻿(function () {

    var controller = function ($scope, $location, $stateParams, themeService) {
        var id = ($stateParams.id) ? $stateParams.id : '';
        var copy = id && ('copy' == $stateParams.m);
        
        $scope.isNew = (id === '' || copy);
        $scope.title = $scope.isNew ? 'Thêm mẫu giao diện' : 'Sửa mẫu giao diện';
        $scope.saveButton = $scope.isNew ? 'Thêm' : 'Sửa';
        $scope.obj = {};

        $scope.save = function () {
            if ($scope.themeForm.$valid) {
                if ($scope.isNew) {
                    themeService.add($scope.obj).then(function (result) {
                        if (result) {
                            webcloud.success("Thêm thành công");
                            back();
                        }
                    });
                }
                else {
                    themeService.edit($scope.obj).then(function (result) {
                        if (result) {
                            webcloud.success("Cập nhật thành công");
                            back();
                        }
                    });
                }
            }
        };

        $scope.cancel = function () {
            back();
        };

        function init() {
            if (id) {
                themeService.get(id).then(function (obj) {
                    if (obj){
                        $scope.obj = obj;
                        if (copy) {
                            $scope.obj.BasedTheme = $scope.obj.ThemeCode;
                            $scope.obj.ThemeCode = $scope.obj.ThemeCode + '_copy';
                            $scope.obj.Name += ' - copy';
                        }
                    }
                });
            } else {
                themeService.new().then(function (newObj) {
                    $scope.obj = newObj;
                });
            }
        }

        function back() {
            $location.url('/ui/theme');
        };

        init();
    };

    controller.$inject = ['$scope', '$location', '$stateParams', 'themeService'];

    angular.module('webcloudAdminApp').controller('themeAddeditController', controller);

}());
