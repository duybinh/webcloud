﻿(function () {

    var configurationController = function ($scope, $location, authService, configurationService, cacheService) {
        var edtDialog = $('#configEditorDialog');
        $scope.data = [];
        $scope.objEdit = {};
        $scope.editorType = 'String';

        $scope.searchObj = {};
        $scope.searchObj.$ = '';
        $scope.searchObj.Group = '';

        $scope.update = function () {
            configurationService.update($scope.objEdit).then(function (result) {
                if (result) {
                    webcloud.success("Cập nhật thành công");
                    edtDialog.modal('hide');
                    reloadTableData();
                }
            });
        };

        $scope.filterGroup = function (group) {
            $scope.searchObj.Group = group;
        };

        $scope.refreshCache = function () {
            cacheService.refreshConfigCache();
            webcloud.success("Refresh dữ liệu cache thành công");
        };


        function init() {
            reloadTableData();

            $('.config-value-container').die("click");
            $('.config-value-container').live('click', function () {
                $scope.objEdit.Key = $(this).attr('data-code');
                $scope.objEdit.Value = $(this).html();
                $scope.objEdit.Name = $(this).attr('data-name');
                $scope.objEdit.EditorType = $(this).attr('data-type');

                edtDialog.modal('show');

                $scope.$apply();
            });
        }

        function reloadTableData() {
            configurationService.getall().then(function (obj) {
                if (obj) {
                    $scope.data = obj;
                }
            });

        }

        //function doDelete(id) {
        //    configurationService.delete(id).then(function (result) {
        //        if (result) {
        //            webcloud.success(webcloud.text.deleteSuccess);
        //            reloadTableData();
        //        }
        //    });
        //};

        init();

    };



    configurationController.$inject = ['$scope', '$location', 'authService', 'configurationService', 'cacheService'];

    angular.module('webcloudAdminApp').controller('configurationController', configurationController);

}());
