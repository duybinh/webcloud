﻿(function () {

    var pageController = function ($scope, $location, authService, pageService, cacheService) {
        $scope.data = [];

        $scope.searchObj = {};
        $scope.searchObj.$ = '';
        $scope.searchObj.Group = '';

        $scope.filterGroup = function (group) {
            $scope.searchObj.Group = group;
        };

        $scope.delete = function (id) {
            webcloud.confirm(webcloud.text.deleteConfirmTitle,
                   webcloud.text.deleteConfirmContent, function () {
                       doDelete(id, false);
                   });
        };

        $scope.install = function (id) {
            pageService.install(id).then(function (result) {
                if (result) {
                    webcloud.success(webcloud.text.deleteSuccess);
                    reloadTableData();
                }
            });
        };

        $scope.refreshCache = function () {
            cacheService.refreshPageCache();
            cacheService.refreshModuleCache();
            webcloud.success("Refresh dữ liệu cache thành công");
        };

        function init() {
            reloadTableData();
        }

        function reloadTableData() {
            pageService.getall().then(function (obj) {
                if (obj) {
                    $scope.data = obj;
                }
            });
        }

        function doDelete(id) {
            pageService.delete(id).then(function (result) {
                if (result) {
                    webcloud.success(webcloud.text.deleteSuccess);
                    reloadTableData();
                }
            });
        };

        init();

    };


    pageController.$inject = ['$scope', '$location', 'authService', 'pageService', 'cacheService'];

    angular.module('webcloudAdminApp').controller('pageController', pageController);

}());
