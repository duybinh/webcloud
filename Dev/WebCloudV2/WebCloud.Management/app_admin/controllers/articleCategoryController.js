﻿(function () {

    var controller = function ($scope, $location, $routeParams, articleCategoryService) {
        $scope.title = 'Danh sách nhóm bài viết';
        $scope.ready = false;
        $scope.source = {
            dataType: "json",
            dataFields: [
                { name: 'GlobalID', type: 'number' },
                { name: 'ArticleCategoryID', type: 'string' },
                { name: 'ParentID', type: 'string' },
                { name: 'SortOrder', type: 'number' },
                { name: 'ArticleCount', type: 'number' },
                { name: 'Name', map: 'Content>Name', type: 'string' },
                { name: 'SeoName', map: 'Content>SeoName', type: 'string' },
                { name: 'Description', map: 'Content>Description', type: 'string' },
                { name: 'IsEnabled', type: 'bool' },
                { name: 'IsHot', type: 'bool' },
                { name: 'AllowComment', type: 'bool' },
                { name: 'IsLeaf', type: 'bool' }
            ],
            hierarchy:
            {
                keyDataField: { name: 'ArticleCategoryID' },
                parentDataField: { name: 'ParentID' }
            },
            id: 'ArticleCategoryID',
        };

        init();

        function init() {
            articleCategoryService.getall().then(function (obj) {
                if (obj) {
                    $scope.source.localData = obj;
                    $scope.dataAdapter = new $.jqx.dataAdapter($scope.source);

                    $(document).ready(function () {
                        // create Tree Grid
                        $("#treeGrid").jqxTreeGrid(
                        {
                            //theme: webcloud.config.jqxTheme,
                            width: '100%',
                            source: $scope.dataAdapter,
                            checkboxes: false,
                            columnsResize: true,
                            sortable: true,
                            ready: function () {
                                for (var i = 0; i < $scope.source.localData.length; i++) {
                                    $("#treeGrid").jqxTreeGrid('expandRow', $scope.source.localData[i].ArticleCategoryID);
                                }

                                $('a.func-delete').die("click");
                                $('a.func-delete').live('click', function () {
                                    var id = $(this).attr('data');
                                    var isLeaf = $(this).attr('isLeaf');
                                    $scope.delete(id, isLeaf == "true");
                                });
                            },
                            columns: [
                                {
                                    text: 'Tên nhóm', minWidth: 200,
                                    cellsRenderer: function (row, column, value, rowData) {
                                        return '<a target="_blank" href="http://localhost:14480/a/' + rowData.SeoName + '/' + rowData.ArticleCategoryID + '">' + rowData.Name + '</a>';

                                    }
                                },
                              { text: 'Thứ tự', dataField: 'SortOrder', width: 70, align: 'right', cellsAlign: 'right' },
                              { text: 'Bài viết', dataField: 'ArticleCount', width: 70, align: 'right', cellsAlign: 'right' },
                              {
                                  text: 'Hiển thị', dataField: 'IsEnabled', width: 70,
                                  cellsRenderer: function (row, column, value, rowData) {
                                      return value ? '<span class="label label-success">Hiển thị</span>' : '<span class="label label-default">Ẩn</span>';
                                  }
                              },
                              {
                                  text: 'Nổi bật', dataField: 'IsHot', width: 70,
                                  cellsRenderer: function (row, column, value, rowData) {
                                      return value ? '<span class="label label-warning">Hot!</span>' : '';
                                  }
                              },
                              {
                                  text: 'Comment', dataField: 'AllowComment', width: 70,
                                  cellsRenderer: function (row, column, value, rowData) {
                                      return value ? '<span class="label label-success">Bật</span>' : '<span class="label label-default">Tắt</span>';
                                  }
                              },
                              {
                                  text: '', width: 80,
                                  cellsRenderer: function (row, column, value, rowData) {
                                      return '<a class="func-edit btn btn-sm green" href="#/article/article-category/edit/'
                                          + rowData.ArticleCategoryID + '"><span class="glyphicon glyphicon-pencil"></span></a>'
                                      + '&nbsp;<a class="func-delete btn btn-sm red" data="' + rowData.ArticleCategoryID + '" isLeaf="' + rowData.IsLeaf + '"><span class="glyphicon glyphicon-remove" ></span></a>';

                                  }
                              }
                            ]
                        });
                        // create context menu
                        var contextMenu = $("#Menu").jqxMenu({ width: 200, height: 58, autoOpenPopup: false, mode: 'popup' });
                        $("#treeGrid").on('contextmenu', function () {
                            return false;
                        });

                        $("#treeGrid").on('rowClick', function (event) {
                            var args = event.args;
                            if (args.originalEvent.button == 2) {
                                var scrollTop = $(window).scrollTop();
                                var scrollLeft = $(window).scrollLeft();
                                contextMenu.jqxMenu('open', parseInt(event.args.originalEvent.clientX) + 5 + scrollLeft, parseInt(event.args.originalEvent.clientY) + 5 + scrollTop);
                                return false;
                            }
                        });

                        $("#Menu").on('itemclick', function (event) {
                            var args = event.args;
                            var selection = $("#treeGrid").jqxTreeGrid('getSelection');
                            var rowid = selection[0].uid
                            if ($.trim($(args).text()) == "Edit Selected Row") {
                                $("#treeGrid").jqxTreeGrid('beginRowEdit', rowid);
                            } else {
                                $("#treeGrid").jqxTreeGrid('deleteRow', rowid);
                            }
                        });
                    });

                    $scope.ready = true;
                }
            });
        }

        function doDelete(id, deleteChilds) {
            articleCategoryService.delete(id, deleteChilds).then(function (result) {
                if (result) {
                    webcloud.success(webcloud.text.deleteSuccess);
                    init();
                }
            });
        };

        $scope.delete = function (id, isLeaf) {
            if (isLeaf) {
                webcloud.confirm(webcloud.text.deleteConfirmTitle,
                    webcloud.text.deleteConfirmContent, function () {
                        doDelete(id, false);
                    });
            } else {
                webcloud.choose(webcloud.text.deleteChoiceTitle,
                    webcloud.text.deleteCategoryChoiceContent,
                    webcloud.text.deleteCategoryYes,
                    webcloud.text.deleteCategoryNo,
                    function () {
                        doDelete(id, true);
                    },
                    function () {
                        doDelete(id, false);
                    });
            }
        };
    };

    controller.$inject = ['$scope', '$location', '$routeParams', 'articleCategoryService'];

    angular.module('webcloudAdminApp').controller('articleCategoryController', controller);

}());
