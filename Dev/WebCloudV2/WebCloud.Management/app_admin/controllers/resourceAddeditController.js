﻿(function () {

    var controller = function ($scope, $location, $stateParams, resourceService) {
        var id = ($stateParams.id) ? $stateParams.id : '';

        $scope.isNew = (id === '');
        $scope.title = $scope.isNew ? 'Thêm resource' : 'Sửa resource';
        $scope.saveButton = $scope.isNew ? 'Thêm' : 'Sửa';
        $scope.obj = {};

        $scope.editorType = 'String';
        
        $scope.save = function () {
            if ($scope.resourceForm.$valid) {
                if ($scope.isNew) {
                    resourceService.add($scope.obj).then(function (result) {
                        if (result) {
                            webcloud.success("Thêm thành công");
                            back();
                        }
                    });
                }
                else {
                    resourceService.edit($scope.obj).then(function (result) {
                        if (result) {
                            webcloud.success("Cập nhật thành công");
                            back();
                        }
                    });
                }
            }
        };

        $scope.cancel = function () {
            back();
        };

        function init() {
            if (id) {
                resourceService.get(id).then(function (obj) {
                    if (obj) {
                        $scope.obj = obj;
                    }
                });
            } else {
                resourceService.new().then(function (newObj) {
                    $scope.obj = newObj;
                });
            }
        }

        function back() {
            $location.url('/resource');
        };

        $scope.$watch('obj.DisplayAsHtml', function (newValue, oldValue) {
            if (newValue) {
                $scope.editorType = 'Html';
            } else {
                $scope.editorType = 'String';
            }
        });
        
        init();
    };

    controller.$inject = ['$scope', '$location', '$stateParams', 'resourceService'];

    angular.module('webcloudAdminApp').controller('resourceAddeditController', controller);

}());
