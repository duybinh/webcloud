﻿(function () {

    var controller = function ($scope, $location, $stateParams, pageService, layoutService) {
        var id = ($stateParams.id) ? $stateParams.id : '';
        var layoutContent;

        $scope.isNew = (id === '');
        $scope.title = $scope.isNew ? 'Thêm trang chức năng' : 'Sửa trang chức năng';
        $scope.saveButton = $scope.isNew ? 'Thêm' : 'Sửa';
        $scope.obj = {};
        $scope.layout = {};

        $scope.save = function () {
            if (true) {
                if ($scope.isNew) {
                    pageService.add($scope.obj).then(function (result) {
                        if (result) {
                            updateLayout();
                            webcloud.success("Thêm thành công");
                            back();
                        }
                    });
                }
                else {
                    pageService.edit($scope.obj).then(function (result) {
                        if (result) {
                            updateLayout();
                            webcloud.success("Cập nhật thành công");
                            back();
                        }
                    });
                }
            }
        };

        $scope.cancel = function () {
            back();
        };

        $scope.updateSeoName = function () {
            $scope.obj.SeoName = webcloud.toSeoString($scope.obj.Name);
        };

        $scope.$watch('obj.LayoutID', function (newValue, oldValue) {
            if (newValue && newValue != '') {
                loadLayout(newValue);
            }
        });

        function updateLayout() {
            if ($scope.layout.Content != layoutContent) {
                layoutService.edit($scope.layout);
            }
        }

        function init() {
            if (id) {
                pageService.get(id).then(function (obj) {
                    if (obj) {
                        $scope.obj = obj;
                        // get page layout
                        loadLayout($scope.obj.LayoutID);
                    }
                });
            } else {
                pageService.new().then(function (newObj) {
                    $scope.obj = newObj;
                });
            }
        }

        function loadLayout(layoutId) {
            // get page layout
            layoutService.get(layoutId).then(function (layout) {
                if (layout) {
                    $scope.layout = layout;
                    layoutContent = layout.Content;
                }
            });
        }

       

        function back() {
            $location.url('/page');
        };

        init();

    };

    controller.$inject = ['$scope', '$location', '$stateParams', 'pageService', 'layoutService'];

    angular.module('webcloudAdminApp').controller('pageAddeditController', controller);

}());
