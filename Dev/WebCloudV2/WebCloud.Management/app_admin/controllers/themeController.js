﻿(function () {

    var themeController = function ($scope, $location, authService, themeService, configurationService, cacheService) {
        $scope.currentTheme = 'default';

        $scope.delete = function (id) {
            webcloud.confirm(webcloud.text.deleteConfirmTitle,
                   webcloud.text.deleteConfirmContent, function () {
                       doDelete(id, false);
                   });
        };

        $scope.useTheme = function (id) {
            configurationService.update({ Key: 'theme', Value: id }).then(function (result) {
                if (result) {
                    webcloud.success('Cài đặt giao diện ' + id + ' thành công.');
                    cacheService.refreshConfigCache();
                    $scope.currentTheme = id;
                }
            });
        };

        var showPreviewImg = function () {
            $.fancybox($(this), {
                live: false,
                type: "image",
                groupAttr: 'data-rel',
                prevEffect: 'none',
                nextEffect: 'none',
                closeBtn: true,
                helpers: {
                    title: {
                        type: 'over'
                    }
                }
            });
        };


        function init() {
            reloadData();
            $('body').off('click', '.btn-view-large');
            $('body').on('click', '.btn-view-large', showPreviewImg);
        }

        function reloadData() {
            themeService.getall().then(function (obj) {
                if (obj) {
                    for (var i = 0; i < obj.length; i++) {
                        obj[i].Preview = webcloud.thumbUrl('themes/' + obj[i].ThemeCode + '/preview.jpg', 300);
                        obj[i].PreviewLarge = webcloud.resourceUrl('themes/' + obj[i].ThemeCode + '/preview.jpg');
                    }
                    $scope.data = obj;
                }
            });

        }

        function doDelete(id) {
            themeService.delete(id).then(function (result) {
                if (result) {
                    webcloud.success(webcloud.text.deleteSuccess);
                    reloadData();
                }
            });
        };

        init();
    };



    themeController.$inject = ['$scope', '$location', 'authService', 'themeService', 'configurationService', 'cacheService'];

    angular.module('webcloudAdminApp').controller('themeController', themeController);

}());
