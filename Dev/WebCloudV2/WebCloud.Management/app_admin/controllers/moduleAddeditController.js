﻿(function () {

    var controller = function ($scope, $location, $stateParams, moduleService) {
        var id = ($stateParams.id) ? $stateParams.id : '';
        var copy = id && ('copy' == $stateParams.m);

        $scope.isNew = (id === '' || copy);
        $scope.title = $scope.isNew ? 'Thêm module chức năng' : 'Sửa module chức năng';
        $scope.saveButton = $scope.isNew ? 'Thêm' : 'Sửa';
        $scope.obj = {};

        $scope.save = function () {
            if ($scope.moduleBaseForm.$valid) {
                if ($scope.isNew) {
                    moduleService.add($scope.obj).then(function (result) {
                        if (result) {
                            webcloud.success("Thêm thành công");
                            back();
                        }
                    });
                }
                else {
                    moduleService.edit($scope.obj).then(function (result) {
                        if (result) {
                            webcloud.success("Cập nhật thành công");
                            back();
                        }
                    });
                }
            }
        };

        $scope.cancel = function () {
            back();
        };

        $scope.addConfig = function () {
            moduleService.newModuleConfig().then(function (newObj) {
                $scope.obj.ModuleBaseConfigs.push(newObj);
            });
        };

        $scope.removeConfig = function (index) {
            webcloud.confirm(webcloud.text.deleteConfirmTitle, webcloud.text.deleteConfirmContent, function () {
                $scope.obj.ModuleBaseConfigs.splice(index, 1);
                $scope.$apply();
            });
        };

        function init() {
            if (id) {
                moduleService.get(id).then(function (obj) {
                    if (obj) {
                        $scope.obj = obj;
                    }
                });
            } else {
                moduleService.new().then(function (newObj) {
                    $scope.obj = newObj;
                });
            }
        }

        function back() {
            $location.url('/module-base');
        };

        init();

    };

    controller.$inject = ['$scope', '$location', '$stateParams', 'moduleService'];

    angular.module('webcloudAdminApp').controller('moduleAddeditController', controller);

}());
