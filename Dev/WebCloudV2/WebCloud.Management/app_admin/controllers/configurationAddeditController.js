﻿(function () {

    var controller = function ($scope, $location, $stateParams, configurationService) {
        var id = ($stateParams.id) ? $stateParams.id : '';

        $scope.isNew = (id === '');
        $scope.title = $scope.isNew ? 'Thêm mẫu giao diện' : 'Sửa mẫu giao diện';
        $scope.saveButton = $scope.isNew ? 'Thêm' : 'Sửa';
        $scope.obj = {};

        $scope.save = function () {
            if ($scope.configForm.$valid) {
                if ($scope.isNew) {
                    configurationService.add($scope.obj).then(function (result) {
                        if (result) {
                            webcloud.success("Thêm thành công");
                            back();
                        }
                    });
                }
                else {
                    configurationService.edit($scope.obj).then(function (result) {
                        if (result) {
                            webcloud.success("Cập nhật thành công");
                            back();
                        }
                    });
                }
            }
        };

        $scope.cancel = function () {
            back();
        };

        function init() {
            if (id) {
                configurationService.get(id).then(function (obj) {
                    if (obj) {
                        $scope.obj = obj;
                    }
                });
            } else {
                configurationService.new().then(function (newObj) {
                    $scope.obj = newObj;
                });
            }
        }

        function back() {
            $location.url('/configuration');
        };

        init();
    };

    controller.$inject = ['$scope', '$location', '$stateParams', 'configurationService'];

    angular.module('webcloudAdminApp').controller('configurationAddeditController', controller);

}());
