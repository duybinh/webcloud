﻿(function () {

    var homeController = function ($scope, $location, authService) {
        $scope.title = "Trang chủ";
    };

    homeController.$inject = ['$scope', '$location', 'authService'];

    angular.module('webcloudAdminApp').controller('homeController', homeController);

}());
