﻿(function () {

    var layoutController = function ($scope, $location, authService, layoutService, cacheService) {
        var layoutContent;
        $scope.data = [];

        $scope.layout = {};
        $scope.mode = 'init';
        $scope.deleteText = 'Xóa';

        $scope.editorMode = 'javascript';
        $scope.$watch('layout.Type', function (newValue, oldValue) {
            if (newValue == 3) {
                $scope.editorMode = 'javascript';
            } else {
                $scope.editorMode = 'html';
            }
        });

        $scope.select = function (layoutId) {
            $scope.mode = 'edit';
            loadLayout(layoutId);
        };

        $scope.new = function (type) {
            layoutService.new(type).then(function (obj) {
                $scope.layout = obj;
                //$scope.layout.Type = type;
                $scope.mode = 'add';
            });
        };

        $scope.save = function () {
            if ($scope.mode == 'add') {
                doAdd();
            } else if ($scope.mode == 'edit') {
                doEdit();
            }

        };

        $scope.delete = function () {
            if ($scope.mode == 'edit' && $scope.layout.LayoutID) {
                if ($scope.layout.SourceType == 0) {// delete local layout
                    doDelete($scope.layout.LayoutID);
                } else if ($scope.layout.SourceType == 1) { // reset to original global layout
                    doReset($scope.layout.LayoutID);
                }
            }
        };

        $scope.refreshCache = function () {
            cacheService.refreshLayoutCache();
            webcloud.success("Refresh dữ liệu cache thành công");
        };

        function loadLayout(layoutId) {
            // get page layout
            layoutService.get(layoutId).then(function (layout) {
                if (layout) {
                    $scope.layout = layout;
                    layoutContent = layout.Content;
                    $scope.deleteText = layout.SourceType == 0 ? "Xóa" : "Reset";
                }
            });
        }

        function goInitState() {
            $scope.layout = {};
            $scope.mode = 'init';
            $scope.deleteText = 'Xóa';
        }

        function init() {
            goInitState();
            reloadLayoutData();
        }


        function reloadLayoutData() {
            layoutService.getall().then(function (obj) {
                if (obj) {
                    $scope.data = obj;
                }
            });
        }

        function doDelete(id) {
            webcloud.confirm("Xóa layout", "Bạn có chắc chắn muốn xóa layout này không?", function () {
                layoutService.delete(id).then(function (result) {
                    if (result) {
                        webcloud.success(webcloud.text.deleteSuccess);
                        goInitState();
                        reloadLayoutData();
                    }
                });
            });
        }

        function doReset(id) {
            webcloud.confirm("Reset layout", "Bạn có chắc chắn muốn reset layout về mặc định của hệ thống không?", function () {
                layoutService.delete(id).then(function (result) {
                    if (result) {
                        webcloud.success("Reset layout về mặc định thành công");
                        loadLayout(id);
                    }
                });
            });
        }

        function doAdd() {
            layoutService.add($scope.layout).then(function (result) {
                if (result) {
                    webcloud.success("Tạo mới thành công");
                    goInitState();
                    reloadLayoutData();
                }
            });;
        }

        function doEdit() {
            if ($scope.layout.Content != layoutContent) {
                layoutService.edit($scope.layout).then(function (result) {
                    if (result) {
                        webcloud.success("Cập nhật thành công");
                    }
                });;
            }
        }

        init();

    };


    layoutController.$inject = ['$scope', '$location', 'authService', 'layoutService', 'cacheService'];

    angular.module('webcloudAdminApp').controller('layoutController', layoutController);

}());
