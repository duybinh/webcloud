﻿(function () {

    var controller = function ($scope, $location, $route, $routeParams, articleService) {
        var searchPanel = $('.box-search .portlet-body');
        $scope.queryObj = articleService.queryObj;
        $scope.queryObj.PageSize = 10;
        $scope.data = [];
        $scope.delete = function (id) {
            webcloud.confirm(webcloud.text.deleteConfirmTitle,
                   webcloud.text.deleteConfirmContent, function () {
                       doDelete(id, false);
                   });
        };

        $scope.query = function () {
            reloadData(function () {
                webcloud.scrollTo($('#btnQuery'));
            });
        };

        $scope.queryReset = function () {
            articleService.defaultQueryObj();
        };

        $scope.toggleSearchPanel = function () {
            webcloud.togglePanel(searchPanel);
        };

        function init() {
            if (!articleService.isDefaultQuery()) {
                $scope.toggleSearchPanel();
            }
        }

        function doDelete(id) {
            articleService.delete(id).then(function (result) {
                if (result) {
                    webcloud.success(webcloud.text.deleteSuccess);
                    reloadData();
                }
            });
        };

        function reloadData(callback) {
            articleService.query($scope.queryObj).then(function (obj) {
                if (obj) {
                    for (var i = 0; i < obj.Data.length; i++) {
                        obj.Data[i].ThumbImage = webcloud.thumbUrl(obj.Data[i].ThumbImage, "sites", 100, 100);
                    }
                    $scope.data = obj.Data;
                    $scope.state.pagination.numberOfPages = Math.ceil(obj.Total / $scope.state.pagination.number);
                }
                if (callback)
                    callback();
            });
        }

        $scope.getData = function getData(tableState) {
            if (!$scope.state)
                $scope.state = tableState;

            $scope.queryObj.PageSize = tableState.pagination.number;
            $scope.queryObj.Page = tableState.pagination.start / tableState.pagination.number + 1;
            reloadData();
        };
        init();
    };

    controller.$inject = ['$scope', '$location', '$route', '$routeParams', 'articleService'];

    angular.module('webcloudAdminApp').controller('articleController', controller);

}());
