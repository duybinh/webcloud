﻿(function () {

    var controller = function ($scope, $location, $stateParams, articleService, articleCategoryService) {
        $scope.color = webcloud.chooseColor();
        var id = ($stateParams.id) ? $stateParams.id : '';
        var copy = id && ('copy' == $stateParams.m);
        $scope.isNew = (id === '' || copy);
        $scope.title = $scope.isNew ? 'Thêm bài viết' : 'Sửa bài viết';
        $scope.saveButton = $scope.isNew ? 'Thêm' : 'Sửa';
        $scope.obj = {};
        // prepare the data
        $scope.categorySource = {
            datatype: "json",
            datafields: [
                { name: 'ArticleCategoryID' },
                { name: 'ParentID' },
                { name: 'Name', map: 'Content>Name' }
            ],
            id: 'ArticleCategoryID',
            localdata: []
        };
        $scope.categorySource.track = false;
        $scope.loadCompleted = false;

        init();

        $scope.updateSeoTitle = function () {
            $scope.obj.Content.SeoTitle = webcloud.toSeoString($scope.obj.Content.Title);
        };

        $scope.save = function () {
            if ($scope.addeditForm.$valid) {
                if ($scope.isNew) {
                    articleService.add($scope.obj).then(function (result) {
                        if (result) {
                            webcloud.success("Thêm thành công");
                            back();
                        }
                    });
                }
                else {
                    articleService.edit($scope.obj).then(function (result) {
                        if (result) {
                            webcloud.success("Cập nhật thành công");
                            back();
                        }
                    });
                }
            }
        };

        $scope.cancel = function () {
            back();
        };

        function init() {
            if (id) {
                articleService.get(id).then(function (obj) {
                    if (obj) {
                        $scope.obj = obj;
                        if (copy) {
                            $scope.obj.ArticleID = '';
                            //$scope.obj.Name += ' - copy';
                        }
                    }

                    $scope.categorySource.track = $scope.loadCompleted;
                    $scope.loadCompleted = true;
                });
            } else {
                $scope.obj = articleService.new();
                $scope.loadCompleted = true;
            }

            articleCategoryService.getall().then(function (obj) {
                if (obj) {
                    $scope.categorySource.localdata = obj;
                    $scope.categorySource.track = $scope.loadCompleted;
                    $scope.loadCompleted = true;
                }
            });
        }

        function back() {
            $location.url('/article/article-list');
        };

    };

    controller.$inject = ['$scope', '$location', '$stateParams', 'articleService', 'articleCategoryService'];

    angular.module('webcloudAdminApp').controller('articleAddeditController', controller);

}());
