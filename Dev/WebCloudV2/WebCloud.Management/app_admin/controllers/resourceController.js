﻿(function () {

    var resourceController = function ($scope, $location, authService, resourceService, cacheService) {
        var edtDialog = $('#resourceEditorDialog');
        $scope.data = [];
        $scope.objEdit = {
            WebsiteId: null,
            LanguageId: null
        };
        $scope.editorType = 'String';
        $scope.selectedLang = null;

        $scope.searchObj = {};
        $scope.searchObj.$ = '';
        $scope.searchObj.Group = '';

        $scope.delete = function (id) {
            webcloud.confirm(webcloud.text.deleteConfirmTitle,
                   webcloud.text.deleteConfirmContent, function () {
                       doDelete(id, false);
                   });
        };

        $scope.update = function () {
            $scope.objEdit.LanguageId = $scope.selectedLang;

            resourceService.updateValue($scope.objEdit).then(function (result) {
                if (result) {
                    webcloud.success("Cập nhật thành công");
                    edtDialog.modal('hide');
                    reloadTableData();
                }
            });
        };

        $scope.filterGroup = function (group) {
            $scope.searchObj.Group = group;
        };

        $scope.refreshCache = function () {
            cacheService.refreshResourceCache();
            webcloud.success("Refresh dữ liệu cache thành công");
        };

        $scope.$watch('selectedLang', function (newValue, oldValue) {
            reloadTableData();
        });

        function init() {
            reloadTableData();

            $('.resource-value-container').die("click");
            $('.resource-value-container').live('click', function () {
                $scope.objEdit.ResourceId = $(this).attr('data-code');
                $scope.objEdit.Name = $(this).attr('data-name');
                $scope.objEdit.Value = $(this).html();
                $scope.objEdit.EditorType = $(this).attr('data-displayAsHtml') == 'true' ? 'Html' : 'String';

                edtDialog.modal('show');

                $scope.$apply();
            });
        }



        var bindData = function (obj) {
            if (obj) {
                $scope.data = obj;
            }
        };

        function reloadTableData() {
            if ($scope.selectedLang) {
                resourceService.getInLang($scope.selectedLang).then(bindData);
            } else {
                resourceService.getall().then(bindData);
            }
        }

        function doDelete(id) {
            resourceService.delete(id).then(function (result) {
                if (result) {
                    webcloud.success(webcloud.text.deleteSuccess);
                    reloadTableData();
                }
            });
        };

        init();

    };

    resourceController.$inject = ['$scope', '$location', 'authService', 'resourceService', 'cacheService'];

    angular.module('webcloudAdminApp').controller('resourceController', resourceController);

}());
