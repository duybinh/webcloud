﻿(function () {

    var moduleController = function ($scope, $location, authService, moduleService) {
        $scope.title = "Module chức năng";
        $scope.data = [];
        $scope.searchObj = {};
        $scope.searchObj.$ = '';
        $scope.searchObj.Group = '';
        $scope.searchObj.Installed = '';
        $scope.filterGroup = function (group) {
            $scope.searchObj.Group = group;
        };

        $scope.uninstall = function (id) {
            webcloud.confirm('Gỡ module', 'Bạn có chắc chắn muốn gỡ module này khỏi website?', function () {
                doUninstall(id);
            });
        };

        $scope.install = function (id) {
            webcloud.confirm('Cài đặt module', 'Để cài đặt module này vào website, bạn có đồng ý các điều khoản sử dụng của tác giả không?', function () {
                doInstall(id);
            });
        };

        function init() {
            reloadTableData();
            $('a.func-delete').die("click");
            $('a.func-delete').live('click', function () {
                var id = $(this).attr('data');
                $scope.delete(id);
            });
        }

        function reloadTableData() {
            moduleService.getall().then(function (obj) {
                if (obj) {
                    for (var i = 0; i < obj.length; i++) {
                        obj[i].Thumbnail = webcloud.thumbUrl(obj[i].Thumbnail, "sites", 100, 100);
                    }

                    $scope.data = obj;
                }
            });

        }

        function doUninstall(id) {
            moduleService.uninstall(id).then(function (result) {
                if (result) {
                    webcloud.success('Gỡ bỏ module thành công!');
                    reloadTableData();
                }
            });
        };

        function doInstall(id) {
            moduleService.install(id).then(function (result) {
                if (result) {
                    webcloud.success('Cài đặt module thành công!');
                    reloadTableData();
                }
            });
        };

        init();

    };


    moduleController.$inject = ['$scope', '$location', 'authService', 'moduleService'];

    angular.module('webcloudAdminApp').controller('moduleController', moduleController);

}());
