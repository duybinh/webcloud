﻿(function () {

    var controller = function ($scope, $location, $routeParams, articleCategoryService) {
        $scope.title = 'Nhóm bài viết đã xóa';
        $scope.ready = false;
        $scope.source = {
            dataType: "json",
            dataFields: [
                { name: 'GlobalID', type: 'number' },
                { name: 'ArticleCategoryID', type: 'string' },
                { name: 'Name', map: 'Content>Name', type: 'string' }
            ],
            id: 'ArticleCategoryID',
        };

        init();

        function init() {
            articleCategoryService.trash().then(function (obj) {
                if (obj) {
                    $scope.source.localData = obj;
                    $scope.dataAdapter = new $.jqx.dataAdapter($scope.source);

                    $(document).ready(function () {
                        // create Tree Grid
                        $("#grid").jqxTreeGrid(
                        {
                            width: '100%',
                            source: $scope.dataAdapter,
                            columnsResize: true,
                            sortable: true,
                            ready: function () {
                                $('a.func-restore').die("click");
                                $('a.func-restore').live('click', function () {
                                    var id = $(this).attr('data');
                                    $scope.restore(id);
                                });

                                $('a.func-remove').die("click");
                                $('a.func-remove').live('click', function () {
                                    var id = $(this).attr('data');
                                    $scope.remove(id);
                                });
                            },
                            columns: [
                              {
                                  text: '', width: 80,
                                  cellsRenderer: function (row, column, value, rowData) {
                                      return '<a class="func-restore btn btn-sm green" title="Phục hồi" data="'
                                            + rowData.ArticleCategoryID + '"><span class="glyphicon glyphicon-repeat"></span></a>'
                                            + '&nbsp;<a class="func-remove btn btn-sm red" title="Xóa" data="'
                                            + rowData.ArticleCategoryID + '" ><span class="glyphicon glyphicon-trash" ></span></a>';
                                  }
                              },
                              { text: 'ID', dataField: 'ArticleCategoryID', minWidth: 100, width: 200 },
                              { text: 'Tên nhóm', dataField: 'Name' }
                            ]
                        });
                    });

                    $scope.ready = true;
                }
            });
        }

        $scope.remove = function (id) {
            webcloud.confirm(webcloud.text.removeConfirmTitle, webcloud.text.removeConfirmContent, function () {
                articleCategoryService.remove(id).then(function (result) {
                    if (result) {
                        webcloud.success(webcloud.text.removeSuccess);
                        init();
                    }
                });
            });
        };

        $scope.restore = function (id) {
            articleCategoryService.restore(id).then(function (result) {
                if (result) {
                    webcloud.success(webcloud.text.restoreSuccess);
                    init();
                }
            });
        };

        $scope.empty = function () {
            webcloud.confirm(webcloud.text.removeConfirmTitle, webcloud.text.emptyTrashConfirmContent, function () {
                articleCategoryService.emptyTrash().then(function (result) {
                    if (result) {
                        webcloud.success(webcloud.text.removeSuccess);
                        init();
                    }
                });
            });
        };

    };

    controller.$inject = ['$scope', '$location', '$routeParams', 'articleCategoryService'];

    angular.module('webcloudAdminApp').controller('articleCategoryTrashController', controller);

}());
