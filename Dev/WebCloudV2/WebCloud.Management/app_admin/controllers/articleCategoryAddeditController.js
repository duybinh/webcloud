﻿(function () {

    var controller = function ($scope, $location, $stateParams, articleCategoryService) {
        var id = ($stateParams.id) ? $stateParams.id : '';
        $scope.isNew = id === '';
        $scope.title = id === '' ? 'Thêm nhóm bài viết' : 'Sửa nhóm bài viết';
        $scope.saveButton = id === '' ? 'Thêm' : 'Sửa';
        $scope.obj = {};

        init();

        $scope.updateSeoName = function () {
            $scope.obj.Content.SeoName = webcloud.toSeoString($scope.obj.Content.Name);
        };

        $scope.save = function () {
            if ($scope.acForm.$valid) {
                if (!id) {
                    articleCategoryService.add($scope.obj).then(function (result) {
                        if (result) {
                            webcloud.success("Thêm thành công");
                            back();
                        }
                    });
                }
                else {
                    articleCategoryService.edit($scope.obj).then(function (result) {
                        if (result) {
                            webcloud.success("Cập nhật thành công");
                            back();
                        }
                    });
                }
            }
        };

        $scope.cancel = function () {
            back();
        };

        // prepare the data
        $scope.source = {
            datatype: "json",
            datafields: [
                { name: 'ArticleCategoryID' },
                { name: 'ParentID' },
                { name: 'Name', map: 'Content>Name' }
            ],
            id: 'ArticleCategoryID',
            localdata : []
        };
        $scope.source.track = false;
        
        function init() {
            if (id) {
                articleCategoryService.get(id).then(function (obj) {
                    if (obj)
                        $scope.obj = obj;
                });
            } else {
                articleCategoryService.new().then(function (newObj) {
                    $scope.obj = newObj;
                });
            }
            
            articleCategoryService.getall().then(function (obj) {
                if (obj) {
                    $scope.source.localdata = obj;
                    $scope.source.track = true;
                }
            });
        }
       
        function back() {
            $location.url('/article/article-category');
        };

    };

    controller.$inject = ['$scope', '$location', '$stateParams', 'articleCategoryService'];

    angular.module('webcloudAdminApp').controller('articleCategoryAddeditController', controller);

}());
