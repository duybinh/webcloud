﻿(function () {

    var app = angular.module('webcloudAdminApp',
        ['ngRoute', 'ngAnimate', 'ngCookies', 'wc.directives', 'LocalStorageModule']);

    app.run(['$http', '$q', '$rootScope', '$location', 'appSettings', 'authService',
        function ($http, $q, $rootScope, $location) {

        }]);
}());

