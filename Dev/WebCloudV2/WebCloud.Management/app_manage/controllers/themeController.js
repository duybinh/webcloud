﻿(function () {

    var themeController = function ($scope, $location, authService, themeService) {
        var tbl = $('#themeTable');

        $scope.title = "Mẫu giao diện";
        $scope.data = [];

        $scope.delete = function (id) {
            webcloud.confirm(webcloud.text.deleteConfirmTitle,
                   webcloud.text.deleteConfirmContent, function () {
                       doDelete(id, false);
                   });
        };

        function init() {
            buildTable();
            reloadTableData();
            $('a.func-delete').die("click");
            $('a.func-delete').live('click', function () {
                var id = $(this).attr('data');
                $scope.delete(id);
            });
        }

        function buildTable() {
            tbl.dataTable({
                "data": $scope.data,
                "order": [1, 'asc'],
                "columns": [
                    {
                        "title": "", "data": "ThemeCode", "width": "11%", "orderable": false,
                        "render": function (data, type, row, meta) {
                            return '<a title="Sửa" class="btn btn-sm green tooltips" href="#/ui/theme/edit/' + data + '"><i class="icon-pencil"></i></a>&nbsp;' +
                                   '<a title="Copy" class="btn btn-sm red tooltips" href="#/ui/theme/copy/' + data + '"><i class="icon-plus"></i></a>&nbsp;' +
                                   '<a title="Xóa" class="btn btn-sm default tooltips func-delete" data="' + data + '"><i class="icon-trash"></i></a>';
                        }
                    },
                    {
                        "title": "Code", "data": "ThemeCode", "width": "10%",
                        "render": function (data, type, row, meta) {
                            return '<a href="#/ui/theme/files/' + data + '">' + data + '</a>';
                        }
                    },
                    { "title": "Tên", "data": "Name", "width": "30%" },
                    { "title": "Mô tả", "data": "Description" },
                    {
                        "title": "Hiển thị", "data": "IsEnabled", "class": "text-center", "width": "10%",
                        "render": function (data, type, row, meta) {
                            return data == true ? '<span class="label label-sm label-success">Hiển thị</span>' : '<span class="label label-sm label-default">Ẩn</span>';
                        }
                    }
                ]
            });
        }

        function reloadTableData() {
            themeService.getall().then(function (obj) {
                if (obj) {
                    $scope.data = obj;
                    var table = tbl.DataTable();
                    var settings = table.settings();
                    // clear data
                    table.clear();
                    // add new data
                    table.rows.add($scope.data);
                    // redraw table
                    table.draw();
                }
            });

        }

        function doDelete(id) {
            themeService.delete(id).then(function (result) {
                if (result) {
                    webcloud.success(webcloud.text.deleteSuccess);
                    reloadTableData();
                }
            });
        };

        init();

    };



    themeController.$inject = ['$scope', '$location', 'authService', 'themeService'];

    angular.module('webcloudAdminApp').controller('themeController', themeController);

}());
