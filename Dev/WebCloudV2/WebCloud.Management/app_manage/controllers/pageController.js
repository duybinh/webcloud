﻿(function () {

    var pageController = function ($scope, $location, authService, pageService) {
        var tbl = $('#pageTable');

        $scope.title = "Trang chức năng";
        $scope.data = [];

        $scope.delete = function (id) {
            webcloud.confirm(webcloud.text.deleteConfirmTitle,
                   webcloud.text.deleteConfirmContent, function () {
                       doDelete(id, false);
                   });
        };

        function init() {
            buildTable();
            reloadTableData();
            $('a.func-delete').die("click");
            $('a.func-delete').live('click', function () {
                var id = $(this).attr('data');
                $scope.delete(id);
            });
        }

        function buildTable() {
            tbl.dataTable({
                "data": $scope.data,
                //"order": [1, 'asc'],
                "columns": [
                    {
                        "title": "", "data": "PageID", "width": "90", "orderable": false,
                        "render": function (data, type, row, meta) {
                            return '<a title="Sửa" class="btn btn-sm green tooltips" href="#/page/edit/' + data + '"><i class="icon-pencil"></i></a>&nbsp;' +
                                   '<a title="Module" class="btn btn-sm blue tooltips" href="#/page/' + data + '/module"><i class="icon-th-large"></i></a>&nbsp;' +
                                   '<a title="Xóa" class="btn btn-sm default tooltips func-delete"  data="' + data + '"><i class="icon-trash"></i></a>';
                        }
                    },
                    { "title": "Id", "data": "PageID", "width": "10%" },
                    {
                        "title": "Tên", "data": "Name",
                        "render": function (data, type, row, meta) {
                            if (row.Level > 0) {
                                var padding = '<span style="font-size: 12px;margin-left: ' + row.Level * 25 + 'px;">└-- </span>';
                                data = padding + data;
                            }
                            return data;
                        }
                    },
                    {
                        "title": "Layout", "data": "LayoutID", "width": "30%",
                        "render": function (data, type, row, meta) {
                            return '<a href="#/ui/layout/edit/' + data + '">' + data + '</a>';
                        }
                    },
                    {
                        "title": "Hiển thị", "data": "IsEnabled", "class": "text-center", "width": "10%",
                        "render": function (data, type, row, meta) {
                            return data == true ? '<span class="label label-sm label-success">Hiển thị</span>' : '<span class="label label-sm label-default">Ẩn</span>';
                        }
                    }
                ]
            });
        }

        function reloadTableData() {
            pageService.getall().then(function (obj) {
                if (obj) {
                    $scope.data = obj;
                    var table = tbl.DataTable();
                    var settings = table.settings();
                    // clear data
                    table.clear();
                    // add new data
                    table.rows.add($scope.data);
                    // redraw table
                    table.draw();
                }
            });

        }

        function doDelete(id) {
            pageService.delete(id).then(function (result) {
                if (result) {
                    webcloud.success(webcloud.text.deleteSuccess);
                    reloadTableData();
                }
            });
        };

        init();

    };


    pageController.$inject = ['$scope', '$location', 'authService', 'pageService'];

    angular.module('webcloudAdminApp').controller('pageController', pageController);

}());
