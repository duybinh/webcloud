﻿(function () {

    var controller = function ($scope, $location, $stateParams, pageService) {
        var id = ($stateParams.id) ? $stateParams.id : '';
        var copy = id && ('copy' == $stateParams.m);

        $scope.isNew = (id === '' || copy);
        $scope.title = $scope.isNew ? 'Thêm trang chức năng' : 'Sửa trang chức năng';
        $scope.saveButton = $scope.isNew ? 'Thêm' : 'Sửa';
        $scope.obj = {};

        $scope.save = function () {
            if ($scope.pageForm.$valid) {
                if ($scope.isNew) {
                    pageService.add($scope.obj).then(function (result) {
                        if (result) {
                            webcloud.success("Thêm thành công");
                            back();
                        }
                    });
                }
                else {
                    pageService.edit($scope.obj).then(function (result) {
                        if (result) {
                            webcloud.success("Cập nhật thành công");
                            back();
                        }
                    });
                }
            }
        };

        $scope.cancel = function () {
            back();
        };

        $scope.updateSeoName = function () {
            $scope.obj.SeoName = webcloud.toSeoString($scope.obj.Name);
        };

        function init() {
            if (id) {
                pageService.get(id).then(function (obj) {
                    if (obj) {
                        $scope.obj = obj;
                        if (copy) {
                            $scope.obj.PageID += '_copy';
                            $scope.obj.Name += ' - copy';
                        }
                    }
                });
            } else {
                pageService.new().then(function (newObj) {
                    $scope.obj = newObj;
                });
            }


        }

        function back() {
            $location.url('/page');
        };

        init();
        
    };

    controller.$inject = ['$scope', '$location', '$stateParams', 'pageService'];

    angular.module('webcloudAdminApp').controller('pageAddeditController', controller);

}());
