﻿(function () {

    var layoutController = function ($scope, $location, authService, layoutService) {
        var tbl = $('#layoutTable');

        $scope.title = "Layout template";

        $scope.data = [];

        $scope.delete = function (id) {
            webcloud.confirm(webcloud.text.deleteConfirmTitle,
                   webcloud.text.deleteConfirmContent, function () {
                       doDelete(id, false);
                   });
        };

        function init() {
            buildTable();
            reloadTableData();
            $('a.func-delete').die("click");
            $('a.func-delete').live('click', function () {
                var id = $(this).attr('data');
                $scope.delete(id);
            });
        }

        function buildTable() {
            tbl.dataTable({
                "data": $scope.data,
                "order": [1, 'asc'],
                "columns": [
                    {
                        "title": "", "data": "LayoutID", "width": "90", "orderable": false,
                        "render": function (data, type, row, meta) {
                            return '<a title="Sửa" class="btn btn-sm green tooltips" href="#/ui/layout/edit/' + data + '"><i class="icon-pencil"></i></a>&nbsp;' +
                                   '<a title="Copy" class="btn btn-sm blue tooltips" href="#/ui/layout/copy/' + data + '"><i class="icon-copy"></i></a>&nbsp;' +
                                   '<a title="Xóa" class="btn btn-sm default tooltips func-delete"  data="' + data + '"><i class="icon-trash"></i></a>';
                        }
                    },
                    { "title": "Id", "data": "LayoutID", "width": "15%" },
                    { "title": "Tên", "data": "Name" },
                    //{ "title": "Mô tả", "data": "Description" },
                    {
                        "title": "Loại", "data": "Type", "width": "15%",
                        //"render": function (data, type, row, meta) {
                        //    return layoutService.typeName(data);
                        //}
                    },
                    {
                        "title": "Hiển thị", "data": "IsEnabled", "class": "text-center", "width": "10%",
                        "render": function (data, type, row, meta) {
                            return data == true ? '<span class="label label-sm label-success">Hiển thị</span>' : '<span class="label label-sm label-default">Ẩn</span>';
                        }
                    }
                ]
            }).rowGrouping({
                bExpandableGrouping: true,
                //asExpandedGroups: ["Other Browsers", "Trident"],
                iGroupingColumnIndex: 3,
                //sGroupBy: "letter",
                bHideGroupingColumn: true
            });
        }

        function reloadTableData() {
            layoutService.getall().then(function (obj) {
                if (obj) {
                    for (var i = 0; i < obj.length; i++) {
                        obj[i].Type = layoutService.typeName(obj[i].Type);
                    }
                    $scope.data = obj;
                    var table = tbl.DataTable();
                    var settings = table.settings();
                    // clear data
                    table.clear();
                    // add new data
                    table.rows.add($scope.data);
                    // redraw table
                    table.draw();
                }
            });

        }

        function doDelete(id) {
            layoutService.delete(id).then(function (result) {
                if (result) {
                    webcloud.success(webcloud.text.deleteSuccess);
                    reloadTableData();
                }
            });
        };

        init();

    };


    layoutController.$inject = ['$scope', '$location', 'authService', 'layoutService'];

    angular.module('webcloudAdminApp').controller('layoutController', layoutController);

}());
