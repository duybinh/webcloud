﻿(function () {

    var controller = function ($scope, $location, $stateParams, themeService, fileService) {
        var filePanel = $('.box-file-addedit .portlet-body');
        $scope.create = function () {
            if ($scope.selected == null || $scope.selected.index == -1) return;
            webcloud.togglePanel(filePanel);
        };
        $scope.isEditingFile = false;
        $scope.objFile = {};
        $scope.filePanelTitle = "";
        $scope.fileType = '';
        $scope.editFile = function () {
            if ($scope.selected == null || $scope.selected.index == -1) return;

            var reImage = new RegExp('(\.css|\.js|\.xml|\.html|.\html|\.txt)$');
            if (!reImage.test($scope.selected.item.Name)) return;
            var fileExt = webcloud.getFileExt($scope.selected.item.Name);
            switch (fileExt) {
                case '.css':
                    $scope.fileType = 'css';
                    break;
                case '.js':
                    $scope.fileType = 'javascript';
                    break;
                case '.html':
                    $scope.fileType = 'html';
                    break;
                case '.htm':
                    $scope.fileType = 'html';
                    break;
                case '.xml':
                    $scope.fileType = 'xml';
                    break;
                default:
                    $scope.fileType = 'text';
                    break;
            }

            fileService.getfile(currentPath() + '/' + $scope.selected.item.Name).then(function (result) {
                if (result) {
                    $scope.isEditingFile = true;
                    $scope.filePanelTitle = "Sửa file";
                    $scope.objFile.Name = $scope.selected.item.Name;
                    $scope.objFile.Content = result;
                    webcloud.openPanel(filePanel);
                    webcloud.scrollTo(filePanel);
                }
            });
        };
        $scope.addFile = function () {
            $scope.isEditingFile = false;
            $scope.filePanelTitle = "Tạo file";
            $scope.objFile.Name = "";
            $scope.objFile.Content = "";
            webcloud.openPanel(filePanel);
            webcloud.scrollTo(filePanel);
        };
        $scope.cancelAddEdit = function () {
            webcloud.closePanel(filePanel);
        };
        $scope.save = function () {
            if ($scope.isEditingFile) {
                fileService.editFile(currentPath(), $scope.objFile).then(function (result) {
                    if (result) {
                        webcloud.success('Sửa file thành công');
                    }
                });
            } else {
                fileService.addFile(currentPath(), $scope.objFile).then(function (result) {
                    if (result) {
                        webcloud.success('Thêm file thành công');
                        $scope.files.splice(0, 0, result);
                        $scope.objFile.Name = "";
                        $scope.objFile.Content = "";
                        webcloud.closePanel(filePanel);
                    }
                });
            }
        };

        var id = ($stateParams.id) ? $stateParams.id : '';
        $scope.obj = {};
        $scope.title = 'Mẫu giao diện: ';
        $scope.files = [];
        $scope.paths = [];
        $scope.currentFolder = '';
        $scope.selected = null;
        $scope.uploadedFile = null;

        $scope.cancel = function () {
            back();
        };

        $scope.goto = function (subdir) {
            $scope.paths.splice($scope.paths.length, 0, subdir);
            $scope.currentFolder = currentPath();
            list();
        };

        $scope.parent = function (index) {
            $scope.paths.splice(index + 1, $scope.paths.length - (index + 1));
            $scope.currentFolder = currentPath();
            list();
        };

        $scope.delete = function () {
            if ($scope.selected == null || $scope.selected.index == -1) return;
            webcloud.confirm(webcloud.text.deleteConfirmTitle,
                 webcloud.text.deleteConfirmContent, function () {
                     fileService.delete(currentPath() + '/' + $scope.selected.item.Name, $scope.selected.item.IsDirectory).then(function (result) {
                         if (result) {
                             webcloud.success('Xóa thành công');
                             $scope.files.splice($scope.selected.index, 1);
                             $scope.selected = null;
                         }
                     });
                 });
        };

        $scope.rename = function () {
            if ($scope.selected == null || $scope.selected.index == -1) return;
            var ext = null;
            if (!$scope.selected.item.IsDirectory) {
                ext = webcloud.getFileExt($scope.selected.item.Name);
            }
            webcloud.prompt('Nhập tên mới', '', function (newname) {
                fileService.rename(currentPath() + '/' + $scope.selected.item.Name, newname, $scope.selected.item.IsDirectory).then(function (result) {
                    if (result) {
                        webcloud.success('Đổi tên thành công');
                        $scope.selected.item.Name = ext ? newname + ext : newname;
                    }
                });
            }, ext);
        };

        $scope.mkdir = function () {
            webcloud.prompt('Nhập tên thư mục mới', '', function (newname) {
                fileService.mkdir(currentPath(), newname).then(function (result) {
                    if (result) {
                        webcloud.success('Thêm thư mục mới thành công');
                        $scope.files.splice(0, 0, { Name: newname, IsDirectory: true });
                    }
                });
            });
        };

        $scope.uploadSuccess = function (file) {
            console.debug(angular.toJson(file));
            webcloud.success('Upload file thành công');
            $scope.files.splice(0, 0, file);
        };

        $scope.convertSize = function (bytes) {
            if (bytes < 1024) return webcloud.math.round(bytes, 2) + ' bytes';
            if (bytes < 1024 * 1024) return webcloud.math.round(bytes / 1024.0, 2) + ' kb';
            return webcloud.math.round(bytes / (1024.0 * 1024.0), 2) + ' mb';
        };

        $scope.thumb = function (file) {
            if (file.IsDirectory)
                return ' <i class="file-item-folder-icon glyphicon glyphicon-folder-open"></i>';

            var reImage = new RegExp('(\.gif|\.jpe?g|\.png)$');
            if (reImage.test(file.Name))
                return '<img class="thumb-img" src="http://localhost:14469/Handlers/ResourceHandler.ashx?location=themes&action=thumb&p=' + $scope.currentFolder + '/' + file.Name + '&w=70" />';

            reImage = new RegExp('(\.pdf)$');
            if (reImage.test(file.Name))
                return '<img src="thumb-pdf" src="/assets/img/file/file-pdf-128.png" />';

            reImage = new RegExp('(\.css)$');
            if (reImage.test(file.Name))
                return '<img src="thumb-css" src="/assets/img/file/file-css-128.png" />';

            reImage = new RegExp('(\.js)$');
            if (reImage.test(file.Name))
                return '<img src="thumb-js" src="/assets/img/file/file-script-128.png" />';

            reImage = new RegExp('(\.htm|\.html)$');
            if (reImage.test(file.Name))
                return '<img src="thumb-html" src="/assets/img/file/file-html-128.png" />';

            reImage = new RegExp('(\.doc|\.docx)$');
            if (reImage.test(file.Name))
                return '<img src="thumb-doc" src="/assets/img/file/file-doc-128.png" />';

            reImage = new RegExp('(\.xls|\.xlsx)$');
            if (reImage.test(file.Name))
                return '<img src="thumb-xls" src="/assets/img/file/file-xls-128.png" />';

            reImage = new RegExp('(\.txt)$');
            if (reImage.test(file.Name))
                return '<img src="thumb-txt" src="/assets/img/file/file-txt-128.png" />';

            reImage = new RegExp('(\.rar)$');
            if (reImage.test(file.Name))
                return '<img src="thumb-rar" src="/assets/img/file/file-rar-128.png" />';

            reImage = new RegExp('(\.xml)$');
            if (reImage.test(file.Name))
                return '<img src="thumb-xml" src="/assets/img/file/file-xml-128.png" />';

            return ' <i class="file-item-file-icon glyphicon glyphicon-file"></i>';

        };

        function init() {
            if (id) {
                themeService.get(id).then(function (obj) {
                    if (obj) {
                        $scope.obj = obj;
                        $scope.title += $scope.obj.Name;

                        $scope.goto(id);
                    }
                });
            }

            $('body').off('click', '.file-item');
            $('body').on('click', '.file-item', function () {
                if ($(this).hasClass('file-item-selected')) {
                    $(this).removeClass('file-item-selected');
                    $scope.selected = null;
                } else {
                    $('.file-item-selected').removeClass('file-item-selected');
                    $(this).addClass('file-item-selected');
                    $scope.selected = getItem($(this).attr('data'));
                }
                console.debug(angular.toJson($scope.selected));
            });
        }


        function back() {
            $location.url('/ui/theme');
        };

        function currentPath() {
            var path = 'Themes';
            if ($scope.paths.length > 0) {
                path += '/' + $scope.paths[0];
                for (var i = 1; i < $scope.paths.length; i++) {
                    path += '/' + $scope.paths[i];
                }
            }

            return path;
        }

        function list() {
            fileService.list( currentPath()).then(function (result) {
                $scope.files = result;
            });
            $scope.selected = null;
        }

        function getItem(name) {
            for (var i = 0 ; i < $scope.files.length; i++) {
                var item = $scope.files[i];
                if (item.Name == name) {
                    return {
                        index: i, item: item
                    };
                }
            }
            return {
                index: -1, item: null
            };
        }

        init();
    };

    controller.$inject = ['$scope', '$location', '$stateParams', 'themeService', 'fileService'];

    angular.module('webcloudAdminApp').controller('themeResourcesController', controller);

}());
