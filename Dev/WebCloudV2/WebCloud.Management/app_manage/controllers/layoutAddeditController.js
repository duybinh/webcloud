﻿(function () {

    var controller = function ($scope, $location, $routeParams, $stateParams, $window, $rootScope, layoutService, cacheService) {
        var id = ($stateParams.id) ? $stateParams.id : '';
        var copy = id && ('copy' == $stateParams.m);

        $scope.isNew = (id === '' || copy);
        $scope.title = $scope.isNew ? 'Thêm layout' : 'Sửa layout';
        $scope.saveButton = $scope.isNew ? 'Thêm' : 'Sửa';
        $scope.obj = {};
        $scope.obj.ModuleBaseConfigs = [];

        $scope.editorMode = 'javascript';
        $scope.$watch('obj.Type', function (newValue, oldValue) {
            if (newValue == 3) {
                $scope.editorMode = 'javascript';
            } else {
                $scope.editorMode = 'html';
            }
        });

        $scope.save = function () {
            if ($scope.layoutForm.$valid) {
                if ($scope.isNew) {
                    layoutService.add($scope.obj).then(function (result) {
                        if (result) {
                            // refresh layout cache
                            cacheService.refreshLayoutCache();
                            
                            webcloud.success("Thêm thành công");
                            back();
                        }
                    });
                }
                else {
                    layoutService.edit($scope.obj).then(function (result) {
                        if (result) {
                            // refresh layout cache
                            cacheService.refreshLayoutCache();
                            
                            webcloud.success("Cập nhật thành công");
                            back();
                        }
                    });
                }
            }
        };

        $scope.cancel = function () {
            back();
        };

        function init() {
            if (id) {
                layoutService.get(id).then(function (obj) {
                    if (obj) {
                        $scope.obj = obj;
                        if (copy) {
                            $scope.obj.LayoutID += '_copy';
                            $scope.obj.Name += ' - copy';
                        }
                    }
                });
            } else {
                layoutService.new().then(function (newObj) {
                    $scope.obj = newObj;
                });
            }
        }

        function back() {
            $window.history.back();
        };

        init();
    };

    controller.$inject = ['$scope', '$location', '$routeParams', '$stateParams', '$window', '$rootScope', 'layoutService', 'cacheService'];

    angular.module('webcloudAdminApp').controller('layoutAddeditController', controller);

}());
