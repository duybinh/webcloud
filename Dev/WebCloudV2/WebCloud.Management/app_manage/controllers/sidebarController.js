﻿(function () {

    var sidebarController = function ($scope, $location, currentUser) {
        $scope.highlight = function (path) {
            return $location.path().indexOf(path) >= 0;
        };
        
        var hasRole = function (role) {
            if (role === '*') return true;

            if (currentUser && currentUser.Roles) {
                return currentUser.Roles.indexOf(role) > -1;
            }
            return false;
        };

        var home = {
            title: 'Trang chủ',
            url: '/home',
            icon: 'icon-home',
            itemClass: 'start',
            role: 'Admin',
            childs: []
        };
        
        var page = {
            title: 'Trang chức năng',
            url: '/page',
            icon: 'icon-file-text',
            itemClass: '',
            role: 'Admin',
            childs: []
        };
        var module = {
            title: 'Module chức năng',
            url: '/module-base',
            icon: 'icon-th-large',
            itemClass: '',
            role: 'Admin',
            childs: []
        };
        var pageModule = {
            title: 'Bố cục, nội dung trang',
            url: '/page-module',
            icon: 'icon-sitemap',
            itemClass: '',
            role: 'Admin',
            childs: [page, module]
        };

        var layout = {
            title: 'Layout template',
            url: '/ui/layout',
            icon: 'icon-align-justify',
            itemClass: '',
            role: 'Admin',
            childs: []
        };
        var theme = {
            title: 'Mẫu giao diện',
            url: '/ui/theme',
            icon: 'icon-eye-open',
            itemClass: '',
            role: 'RootAdmin',
            childs: []
        };
        var ui = {
            title: 'Giao diện',
            url: '/ui',
            icon: 'icon-bookmark-empty',
            itemClass: '',
            role: 'Admin',
            childs: [layout, theme]
        };
        
        var config = {
            title: 'Thiết lập cấu hình',
            url: '/configuration',
            icon: 'icon-cogs',
            itemClass: '',
            role: 'RootAdmin',
            childs: []
        };
        
        var resource = {
            title: 'Resource content',
            url: '/resource',
            icon: 'icon-cogs',
            itemClass: '',
            role: 'RootAdmin',
            childs: []
        };
        
        $scope.menuItems = [home, pageModule, ui, config, resource];
        $scope.displayItems = [];
        
        var filterChild = function (item) {
            if (!item.childs || item.childs.length == 0) return;

            for (var i = 0; i < item.childs.length; i++) {
                if (!hasRole(item.childs[i].role)) {
                    item.childs.splice(i, 1);
                    i = i - 1;
                } else {
                    filterChild(item.childs[i]);
                }
            }
        };
        
        for (var i = 0; i < $scope.menuItems.length; i++) {
            var item = $scope.menuItems[i];
            if (hasRole(item.role)) {
                $scope.displayItems.push(item);
                filterChild(item);
            }
        }

       
    };

    sidebarController.$inject = ['$scope', '$location', 'currentUser'];

    angular.module('webcloudAdminApp').controller('sidebarController', sidebarController);

}());
