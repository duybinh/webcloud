﻿(function () {

    var controller = function ($scope, $location, $stateParams, $state, $window, $rootScope, pageService, moduleService, cacheService) {
        var id = ($stateParams.id) ? $stateParams.id : '';
        var commitSuccess = false;

        $scope.displayAsModal = $stateParams.displayAsModal;

        $scope.isNew = id === '';
        $scope.title = $scope.isNew ? 'Thêm module thuộc trang' : 'Sửa module thuộc trang';
        $scope.saveButton = $scope.isNew ? 'Thêm' : 'Sửa';
        $scope.obj = {};
        $scope.moduleConfigs = [];

        $scope.save = function () {
            if (true || $scope.moduleInPageForm.$valid) {
                // update config
                $scope.obj.ModuleConfigs.splice(0, $scope.obj.ModuleConfigs.length);
                for (var i = 0 ; i < $scope.moduleConfigs.length; i++) {
                    var config = $scope.moduleConfigs[i];
                    $scope.obj.ModuleConfigs.push({
                        ModuleBaseID: config.ModuleBaseID,
                        ModuleBaseConfigID: config.ModuleBaseConfigID,
                        Value: config.Value,
                    });
                }
                if ($scope.isNew) {
                    pageService.addModule($scope.obj).then(function (result) {
                        if (result) {
                            webcloud.success("Thêm thành công");
                            // refresh cache in frontend site
                            cacheService.refreshModuleCache();
                            goBack(true);
                        }
                    });
                }
                else {
                    pageService.editModule($scope.obj).then(function (result) {
                        if (result) {
                            webcloud.success("Cập nhật thành công");
                            // refresh cache in frontend site
                            cacheService.refreshModuleCache();
                            goBack(true);
                        }
                    });
                }
            }
        };

        $scope.cancel = function () {
            goBack(false);
        };


        function init() {
            if ($scope.displayAsModal) {
                $('#moduleInPageForm').appendTo("#moduleInPageFormModal .modal-body");
                $('#moduleInPageFormModal').modal('show');
                $('#moduleInPageFormModal').on('hidden.bs.modal', function (e) {
                    back();
                });
            }

            if (id) {
                pageService.getModule(id).then(function (obj) {
                    if (obj) {
                        $scope.obj = obj;
                    }
                });
            } else {
                pageService.newModule().then(function (newObj) {
                    $scope.obj = newObj;
                    $scope.obj.PageID = $stateParams.pageId;
                    if ($stateParams.moduleBaseId)
                        $scope.obj.ModuleBaseID = $stateParams.moduleBaseId;
                    if ($stateParams.section)
                        $scope.obj.Section = $stateParams.section;
                });
            }
        }

        function goBack(result) {
            commitSuccess = result;
            if ($scope.displayAsModal) {
                $('#moduleInPageFormModal').modal('hide');
            } else {
                back();
            }
        };

        function back() {
            $window.history.back();
            // callback
            if (commitSuccess) {
                var onSuccess = $stateParams.onSuccess;
                //var onSuccess = $state.current.data.onSuccess;
                if (onSuccess) {
                    onSuccess();
                }
            } else {
                var onCancel = $stateParams.onCancel;
                //var onCancel = $state.current.data.onCancel;
                if (onCancel) {
                    onCancel();
                }
            }
        };

        function loadConfigs(moduleBaseId) {
            $scope.moduleConfigs.splice(0, $scope.moduleConfigs.length);

            if (!moduleBaseId || moduleBaseId == "")
                return;

            moduleService.get(moduleBaseId).then(function (obj) {
                if (!obj)
                    return;

                for (var i = 0 ; i < obj.ModuleBaseConfigs.length; i++) {
                    var config = obj.ModuleBaseConfigs[i];
                    var tmp = jQuery.grep($scope.obj.ModuleConfigs, function (element, index) {
                        return element.ModuleBaseConfigID == config.ModuleBaseConfigID;
                    });

                    if (tmp.length > 0) {
                        config.Value = tmp[0].Value;
                    } else {
                        config.Value = config.DefaultValue;
                    }
                    console.debug(config);
                    $scope.moduleConfigs.push(config);
                }
                console.debug($scope.moduleConfigs);
            });
        }

        $scope.$watch('obj.ModuleBaseID', function (newValue, oldValue) {
            loadConfigs(newValue);
        });

        init();

    };

    controller.$inject = ['$scope', '$location', '$stateParams', '$state', '$window', '$rootScope', 'pageService', 'moduleService', 'cacheService'];

    angular.module('webcloudAdminApp').controller('moduleInPageAddeditController', controller);

}());
