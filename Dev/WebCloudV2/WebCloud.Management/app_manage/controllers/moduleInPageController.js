﻿(function () {

    var moduleInPageController = function ($scope, $location, $stateParams, authService, pageService) {
        var tbl = $('#moduleInPageTable');
        $scope.pageId = $stateParams.pageId;
        console.debug($scope.pageId);

        $scope.title = "Cài đặt module thuộc trang";
        $scope.data = [];

        $scope.delete = function (id) {
            webcloud.confirm(webcloud.text.deleteConfirmTitle,
                   webcloud.text.deleteConfirmContent, function () {
                       doDelete(id, false);
                   });
        };

        $scope.newModule = function () {
            $state.go('.new', $stateParams);
        };

        function init() {
            buildTable();
            reloadTableData();
            $('a.func-delete').die("click");
            $('a.func-delete').live('click', function () {
                var id = $(this).attr('data');
                $scope.delete(id);
            });
        }

        function buildTable() {
            tbl.dataTable({
                "data": $scope.data,
                "order": [2, 'asc'],
                "columns": [
                    {
                        "title": "", "data": "ModuleID", "width": "90", "orderable": false,
                        "render": function (data, type, row, meta) {
                            return '<a title="Sửa" class="btn btn-sm green tooltips" href="#/page/' + $scope.pageId + '/module/edit/' + data + '"><i class="icon-pencil"></i></a>&nbsp;' +
                                   '<a title="Xóa" class="btn btn-sm default tooltips func-delete"  data="' + data + '"><i class="icon-trash"></i></a>';
                        }
                    },
                    {
                        "title": "Tên", "data": "Name",
                        "render": function (data, type, row, meta) {
                            return '<img src="' + webcloud.thumbUrl(row.ModuleBase.Thumbnail, "sites", 32, 32) + '" style="" /> ' + data;
                        }
                    },
                    //{ "title": "Id", "data": "ModuleID", "width": "10%" },
                    { "title": "Vị trí", "data": "Section", "class": "text-center", "width": "10%" },
                    { "title": "Thứ tự", "data": "SortOrder", "class": "text-center", "width": "10%" },
                    {
                        "title": "Trên mọi trang", "data": "OnAllPage", "class": "text-center", "width": "10%",
                        "render": function (data, type, row, meta) {
                            return data == true ? '<span class=""><i class=" icon-pushpin" style="font-size:16px; color: #2c87d6" /></span>' : '';
                    }},
                    {
                        "title": "Hiển thị", "data": "IsEnabled", "class": "text-center", "width": "10%",
                        "render": function (data, type, row, meta) {
                            return data == true ? '<span class="label label-sm label-success">Hiển thị</span>' : '<span class="label label-sm label-default">Ẩn</span>';
                        }
                    }
                ]
            }).rowGrouping({
                bExpandableGrouping: true,
                //asExpandedGroups: ["Other Browsers", "Trident"],
                iGroupingColumnIndex: 2,
                //sGroupBy: "letter",
                bHideGroupingColumn: true
            });
        }

        function reloadTableData() {
            pageService.getAllModule($scope.pageId).then(function (obj) {
                if (obj) {
                    $scope.data = obj;
                    var table = tbl.DataTable();
                    var settings = table.settings();
                    // clear data
                    table.clear();
                    // add new data
                    table.rows.add($scope.data);
                    // redraw table
                    table.draw();
                }
            });

        }

        function doDelete(id) {
            pageService.deleteModule(id).then(function (result) {
                if (result) {
                    webcloud.success(webcloud.text.deleteSuccess);
                    reloadTableData();
                }
            });
        };

        init();

    };


    moduleInPageController.$inject = ['$scope', '$location', '$stateParams', 'authService', 'pageService'];

    angular.module('webcloudAdminApp').controller('moduleInPageController', moduleInPageController);

}());
