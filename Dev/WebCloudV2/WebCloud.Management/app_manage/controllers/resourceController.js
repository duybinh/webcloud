﻿(function () {

    var resourceController = function ($scope, $location, authService, resourceService) {
        var tbl = $('#resourceTable');
        var edtDialog = $('#resourceEditorDialog');
        $scope.title = "Nội dung";
        $scope.data = [];
        $scope.objEdit = {
            WebsiteId: null,
            LanguageId: null
        };
        $scope.editorType = 'String';
        $scope.selectedLang = null;
      
        $scope.delete = function (id) {
            webcloud.confirm(webcloud.text.deleteConfirmTitle,
                   webcloud.text.deleteConfirmContent, function () {
                       doDelete(id, false);
                   });
        };

        $scope.update = function () {
            $scope.objEdit.LanguageId = $scope.selectedLang;

            resourceService.updateValue($scope.objEdit).then(function (result) {
                if (result) {
                    webcloud.success("Cập nhật thành công");
                    edtDialog.modal('hide');
                    reloadTableData();
                }
            });
        };

        $scope.$watch('selectedLang', function (newValue, oldValue) {
            reloadTableData();
        });
        
        function init() {
            buildTable();
            reloadTableData();
            $('a.func-delete').die("click");
            $('a.func-delete').live('click', function () {
                var id = $(this).attr('data');
                $scope.delete(id);
            });

            $('.resource-value-container').die("click");
            $('.resource-value-container').live('click', function () {
                $scope.objEdit.ResourceId = $(this).attr('code');
                $scope.objEdit.Name = $(this).attr('name');
                $scope.objEdit.Value = $(this).html();
                $scope.objEdit.EditorType = $(this).attr('displayAsHtml') == 'true' ? 'Html' : 'String';

                edtDialog.modal('show');

                $scope.$apply();
            });
        }

        function buildTable() {
            tbl.dataTable({
                "data": $scope.data,
                "order": [1, 'asc'],
                "columns": [
                    {
                        "title": "", "data": "ResourceID", "width": "8%", "orderable": false,
                        "render": function (data, type, row, meta) {
                            return '<a title="Sửa" class="btn btn-sm green tooltips" href="#/resource/edit/' + data + '"><i class="icon-pencil"></i></a>&nbsp;' +
                                   '<a title="Xóa" class="btn btn-sm default tooltips func-delete" data="' + data + '"><i class="icon-trash"></i></a>';
                        }
                    },
                    { "title": "Code", "data": "ResourceID", "width": "10%" },
                    { "title": "Nhóm", "data": "Group", "width": "10%" },
                    {
                        "title": "Mô tả", "data": "Name", "width": "20%",
                        "render": function (data, type, row, meta) {
                            return '<div class="tooltips" title="' + row.Description + '">' + data + '</div>';
                        }
                    },
                    {
                        "title": "Nội dung", "data": "Value",
                        "render": function (data, type, row, meta) {
                            return '<div class="resource-value-container" name="' + row.Name + '" code="' + row.ResourceID + '" displayAsHtml="' + row.DisplayAsHtml + '">' + data + '</div>';
                        }
                    }
                ]
            });
        }

        var bindData = function(obj) {
            if (obj) {
                $scope.data = obj;
                var table = tbl.DataTable();
                // clear data
                table.clear();
                // add new data
                table.rows.add($scope.data);
                // redraw table
                table.draw();
            }
        };
        
        function reloadTableData() {
            if ($scope.selectedLang) {
                resourceService.getInLang($scope.selectedLang).then(bindData);
            } else {
                resourceService.getall().then(bindData);
            }
        }

        function doDelete(id) {
            resourceService.delete(id).then(function (result) {
                if (result) {
                    webcloud.success(webcloud.text.deleteSuccess);
                    reloadTableData();
                }
            });
        };

        init();

    };

    resourceController.$inject = ['$scope', '$location', 'authService', 'resourceService'];

    angular.module('webcloudAdminApp').controller('resourceController', resourceController);

}());
