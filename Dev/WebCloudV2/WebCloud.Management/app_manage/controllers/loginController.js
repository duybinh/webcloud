﻿(function () {

    var loginController = function ($scope, $location, $routeParams, authService, appSettings) {
        $scope.username = null;
        $scope.password = null;
        $scope.rememberMe = false;
        $scope.login = function () {
            var loginData = {
                userName: $scope.username,
                password: $scope.password
            };
            authService.login(loginData).then(function (response) {
                // redirect
                var path = '/';
                var redirect = webcloud.getUrlParam('redirect');
                if (redirect != '')
                    path = path + '#' + redirect;

                window.location = path;

            }, function (err) {
                webcloud.error(err.error_description);
            });
        };
    };

    loginController.$inject = ['$scope', '$location', '$routeParams', 'authService', 'appSettings'];

    angular.module('webcloudAdminApp').controller('loginController', loginController);

}());