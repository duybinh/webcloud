﻿(function () {

    var topbarController = function ($scope, $location, authService, currentUser) {
        // check authenticated
        if (!authService.isAuthenticated) {
//            window.location = '/login';
        } else {

            // load user profile
            $scope.user = currentUser;
        }

        $scope.logout = function () {
            if (authService.logout())
                redirectToLogin();
        };

        function redirectToLogin() {
            var path = '/login?redirect=' + $location.$$path;
            window.location = path;
        }

        $scope.$on('redirectToLogin', function () {
            redirectToLogin();
        });
    };

    topbarController.$inject = ['$scope', '$location', 'authService', 'currentUser'];

    angular.module('webcloudAdminApp').controller('topbarController', topbarController);

}());
