﻿(function () {

    var moduleController = function ($scope, $location, authService, moduleService) {
        var tbl = $('#moduleBaseTable');

        $scope.title = "Module chức năng";
        $scope.data = [];

        $scope.delete = function (id) {
            webcloud.confirm(webcloud.text.deleteConfirmTitle,
                   webcloud.text.deleteConfirmContent, function () {
                       doDelete(id, false);
                   });
        };

        function init() {
            buildTable();
            reloadTableData();
            $('a.func-delete').die("click");
            $('a.func-delete').live('click', function () {
                var id = $(this).attr('data');
                $scope.delete(id);
            });
        }

        function buildTable() {
            tbl.dataTable({
                "data": $scope.data,
                //"order": [1, 'asc'],
                "columns": [
                    {
                        "title": "", "data": "ModuleBaseID", "width": "50", "orderable": false,
                        "render": function (data, type, row, meta) {
                            return '<a title="Sửa" class="btn btn-sm green tooltips" href="#/module-base/edit/' + data + '"><i class="icon-pencil"></i></a>&nbsp;' +
                                   '<a title="Xóa" class="btn btn-sm default tooltips func-delete"  data="' + data + '"><i class="icon-trash"></i></a>';
                        }
                    },
                    {
                        "title": "", "data": "Thumbnail", "class": "", 
                        "render": function (data, type, row, meta) {
                            return '<img src="' + webcloud.thumbUrl(data, "sites", 60, 60) + '" class="grid-img-cell module-thumbnail" /> '
                                + '<p><span class="ingrid-module-id">' + row.ModuleBaseID + '</span><br/><span class=""ingrid-module-name">' + row.Name + '<span></p>';
                        }
                    },
                    //{ "title": "Id", "data": "ModuleBaseID", "width": "10%" },
                    //{ "title": "Tên", "data": "Name" },
                    { "title": "Nhóm", "data": "Group", "width": "10%" },
                    {
                        "title": "Layout", "data": "LayoutID", "width": "20%",
                        "render": function (data, type, row, meta) {
                            return '<a href="#/ui/layout/edit/' + data + '">' + data + '</a>';
                        }
                    },
                    {
                        "title": "Hiển thị", "data": "IsEnabled", "class": "text-center", "width": "10%",
                        "render": function (data, type, row, meta) {
                            return data == true ? '<span class="label label-sm label-success">Hiển thị</span>' : '<span class="label label-sm label-default">Ẩn</span>';
                        }
                    }
                ]
            }).rowGrouping({
                bExpandableGrouping: true,
                //asExpandedGroups: ["Other Browsers", "Trident"],
                iGroupingColumnIndex: 2,
                //sGroupBy: "letter",
                bHideGroupingColumn: true
            });
        }

        function reloadTableData() {
            moduleService.getall().then(function (obj) {
                if (obj) {
                    $scope.data = obj;
                    var table = tbl.DataTable();
                    var settings = table.settings();
                    // clear data
                    table.clear();
                    // add new data
                    table.rows.add($scope.data);
                    // redraw table
                    table.draw();
                }
            });

        }

        function doDelete(id) {
            moduleService.delete(id).then(function (result) {
                if (result) {
                    webcloud.success(webcloud.text.deleteSuccess);
                    reloadTableData();
                }
            });
        };

        init();

    };


    moduleController.$inject = ['$scope', '$location', 'authService', 'moduleService'];

    angular.module('webcloudAdminApp').controller('moduleController', moduleController);

}());
