﻿(function () {

    var configurationController = function ($scope, $location, authService, configurationService) {
        var tbl = $('#configurationTable');

        $scope.title = "Cấu hình";
        $scope.data = [];

        $scope.delete = function (id) {
            webcloud.confirm(webcloud.text.deleteConfirmTitle,
                   webcloud.text.deleteConfirmContent, function () {
                       doDelete(id, false);
                   });
        };

        function init() {
            buildTable();
            reloadTableData();
            $('a.func-delete').die("click");
            $('a.func-delete').live('click', function () {
                var id = $(this).attr('data');
                $scope.delete(id);
            });
        }

        function buildTable() {
            tbl.dataTable({
                "data": $scope.data,
                "order": [1, 'asc'],
                "columns": [
                    {
                        "title": "", "data": "ConfigID", "width": "8%", "orderable": false,
                        "render": function (data, type, row, meta) {
                            return '<a title="Sửa" class="btn btn-sm green tooltips" href="#/configuration/edit/' + data + '"><i class="icon-pencil"></i></a>&nbsp;' +
                                   '<a title="Xóa" class="btn btn-sm default tooltips func-delete"  data="' + data + '"><i class="icon-trash"></i></a>';
                        }
                    },
                    { "title": "Code", "data": "ConfigID", "width": "10%" },
                    { "title": "Tên", "data": "Name", "width": "25%" },
                    { "title": "Kiểu", "data": "DataType", "width": "8%" },
                    { "title": "Giá trị mặc định", "data": "DefaultValue" },
//                    {
//                        "title": "H.thị", "data": "IsEnabled", "class": "text-center", "width": "7%",
//                        "render": function (data, type, row, meta) {
//                            return data == true ? '<span class="label label-sm label-success">Hiển thị</span>' : '<span class="label label-sm label-default">Ẩn</span>';
//                        }
//                    }
                ]
            });
        }

        function reloadTableData() {
            configurationService.getall().then(function (obj) {
                if (obj) {
                    $scope.data = obj;
                    var table = tbl.DataTable();
                    var settings = table.settings();
                    // clear data
                    table.clear();
                    // add new data
                    table.rows.add($scope.data);
                    // redraw table
                    table.draw();
                }
            });

        }

        function doDelete(id) {
            configurationService.delete(id).then(function (result) {
                if (result) {
                    webcloud.success(webcloud.text.deleteSuccess);
                    reloadTableData();
                }
            });
        };

        init();

    };



    configurationController.$inject = ['$scope', '$location', 'authService', 'configurationService'];

    angular.module('webcloudAdminApp').controller('configurationController', configurationController);

}());
