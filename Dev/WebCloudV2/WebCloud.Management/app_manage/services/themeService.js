﻿(function () {

    var themeFactory = function ($http, $q) {
        var factory = {};
        $http.defaults.headers.common.AuthToken = $.cookie('AuthToken');

        factory.new = function () {
            return $q.when({
                ThemeCode: null,
                Name: null,
                Description: null,
                ThemeCategoryID: null,
                BasedTheme: null,
                Author: null,
                RolesToView: '*',
                RolesToInstall: '*',
                IsEnabled: true,
                SortOrder: 0,
            });
        };

        factory.getall = function () {
            return $http.get(webcloud.config.apiUrl + 'theme').then(
              function (results) {
                  if (results.data.IsSuccess) {
                      return results.data.Data;
                  } else {
                      webcloud.error(results.data);
                      return null;
                  }
              });
        };

        factory.get = function (id) {
            return $http.get(webcloud.config.apiUrl + 'theme/' + id).then(
              function (results) {
                  if (results.data.IsSuccess) {
                      return results.data.Data;
                  } else {
                      webcloud.error(results.data);
                      return null;
                  }
              });
        };

        factory.add = function (obj) {
            console.debug(angular.toJson(obj));
            return $http.post(webcloud.config.apiUrl + 'theme/add', obj).then(
              function (results) {
                  if (results.data.IsSuccess) {
                      return true;
                  } else {
                      webcloud.error(results.data);
                      return null;
                  }
              });
        };

        factory.edit = function (obj) {
            console.debug(angular.toJson(obj));
            return $http.post(webcloud.config.apiUrl + 'theme/edit', obj).then(
              function (results) {
                  if (results.data.IsSuccess) {
                      return true;
                  } else {
                      webcloud.error(results.data);
                      return false;
                  }
              });
        };

        factory.delete = function (id) {
            var url = webcloud.config.apiUrl + 'theme/delete/' + id;
            return $http.get(url).then(
               function (results) {
                   if (results.data.IsSuccess) {
                       return true;
                   } else {
                       webcloud.error(results.data);
                       return false;
                   }
               });
        };
        
        return factory;
    };

    themeFactory.$inject = ['$http', '$q'];

    angular.module('webcloudAdminApp').factory('themeService', themeFactory);

}());