﻿(function () {

    angular.module('webcloudAdminApp')
        .config(['$httpProvider', function ($httpProvider) {

            var httpInterceptor = function ($q, $rootScope) {

                var success = function (response) {
                    return response;
                };

                var error = function (res) {
                    // show error
                    webcloud.error(res.data.Message);
                    if (res.status === 401) {
                        //Raise event so listener (sidebarController) can act on it
                        //$rootScope.$broadcast('redirectToLogin', null);
                        return $q.reject(res);
                    }
                    return $q.reject(res);
                };

                return function (promise) {
                    return promise.then(success, error);
                };

            };

            httpInterceptor.$inject = ['$q', '$rootScope'];

            $httpProvider.responseInterceptors.push(httpInterceptor);

        }]);

}());