﻿(function () {

    var resourceFactory = function ($http, $q) {
        var factory = {};
        $http.defaults.headers.common.AuthToken = $.cookie('AuthToken');

        factory.new = function () {
            return $q.when({
                ResourceID: null,
                WebsiteID: null,
                Group: '',
                Name: '',
                Description: null,
                RolesToView: '*',
                RolesToEdit: '*',
                DisplayAsHtml: false,
                Value: '',
                IsEnabled: true,
                SortOrder: 0,
            });
        };

        factory.getall = function () {
            return $http.get(webcloud.config.apiUrl + 'resource').then(
              function (results) {
                  if (results.data.IsSuccess) {
                      return results.data.Data;
                  } else {
                      webcloud.error(results.data);
                      return null;
                  }
              });
        };
        
        factory.getInLang = function (lang) {
            return $http.get(webcloud.config.apiUrl + 'resource/lang/' + lang).then(
              function (results) {
                  if (results.data.IsSuccess) {
                      return results.data.Data;
                  } else {
                      webcloud.error(results.data);
                      return null;
                  }
              });
        };

        factory.get = function (id) {
            return $http.get(webcloud.config.apiUrl + 'resource/' + id).then(
              function (results) {
                  if (results.data.IsSuccess) {
                      return results.data.Data;
                  } else {
                      webcloud.error(results.data);
                      return null;
                  }
              });
        };

        factory.add = function (obj) {
            console.debug(angular.toJson(obj));
            return $http.post(webcloud.config.apiUrl + 'resource/add', obj).then(
              function (results) {
                  if (results.data.IsSuccess) {
                      return true;
                  } else {
                      webcloud.error(results.data);
                      return null;
                  }
              });
        };

        factory.edit = function (obj) {
            console.debug(angular.toJson(obj));
            return $http.post(webcloud.config.apiUrl + 'resource/edit', obj).then(
              function (results) {
                  if (results.data.IsSuccess) {
                      return true;
                  } else {
                      webcloud.error(results.data);
                      return false;
                  }
              });
        };

        factory.updateValue = function (obj) {
            console.debug(angular.toJson(obj));
            return $http.post(webcloud.config.apiUrl + 'resource/update', obj).then(
              function (results) {
                  if (results.data.IsSuccess) {
                      return true;
                  } else {
                      webcloud.error(results.data);
                      return false;
                  }
              });
        };

        factory.delete = function (id) {
            var url = webcloud.config.apiUrl + 'resource/delete/' + id;
            return $http.get(url).then(
               function (results) {
                   if (results.data.IsSuccess) {
                       return true;
                   } else {
                       webcloud.error(results.data);
                       return false;
                   }
               });
        };
        
        return factory;
    };

    resourceFactory.$inject = ['$http', '$q'];

    angular.module('webcloudAdminApp').factory('resourceService', resourceFactory);

}());