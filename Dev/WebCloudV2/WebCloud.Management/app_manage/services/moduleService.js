﻿(function () {

    var moduleFactory = function ($http, $q) {
        var factory = {};
        $http.defaults.headers.common.AuthToken = $.cookie('AuthToken');

        factory.new = function () {
            return $q.when({
                ModuleBaseID: null,
                DisplayControl: null,
                ManageControl: null,
                Name: null,
                Description: null,
                LayoutID: null,
                RolesToInstall: '*',
                RolesToCreate: '*',
                RolesToEdit: '*',
                RolesToView: '*',
                Group: null,
                Thumbnail: null,
                Version: '1.0',
                Author: 'WebCloud Team',
                SortOrder: 0,
                IsEnabled: true,
                IsGroup: false,
                ModuleBaseConfigs: []
            });
        };
        
        factory.newModuleConfig = function () {
            return $q.when({
                ModuleBaseConfigID: '',
                Name: '',
                Description: '',
                Hint: '',
                DataType: 'String',
                DefaultValue: '',
                RolesToConfig: '*',
                SortOrder: 0,
                IsEnabled: true,
                AllLanguage: true,
            });
        };

        factory.getall = function () {
            return $http.get(webcloud.config.apiUrl + 'module-base').then(
              function (results) {
                  if (results.data.IsSuccess) {
                      return results.data.Data;
                  } else {
                      webcloud.error(results.data);
                      return null;
                  }
              });
        };

        factory.get = function (id) {
            return $http.get(webcloud.config.apiUrl + 'module-base/get/' + id).then(
              function (results) {
                  if (results.data.IsSuccess) {
                      return results.data.Data;
                  } else {
                      webcloud.error(results.data);
                      return null;
                  }
              });
        };

        factory.add = function (obj) {
            console.debug(angular.toJson(obj));
            //return;
            return $http.post(webcloud.config.apiUrl + 'module-base/add', obj).then(
              function (results) {
                  if (results.data.IsSuccess) {
                      return true;
                  } else {
                      webcloud.error(results.data);
                      return null;
                  }
              });
        };

        factory.edit = function (obj) {
            console.debug(angular.toJson(obj));
            //return;
            return $http.post(webcloud.config.apiUrl + 'module-base/edit', obj).then(
              function (results) {
                  if (results.data.IsSuccess) {
                      return true;
                  } else {
                      webcloud.error(results.data);
                      return false;
                  }
              });
        };

        factory.delete = function (id) {
            var url = webcloud.config.apiUrl + 'module-base/delete/' + id;
            return $http.get(url).then(
               function (results) {
                   if (results.data.IsSuccess) {
                       return true;
                   } else {
                       webcloud.error(results.data);
                       return false;
                   }
               });
        };

        return factory;
    };

    moduleFactory.$inject = ['$http', '$q'];

    angular.module('webcloudAdminApp').factory('moduleService', moduleFactory);

}());