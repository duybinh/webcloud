﻿(function () {

    var authFactory = function ($http, $q, $rootScope, constant) {
        var factory = {
            loginPath: '/login',
            isAuthenticated: false,
            authToken: null
        };

        // init
        if ($.cookie(constant.authTokenCookie)) {
            factory.authToken = $.cookie(constant.authTokenCookie);
            factory.isAuthenticated = true;
        }

        factory.login = function (username, password, remember) {

            return $http.post(constant.apiUrl + 'auth', { UserId: username, Password: password }).then(
                function (results) {
                    var authResult = results.data;
                    factory.isAuthenticated = authResult.IsSuccess;
                    if (authResult.IsSuccess) {
                        factory.authToken = authResult.Data;

                        if (remember) {
                            $.cookie(constant.authTokenCookie, angular.toJson(factory.authToken), { expires: constant.cookieExpires });
                        } else {
                            $.cookie(constant.authTokenCookie, JSON.stringify(factory.authToken));
                        }
                    }

                    return authResult;
                });
        };

        factory.logout = function () {
            // reset properties
            factory.isAuthenticated = false;
            factory.authToken = null;
            // clear cookies
            $.removeCookie(constant.authTokenCookie);
            return true;
        };

        return factory;
    };

    authFactory.$inject = ['$http', '$q', '$rootScope', 'constant'];

    angular.module('webcloudAdminApp').factory('authService', authFactory);

}());

