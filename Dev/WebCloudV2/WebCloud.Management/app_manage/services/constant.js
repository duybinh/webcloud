﻿(function () {

    var constant = {
        apiUrl: 'http://localhost:14469/api/',
        authTokenCookie: 'AuthToken',
        userProfileCookie: 'UserProfile',
        cookieExpires: 7
    };

    angular.module('webcloudAdminApp').value('constant', constant);

}());