﻿(function () {

    var pageFactory = function ($http, $q) {
        var factory = {};
        $http.defaults.headers.common.AuthToken = $.cookie('AuthToken');

        factory.new = function () {
            return $q.when({
                PageID: null,
                ParentID: null,
                LayoutID: null,
                Name: null,
                SeoName: null,
                MetaTitle: null,
                MetaKeyword: null,
                MetaDescription: null,
                SortOrder: 0,
                IsEnabled: true,
                IsGroup: false
            });
        };

        factory.getall = function () {
            return $http.get(webcloud.config.apiUrl + 'page').then(
              function (results) {
                  if (results.data.IsSuccess) {
                      return results.data.Data;
                  } else {
                      webcloud.error(results.data);
                      return null;
                  }
              });
        };

        factory.get = function (id) {
            return $http.get(webcloud.config.apiUrl + 'page/' + id).then(
              function (results) {
                  if (results.data.IsSuccess) {
                      return results.data.Data;
                  } else {
                      webcloud.error(results.data);
                      return null;
                  }
              });
        };

        factory.add = function (obj) {
            console.debug(angular.toJson(obj));
            //return;
            return $http.post(webcloud.config.apiUrl + 'page/add', obj).then(
              function (results) {
                  if (results.data.IsSuccess) {
                      return true;
                  } else {
                      webcloud.error(results.data);
                      return null;
                  }
              });
        };

        factory.edit = function (obj) {
            console.debug(angular.toJson(obj));
            //return;
            return $http.post(webcloud.config.apiUrl + 'page/edit', obj).then(
              function (results) {
                  if (results.data.IsSuccess) {
                      return true;
                  } else {
                      webcloud.error(results.data);
                      return false;
                  }
              });
        };

        factory.delete = function (id) {
            var url = webcloud.config.apiUrl + 'page/delete/' + id;
            return $http.get(url).then(
               function (results) {
                   if (results.data.IsSuccess) {
                       return true;
                   } else {
                       webcloud.error(results.data);
                       return false;
                   }
               });
        };

        factory.newModule = function () {
            return $q.when({
                ModuleID: null,
                ModuleBaseID: null,
                PageID: null,
                Name: null,
                Section: null,
                OnAllPage: false,
                IsBasic: false,
                IsEnabled: true,
                SortOrder: 0,
                ModuleConfigs: []
            });
        };

        factory.getAllModule = function (pageId) {
            return $http.get(webcloud.config.apiUrl + 'module/page/' + pageId).then(
              function (results) {
                  if (results.data.IsSuccess) {
                      return results.data.Data;
                  } else {
                      webcloud.error(results.data);
                      return null;
                  }
              });
        };

        factory.getModule = function (moduleId) {
            return $http.get(webcloud.config.apiUrl + 'module/' + moduleId).then(
              function (results) {
                  if (results.data.IsSuccess) {
                      return results.data.Data;
                  } else {
                      webcloud.error(results.data);
                      return null;
                  }
              });
        };

        factory.addModule = function (obj) {
            console.debug(angular.toJson(obj));
            //return;
            return $http.post(webcloud.config.apiUrl + 'module/add', obj).then(
              function (results) {
                  if (results.data.IsSuccess) {
                      return true;
                  } else {
                      webcloud.error(results.data);
                      return null;
                  }
              });
        };

        factory.editModule = function (obj) {
            console.debug(angular.toJson(obj));
            //return;
            return $http.post(webcloud.config.apiUrl + 'module/edit', obj).then(
              function (results) {
                  if (results.data.IsSuccess) {
                      return true;
                  } else {
                      webcloud.error(results.data);
                      return false;
                  }
              });
        };

        factory.updateModulePosition = function (obj) {
            console.debug(angular.toJson(obj));
            //return;
            return $http.post(webcloud.config.apiUrl + 'module/edit/posistion', obj).then(
              function (results) {
                  if (results.data.IsSuccess) {
                      return true;
                  } else {
                      webcloud.error(results.data);
                      return false;
                  }
              });
        };

        factory.deleteModule = function (moduleId) {
            var url = webcloud.config.apiUrl + 'module/delete/' + moduleId;
            return $http.get(url).then(
               function (results) {
                   if (results.data.IsSuccess) {
                       return true;
                   } else {
                       webcloud.error(results.data);
                       return false;
                   }
               });
        };

        factory.refreshModuleCache = function () {
            var url = webcloud.config.siteUrl + 'CacheHandler.ashx?CacheKey=Module&AllLanguage=true';

            //return $http.get(url).then(
            //   function (results) {
            //       if (results.data.IsSuccess) {
            //           return true;
            //       } else {
            //           webcloud.error(results.data);
            //           return false;
            //       }
            //   });

            $.getJSON(url, function (data) {
                console.debug(data);
            });
        };


        return factory;
    };

    pageFactory.$inject = ['$http', '$q'];

    angular.module('webcloudAdminApp').factory('pageService', pageFactory);

}());