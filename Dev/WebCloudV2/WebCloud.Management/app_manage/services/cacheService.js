﻿(function () {

    var cacheFactory = function ($q) {
        var factory = {};
        factory.refreshCache = function (cacheKey, allLang, allWebsite) {
            var url = webcloud.config.siteUrl + 'CacheHandler.ashx?CacheKey=' + cacheKey;

            if (allLang)
                url += '&AllLanguage=true';
            
            if (allWebsite)
                url += '&AllWebsite=true';
           
            $.getJSON(url, function (data) {
                // console.debug(data);
            });
        };
        
        factory.refreshModuleCache = function () {
            factory.refreshCache('Module', true, false);
        };
        
        factory.refreshLayoutCache = function () {
            factory.refreshCache('Layout', true, false);
        };

        return factory;
    };

    cacheFactory.$inject = ['$q'];

    angular.module('webcloudAdminApp').factory('cacheService', cacheFactory);

}());