var Custom = function () {
    return {
        init: function () {
            // watch checked property of checkbox changed and update view by Uniform
            $('input[type=checkbox]').watch('checked', function () {
                $.uniform.update(this);
            });

            if ($.cookie) {
                $.cookie.raw = true;
            }

            toastr.options = {
                "closeButton": true,
                "debug": true,
                "positionClass": "toast-top-center",
                "onclick": null,
                "showDuration": "1000",
                "hideDuration": "1000",
                "timeOut": "5000",
                "extendedTimeOut": "1000",
                "showEasing": "swing",
                "hideEasing": "linear",
                "showMethod": "fadeIn",
                "hideMethod": "fadeOut"
            };

            // toggle show-hide item by hover / click
            $(document).on('click', '.toggle-show-parent', function () {
                var child = $(this).children('.toggle-show');
                if (child) {
                    if (child.hasClass('toggle-show-selected')) {
                        child.removeClass('toggle-show-selected');
                        child.addClass('toggle-show-unselected');
                    } else {
                        child.removeClass('toggle-show-unselected');
                        child.addClass('toggle-show-selected');
                    }
                }
            });

            $(document).on('mouseenter', '.toggle-show-parent', function () {
                var child = $(this).children('.toggle-show');
                if (child) {
                    child.removeClass('toggle-show-hoverout');
                    child.addClass('toggle-show-hoverin');
                }
            });

            $(document).on('mouseleave', '.toggle-show-parent', function () {
                var child = $(this).children('.toggle-show');
                if (child) {
                    child.removeClass('toggle-show-hoverin');
                    child.addClass('toggle-show-hoverout');
                }
            });
        }
    };
}();

