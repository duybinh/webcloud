﻿(function (webcloud, $, undefined) {
    webcloud.text = {
        yes: 'Có',
        no: 'Không',
        cancel: 'Bỏ qua',
        
		deleteConfirmTitle: 'Xoá?',
		deleteConfirmContent: 'Bạn có chắc chắn muốn xóa?',
		deleteSuccess: 'Xóa thành công!',
        
		deleteChoiceTitle: 'Lựa chọn xóa',
        deleteCategoryChoiceContent: 'Nhóm đang xóa có nhiều nhóm con. Bạn hãy lựa chọn xóa tất cả các nhóm con hay các nhóm con chuyển lên nhóm cha??',
        deleteCategoryYes: 'Xóa tất cả',
        deleteCategoryNo: 'Chuyển lên nhóm cha',
        
		removeConfirmTitle: 'Xoá?',
		removeConfirmContent: 'Bạn có chắc chắn muốn xóa hoàn toàn (không khôi phục lại được)?',
		removeSuccess: 'Xóa thành công!',
		
		emptyTrashConfirmContent: 'Bạn có chắc chắn muốn xóa hoàn toàn tất cả các nhóm trong thùng rác (không khôi phục lại được) không?',

		restoreSuccess: 'Khôi phục thành công!',
		
	};
}(window.webcloud = window.webcloud || {}, jQuery));