﻿(function (webcloud, $, undefined) {
    webcloud.lang = 'vi';
    webcloud.site = 'default';

    webcloud.config = {
        apiUrl: 'http://localhost:14469/api/',
        fileHandler: 'http://localhost:12538/Handlers/FileHandler.ashx',
        resourceHandler: 'http://localhost:12538/Handlers/ResourceHandler.ashx',
        siteUrl: 'http://localhost:14480/',
        authTokenCookie: 'AuthToken',
        userProfileCookie: 'UserProfile',
        cookieExpires: 7,
        jqxTheme: 'metro'
    };

    webcloud.store = {

    };

    webcloud.error = function (err, title) {
        if (!title) {
            title = 'Lỗi';
        }

        var type = $.type(err);
        var detail = '';
        if (type === 'string') {
            detail = err;
        } else if (type === 'object') {
            var msg = err.Message;
            if (!msg && err.responseJSON) {
                msg = err.responseJSON.Message;
            }
            if (msg) {
                detail = msg.Code + ' ' + msg.Title;
            } else {
                detail = JSON.stringify(err);
            }
        }
        toastr.error(detail, title);
    };

    webcloud.success = function (detail, title) {
        if (!title) {
            title = 'Thành công';
        }
        toastr.success(detail, title);
    };

    webcloud.toggleForm = function (form) {
        var el = jQuery(form).closest(".portlet-body");
        var btn = el.closest(".portlet").find('.portlet-title .tools > .showhide');
        if (btn.hasClass("collapse")) {
            btn.removeClass("collapse").addClass("expand");
            el.slideUp(200);
        } else {
            btn.removeClass("expand").addClass("collapse");
            el.slideDown(200);
        }
    };

    webcloud.prompt = function (title, msg, callback, inputAddon) {
        var modelTemplate =
      '<div class="webcloud-modal modal fade" id="#id#" tabindex="-1" aria-hidden="true">' +
        '<div class="modal-dialog">' +
            '<div class="modal-content">' +
                '<div class="modal-header">' +
                    '<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>' +
                    '<h4 class="modal-title">#title#</h4>' +
                '</div>' +
                '<div class="modal-body">' +
                     '<label> #msg# </label><div class="input-group" style="width:100%"><input type="text" class="form-control prompt-input" />#addon#</div>' +
                '</div>' +
                '<div class="modal-footer">' +
                    '<button type="button" class="btn blue btn-ok">' + webcloud.text.yes + '</button>' +
                    '<button type="button" class="btn default btn-cancel">' + webcloud.text.no + '</button>' +
                '</div>' +
            '</div>' +
        '</div>' +
    '</div>';
        var modalId = "confirmModal" + parseInt(Date.now());
        modelTemplate = modelTemplate.replace('#id#', modalId);
        modelTemplate = modelTemplate.replace('#title#', title);
        modelTemplate = modelTemplate.replace('#msg#', msg);
        modelTemplate = modelTemplate.replace('#addon#', inputAddon ? '<div class="input-group-addon">' + inputAddon + '</div>' : '');

        $('body').append(modelTemplate);
        var modal = $('#' + modalId);
        modal.modal('show');
        $('#' + modalId + ' .prompt-input').focus();
        $('.btn-ok', modal).on('click', function (event) {
            modal.modal('hide');
            callback($('#' + modalId + ' .prompt-input').val());
        });
        $('.btn-cancel', modal).on('click', function (event) {
            modal.modal('hide');

        });
        // delete modal after closed
        $(modal).on('hidden.bs.modal', function () {
            modal.remove();
        });
    };

    webcloud.confirm = function (title, msg, callback) {
        var modelTemplate =
      '<div class="webcloud-modal modal fade" id="#id#" tabindex="-1" aria-hidden="true">' +
        '<div class="modal-dialog">' +
            '<div class="modal-content">' +
                '<div class="modal-header">' +
                    '<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>' +
                    '<h4 class="modal-title">#title#</h4>' +
                '</div>' +
                '<div class="modal-body">' +
                    '#msg#' +
                '</div>' +
                '<div class="modal-footer">' +
                    '<button type="button" class="btn blue btn-ok">' + webcloud.text.yes + '</button>' +
                    '<button type="button" class="btn default btn-cancel">' + webcloud.text.no + '</button>' +
                '</div>' +
            '</div>' +
        '</div>' +
    '</div>';
        var modalId = "confirmModal" + parseInt(Date.now());
        modelTemplate = modelTemplate.replace('#id#', modalId);
        modelTemplate = modelTemplate.replace('#title#', title);
        modelTemplate = modelTemplate.replace('#msg#', msg);
        $('body').append(modelTemplate);
        var modal = $('#' + modalId);
        //$('#' + modalId + ' .btn-ok').focus();
        modal.modal('show');
        $('.btn-ok', modal).on('click', function (event) {
            modal.modal('hide');
            callback();
        });
        $('.btn-cancel', modal).on('click', function (event) {
            modal.modal('hide');

        });
        // delete modal after closed
        $(modal).on('hidden.bs.modal', function () {
            modal.remove();
        });
    };

    webcloud.choose = function (title, msg, yesLabel, noLabel, yesCallback, noCallback) {
        var modelTemplate =
      '<div class="webcloud-modal modal fade" id="#id#" tabindex="-1" aria-hidden="true">' +
        '<div class="modal-dialog">' +
            '<div class="modal-content">' +
                '<div class="modal-header">' +
                    '<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>' +
                    '<h4 class="modal-title">#title#</h4>' +
                '</div>' +
                '<div class="modal-body">' +
                    '#msg#' +
                '</div>' +
                '<div class="modal-footer">' +
                    '<button type="button" class="btn blue btn-ok">#yesLabel#</button>' +
                    '<button type="button" class="btn red btn-no">#noLabel#</button>' +
                    '<button type="button" class="btn default btn-cancel">' + webcloud.text.cancel + '</button>' +
                '</div>' +
            '</div>' +
        '</div>' +
    '</div>';
        var modalId = "confirmModal" + parseInt(Date.now());
        modelTemplate = modelTemplate.replace('#id#', modalId);
        modelTemplate = modelTemplate.replace('#title#', title);
        modelTemplate = modelTemplate.replace('#msg#', msg);
        modelTemplate = modelTemplate.replace('#yesLabel#', yesLabel);
        modelTemplate = modelTemplate.replace('#noLabel#', noLabel);

        $('body').append(modelTemplate);
        var modal = $('#' + modalId);
        modal.modal('show');

        $('.btn-ok', modal).on('click', function (event) {
            modal.modal('hide');
            yesCallback();
        });

        $('.btn-no', modal).on('click', function (event) {
            modal.modal('hide');
            noCallback();
        });

        $('.btn-cancel', modal).on('click', function (event) {
            modal.modal('hide');
        });

        // delete modal after closed
        $(modal).on('hidden.bs.modal', function () {
            modal.remove();
        });
    };

    webcloud.alert = function (title, msg, callback) {
        var modelTemplate =
      '<div class="modal fade" id="#id#" tabindex="-1" aria-hidden="true">' +
        '<div class="modal-dialog">' +
            '<div class="modal-content">' +
                '<div class="modal-header">' +
                    '<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>' +
                    '<h4 class="modal-title">#title#</h4>' +
                '</div>' +
                '<div class="modal-body">' +
                    '#msg#' +
                '</div>' +
                '<div class="modal-footer">' +
                    '<button type="button" class="btn blue btn-ok">' + webcloud.text.Ok + '</button>' +
                '</div>' +
            '</div>' +
        '</div>' +
    '</div>';
        var modalId = "alertModal" + parseInt(Date.now());
        modelTemplate = modelTemplate.replace('#id#', modalId);
        modelTemplate = modelTemplate.replace('#title#', title);
        modelTemplate = modelTemplate.replace('#msg#', msg);
        $('body').append(modelTemplate);
        var modal = $('#' + modalId);
        modal.modal('show');
        $('.btn-ok', modal).on('click', function (event) {
            modal.modal('hide');
            callback();
        });
    };

    webcloud.toSeoString = function (input) {
        if (!input) return null;
        var str = input.toLowerCase();
        str = str.replace(/à|á|ạ|ả|ã|â|ầ|ấ|ậ|ẩ|ẫ|ă|ằ|ắ|ặ|ẳ|ẵ/g, "a");
        str = str.replace(/è|é|ẹ|ẻ|ẽ|ê|ề|ế|ệ|ể|ễ/g, "e");
        str = str.replace(/ì|í|ị|ỉ|ĩ/g, "i");
        str = str.replace(/ò|ó|ọ|ỏ|õ|ô|ồ|ố|ộ|ổ|ỗ|ơ|ờ|ớ|ợ|ở|ỡ/g, "o");
        str = str.replace(/ù|ú|ụ|ủ|ũ|ư|ừ|ứ|ự|ử|ữ/g, "u");
        str = str.replace(/ỳ|ý|ỵ|ỷ|ỹ/g, "y");
        str = str.replace(/đ/g, "d");
        str = str.replace(/!|@|\$|%|\^|\*|\(|\)|\+|\=|\<|\>|\?|\/|\\|,|\.|\;|\:|\'| |\"|\&|\#|\[|\]|~/g, "-");
        //        var re = new RegExp('[^A-Za-z0-9\-]');
        //        str = str.replace(re, "-"); //thay thế 2- thành 1-
        str = str.replace(/-+-/g, "-"); //thay thế 2- thành 1-
        str = str.replace(/^\-+|\-+$/g, ""); //cắt bỏ ký tự - ở đầu và cuối chuỗi
        return str;
    };

    webcloud.resourceUrl = function (path) {
        return 'http://localhost:14469/resources/' + path + '?WebsiteId=' + $.cookie('WebsiteId');
    };

    webcloud.thumbUrl = function (path, width, heigh, relative) {
        var p = 'http://localhost:12538/Handlers/ResourceHandler.ashx?WebsiteId=' + $.cookie('WebsiteId')
            + '&LanguageId=' + $.cookie('LanguageId');
        if (width && width != '') {
            p += '&w=' + width;
        }
        if (heigh && heigh != '') {
            p += '&h=' + heigh;
        }
        if (relative) {
            p += '&relative=' + relative;
        }
        return p + '&action=thumb&p=' + path;
    };

    webcloud.openPanel = function (target) {
        target.slideDown(200);
        target.removeClass("collapse").addClass("expand");
    };
    webcloud.closePanel = function (target) {
        target.slideUp(200);
        target.removeClass("expand").addClass("collapse");
    };
    webcloud.togglePanel = function (target) {
        if (target.hasClass("collapse")) {
            target.slideDown(200);
            target.removeClass("collapse").addClass("expand");
        } else {
            target.slideUp(200);
            target.removeClass("expand").addClass("collapse");
        }
    };

    webcloud.scrollTo = function (el) {
        $('html, body').animate({
            scrollTop: el.offset().top
        }, 'slow');
    };

    webcloud.chooseColor = function () {
        var colors = ["yellow", "red", "green", "blue", "grey", "purple"];
        var random = Math.floor(Math.random() * 4);
        return colors[random];
    };

    webcloud.getUrlParam = function (sParam) {
        var sPageUrl = window.location.search.substring(1);
        var sUrlVariables = sPageUrl.split('&');
        for (var i = 0; i < sUrlVariables.length; i++) {
            var sParameterName = sUrlVariables[i].split('=');
            if (sParameterName[0] == sParam) {
                return sParameterName[1];
            }
        }
        return '';
    };

    webcloud.getFileExt = function (filename) {
        var index = filename.lastIndexOf('.');
        if (index > 0) {
            return filename.substring(index);
        }
        return '';
    };

    webcloud.endsWith = function (suffix) {
        return this.indexOf(suffix, this.length - suffix.length) !== -1;
    };

    webcloud.math = {};
    webcloud.math.round = function round(value, decimals) {
        return Number(Math.round(value + 'e' + decimals) + 'e-' + decimals);
    };
}(window.webcloud = window.webcloud || {}, jQuery));
