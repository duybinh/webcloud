﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebCloud.Common.Constant;

namespace WebCloud.Management.Controllers
{
    public class BaseController : Controller
    {
        public string WebsiteId { set; get; }
        public string LanguageId { set; get; }

        protected void InitContext()
        {
            WebsiteId = Default.WebsiteId;
            LanguageId = Default.LanguageId;
            if (!string.IsNullOrEmpty(Request["WebsiteId"]))
            {
                WebsiteId = Request["WebsiteId"];
            }
            if (!string.IsNullOrEmpty(Request["LanguageId"]))
            {
                LanguageId = Request["LanguageId"];
            }

            Response.Cookies.Add(new HttpCookie("WebsiteId", WebsiteId));
            Response.Cookies.Add(new HttpCookie("LanguageId", LanguageId));

            Response.Cookies.Add(new HttpCookie("ApplicationId", WebsiteId == Default.WebsiteId ? "WebCloudManagement" : "WebCloudAdmin"));
        }
    }
}