﻿using System.Web;
using System.Web.Mvc;
using WebCloud.Common.Constant;

namespace WebCloud.Management.Controllers
{
    public class HomeController : BaseController
    {
        public ActionResult Index()
        {
            InitContext();

            if (WebsiteId == Default.WebsiteId)
            {
                Response.Cookies.Add(new HttpCookie("ApplicationId", "WebCloudManagement"));
                return View();
            }

            Response.Cookies.Add(new HttpCookie("ApplicationId", "WebCloudAdmin"));
            return View("Admin");
        }
    }
}
