﻿(function () {

    var layoutFactory = function ($http, $q) {
        var factory = {};

        factory.typeName = function (type) {
            switch(type) {
                case 0:
                    return "Page";
                case 1:
                    return "Module";
                case 2:
                    return "Snippet";
                case 3:
                    return "Script";
            }
            return "";
        };

        factory.typeValue = function (type) {
            switch (type) {
                case "Page":
                    return 0;
                case "Module":
                    return 1;
                case "Snippet":
                    return 2;
                case "Script":
                    return 3;
            }
            return undefined;
        };
        
        factory.new = function (type) {
            if (type == undefined)
                type = 0;

            var content;
            if (type == 3) {
                content = '(function () {\n// add script content here\n}());';
            } else {
                content = '<!DOCTYPE html>\n\n<html>\n\n <head>\n\n </head>\n\n <body>\n\n </body>\n\n</html>';
            }

            return $q.when({
                LayoutID: null,
                Name: null,
                Description: null,
                Content: content,
                Type: type,
                SortOrder: 0,
                IsEnabled: true
            });
        };

        factory.getall = function (type) {
            var url = webcloud.config.apiUrl + 'layout';
            if (type != undefined)
                url += '/type/' + type;

            return $http.get(url).then(
              function (results) {
                  if (results.data.IsSuccess) {
                      return results.data.Data;
                  } else {
                      webcloud.error(results.data);
                      return null;
                  }
              });
        };

        factory.get = function (id) {
            return $http.get(webcloud.config.apiUrl + 'layout/' + id).then(
              function (results) {
                  if (results.data.IsSuccess) {
                      return results.data.Data;
                  } else {
                      webcloud.error(results.data);
                      return null;
                  }
              });
        };

        factory.add = function (obj) {
            console.debug(angular.toJson(obj));
            //return;
            return $http.post(webcloud.config.apiUrl + 'layout/add', obj).then(
              function (results) {
                  if (results.data.IsSuccess) {
                      return results.data.Data;
                  } else {
                      webcloud.error(results.data);
                      return null;
                  }
              });
        };

        factory.edit = function (obj) {
            console.debug(angular.toJson(obj));
            //return;
            return $http.post(webcloud.config.apiUrl + 'layout/edit', obj).then(
              function (results) {
                  if (results.data.IsSuccess) {
                      return true;
                  } else {
                      webcloud.error(results.data);
                      return false;
                  }
              });
        };

        factory.delete = function (id) {
            var url = webcloud.config.apiUrl + 'layout/delete/' + id;
            return $http.get(url).then(
               function (results) {
                   if (results.data.IsSuccess) {
                       return true;
                   } else {
                       webcloud.error(results.data);
                       return false;
                   }
               });
        };
        
        return factory;
    };

    layoutFactory.$inject = ['$http', '$q'];

    angular.module('webcloudAdminApp').factory('layoutService', layoutFactory);

}());