﻿(function () {

    var fileFactory = function ($http, $q) {
        var factory = {};

        factory.list = function (folder) {
            return $http.get(webcloud.config.resourceHandler + '?action=listdir&path=' + folder).then(
              function (results) {
                  if (results.data.IsSuccess) {
                      return results.data.Data;
                  } else {
                      webcloud.error(results.data);
                      return null;
                  }
              });
        };

        factory.editFile = function (dir, obj) {
            return $http.post(webcloud.config.resourceHandler + '?action=editfile&dir=' + dir, obj).then(
              function (results) {
                  if (results.data.IsSuccess) {
                      return true;
                  } else {
                      webcloud.error(results.data);
                      return false;
                  }
              });
        };
        
        factory.addFile = function (dir, obj) {
            return $http.post(webcloud.config.resourceHandler + '?action=addfile&dir=' + dir, obj).then(
              function (results) {
                  if (results.data.IsSuccess) {
                      return results.data.Data;
                  } else {
                      webcloud.error(results.data);
                      return false;
                  }
              });
        };

        factory.delete = function (path, isFolder) {
            return $http.get(webcloud.config.resourceHandler + '?action=' + (isFolder ? 'deletedir' : 'deletefile') + '&path=' + path).then(
              function (results) {
                  if (results.data.IsSuccess) {
                      return true;
                  } else {
                      webcloud.error(results.data);
                      return false;
                  }
              });
        };

        factory.rename = function (path, newname, isFolder) {
            return $http.get(webcloud.config.resourceHandler + '?action=' + (isFolder ? 'renamedir' : 'renamefile') + '&path=' + path + '&newname=' + newname).then(
              function (results) {
                  if (results.data.IsSuccess) {
                      return true;
                  } else {
                      webcloud.error(results.data);
                      return false;
                  }
              });
        };

        factory.mkdir = function (path, dirname) {
            return $http.get(webcloud.config.resourceHandler + '?action=mkdir&path=' + path + '&dirname=' + dirname).then(
              function (results) {
                  if (results.data.IsSuccess) {
                      return true;
                  } else {
                      webcloud.error(results.data);
                      return false;
                  }
              });
        };

        factory.getfile = function (path) {
            return $http.get(webcloud.config.resourceHandler + '?action=getfile&path=' + path).then(
              function (results) {
                  if (results.data.IsSuccess) {
                      return results.data.Data;
                  } else {
                      webcloud.error(results.data);
                      return false;
                  }
              });
        };
        return factory;
    };

    fileFactory.$inject = ['$http', '$q'];

    angular.module('webcloudAdminApp').factory('fileService', fileFactory);

}());