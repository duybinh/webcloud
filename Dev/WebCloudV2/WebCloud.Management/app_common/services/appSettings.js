﻿(function () {
    var appSettings = {
        auth : {
            url: 'http://localhost:23937/',
            clientId: 'WebCloudManagementApp'
        },
    };

    angular.module('webcloudAdminApp').value('appSettings', appSettings);

}());