﻿
'use strict';
angular.module('webcloudAdminApp').factory('authInterceptorService', ['$q', '$injector', '$location', 'localStorageService', function ($q, $injector, $location, localStorageService) {

    var authInterceptorServiceFactory = {};
    var $http;

    var request = function (config) {

        config.headers = config.headers || {};

        var authData = localStorageService.get('authorizationData');
        if (authData) {
            config.headers.Authorization = 'Bearer ' + authData.token;
        }

        var websiteData = localStorageService.get('websiteData');
        if (websiteData) {
            config.headers.WebsiteId = websiteData.WebsiteId;
            config.headers.LanguageId = websiteData.LanguageId;
        }

        return config;
    };

    var responseError = function (rejection) {
        var deferred = $q.defer();
        if (rejection.status === 401) {
            console.debug('auth error');
            var authService = $injector.get('authService');
            authService.refreshToken().then(function (response) {
                retryHttpRequest(rejection.config, deferred);
            }, function () {
                authService.logout();
                //$location.path('/login');
//                window.location = '/login';
                deferred.reject(rejection);
            });
        } else {
            deferred.reject(rejection);
        }
        return deferred.promise;
    };

    var retryHttpRequest = function (config, deferred) {
        $http = $http || $injector.get('$http');
        $http(config).then(function (response) {
            deferred.resolve(response);
        }, function (response) {
            deferred.reject(response);
        });
    };

    authInterceptorServiceFactory.request = request;
    authInterceptorServiceFactory.responseError = responseError;

    return authInterceptorServiceFactory;
}]);
