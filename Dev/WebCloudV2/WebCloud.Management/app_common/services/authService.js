﻿(function () {

    var authFactory = function ($http, $q, $rootScope, appSettings, localStorageService) {
        var factory = {
            loginPath: '/login',
            isAuthenticated: false,
        };

        // init
        factory.init = function () {
            var authData = localStorageService.get('authorizationData');
            if (authData) {
                factory.isAuthenticated = true;
                webcloud.store.authData = authData;
            }
        };

        factory.login = function (loginData) {
            var deferred = $q.defer();

            var data = "grant_type=password&username=" + loginData.userName + "&password=" + loginData.password + "&client_id=" + appSettings.auth.clientId;
            var header = {
                headers: {
                    //'WebsiteId': 'default',
                    'AudienceId': 'WebCloudBackend',
                    'Content-Type': 'application/x-www-form-urlencoded',
                }
            };
            $http.post(appSettings.auth.url + 'token', data, header).success(function (response) {
                var authData = {
                    token: response.access_token,
                    userName: loginData.userName,
                    refreshToken: response.refresh_token
                };
                localStorageService.set('authorizationData', authData);
                webcloud.store.authData = authData;
                factory.isAuthenticated = true;

                deferred.resolve(response);

            }).error(function (err, status) {
                factory.logout();
                deferred.reject(err);
            });

            return deferred.promise;
        };

        factory.logout = function () {
            localStorageService.remove('authorizationData');
            webcloud.store.authData = null;
            factory.isAuthenticated = false;
            return true;
        };

        factory.refreshToken = function () {
            var deferred = $q.defer();

            var authData = localStorageService.get('authorizationData');
            if (authData) {
                localStorageService.remove('authorizationData');
                webcloud.store.authData = null;

                var data = "grant_type=refresh_token&refresh_token=" + authData.refreshToken + "&client_id=" + appSettings.auth.clientId;
                var header = {
                    headers: {
                        'WebsiteId': 'default',
                        'AudienceId': 'WebCloudBackend',
                        'Content-Type': 'application/x-www-form-urlencoded',
                    }
                };

                $http.post(appSettings.auth.url + 'token', data, header).success(function (response) {
                    console.debug('auth: refresh token successfully');
                    authData = {
                        token: response.access_token,
                        userName: response.userName,
                        refreshToken: response.refresh_token
                    };
                    localStorageService.set('authorizationData', authData);
                    webcloud.store.authData = authData;

                    deferred.resolve(response);

                }).error(function (err, status) {
                    factory.logout();
                    deferred.reject(err);
                });
            }


            return deferred.promise;
        };

        return factory;
    };

    authFactory.$inject = ['$http', '$q', '$rootScope', 'appSettings', 'localStorageService'];

    angular.module('webcloudAdminApp').factory('authService', authFactory);

}());

