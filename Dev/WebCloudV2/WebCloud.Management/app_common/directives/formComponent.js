﻿(function () {

    var ngCheckbox = function () {
        return {
            restrict: 'A', //E = element, A = attribute, C = class, M = comment         
            transclude: true,
            scope: {
                model: '='
            },
            link: function (scope, element, attrs) {
                element.addClass("checkbox");
                var help = '';
                if (attrs.help) {
                    help = '&nbsp;<i class="icon-question-sign icon-help tooltips" data-html="true" title="' + attrs.help + '"></i>';
                }
                element.html('<label><input type="checkbox" value="true"/>' + attrs.title + '</label>' + help);

                var input = element.find('input');
                input.uniform();
                var modelChanged = true;
                input.change(function () {
                    //console.debug('ngCheckbox changed ' + input.val() + ' ' + input.is(":checked"));
                    modelChanged = false;
                    scope.model = input.is(":checked");
                    scope.$apply();
                });

                scope.$watch('model', function (newValue, oldValue) {
                    if (modelChanged == true) {
                        //console.debug('ngCheckbox modelchanged ' + newValue);
                        input.val(newValue);
                        if (newValue == true || newValue == 'true' || newValue == 'True') {
                            input.attr("checked", "checked");
                        } else {
                            input.removeAttr("checked");
                        }
                        $.uniform.update(input);
                    } else {
                        modelChanged = true;
                    }
                });

            }
        };
    };

    var ngInputDecimal = function () {
        return {
            restrict: 'A', //E = element, A = attribute, C = class, M = comment         
            transclude: true,
            scope: {
                model: '='
            },
            link: function (scope, element, attrs) {
                var input = $(element[0]);

                input.inputmask("decimal", { radixPoint: ".", autoGroup: true, groupSeparator: ",", groupSize: 3 });
                var modelChanged = true;
                input.on("change", function (e) {
                    modelChanged = false;
                    scope.model = input.val();
                    console.debug(scope.model);
                    scope.$apply();
                });

                scope.$watch('model', function (newValue, oldValue) {
                    if (newValue && modelChanged == true) {
                        input.val(Number(newValue));
                    } else {
                        modelChanged = true;
                    }
                });
            }
        };
    };

    var ngInputNumber = function () {
        return {
            restrict: 'A', //E = element, A = attribute, C = class, M = comment         
            transclude: true,
            scope: {
                model: '='
            },
            link: function (scope, element, attrs) {
                var input = $(element[0]);

                input.inputmask("integer", { autoGroup: true, groupSeparator: ",", groupSize: 3 });
                var modelChanged = true;
                input.on("change", function (e) {
                    modelChanged = false;
                    scope.model = input.val();
                    console.debug(scope.model);
                    scope.$apply();
                });

                scope.$watch('model', function (newValue, oldValue) {
                    if (newValue && modelChanged == true) {
                        input.val(newValue);
                    } else {
                        modelChanged = true;
                    }
                });
            }
        };
    };

    var ngSpinNumber = function () {
        return {
            restrict: 'A', //E = element, A = attribute, C = class, M = comment         
            transclude: true,
            scope: {
                model: '='
            },
            link: function (scope, element, attrs) {
                var input = $(element[0]);

                input.TouchSpin({
                    min: -100,
                    max: 1000,
                    stepinterval: 1,
                    initval: parseInt(scope.model),
                    verticalbuttons: true
                });
                var modelChanged = true;    
                input.on("change", function (e) {
                    modelChanged = false;
                    scope.model = input.val();
                    console.debug("ngSpinNumber:" + scope.model);
                    scope.$apply();
                });

                scope.$watch('model', function (newValue, oldValue) {
                    if (newValue && modelChanged == true) {
                        input.val(newValue);
                    } else {
                        modelChanged = true;
                    }
                });
            }
        };
    };

    var ngHtmlEditor = function () {
        return {
            restrict: 'A', //E = element, A = attribute, C = class, M = comment         
            transclude: true,
            scope: {
                model: '=ngModel'
            },

            link: function (scope, element, attrs) {
                var settings = {};
                if (attrs.ngHtmlEditor) {
                    settings = eval("(" + attrs.ngHtmlEditor + ")");
                }
                settings.extraPlugins = 'onchange';
                if (!settings.height)
                    settings.height = 580;

                var editor = CKEDITOR.replace(attrs.id, settings);
                var modelChange = true;
                editor.on('change', function (e) {
                    modelChange = false;
                    scope.model = e.editor.getData();
                    scope.$apply();
                    console.debug('CKEDITOR change: ' + scope.model);
                });

                scope.$watch('model', function (newValue, oldValue) {
                    if (modelChange)
                        editor.setData(newValue);
                    else
                        modelChange = true;
                });
            }
        };
    };

    var ngTagInput = function ($compile, tagService) {
        return {
            restrict: 'A', //E = element, A = attribute, C = class, M = comment         
            transclude: true,
            scope: {
                model: '=ngModel'
            },

            link: function (scope, element, attrs) {
                var input = $(element[0]);
                var settings = {};

                if (attrs.ngTagInput) {
                    settings = eval("(" + attrs.ngTagInput + ")");
                }
                settings.maximumInputLength = 250;
                // set data
                tagService.getall().then(function (result) {
                    settings.tags = result;
                    input.select2(settings);
                });

                input.on("change", function (e) {
                    scope.model = e.val;
                    scope.$apply();
                    if (e.added != null) {
                        tagService.add(e.added.text);
                    }
                });

                scope.$watch('model', function (newValue, oldValue) {
                    input.select2("val", newValue);
                });
            }
        };
    };

    var ngFileUpload = function () {
        return {
            restrict: 'A', //E = element, A = attribute, C = class, M = comment         
            transclude: true,
            scope: {
                model: '=ngModel',
                dir: '=',
                uploadsuccess: '&'
            },

            link: function (scope, element, attrs) {
                var settings = {};
                if (attrs.ngFileUpload) {
                    settings = eval("(" + attrs.ngFileUpload + ")");
                }

                if (!settings.location)
                    settings.location = 'sites';

                if (scope.dir) {
                    alert(scope.dir);
                    settings.dir = scope.dir;
                }

                if (!settings.dir) {
                    settings.dir = "Files";
                }

                console.debug(angular.toJson(settings));

                var uploadButton = $('<button/>')
                    .addClass('btn btn-primary')
                    .prop('disabled', true)
                    .text('Processing...')
                    .on('click', function () {
                        var $this = $(this), data = $this.data();
                        $this.off('click')
                            .text('Abort')
                            .on('click', function () {
                                $this.remove();
                                data.abort();
                            });
                        data.submit().always(function () {
                            $this.remove();
                        });
                    });

                var div = $(element[0]);
                div.addClass('file-upload-container');

                var thumbnailArea = settings.hideThumb ? $('') : $('<div class="fileupload-new thumbnail">'
                            + '<img src="http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image" alt="" />'
                            + '</div>');
                div.append(thumbnailArea);

                var fileNameText = $('<span class="upload-filename"></span>');
                var errorText = $(' <span class="upload-text-danger text-danger"></span>').css('display', 'none');;
                div.append($('<p/>').append(fileNameText).append(errorText));

                var fileInput = $('<input type="file" />');
                div.append($('<span class="btn btn-success fileinput-button">'
                                + '<i class="glyphicon glyphicon-plus"></i>'
                                + '<span>Select files...</span>'
                                + '</span>').append(fileInput));

                var buttonArea = $('<span class="btn-upload-holder"></span>');
                div.append(buttonArea);
                var progressArea = $('<div id="progress" class="progress">'
                                    + '<div class="progress-bar progress-bar-success"></div>'
                                    + '</div>');
                div.append(progressArea);
                var uploadConfig = {
                    url: webcloud.config.resourceHandler + '?action=upload&dir=' + settings.dir,
                    dataType: 'json',
                    autoUpload: false,
                    acceptFileTypes: /(\.|\/)(gif|jpe?g|png|css|js|html|htm|txt|xml|eot|svg|ttf|woff|woff2|otf)$/i,
                    maxFileSize: 5000000, // 5 MB
                    // Enable image resizing, except for Android and Opera,
                    // which actually support image resizing, but fail to
                    // send Blob objects via XHR requests:
                    disableImageResize: /Android(?!.*Chrome)|Opera/
                        .test(window.navigator.userAgent),
                    previewMaxWidth: 100,
                    previewMaxHeight: 100,
                    previewCrop: true,
                    beforeSend: function (xhr, data) {
                        // add xhr header
                        xhr.setRequestHeader('Authorization', 'Bearer ' + webcloud.store.authData.token);
                        xhr.setRequestHeader('WebsiteId', $.cookie('WebsiteId'));
                        xhr.setRequestHeader('LanguageId', $.cookie('LanguageId'));
                    }
                };
                fileInput.fileupload(uploadConfig).on('fileuploadadd', function (e, data) {
                    var file = data.files[data.files.length - 1];
                    buttonArea.empty().append(uploadButton.clone(true).data(data));
                    fileNameText.text(file.name);
                }).on('fileuploadprocessalways', function (e, data) {
                    var index = data.index,
                        file = data.files[index];

                    if (file.error) {
                        errorText.text(file.error).css('display', 'block');
                    } else {
                        errorText.css('display', 'none');
                    }
                    if (index + 1 === data.files.length) {
                        buttonArea.find('button').text('Upload').prop('disabled', !!data.files.error);
                    }
                }).on('fileuploadprogressall', function (e, data) {
                    var progress = parseInt(data.loaded / data.total * 100, 10);
                    progressArea.find('.progress-bar').css(
                        'width',
                        progress + '%'
                    );
                }).on('fileuploaddone', function (e, data) {
                    console.debug(angular.toJson(data.result));
                    if (data.result.IsSuccess) {
                        $.each(data.result.Data, function (index, file) {
                            // update model
                            scope.model = settings.dir + '/' + file.Name;
                            scope.$apply();
                            if (scope.uploadsuccess()) {
                                scope.uploadsuccess()(file);
                            }
                        });
                    } else {
                        errorText.text(data.result.Code + '-' + data.result.Title).css('display', 'block');
                        webcloud.error(data.result);
                    }

                }).on('fileuploadfail', function (e, data) {
                    $.each(data.files, function (index, file) {
                        errorText.text('File upload failed.');
                    });
                }).prop('disabled', !$.support.fileInput).parent().addClass($.support.fileInput ? undefined : 'disabled');

                scope.$watch('model', function (newValue, oldValue) {

                    if (newValue) {
                        var url = webcloud.thumbUrl(newValue, settings.location);
                        if (fileNameText.parent().is('a')) {
                            fileNameText.parent().prop('href', url);
                        } else {
                            var link = $('<a>').attr('target', '_blank').prop('href', url);
                            fileNameText.wrap(link);
                        }
                        thumbnailArea.find('img').attr('src', url);
                    }
                });

                scope.$watch('dir', function (newValue, oldValue) {
                    if (newValue) {
                        uploadConfig.url = webcloud.config.resourceHandler + '?action=upload&dir=' + newValue;
                        fileInput.fileupload(uploadConfig);
                    }
                });
            }
        };
    };

    var ngDatePicker = function () {
        return {
            restrict: 'A', //E = element, A = attribute, C = class, M = comment         
            transclude: true,
            scope: {
                model: '=ngModelDate'
            },

            link: function (scope, element, attrs) {
                var input = $(element[0]);
                var settings = {};

                if (attrs.ngDatePicker) {
                    settings = eval("(" + attrs.ngDatePicker + ")");
                }

                settings.autoclose = true;
                settings.todayBtn = true;
                settings.todayHighlight = true;
                settings.format = 'dd/mm/yyyy';

                input.datepicker(settings);

                var modelChanged = true;
                input.on("changeDate", function (e) {
                    modelChanged = false;
                    scope.model = input.datepicker('getUTCDate');
                    console.debug(scope.model);
                    scope.$apply();
                });

                scope.$watch('model', function (newValue, oldValue) {
                    if (newValue && modelChanged == true) {
                        input.datepicker('update', new Date(newValue));
                    } else {
                        modelChanged = true;
                    }
                });
            }
        };
    };

    var ngCodeEditor = function () {
        return {
            restrict: 'A', //E = element, A = attribute, C = class, M = comment         
            transclude: true,
            scope: {
                model: '=ngModel',
                mode: '='
            },

            link: function (scope, element, attrs) {
                var settings = {};
                if (attrs.ngCodeEditor) {
                    settings = eval("(" + attrs.ngCodeEditor + ")");
                }
                settings.enableBasicAutocompletion = true; // enable autocompletion
                settings.enableSnippets = false;
                settings.enableLiveAutocompletion = false;

                var editor = ace.edit(attrs.id);
                if (scope.mode && scope.mode != "")
                    editor.session.setMode("ace/mode/" + scope.mode);
                else
                    editor.session.setMode("ace/mode/html");

                editor.setTheme("ace/theme/chrome");
                editor.setOptions(settings);

                var modelChange = true;
                editor.getSession().on('change', function (e) {
                    modelChange = false;
                    scope.model = editor.getValue();
                    scope.$apply();
                });

                scope.$watch('model', function (newValue, oldValue) {
                    if (modelChange) {
                        editor.setValue(newValue);
                    } else {
                        modelChange = true;
                    }
                });

                scope.$watch('mode', function (newValue, oldValue) {
                    console.debug('mode change: ' + newValue);
                    if (newValue && newValue != "")
                        editor.session.setMode("ace/mode/" + newValue);
                });
            }
        };
    };

    var ngEditable = function () {
        return {
            restrict: 'A', //E = element, A = attribute, C = class, M = comment         
            transclude: false,
            scope: {
                model: '=',
                type: '='
            },

            link: function (scope, element, attrs) {
                var modelChange = true;
                var settings = {};
                if (attrs.ngEditable) {
                    settings = eval("(" + attrs.ngEditable + ")");
                }

                settings.mode = 'inline'; // enable autocompletion
                settings.inputclass = 'form-control';
                settings.success = function (response, newValue) {
                };

                var el = $(element[0]);
                el.on('save', function (e, params) {
                    modelChange = false;
                    if (scope.type == 'Date') {
                        scope.model = moment(params.newValue).format('DD/MM/YYYY');
                    } else {
                        scope.model = params.newValue;
                    }
                    console.debug('Editable value: ' + scope.model);
                    scope.$apply();
                });

                scope.$watch('model', function (newValue, oldValue) {
                    if (modelChange) {
                        console.debug('editable model change: ' + newValue);
                        el.editable('setValue', newValue, true);
                    } else {
                        modelChange = true;
                    }
                });

                scope.$watch('type', function (newValue, oldValue) {
                    console.debug('editable type change: ' + newValue);
                    var inputType = 'text';
                    switch (newValue) {
                        case 'Html':
                            inputType = 'wysihtml5';
                            break;
                        case 'Date':
                            inputType = 'date';
                            settings.format = 'dd/mm/yyyy';
                            settings.viewformat = 'dd/mm/yyyy';
                            break;
                        case 'Number':
                            inputType = 'number';
                            break;
                        case 'Decimal':
                            inputType = 'range';
                            break;
                        case 'Time':
                            inputType = 'time';
                            break;
                        default:
                            break;
                    }
                    settings.type = inputType;
                    el.editable('destroy');
                    el.editable(settings);
                    console.debug('editable type change: ' + inputType);

                });
            }
        };
    };

    var ngTypeEditor = function ($compile) {
        return {
            restrict: 'A', //E = element, A = attribute, C = class, M = comment         
            transclude: false,
            terminal: true,
            priority: 1000,
            scope: {
                model: '=',
                type: '='
            },

            link: function (scope, element, attrs) {

                element.removeAttr("ng-type-editor");

                scope.$watch('type', function (newValue, oldValue) {
                    console.debug('ngTypeEditor: editable type change: ' + newValue);
                    switch (newValue) {
                        case 'Html':
                            element.html('<textarea class="form-control" ng-html-editor="{height:200}" cols="80" id="' + attrs.model + '" ng-model="model" />');
                            break;
                        case 'DateTime':
                        case 'Time':
                        case 'Date':
                            element.html('<input class="form-control form-control-inline input-small"  size="16" type="text" title="ngày/tháng/năm" ng-date-picker="" ng-model-date="model" />');
                            break;
                        case 'Number':
                            element.html('<input type="text" ng-input-number class="form-control input-small" model="model" />');
                            break;
                        case 'Decimal':
                            element.html('<input type="text" ng-input-decimal class="form-control input-small" model="model" />');
                            break;
                        case 'Boolean':
                            element.html('<div ng-checkbox title="" model="model"></div>');
                            break;
                        default:
                            element.html('<input type="text" class="form-control" ng-model="model" maxlength="250" />');
                            break;
                    }
                    $compile(element)(scope);
                });
            }
        };
    };

    var ngSelectDropdown = function () {
        return {
            restrict: 'A', //E = element, A = attribute, C = class, M = comment         
            transclude: true,
            scope: {
                model: '=',
                source: '=',
            },
            link: function (scope, element, attrs) {
                var input = $(element[0]);
                var settings = {};

                if (attrs.settings) {
                    settings = eval("(" + attrs.settings + ")");
                }
                if (settings.allowClear == undefined) {
                    settings.allowClear = true;
                }
                settings.formatSelection = function (item) {
                    return item.text;
                };
                settings.formatResult = function (item) {
                    return item.text;
                };

                // set data
                scope.$watch('source.track', function (newValue, oldValue) {
                    if (newValue) {
                        console.debug('SelectDropdown - load data source completed');
                        settings.data = scope.source;
                        input.select2(settings);
                        input.select2("val", scope.model);
                    }
                });

                var modelChanged = true;
                input.on("change", function (e) {
                    modelChanged = false;
                    scope.model = e.val;
                    scope.$apply();
                    console.debug('SelectDropdown - onchange:' + scope.model);
                });

                scope.$watch('model', function (newValue, oldValue) {
                    if (modelChanged == true) {
                        input.select2("val", newValue);
                        console.debug('SelectDropdown - model value changed:' + newValue);
                    } else {
                        modelChanged = true;
                    }
                });
            }
        };
    };

    var ngSelectLang = function ($compile, languageService) {
        return {
            restrict: 'A', //E = element, A = attribute, C = class, M = comment         
            transclude: true,
            scope: {
                model: '='
            },
            link: function (scope, element, attrs) {
                scope.source = {
                    results: [],
                    text: 'text',
                    track: false
                };
                languageService.getall().then(function (obj) {
                    if (obj) {
                        scope.source.results = [];
                        for (var i = 0; i < obj.length; i++) {
                            scope.source.results.push({ id: obj[i].LanguageID, text: obj[i].DisplayName });
                        }
                        scope.source.track = true;
                    }
                });
                ngSelectDropdown().link(scope, element, attrs);
            }
        };
    };

    var ngSelectLayout = function ($compile, layoutService) {
        return {
            restrict: 'A', //E = element, A = attribute, C = class, M = comment         
            transclude: true,
            scope: {
                model: '='
            },
            link: function (scope, element, attrs) {
                //if (attrs.type) {
                //    settings = eval("(" + attrs.ngEditable + ")");
                //}

                scope.source = {
                    results: [],
                    text: 'text',
                    track: false
                };
                layoutService.getall(layoutService.typeValue(attrs.sourceType)).then(function (obj) {
                    if (obj) {
                        scope.source.results = [];
                        for (var i = 0; i < obj.length; i++) {
                            scope.source.results.push({
                                id: obj[i].LayoutID,
                                text: '<span class="dropdown-value-col tooltips" data-placement="right" title="' + obj[i].Name + '">' + obj[i].LayoutID + '</span>'
                            });
                        }
                        scope.source.track = true;
                    }
                });
                ngSelectDropdown().link(scope, element, attrs);
            }
        };
    };

    var ngSelectModuleBase = function ($compile, moduleService) {
        return {
            restrict: 'A', //E = element, A = attribute, C = class, M = comment         
            transclude: true,
            scope: {
                model: '='
            },
            link: function (scope, element, attrs) {
                scope.source = {
                    results: [],
                    text: 'text',
                    track: false
                };
                moduleService.getall().then(function (obj) {
                    if (obj) {
                        scope.source.results = [];
                        for (var i = 0; i < obj.length; i++) {
                            scope.source.results.push({
                                id: obj[i].ModuleBaseID,
                                text: '<span class="dropdown-value-col"><img class="dropdown-icon" src="' + webcloud.thumbUrl(obj[i].Thumbnail, "sites", 80, 80) + '" /></span><span class="dropdown-value-col tooltips" data-placement="right" title="' + obj[i].ModuleBaseID + '">' + obj[i].Name + '</span>'
                            });
                        }
                        scope.source.track = true;
                    }
                });
                ngSelectDropdown().link(scope, element, attrs);
            }
        };
    };

    angular.module('webcloudAdminApp').directive('ngCheckbox', ngCheckbox);
    angular.module('webcloudAdminApp').directive('ngInputDecimal', ngInputDecimal);
    angular.module('webcloudAdminApp').directive('ngInputNumber', ngInputNumber);
    angular.module('webcloudAdminApp').directive('ngSpinNumber', ngSpinNumber);
    angular.module('webcloudAdminApp').directive('ngHtmlEditor', ngHtmlEditor);
    angular.module('webcloudAdminApp').directive('ngTagInput', ngTagInput);
    angular.module('webcloudAdminApp').directive('ngFileUpload', ngFileUpload);
    angular.module('webcloudAdminApp').directive('ngDatePicker', ngDatePicker);
    angular.module('webcloudAdminApp').directive('ngCodeEditor', ngCodeEditor);
    angular.module('webcloudAdminApp').directive('ngEditable', ngEditable);
    angular.module('webcloudAdminApp').directive('ngTypeEditor', ngTypeEditor);
    angular.module('webcloudAdminApp').directive('ngSelectDropdown', ngSelectDropdown);
    angular.module('webcloudAdminApp').directive('ngSelectLang', ngSelectLang);
    angular.module('webcloudAdminApp').directive('ngSelectLayout', ngSelectLayout);
    angular.module('webcloudAdminApp').directive('ngSelectModuleBase', ngSelectModuleBase);

}());