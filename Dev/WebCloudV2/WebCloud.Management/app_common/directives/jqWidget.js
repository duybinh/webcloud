﻿(function () {

    var ngJqxNumberInput = function () {
        return {
            restrict: 'A', //E = element, A = attribute, C = class, M = comment         
            transclude: true,
            scope: {
                model: '=ngModel'
            },
            link: function (scope, element, attrs) {

                var settings = eval("(" + attrs.ngJqxNumberInput + ")");
                settings.theme = webcloud.config.jqxTheme;
                $(document).ready(function () {
                    $(element[0]).jqxNumberInput(settings);

                    $(element[0]).on('valuechanged', function (event) {
                        scope.model = event.args.value;
                        //scope.$apply();
                    });

                    scope.$watch('model', function (newValue, oldValue) {
                        $(element[0]).jqxNumberInput('setDecimal', newValue);
                    });
                });
            }
        };
    };

    var ngJqxDropdownTree = function () {
        return {
            restrict: 'A', //E = element, A = attribute, C = class, M = comment         
            transclude: true,
            scope: {
                source: '=ngSource',
                model: '=ngModel'
            },
            link: function (scope, element, attrs) {
                var dropdownSettings = eval("(" + attrs.dropdownSettings + ")");
                dropdownSettings.theme = webcloud.config.jqxTheme;
                var treeSettings = eval("(" + attrs.treeSettings + ")");
                treeSettings.theme = webcloud.config.jqxTheme;

                var div = $(element[0]);
                var btnClear = $('<span class="input-group-btn"><button type="button" class="btn blue">Bỏ</button></span>');
                div.append(btnClear);

                var eDropdown = $('<div></div>');
                div.append(eDropdown);

                var eTree = $('<div></div>');
                eDropdown.append(eTree);

                eDropdown.jqxDropDownButton(dropdownSettings);

                if (attrs.multiSelect == 'true') {
                    var itemLabels = [];
                    btnClear.on('click', function () {
                        //alert(angular.toJson(scope.model));
                        eTree.jqxTree('selectItem', null);
                        eDropdown.jqxDropDownButton('setContent', "");

                        itemLabels = [];

                        if (scope.model == null || scope.model.length != 0) {
                            scope.model = [];
                            //scope.$apply();
                        }

                        eTree.jqxTree('uncheckAll');
                    });


                    eTree.on('checkChange', function (event) {
                        var args = event.args;
                        var item = eTree.jqxTree('getItem', args.element);
                        if (args.checked) {
                            if (scope.model.indexOf(item.value) < 0) {
                                scope.model.splice(0, 0, item.value);
                            }
                            itemLabels.splice(0, 0, item.label);
                        } else {
                            if (item != null) {
                                var index = scope.model.indexOf(item.value);
                                if (index >= 0) {
                                    scope.model.splice(index, 1);
                                }

                                index = itemLabels.indexOf(item.label);
                                if (index >= 0) {
                                    itemLabels.splice(index, 1);
                                }
                            }
                        }
                        var dropDownContent = '<div class="dropdown-tree-labels">';
                        for (var i = 0; i < itemLabels.length; i++) {
                            dropDownContent += '<span>' + itemLabels[i] + '</span>';
                        }
                        dropDownContent += '<div/>';
                        eDropdown.jqxDropDownButton('setContent', dropDownContent);
                        // scope.$apply();
                    });
                } else {
                    btnClear.on('click', function () {
                        eTree.jqxTree('selectItem', null);
                        eDropdown.jqxDropDownButton('setContent', "");
                        if (scope.model != null) {
                            scope.model = null;
                            // scope.$apply();
                        }
                    });

                    eTree.on('select', function (event) {
                        var args = event.args;
                        var item = eTree.jqxTree('getItem', args.element);
                        var dropDownContent = '<div style="position: relative; margin-left: 3px; margin-top: 5px;">' + item.label + '</div>';
                        eDropdown.jqxDropDownButton('setContent', dropDownContent);
                        scope.model = item.value;
                        //scope.$apply();
                    });
                }


                scope.$watch('source.track', function (newValue, oldValue) {
                    if (newValue) {
                        // create data adapter.
                        var dataAdapter = new $.jqx.dataAdapter(scope.source);
                        // perform Data Binding.
                        dataAdapter.dataBind();
                        // get the tree items. The first parameter is the item's id. The second parameter is the parent item's id. The 'items' parameter represents
                        // the sub items collection name. Each jqxTree item has a 'label' property, but in the JSON data, we have a 'text' field. The last parameter
                        // specifies the mapping between the 'text' and 'label' fields.
                        var records = dataAdapter.getRecordsHierarchy(attrs.idField, attrs.parentField, 'items', [{ name: attrs.idField, map: 'id' }, { name: attrs.idField, map: 'value' }, { name: attrs.textField, map: 'label' }]);
                        treeSettings.source = records;
                        eTree.jqxTree(treeSettings);
                        eTree.jqxTree('expandAll');
                        //alert(angular.toJson(scope.model));
                        if (attrs.multiSelect == 'true') {
                            for (var i = 0; i < scope.model.length; i++) {
                                eTree.jqxTree('checkItem', eTree.find("#" + scope.model[i])[0], true);
                            }
                        } else {
                            eTree.jqxTree('val', scope.model);
                        }
                    }
                });

                scope.$watch('model', function (newValue, oldValue) {
                    if (newValue) {
                        eTree.jqxTree('val', newValue);
                    }

                    if (newValue == null || newValue.length == 0) {
                        btnClear.click();
                    }
                });
            }
        };
    };

    var ngArticleCategoryTree = function ($compile, articleCategoryService) {
        return {
            restrict: 'A', //E = element, A = attribute, C = class, M = comment         
            transclude: true,
            scope: {
                model: '=ngModel'
            },
            link: function (scope, element, attrs) {
                scope.source = {
                    datatype: "json",
                    datafields: [
                        { name: 'ArticleCategoryID' },
                        { name: 'ParentID' },
                        { name: 'Name', map: 'Content>Name' }
                    ],
                    id: 'ArticleCategoryID',
                    localdata: []
                };
                scope.source.track = false;
                articleCategoryService.getall().then(function (obj) {
                    if (obj) {
                        scope.source.localdata = obj;
                        scope.source.track = true;
                    }
                });
                ngJqxDropdownTree().link(scope, element, attrs);
            }
        };
    };

    var ngJqxGrid = function () {
        return {
            restrict: 'A', //E = element, A = attribute, C = class, M = comment         
            transclude: false,
            scope: {
                setting: '=ngSetting',
                page: '=ngPagenum',
                pageSize: '=ngPagesize',
                dataVersion: '=ngDataVersion'
            },
            link: function (scope, element, attrs) {

                $(document).ready(function () {
                    var grid = $(element[0]);

                    grid.jqxGrid().on("pagechanged", function (event) {
                        alert(event.args.pagenum + ' ' + event.args.pagesize);
                        //$scope.queryObj.Page = event.args.pagenum;
                        //$scope.queryObj.PageSize = event.args.pagesize;
                        //init();
                    });

                    grid.on("pagesizechanged", function (event) {
                        alert(event.args.pagenum + ' ' + event.args.pagesize);
                        //$scope.queryObj.Page = event.args.pagenum;
                        //$scope.queryObj.PageSize = event.args.pagesize;
                        //init();
                    });
                    grid.on('rowclick', function (event) {
                        var args = event.args;
                        alert(args.rowindex);
                    });
                    scope.$watch('dataVersion', function (newValue, oldValue) {
                        grid.jqxGrid('updatebounddata');
                    });
                    grid.jqxGrid(scope.setting);
                });
            }
        };
    };

    angular.module('webcloudAdminApp').directive('ngJqxNumberInput', ngJqxNumberInput);
    angular.module('webcloudAdminApp').directive('ngJqxDropdownTree', ngJqxDropdownTree);
    angular.module('webcloudAdminApp').directive('ngArticleCategoryTree', ngArticleCategoryTree);
    angular.module('webcloudAdminApp').directive('ngJqxGrid', ngJqxGrid);

}());