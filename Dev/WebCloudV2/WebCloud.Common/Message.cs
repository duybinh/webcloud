﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using WebCloud.Common.Constant;

namespace WebCloud.Common
{
    [Serializable]
    [DataContract]
    public class Message
    {
        [DataMember]
        public String Code { set; get; }

        [DataMember]
        public String Title { set; get; }

        [DataMember]
        public String Detail { set; get; }

        public Message()
        {

        }

        public Message(string code)
        {
            Code = code;
        }

        public Message(System.Exception exception)
        {
            Code = MessageCode.System_Error;
            Title = exception.Message;
            Detail = exception.ToString();
        }
    }
}
