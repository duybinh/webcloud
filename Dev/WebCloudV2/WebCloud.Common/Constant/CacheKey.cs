﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace WebCloud.Common.Constant
{
    public class CacheKey
    {
        public static readonly string UsersInBlackList = "UsersInBlackList";
        public static readonly string TextResource = "TextResource";

    }
}
