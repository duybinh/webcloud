﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WebCloud.Common.Constant
{
    public static class Default
    {
        public const string LanguageId = "vi";
        public const string WebsiteId = "default";
        public const string ThemeId = "default";
    }
}
