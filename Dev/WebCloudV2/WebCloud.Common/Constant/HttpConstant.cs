﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WebCloud.Common.Constant
{
    public static class HttpConstant
    {
        public const string AuthToken = "AuthToken";
        public const string AuthResult = "AuthResult";
        public const string LoggedInUser = "LoggedInUser";

    }
}
