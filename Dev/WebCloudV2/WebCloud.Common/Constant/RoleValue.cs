﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace WebCloud.Common.Constant
{
    public class RoleValue
    {
        public const string RootAdmin = "RootAdmin";
        public const string GlobalAdmin = "GlobalAdmin";
        public const string SetupAdmin = "SetupAdmin";
        public const string Admin = "Admin";

        // Website               
        public const string WebsiteView = "WebsiteView";
        public const string WebsiteCreate = "WebsiteCreate";
        public const string WebsiteEdit = "WebsiteEdit";
        public const string WebsiteDelete = "WebsiteDelete";
        public const string WebsiteDeleteDirectly = "WebsiteDeleteDirectly";
        public const string WebsiteOnlyOwn = "WebsiteOnlyOwn";

        // User
        public const string UserView = "UserView ";
        public const string UserCreate = "UserCreate";
        public const string UserEdit = "UserEdit";
        public const string UserDelete = "UserDelete";
        public const string UserDeleteDirectly = "UserDeleteDirectly";
        public const string UserOnlyOwn = "UserOnlyOwn";

        // Configuration
        public const string ConfigurationView = "ConfigurationView";
        public const string ConfigurationUpdate = "ConfigurationUpdate";

        // TextResource
        public const string TextResourceView = "TextResourceView ";
        public const string TextResourceUpdate = "TextResourceUpdate";

        // File
        public const string FileManagement = "FileManagement";

        public static string Roles(params string[] role)
        {
            return string.Join(",", role);
        }
    }
}
