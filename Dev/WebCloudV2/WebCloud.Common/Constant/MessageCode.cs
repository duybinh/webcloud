﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace WebCloud.Common.Constant
{
    public static class MessageCode
    {
        public const string System_Error = "System_Error";
        public const string Unknow = "Unknow";

        public const string Resource_LocationInvalid = "Resource_LocationInvalid";
        public const string Resource_ActionInvalid = "Resource_ActionInvalid";
        public const string Resource_NotFound = "Resource_NotFound";
        public const string Resource_Unauthorized = "Resource_Unauthorized";
        public const string Resource_NoPermissionToAccess = "Resource_NoPermissionToAccess";
        public const string Resource_FolderNameInvalid = "Resource_FolderNameInvalid";
        public const string Resource_FileNameInvalid = "Resource_FileNameInvalid";
        public const string Resource_NewNameExist = "Resource_NewNameExist";
        public const string Resource_UploadFileInvalid = "Resource_UploadFileInvalid";
        public const string Resource_FileContentInvalid = "Resource_FileContentInvalid";

        public const string Auth_UserNotExist = "Auth_UserNotExist";
        public const string Auth_PasswordFailed = "Auth_PasswordFailed";
        public const string Auth_Success = "Auth_Success";
        public const string Auth_UserLocked = "Auth_UserLocked";
        public const string Auth_TokenEmpty = "Auth_TokenEmpty";
        public const string Auth_TokenNotValid = "Auth_TokenNotValid";
        public const string Auth_TokenExpired = "Auth_TokenExpired";
        public const string Auth_TokenInBlacklist = "Auth_TokenInBlacklist";
        public const string Auth_NotAuthorize = "Auth_NotAuthorize";
        public const string Auth_UnAuthenticatedApplication = "Auth_UnAuthenticatedApplication";

        public const string Config_KeyNotExist = "Config_KeyNotExist";
        public const string Config_NoPermissionToEdit = "Config_NoPermissionToEdit";

        public const string Permission_ApplicationRequired = "Permission_ApplicationRequired";
        public const string Permission_PrincipalRequired = "Permission_PrincipalRequired";
        public const string Permission_RoleRequired = "Permission_RoleRequired";

        public const string Role_IdExist = "Role_IdExist";
        public const string Role_IdNotExist = "Role_IdNotExist";

        public const string Language_IdExist = "Language_IdExist";
        public const string Language_IdNotExist = "Language_IdNotExist";

        public const string TextResource_IdExist = "TextResource_IdExist";
        public const string TextResource_IdNotExist = "TextResource_IdNotExist";
        public const string TextResource_IdNotUnique = "TextResource_IdNotUnique";


        // Validate
        public const string Id_Required = "Id_Required";
        public const string Id_Invalid = "Id_Invalid";
        public const string Name_Required = "Name_Required";
        public const string Name_Length_Invalid = "Name_Length_Invalid";
        public const string Title_Required = "Title_Required";
        public const string Title_Length_Invalid = "Title_Length_Invalid";
        public const string Description_Length_Invalid = "Description_Length_Invalid";
        public const string Content_Required = "Content_Required";
        public const string SeoName_Required = "SeoName_Required";
        public const string SeoName_Length_Invalid = "SeoName_Length_Invalid";
        public const string MetaTitle_Length_Invalid = "MetaTitle_Length_Invalid";
        public const string MetaKeyword_Length_Invalid = "MetaKeyword_Length_Invalid";
        public const string MetaDescription_Length_Invalid = "MetaDescription_Length_Invalid";
        public const string ThumbImage_Length_Invalid = "ThumbImage_Length_Invalid";

        // Article Category
        public const string ArticleCategory_IdNotExist = "ArticleCategory_IdNotExist";
        public const string ArticleCategory_ParentIdInvalid = "ArticleCategory_ParentIdInvalid";

        // Article
        public const string Article_IdNotExist = "Article_IdNotExist";

        // Layout
        public const string Layout_IdExist = "Layout_IdExist";
        public const string Layout_IdNotExist = "Layout_IdNotExist";

        // ModuleBase
        public const string ModuleBase_IdExist = "ModuleBase_IdExist";
        public const string ModuleBase_IdNotExist = "ModuleBase_IdNotExist";

        // ModuleBase
        public const string Module_IdExist = "Module_IdExist";
        public const string Module_IdNotExist = "Module_IdNotExist";

        // Page
        public const string Page_IdExist = "Page_IdExist";
        public const string Page_IdNotExist = "Page_IdNotExist";
        public const string Page_SeoNameNotExist = "Page_SeoNameNotExist";
        public const string Page_SeoNameRequired = "Page_SeoNameRequired";
        public const string Page_LayoutIdNotExist = "Page_LayoutIdNotExist";
        public const string Page_LayoutIdRequired = "Page_LayoutIdRequired";

        // Theme
        public const string Theme_IdNotExist = "Theme_IdNotExist";
        public const string Theme_IdExist = "Theme_IdExist";
        public const string Theme_BaseIdNotExist = "Theme_BaseIdNotExist";
        public const string Theme_BasedTheme_Invalid = "Theme_BasedTheme_Invalid";
        public const string Theme_BasedThemeNotExist = "Theme_BasedThemeNotExist";
        public const string Theme_Author_Length_Invalid = "Theme_Author_Length_Invalid";
        public const string Theme_RolesToView_Length_Invalid = "Theme_RolesToView_Length_Invalid";
        public const string Theme_RolesToInstall_Length_Invalid = "Theme_RolesToInstall_Length_Invalid";
        public const string Theme_BasedTheme_Same_ThemeCode = "Theme_BasedTheme_Same_ThemeCode";

        public const string Config_IdNotExist = "Config_IdNotExist";
        public const string Config_IdExist = "Config_IdExist";
        public const string Config_RolesToView_Length_Invalid = "Config_RolesToView_Length_Invalid";
        public const string Config_RolesToEdit_Length_Invalid = "Config_RolesToEdit_Length_Invalid";
        public const string Config_Group_Length_Invalid = "Config_Group_Length_Invalid";
        public const string Config_DataType_Required = "Config_DataType_Required";
        public const string Config_DataType_Invalid = "Config_DataType_Invalid";
        public const string Config_DefaultValue_Required = "Config_DefaultValue_Required";
        public const string Config_DefaultValue_Invalid = "Config_DefaultValue_Invalid";

        public const string Resource_IdNotExist = "Resource_IdNotExist";
        public const string Resource_IdExist = "Resource_IdExist";
        public const string Resource_RolesToView_Length_Invalid = "Resource_RolesToView_Length_Invalid";
        public const string Resource_RolesToEdit_Length_Invalid = "Resource_RolesToEdit_Length_Invalid";
        public const string Resource_Type_Length_Invalid = "Resource_Type_Length_Invalid";
        public const string Resource_DefaultValue_Required = "Resource_DefaultValue_Required";
        public const string Resource_DefaultValue_Invalid = "Resource_DefaultValue_Invalid";
    }
}
