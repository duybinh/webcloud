﻿using System.Configuration;
using DHSoft.Utility.Common;

namespace WebCloud.Common.Wrapper
{
    public class CommonConfiguration
    {
        private static readonly CommonConfiguration Instance = new CommonConfiguration();
        private readonly int _cacheTime;
        private readonly string _defaultWebsiteId;
        private readonly int _tokenExpireTime;
        private readonly string _sqlDbConnectionString;
        private CommonConfiguration()
        {
            _cacheTime = ConfigurationManager.AppSettings["CacheTime"].ToInt32TryParse(7);
            _defaultWebsiteId = ConfigurationManager.AppSettings["DefaultWebsiteID"];
            _tokenExpireTime = ConfigurationManager.AppSettings["TokenExpireTime"].ToInt32TryParse(7);
            if (ConfigurationManager.ConnectionStrings["WebCloudV2Context"] != null)
                _sqlDbConnectionString = ConfigurationManager.ConnectionStrings["WebCloudV2Context"].ConnectionString;
        }
        public static int CacheTime
        {
            get
            {
                return Instance._cacheTime;
            }
        }

        public static string DefaultWebsiteID
        {
            get
            {
                return Instance._defaultWebsiteId;
            }
        }

        /// <summary>
        /// Expire days of a token
        /// </summary>
        public static int TokenExpireTime
        {
            get
            {
                return Instance._tokenExpireTime;
            }
        }

        public static string SqlDbConnectionString
        {
            get
            {
                return Instance._sqlDbConnectionString;
            }
        }
    }
}
