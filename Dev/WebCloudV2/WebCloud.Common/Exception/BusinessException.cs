﻿using System;

namespace WebCloud.Common.Exception
{
    public class BusinessException : System.Exception
    {
        public String ErrorCode { set; get; }
        public BusinessException(String errorCode)
        {
            ErrorCode = errorCode;
        }
    }
}
