﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace WebCloud.Common
{
    [Serializable]
    [DataContract]
    public class ResultPagging<T>:ResultBase
    {
        [DataMember]
        public int Total { set; get; }
        
        [DataMember]
        public List<T> Data { set; get; } 
    }


    [Serializable]
    [DataContract]
    public class ResultArray : ResultBase
    {
        [DataMember]
        public int Total { set; get; }

        [DataMember]
        public int Count { set; get; }

        [DataMember]
        public object[] Data { set; get; }
    }
}
