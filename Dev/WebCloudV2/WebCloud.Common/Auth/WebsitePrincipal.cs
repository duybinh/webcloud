﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WebCloud.Common.Auth
{
    public class WebsitePrincipal : BasePrincipal
    {
        public WebsitePrincipal(long globalID, string websiteID, string userID)
            : base(globalID, websiteID, userID)
        {
        }
        public string DisplayName { get; set; }
        public string Avatar { get; set; }

        public string ApiToken { get; set; }
    }
}
