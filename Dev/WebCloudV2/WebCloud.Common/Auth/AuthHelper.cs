﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using DHSoft.Utility.Security;
using Newtonsoft.Json;
using WebCloud.Common.Constant;

namespace WebCloud.Common.Auth
{
    public static class AuthHelper
    {
        private const string EncryptKey = "webcloud_setup";

        public static void ProcessLogout(this Controller controller)
        {
            // close session
            if (controller.HttpContext.Session != null)
                controller.HttpContext.Session.Abandon();

            // remove cookies
            if (controller.HttpContext.Request.Cookies[HttpConstant.AuthToken] != null)
            {
                controller.HttpContext.Response.Cookies.Add(new HttpCookie(HttpConstant.AuthToken) { Expires = DateTime.Now.AddDays(-1d) });
            }

            if (controller.HttpContext.Request.Cookies[HttpConstant.LoggedInUser] != null)
            {
                controller.HttpContext.Response.Cookies.Add(new HttpCookie(HttpConstant.LoggedInUser) { Expires = DateTime.Now.AddDays(-1d) });
            }
        }

        public static void AddCookie(this Controller controller, string key, string value, bool isPersistent)
        {
            var cookie = new HttpCookie(key, value);
            
            if (isPersistent)
            {
                cookie.Expires = DateTime.Now.AddDays(30);
            }
            controller.HttpContext.Response.Cookies.Add(cookie);
        }

        public static void ProcessAuthenticateRequest(this HttpApplication httpApplication)
        {
            HttpCookie authCookie = httpApplication.Request.Cookies[HttpConstant.AuthToken];
            HttpCookie userCookie = httpApplication.Request.Cookies[HttpConstant.LoggedInUser];

            if (authCookie != null && userCookie != null)
            {
                string userModelStr = userCookie.Value.Decrypt(EncryptKey);

                var userModel = JsonConvert.DeserializeObject<WebsitePrincipalModel>(userModelStr);
                if (userModel == null || string.IsNullOrEmpty(userModel.WebsiteID)
                    || string.IsNullOrEmpty(userModel.UserID) || userModel.GlobalID <= 0)
                    return;

                var principal = new WebsitePrincipal(userModel.GlobalID, userModel.WebsiteID, userModel.UserID);
                principal.Avatar = userModel.Avatar;
                principal.DisplayName = userModel.DisplayName;
                principal.Email = userModel.Email;
                principal.IsVerifiedEmail = userModel.IsVerifiedEmail;
                principal.LanguageID = userModel.LanguageID;
                principal.Roles = userModel.Roles;
                principal.ApiToken = authCookie.Value;

                HttpContext.Current.User = principal;
            }
        }

        public static void ProcessLogin(this Controller controller, string token, object userModel, bool isPersistent)
        {
            // serialize model
            string modelSerialized = JsonConvert.SerializeObject(userModel);
            // encrypt model and add to cookie 
            controller.AddCookie(HttpConstant.LoggedInUser, modelSerialized.Encrypt(EncryptKey), isPersistent);
            // add auth token to cookie
            controller.AddCookie(HttpConstant.AuthToken, token, isPersistent);
        }
    }
}
