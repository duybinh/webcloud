﻿using System;
using System.Linq;
using System.Security.Principal;
using WebCloud.Common.Constant;

namespace WebCloud.Common.Auth
{
    public class WebCloudPrincipal : IWebCloudPrincipal
    {
        public WebCloudPrincipal(long globalID, string websiteID, string userID)
        {
            UserID = userID;
            WebsiteID = websiteID;
            GlobalID = globalID;
            Identity = new GenericIdentity(websiteID + "." + userID);
        }

        public IIdentity Identity { get; private set; }
        public long GlobalID { get; private set; }
        public string WebsiteID { get; private set; }
        public string UserID { get; private set; }
        public string LanguageID { get; set; }
        public string Email { get; set; }
        public bool IsVerifiedEmail { get; set; }
        public string[] Roles { get; set; }

        public bool IsInRole(string role)
        {
            return Roles.Contains(role) || Roles.Contains(RoleValue.RootAdmin);
        }

        public bool IsInRoles(params string[] roles)
        {
            // Bypass RootAdmin 
            if (Roles.Contains(RoleValue.RootAdmin)) return true;

            return roles.Any(role => Roles.Contains(role));
        }

        public bool HasPermission(string requiredRoles)
        {
            if (requiredRoles == "*")
                return true;

            var roles = requiredRoles.Split(new []{";"}, StringSplitOptions.RemoveEmptyEntries);
            return IsInAllRoles(roles);
        }

        public bool IsInAllRoles(params string[] roles)
        {
            // Bypass RootAdmin 
            if (Roles.Contains(RoleValue.RootAdmin)) return true;

            return roles.All(role => Roles.Contains(role));
        }
    }
}
