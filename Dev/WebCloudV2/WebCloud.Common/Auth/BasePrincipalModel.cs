﻿using System;

namespace WebCloud.Common.Auth
{
    [Serializable]
    public class BasePrincipalModel
    {
        public long GlobalID { get; set; }
        public string WebsiteID { get; set; }
        public string UserID { get; set; }
        public string LanguageID { get; set; }
        public string Email { get; set; }
        public bool IsVerifiedEmail { get; set; }
        public string[] Roles { get; set; }
    }
}
