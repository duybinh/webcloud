﻿using System.Security.Principal;

namespace WebCloud.Common.Auth
{
    public interface IWebCloudPrincipal : IPrincipal
    {
        long GlobalID { get; }
        string WebsiteID { get; }
        string UserID { get; }
        string LanguageID { get; }
        string Email { get; }
        bool IsVerifiedEmail { get; }
        string TargetWebsiteID { get; }
        string[] Roles { get; }
        string[] Groups { get; }

        bool IsInRoles(params string[] roles);
        bool HasPermission(string requiredRoles);
        bool IsInAllRoles(params string[] roles);
    }

}
