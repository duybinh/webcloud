﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Security.Principal;
using System.Text;
using System.Threading.Tasks;
using DHSoft.Utility.Common;
using WebCloud.Common.Constant;

namespace WebCloud.Common.Auth
{
    public class WebCloudPrincipal : IWebCloudPrincipal
    {
        public IIdentity Identity { get; private set; }
        public ClaimsPrincipal OriginalClaimsPrincipal { get; private set; }

        public long GlobalID { get; private set; }
        public string WebsiteID { get; private set; }
        public string UserID { get; private set; }
        public string LanguageID { get; private set; }
        public string Email { get; private set; }
        public bool IsVerifiedEmail { get; private set; }
        public string[] Roles { get; private set; }
        public string[] Groups { get; private set; }
        public string TargetWebsiteID { get; private set; }

        public WebCloudPrincipal(ClaimsPrincipal principal)
        {
            this.Identity = principal.Identity;
            this.OriginalClaimsPrincipal = principal;

            var identity = (ClaimsIdentity) Identity;

            var tmpClaim = identity.Claims.FirstOrDefault(o => o.Type == ClaimTypes.Name);
            if (tmpClaim != null)
                this.UserID = tmpClaim.Value;

            tmpClaim = identity.Claims.FirstOrDefault(o => o.Type == "global_id");
            if (tmpClaim != null)
                this.GlobalID = tmpClaim.Value.ToInt64TryParse();

            tmpClaim = identity.Claims.FirstOrDefault(o => o.Type == "user_website");
            if (tmpClaim != null)
                this.WebsiteID = tmpClaim.Value;

            tmpClaim = identity.Claims.FirstOrDefault(o => o.Type == "target_website");
            if (tmpClaim != null)
                this.TargetWebsiteID = tmpClaim.Value;

            tmpClaim = identity.Claims.FirstOrDefault(o => o.Type == "email");
            if (tmpClaim != null)
                this.Email = tmpClaim.Value;

            tmpClaim = identity.Claims.FirstOrDefault(o => o.Type == "is_verified_email");
            if (tmpClaim != null)
                this.IsVerifiedEmail = tmpClaim.Value.ToBooleanTryParse(false);

            tmpClaim = identity.Claims.FirstOrDefault(o => o.Type == ClaimTypes.Role);
            if (tmpClaim != null)
                this.Roles = tmpClaim.Value.SplitToArray(",");

            tmpClaim = identity.Claims.FirstOrDefault(o => o.Type == "groups");
            if (tmpClaim != null)
                this.Groups = tmpClaim.Value.SplitToArray(",");

        }

        public bool IsInRole(string role)
        {
            return Roles.Contains(role) || Roles.Contains(RoleValue.RootAdmin);
        }

        public bool IsInRoles(params string[] roles)
        {
            // Bypass RootAdmin 
            if (Roles.Contains(RoleValue.RootAdmin)) return true;

            return roles.Any(role => Roles.Contains(role));
        }

        public bool HasPermission(string requiredRoles)
        {
            if (requiredRoles == "*")
                return true;

            var roles = requiredRoles.Split(new[] { ";" }, StringSplitOptions.RemoveEmptyEntries);
            return IsInAllRoles(roles);
        }

        public bool IsInAllRoles(params string[] roles)
        {
            // Bypass RootAdmin 
            if (Roles.Contains(RoleValue.RootAdmin)) return true;

            return roles.All(role => Roles.Contains(role));
        }

    }
}
