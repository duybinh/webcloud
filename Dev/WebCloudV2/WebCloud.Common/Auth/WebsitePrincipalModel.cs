﻿namespace WebCloud.Common.Auth
{
    public class WebsitePrincipalModel : BasePrincipalModel
    {
        public string DisplayName { get; set; }
        public string Avatar { get; set; }
    }
}
