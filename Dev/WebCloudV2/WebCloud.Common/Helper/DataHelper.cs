﻿using System;

namespace WebCloud.Common.Helper
{
    public class DataHelper
    {
        public static string GenerateId()
        {
            return Guid.NewGuid().ToString();
        }
       
        /// <summary>
        ///  return "Table_Id_Field"
        /// </summary>
        /// <param name="table"></param>
        /// <param name="objectId"></param>
        /// <param name="field"></param>
        /// <returns></returns>
        public static string CombineTextId(string table, string objectId, string field)
        {
            return string.Format("{0}__{1}__{2}", table,objectId, field);
        }

        public static bool ExtractTextId(string textId, out string table, out string objectId, out string field)
        {
            table = field = objectId = null;

            if (textId.Contains("__"))
            {
                string[] strs = textId.Split(new[] { "__" }, StringSplitOptions.RemoveEmptyEntries);
                if (strs.Length == 3)
                {
                    table = strs[0];
                    objectId = strs[1];
                    field = strs[2];
                    return true;
                }
            }

            return false;
        }
    }
}
