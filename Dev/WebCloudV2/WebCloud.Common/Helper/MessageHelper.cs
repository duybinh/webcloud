﻿using WebCloud.Common.Exception;

namespace WebCloud.Common.Helper
{
    public static class MessageHelper
    {
        public static Message GenerateMessage(this System.Exception exception)
        {
            if (exception is BusinessException)
            {
                return new Message (((BusinessException)exception).ErrorCode);
            }

            return new Message(exception);
        }
    }
}
