﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace WebCloud.Common
{
    [Serializable]
    [DataContract]
    public class ResultBase
    {
        [DataMember]
        public Boolean IsSuccess { set; get; }
        [DataMember]
        public Message Message { set; get; }
    }
}
