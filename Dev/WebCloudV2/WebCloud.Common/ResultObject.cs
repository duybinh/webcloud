﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace WebCloud.Common
{
    [Serializable]
    [DataContract]
    public class ResultObject<T> : ResultBase
    {
        [DataMember]
        public T Data { set; get; }
    }
}
