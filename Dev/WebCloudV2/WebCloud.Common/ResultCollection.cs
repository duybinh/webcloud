﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace WebCloud.Common
{
    [Serializable]
    [DataContract]
    public class ResultCollection<T>:ResultBase
    {
        [DataMember]
        public List<T> Data { set; get; } 
    }
}
