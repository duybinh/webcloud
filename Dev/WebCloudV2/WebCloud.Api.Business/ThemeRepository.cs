﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using DHSoft.Utility.Validation;
using WebCloud.Api.DataAccess;
using WebCloud.Common;
using WebCloud.Common.Constant;
using WebCloud.Common.Exception;
using WebCloud.Common.Helper;
using WebCloud.Entity;

namespace WebCloud.Api.Business
{
    public interface IThemeRepository : IContextRepository
    {
        ResultObject<List<Theme>> GetAll();
        ResultObject<List<Theme>> GetThemesInWebsite();
        ResultObject<bool> IsExist(string id);
        ResultBase Add(Theme theme);
        ResultBase Edit(Theme theme);
        ResultBase Delete(string id);
        ResultObject<Theme> GetById(string id);
        void Validate(Theme theme, bool isNew);
        string GetThemeFolder(string theme);
    }

    public class ThemeRepository : IThemeRepository
    {
        private readonly IThemeDataRepository _themeDataRepository;

        public ThemeRepository(IThemeDataRepository themeDataRepository)
        {
            _themeDataRepository = themeDataRepository;
        }

        public ResultObject<List<Theme>> GetAll()
        {
            try
            {
                var result = _themeDataRepository.GetAll();
                return new ResultObject<List<Theme>> { IsSuccess = true, Data = result };
            }
            catch (Exception exception)
            {
                return new ResultObject<List<Theme>> { IsSuccess = false, Message = MessageHelper.GenerateMessage(exception) };
            }
        }

        public ResultObject<List<Theme>> GetThemesInWebsite()
        {
            throw new NotImplementedException();
        }

        public ResultObject<bool> IsExist(string id)
        {
            try
            {
                var result = _themeDataRepository.IsExist(id);
                return new ResultObject<bool> { IsSuccess = true, Data = result };
            }
            catch (Exception exception)
            {
                return new ResultObject<bool> { IsSuccess = false, Message = MessageHelper.GenerateMessage(exception) };
            }
        }

        public ResultBase Add(Theme theme)
        {
            try
            {
                Validate(theme, true);
                if (_themeDataRepository.Add(theme))
                {
                    // create theme folder
                    string themeFolder = GetThemeFolder(theme.ThemeCode);
                    if (!Directory.Exists(themeFolder))
                    {
                        Directory.CreateDirectory(themeFolder);
                    }
                    return new ResultBase { IsSuccess = true };
                }

                throw new BusinessException(MessageCode.Unknow);
            }
            catch (Exception exception)
            {
                return new ResultBase { IsSuccess = false, Message = MessageHelper.GenerateMessage(exception) };
            }
        }

        public ResultBase Edit(Theme theme)
        {
            try
            {
                Validate(theme, false);
                if (_themeDataRepository.Edit(theme))
                {
                    return new ResultBase { IsSuccess = true };
                }

                throw new BusinessException(MessageCode.Theme_IdNotExist);
            }
            catch (Exception exception)
            {
                return new ResultBase { IsSuccess = false, Message = MessageHelper.GenerateMessage(exception) };
            }
        }

        public ResultBase Delete(string id)
        {
            try
            {
                if (!_themeDataRepository.IsExist(id))
                {
                    throw new BusinessException(MessageCode.Theme_IdNotExist);
                }

                if (_themeDataRepository.Delete(id))
                {
                    // create theme folder
                    string themeFolder = GetThemeFolder(id);
                    if (Directory.Exists(themeFolder))
                    {
                        Directory.Delete(themeFolder, true);
                    }
                    return new ResultBase { IsSuccess = true };
                }

                throw new BusinessException(MessageCode.Unknow);
            }
            catch (Exception exception)
            {
                return new ResultBase { IsSuccess = false, Message = MessageHelper.GenerateMessage(exception) };
            }
        }

        public ResultObject<Theme> GetById(string id)
        {
            try
            {
                var result = _themeDataRepository.GetById(id);
                if (result == null)
                {
                    throw new BusinessException(MessageCode.Theme_IdNotExist);
                }

                return new ResultObject<Theme> { IsSuccess = true, Data = result };
            }
            catch (Exception exception)
            {
                return new ResultObject<Theme> { IsSuccess = false, Message = MessageHelper.GenerateMessage(exception) };
            }
        }

        public void Validate(Theme obj, bool isNew)
        {
            if (isNew)
            {
                if (!obj.ThemeCode.IsValidId())
                    throw new BusinessException(MessageCode.Id_Invalid);

                if (_themeDataRepository.IsExist(obj.ThemeCode))
                    throw new BusinessException(MessageCode.Theme_IdExist);
            }

            if (!string.IsNullOrEmpty(obj.BasedTheme))
            {
                if (!obj.ThemeCode.IsValidId())
                    throw new BusinessException(MessageCode.Theme_BasedTheme_Invalid);

                if (!_themeDataRepository.IsExist(obj.BasedTheme))
                    throw new BusinessException(MessageCode.Theme_BasedThemeNotExist);

                if (obj.BasedTheme == obj.ThemeCode)
                    throw new BusinessException(MessageCode.Theme_BasedTheme_Same_ThemeCode);
            }

            if (string.IsNullOrEmpty(obj.Name))
                throw new BusinessException(MessageCode.Name_Required);

            if (!obj.Name.IsValidLength(1, 250))
                throw new BusinessException(MessageCode.Name_Length_Invalid);

            if (!obj.Description.IsValidLength(0, 500))
                throw new BusinessException(MessageCode.Description_Length_Invalid);

            if (!obj.Author.IsValidLength(0, 250))
                throw new BusinessException(MessageCode.Theme_Author_Length_Invalid);

            if (!obj.RolesToView.IsValidLength(0, 250))
                throw new BusinessException(MessageCode.Theme_RolesToView_Length_Invalid);

            if (!obj.RolesToInstall.IsValidLength(0, 250))
                throw new BusinessException(MessageCode.Theme_RolesToInstall_Length_Invalid);

        }

        public string GetThemeFolder(string theme)
        {
            if (HttpContext.Current == null) return null;
            return HttpContext.Current.Server.MapPath(string.Format("~/Resources/websites/{0}/themes/{1}", Context.WebsiteId, theme));
        }

        public IContext Context { get; set; }
    }
}
