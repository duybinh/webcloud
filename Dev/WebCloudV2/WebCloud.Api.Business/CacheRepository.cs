﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Caching;
using DHSoft.Utility.Common;
using WebCloud.Common.Wrapper;

namespace WebCloud.Api.Business
{
    public interface ICacheRepository : IContextRepository
    {
        void InsertToCache(object data, string key, bool isGlobalCache = false, bool allLanguages = false, bool updateExist = true);
        object GetFromCache(string key, bool isGlobalCache = false, bool allLanguages = false);
        object DeleteCache(string key, bool isGlobalCache = false, bool allLanguages = false);
        string BuildCacheKey(string key, bool isGlobalCache = false, bool allLanguages = false);
        void ClearSystemCache();
        void ClearCacheMatchWebsite(string websiteId);
        void ClearCacheMatchKey(string key);
        void ClearCacheMatchKeyAndLanguage(string key, string languageId);
    }

    public class CacheRepository : ContextRepository, ICacheRepository
    {
        public void InsertToCache(object data, string key, bool isGlobalCache = false, bool allLanguages = false, bool updateExist = true)
        {
            key = BuildCacheKey(key, isGlobalCache, allLanguages);
            Cache cache = HttpContext.Current.Cache;
            if (cache[key] != null)
            {
                if (updateExist)
                {
                    cache.Remove(key);
                    cache.Insert(key, data, null, DateTime.Now.AddDays(CommonConfiguration.CacheTime), TimeSpan.Zero);
                }
                else
                {
                    cache[key] = data;
                }
            }
            else
            {
                cache.Insert(key, data, null, DateTime.Now.AddDays(CommonConfiguration.CacheTime), TimeSpan.Zero);
            }
        }

        public object GetFromCache(string key, bool isGlobalCache = false, bool allLanguages = false)
        {
            key = BuildCacheKey(key, isGlobalCache, allLanguages);
            Cache cache = HttpContext.Current.Cache;
            return cache[key];
        }

        public object DeleteCache(string key, bool isGlobalCache = false, bool allLanguages = false)
        {
            key = BuildCacheKey(key, isGlobalCache, allLanguages);
            Cache cache = HttpContext.Current.Cache;
            return cache.Remove(key);
        }

        public string BuildCacheKey(string key, bool isGlobalCache = false, bool allLanguages = false)
        {
            if (!isGlobalCache) key += "__" + Context.WebsiteId;
            if (!allLanguages) key += "__" + Context.LanguageId;
            return key;
        }

        public void ClearSystemCache()
        {
            Cache cache = HttpContext.Current.Cache;
            IDictionaryEnumerator enumerator = cache.GetEnumerator();
            while (enumerator.MoveNext())
            {
                cache.Remove(enumerator.Key.ToString());
            }
        }

        public void ClearCacheMatchWebsite(string websiteId)
        {
            Cache cache = HttpContext.Current.Cache;
            IDictionaryEnumerator enumerator = cache.GetEnumerator();
            while (enumerator.MoveNext())
            {
                if (IsCacheOfWebsite(enumerator.Key.ToString(), websiteId))
                    cache.Remove(enumerator.Key.ToString());
            }
        }

        public void ClearCacheMatchKey(string key)
        {
            Cache cache = HttpContext.Current.Cache;
            IDictionaryEnumerator enumerator = cache.GetEnumerator();
            while (enumerator.MoveNext())
            {
                if (IsCacheOfKey(enumerator.Key.ToString(), key))
                    cache.Remove(enumerator.Key.ToString());
            }
        }

        public void ClearCacheMatchKeyAndLanguage(string key, string languageId)
        {
            Cache cache = HttpContext.Current.Cache;
            IDictionaryEnumerator enumerator = cache.GetEnumerator();
            while (enumerator.MoveNext())
            {
                if (IsCacheOfKeyAndLanguage(enumerator.Key.ToString(), key, languageId))
                    cache.Remove(enumerator.Key.ToString());
            }
        }

        #region private method
        private bool IsCacheOfWebsite(string cacheKey, string websiteId)
        {
            if (cacheKey != null && cacheKey.Contains("__"))
            {
                string[] strs = cacheKey.Split(new[] { "__" }, StringSplitOptions.None);
                return strs.Length > 1 && strs[1].Equals(websiteId);
            }
            return false;
        }

        private bool IsCacheOfKey(string cacheKey, string key)
        {
            if (cacheKey != null && cacheKey.Contains("__"))
            {
                string[] strs = cacheKey.Split(new[] { "__" }, StringSplitOptions.None);
                return strs.Length > 0 && strs[0].Equals(key);
            }
            return false;
        }

        private bool IsCacheOfKeyAndLanguage(string cacheKey, string key, string languageId)
        {
            if (cacheKey != null && cacheKey.Contains("__"))
            {
                string[] strs = cacheKey.Split(new[] { "__" }, StringSplitOptions.None);
                return strs.Length > 2 && strs[0].Equals(key) && strs[2].Equals(languageId);
            }
            return false;
        }
        #endregion
    }
}
