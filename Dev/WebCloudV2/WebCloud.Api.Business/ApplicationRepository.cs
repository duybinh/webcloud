﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DHSoft.Utility.Security;
using WebCloud.Common;
using WebCloud.Api.DataAccess;
using WebCloud.Entity;

namespace WebCloud.Api.Business
{
    public interface IApplicationRepository
    {
        bool ValidateApplicationSecret(string appId, string secret);
        ResultCollection<Application> GetAll();
        void ClearCache();
    }

    public class ApplicationRepository : IApplicationRepository
    {
        private readonly IApplicationDataRepository _applicationDataRepository;
        private readonly ICacheRepository _cacheRepository;

        public ApplicationRepository(IApplicationDataRepository applicationDataRepository, ICacheRepository cacheRepository)
        {
            _applicationDataRepository = applicationDataRepository;
            _cacheRepository = cacheRepository;
        }

        #region Implementation of IApplicationRepository

        public bool ValidateApplicationSecret(string appId, string secret)
        {
            secret = secret.GetMd5Hash();
            var result = GetAll();
            if (result.IsSuccess)
            {
                return result.Data.Any(obj => obj.ApplicationId == appId && obj.SecretKey == secret);
            }
            return false;
        }

        public ResultCollection<Application> GetAll()
        {
            var applications = (List<Application>)_cacheRepository.GetFromCache("ListApplication", true, true);

            if (applications == null)
            {
                applications = _applicationDataRepository.GetAll();
                _cacheRepository.InsertToCache(applications, "ListApplication", true, true);
            }
            return new ResultCollection<Application>
                       {
                           IsSuccess = true,
                           Data = applications
                       };

        }

        public void ClearCache()
        {
            _cacheRepository.DeleteCache("ListApplication", true, true);
        }

        #endregion
    }
}
