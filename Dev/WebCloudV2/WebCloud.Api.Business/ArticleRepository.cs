﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DHSoft.Utility.Validation;
using WebCloud.Api.DataAccess;
using WebCloud.Common;
using WebCloud.Common.Constant;
using WebCloud.Common.Exception;
using WebCloud.Common.Helper;
using WebCloud.Entity;

namespace WebCloud.Api.Business
{
    public interface IArticleRepository : IContextRepository
    {
        ResultPagging<Article> Query(ArticleQuery query);
        ResultObject<List<Article>> GetAll();
        ResultObject<List<Article>> Trash();
        ResultObject<Article> GetById(string id);
        ResultObject<bool> IsExist(string id);
        ResultObject<string> Add(Article obj);
        ResultBase Edit(Article obj);
        ResultBase Delete(string id);
        ResultBase Remove(string id);
        ResultBase Restore(string id);
        ResultBase EmptyTrash();
        ResultBase DeleteInLanguage(string id);
        void Validate(Article obj, bool isNew);
        ResultBase UpdateFields(string id, Dictionary<string, object> fields);
    }

    public class ArticleRepository : IArticleRepository
    {
        private readonly IArticleDataRepository _dataRepository;
        public ArticleRepository(IArticleDataRepository dataRepository)
        {
            _dataRepository = dataRepository;
        }

        public IContext Context { get; set; }

        public ResultPagging<Article> Query(ArticleQuery query)
        {
            try
            {
                int total;
                if (query == null) query = new ArticleQuery();
                return new ResultPagging<Article>
                {
                    IsSuccess = true,
                    Data = _dataRepository.Query(out total, query, Context.WebsiteId, Context.LanguageId),
                    Total = total
                };
            }
            catch (Exception exception)
            {
                return new ResultPagging<Article> { IsSuccess = false, Message = MessageHelper.GenerateMessage(exception) };
            }
        }

        public ResultObject<List<Article>> GetAll()
        {
            try
            {
                var result = _dataRepository.GetAll(Context.WebsiteId, Context.LanguageId);
                return new ResultObject<List<Article>>
                {
                    IsSuccess = true,
                    Data = result
                };
            }
            catch (Exception exception)
            {
                return new ResultObject<List<Article>> { IsSuccess = false, Message = MessageHelper.GenerateMessage(exception) };
            }
        }

        public ResultObject<List<Article>> Trash()
        {
            try
            {
                var result = _dataRepository.GetAll(Context.WebsiteId, Context.LanguageId, Default.LanguageId, true);
                return new ResultObject<List<Article>>
                {
                    IsSuccess = true,
                    Data = result
                };
            }
            catch (Exception exception)
            {
                return new ResultObject<List<Article>> { IsSuccess = false, Message = MessageHelper.GenerateMessage(exception) };
            }
        }

        public ResultObject<Article> GetById(string id)
        {
            try
            {
                if (string.IsNullOrWhiteSpace(id))
                    throw new BusinessException(MessageCode.Id_Required);

                var result = _dataRepository.GetById(Context.WebsiteId, id, Context.LanguageId);

                if (result == null)
                    throw new BusinessException(MessageCode.Article_IdNotExist);

                return new ResultObject<Article> { IsSuccess = true, Data = result };
            }
            catch (Exception exception)
            {
                return new ResultObject<Article> { IsSuccess = false, Message = MessageHelper.GenerateMessage(exception) };
            }
        }

        public ResultObject<bool> IsExist(string id)
        {
            throw new NotImplementedException();
        }

        public ResultObject<string> Add(Article obj)
        {
            try
            {
                Validate(obj, true);

                obj.ArticleID = IdGenerator.GenerateId();
                SetInsertInfo(obj);

                if (_dataRepository.Add(obj))
                {
                    return new ResultObject<string> { IsSuccess = true, Data = obj.ArticleID };
                }

                throw new BusinessException(MessageCode.Unknow);
            }
            catch (Exception exception)
            {
                return new ResultObject<string> { IsSuccess = false, Message = MessageHelper.GenerateMessage(exception) };
            }
        }

        public ResultBase Edit(Article obj)
        {
            try
            {
                Validate(obj, false);

                obj.Content.LanguageID = Context.LanguageId;
                SetUpdateInfo(obj);

                if (_dataRepository.Edit(obj))
                {
                    return new ResultBase { IsSuccess = true };
                }
                throw new BusinessException(MessageCode.Article_IdNotExist);
            }
            catch (Exception exception)
            {
                return new ResultBase { IsSuccess = false, Message = MessageHelper.GenerateMessage(exception) };
            }
        }

        public ResultBase Delete(string id)
        {
            try
            {
                var obj = new Article { ArticleID = id };
                SetUpdateInfo(obj);

                if (_dataRepository.Delete(obj))
                    return new ResultBase { IsSuccess = true };

                throw new BusinessException(MessageCode.Article_IdNotExist);
            }
            catch (Exception exception)
            {
                return new ResultBase { IsSuccess = false, Message = MessageHelper.GenerateMessage(exception) };
            }
        }

        public ResultBase Remove(string id)
        {
            try
            {
                var obj = new Article { ArticleID = id };
                SetUpdateInfo(obj);

                if (_dataRepository.Remove(obj))
                    return new ResultBase { IsSuccess = true };

                throw new BusinessException(MessageCode.Article_IdNotExist);
            }
            catch (Exception exception)
            {
                return new ResultBase { IsSuccess = false, Message = MessageHelper.GenerateMessage(exception) };
            }
        }

        public ResultBase Restore(string id)
        {
            try
            {
                var obj = new Article { ArticleID = id };
                SetUpdateInfo(obj);

                if (_dataRepository.Restore(obj))
                    return new ResultBase { IsSuccess = true };

                throw new BusinessException(MessageCode.Article_IdNotExist);
            }
            catch (Exception exception)
            {
                return new ResultBase { IsSuccess = false, Message = MessageHelper.GenerateMessage(exception) };
            }
        }

        public ResultBase EmptyTrash()
        {
            try
            {
                if (_dataRepository.EmptyTrash(Context.WebsiteId))
                    return new ResultBase { IsSuccess = true };

                throw new BusinessException(MessageCode.Unknow);
            }
            catch (Exception exception)
            {
                return new ResultBase { IsSuccess = false, Message = MessageHelper.GenerateMessage(exception) };
            }
        }

        public ResultBase DeleteInLanguage(string id)
        {
            try
            {
                var obj = new Article()
                {
                    ArticleID = id,
                    Content = new ArticleContent()
                    {
                        LanguageID = Context.LanguageId
                    }
                };
                SetUpdateInfo(obj);

                if (_dataRepository.Delete(obj))
                    return new ResultBase { IsSuccess = true };

                throw new BusinessException(MessageCode.Article_IdNotExist);
            }
            catch (Exception exception)
            {
                return new ResultBase { IsSuccess = false, Message = MessageHelper.GenerateMessage(exception) };
            }
        }

        public void Validate(Article obj, bool isNew)
        {
            if (obj.DisplayDatetime.Year == 1)
            {
                obj.DisplayDatetime = DateTime.Now;
            }
            else
            {
                obj.DisplayDatetime = new DateTime(obj.DisplayDatetime.Year, obj.DisplayDatetime.Month, obj.DisplayDatetime.Day, DateTime.Now.Hour, DateTime.Now.Minute, DateTime.Now.Second);
            }

            if (!isNew)
            {
                if (string.IsNullOrEmpty(obj.ArticleID))
                    throw new BusinessException(MessageCode.Id_Required);
            }

            if (obj.Content == null)
                throw new BusinessException(MessageCode.Title_Required);

            if (string.IsNullOrEmpty(obj.Content.Title))
                throw new BusinessException(MessageCode.Title_Required);

            if (!obj.Content.Title.IsValidLength(1, 250))
                throw new BusinessException(MessageCode.Title_Length_Invalid);

            if (!obj.Content.Description.IsValidLength(0, 500))
                throw new BusinessException(MessageCode.Description_Length_Invalid);

            if (string.IsNullOrWhiteSpace(obj.Content.Content))
                throw new BusinessException(MessageCode.Content_Required);

            if (!obj.Content.MetaDescription.IsValidLength(0, 500))
                throw new BusinessException(MessageCode.MetaDescription_Length_Invalid);

            if (!obj.Content.MetaKeyword.IsValidLength(0, 500))
                throw new BusinessException(MessageCode.MetaKeyword_Length_Invalid);

            if (!obj.Content.MetaTitle.IsValidLength())
                throw new BusinessException(MessageCode.MetaTitle_Length_Invalid);

            if (!obj.ThumbImage.IsValidLength())
                throw new BusinessException(MessageCode.ThumbImage_Length_Invalid);

            if (obj.TagValues != null && obj.TagValues.Count > 0)
            {
                obj.TagValues.RemoveAll(string.IsNullOrWhiteSpace);

                for (int i = 0; i < obj.TagValues.Count; i++)
                {
                    obj.TagValues[i] = obj.TagValues[i].Trim();
                }
            }
        }

        public ResultBase UpdateFields(string id, Dictionary<string, object> fields)
        {
            try
            {
                Validate(fields);
                SetUpdateInfo(fields);

                if (_dataRepository.UpdateFields(Context.WebsiteId, id, Context.LanguageId, fields))
                    return new ResultBase { IsSuccess = true };

                throw new BusinessException(MessageCode.Article_IdNotExist);
            }
            catch (Exception exception)
            {
                return new ResultBase { IsSuccess = false, Message = MessageHelper.GenerateMessage(exception) };
            }
        }

        #region Private methods

        private void Validate(Dictionary<string, object> fields)
        {
            foreach (var field in fields)
            {
                string tmp;
                switch (field.Key)
                {
                    case "Content.Title":
                        tmp = field.Value.ToString();
                        if (string.IsNullOrEmpty(tmp))
                            throw new BusinessException(MessageCode.Title_Required);
                        if (!tmp.IsValidLength(1, 250))
                            throw new BusinessException(MessageCode.Title_Length_Invalid);
                        break;

                    case "Content.Description":
                        tmp = field.Value.ToString();
                        if (!tmp.IsValidLength(0, 500))
                            throw new BusinessException(MessageCode.Description_Length_Invalid);
                        break;

                    case "Content.Content":
                        tmp = field.Value.ToString();
                        if (string.IsNullOrWhiteSpace(tmp))
                            throw new BusinessException(MessageCode.Content_Required);
                        break;
                }
            }
        }

        private void SetUpdateInfo(Article obj)
        {
            obj.WebsiteID = Context.WebsiteId;
            obj.LastEditedUserName = Context.Principal.UserID;
            obj.LastEditedUserID = Context.Principal.GlobalID;
            obj.LastEditedDatetime = DateTime.Now;
        }
        private void SetUpdateInfo(Dictionary<string, object> fields)
        {
            fields["LastEditedUserName"] = Context.Principal.UserID;
            fields["LastEditedUserID"] = Context.Principal.GlobalID;
            fields["LastEditedDatetime"] = DateTime.Now;
        }
        private void SetInsertInfo(Article obj)
        {
            obj.WebsiteID = Context.WebsiteId;
            obj.Content.LanguageID = Context.LanguageId;
            obj.CreatedUserName = obj.LastEditedUserName = Context.Principal.UserID;
            obj.CreatedUserID = obj.LastEditedUserID = Context.Principal.GlobalID;
            obj.CreatedDatetime = obj.LastEditedDatetime = DateTime.Now;
        }

        #endregion
    }
}
