﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DHSoft.Utility.Common;
using DHSoft.Utility.Validation;
using WebCloud.Common;
using WebCloud.Common.Constant;
using WebCloud.Api.DataAccess;
using WebCloud.Common.Exception;
using WebCloud.Common.Helper;
using WebCloud.Entity;

namespace WebCloud.Api.Business
{
    public interface IConfigurationRepository : IContextRepository
    {
        #region Management
        ResultCollection<Config> GetAll();
        ResultObject<Config> GetById(string key);
        ResultObject<bool> IsExist(string key);
        ResultBase Add(Config obj);
        ResultBase Edit(Config obj);
        ResultBase Delete(string key);
        void Validate(Config obj, bool isNew);
        #endregion


        ResultObject<Dictionary<string, Object>> GetForSite();
        ResultBase Update(string key, Object value);
        //ResultBase Update(Dictionary<string, Object> configs);
    }

    public class ConfigurationRepository : ContextRepository, IConfigurationRepository
    {
        private readonly IConfigurationDataRepository _configDataRepository;

        public ConfigurationRepository(IConfigurationDataRepository configDataRepository)
        {
            _configDataRepository = configDataRepository;
        }

        #region Management
        public ResultCollection<Config> GetAll()
        {
            try
            {
                List<Config> configs;
                if (Context.WebsiteId == Default.WebsiteId)
                {
                    configs = _configDataRepository.GetAll();
                    foreach (var config in configs)
                    {
                        config.Value = config.DefaultValue;
                    }
                }
                else
                {
                    configs = _configDataRepository.GetAll(isEnabled: true);
                    var configInWebsite = _configDataRepository.GetConfigsInWebsite(Context.WebsiteId);
                    foreach (var config in configs)
                    {
                        if (configInWebsite.ContainsKey(config.ConfigID))
                            config.Value = configInWebsite[config.ConfigID];
                        else
                            config.Value = config.DefaultValue;
                    }
                }

                return new ResultCollection<Config> { IsSuccess = true, Data = configs };
            }
            catch (Exception exception)
            {
                return new ResultCollection<Config> { IsSuccess = false, Message = MessageHelper.GenerateMessage(exception) };
            }
        }

        public ResultObject<Config> GetById(string key)
        {
            try
            {
                var config = _configDataRepository.GetById(key);
                if (config == null)
                    throw new BusinessException(MessageCode.Config_IdNotExist);

                if (Context.WebsiteId != Default.WebsiteId)
                {
                    string configInWebsite = _configDataRepository.GetConfigInWebsite(Context.WebsiteId, key);
                    if (configInWebsite != null)
                        config.Value = configInWebsite;
                    else
                        config.Value = config.DefaultValue;
                }

                return new ResultObject<Config> { IsSuccess = true, Data = config };
            }
            catch (Exception exception)
            {
                return new ResultObject<Config> { IsSuccess = false, Message = MessageHelper.GenerateMessage(exception) };
            }
        }

        public ResultObject<bool> IsExist(string key)
        {
            throw new NotImplementedException();
        }

        public ResultBase Add(Config entity)
        {
            try
            {
                Validate(entity, true);
                if (_configDataRepository.Add(entity))
                {
                    return new ResultBase { IsSuccess = true };
                }

                throw new BusinessException(MessageCode.Unknow);
            }
            catch (Exception exception)
            {
                return new ResultBase { IsSuccess = false, Message = MessageHelper.GenerateMessage(exception) };
            }
        }

        public ResultBase Edit(Config entity)
        {
            try
            {
                Validate(entity, false);
                if (_configDataRepository.Edit(entity))
                {
                    return new ResultBase { IsSuccess = true };
                }

                throw new BusinessException(MessageCode.Config_IdNotExist);
            }
            catch (Exception exception)
            {
                return new ResultBase { IsSuccess = false, Message = MessageHelper.GenerateMessage(exception) };
            }
        }

        public ResultBase Delete(string key)
        {
            try
            {
                if (!_configDataRepository.IsExist(key))
                {
                    throw new BusinessException(MessageCode.Config_IdNotExist);
                }

                if (_configDataRepository.Delete(key))
                {

                    return new ResultBase { IsSuccess = true };
                }

                throw new BusinessException(MessageCode.Unknow);
            }
            catch (Exception exception)
            {
                return new ResultBase { IsSuccess = false, Message = MessageHelper.GenerateMessage(exception) };
            }
        }

        public void Validate(Config obj, bool isNew)
        {
            if (isNew)
            {
                if (!obj.ConfigID.IsValidId())
                    throw new BusinessException(MessageCode.Id_Invalid);

                if (_configDataRepository.IsExist(obj.ConfigID))
                    throw new BusinessException(MessageCode.Config_IdExist);
            }

            if (string.IsNullOrEmpty(obj.Name))
                throw new BusinessException(MessageCode.Name_Required);

            if (!obj.Name.IsValidLength(1, 250))
                throw new BusinessException(MessageCode.Name_Length_Invalid);

            if (!obj.Description.IsValidLength(0, 500))
                throw new BusinessException(MessageCode.Description_Length_Invalid);

            if (!obj.RolesToView.IsValidLength(0, 250))
                throw new BusinessException(MessageCode.Config_RolesToView_Length_Invalid);

            if (!obj.RolesToEdit.IsValidLength(0, 250))
                throw new BusinessException(MessageCode.Config_RolesToEdit_Length_Invalid);

            if (!obj.EditingControl.IsValidLength(0, 250))
                throw new BusinessException(MessageCode.Config_RolesToEdit_Length_Invalid);

            if (!obj.Group.IsValidLength(0, 250))
                throw new BusinessException(MessageCode.Config_Group_Length_Invalid);

            // check data type & default value
            if (string.IsNullOrEmpty(obj.DataType))
                throw new BusinessException(MessageCode.Config_DataType_Required);

            if (string.IsNullOrEmpty(obj.DefaultValue))
                throw new BusinessException(MessageCode.Config_DefaultValue_Required);

            var defaultValue = obj.DefaultValue;
            if (!ValidationUtils.GetValidValueType(ref defaultValue, obj.DataType))
                throw new BusinessException(MessageCode.Config_DefaultValue_Invalid);

            obj.DefaultValue = defaultValue;
        }
        #endregion

        #region Website's configuration
        public ResultObject<Dictionary<string, object>> GetForSite()
        {
            try
            {
                var result = GetAll();
                if (!result.IsSuccess)
                    throw new BusinessException(result.Message.Code);

                // convert to real type
                var configsDict = result.Data.ToDictionary(config => config.ConfigID, config => config.Value.ToCorrectType(config.DataType));

                return new ResultObject<Dictionary<string, object>>
                {
                    IsSuccess = true,
                    //                    Data = result.Data.ToDictionary(o => o.ConfigID, o => o.Value)
                    Data = configsDict
                };
            }
            catch (BusinessException exception)
            {
                return new ResultObject<Dictionary<string, object>>
                {
                    IsSuccess = false,
                    Message = new Message(exception.ErrorCode)
                };
            }
        }

        public ResultBase Update(string key, object value)
        {
            try
            {
                var config = _configDataRepository.GetById(key);
                // check config key exist
                if (config == null)
                    throw new BusinessException(MessageCode.Config_KeyNotExist);

                // check permission to edit
                if (!config.RolesToEdit.IsContainRole(Context.Principal.Roles))
                    throw new BusinessException(MessageCode.Config_NoPermissionToEdit);

                _configDataRepository.UpdateWebsiteConfigs(Context.WebsiteId, new Dictionary<string, string>{{key,value.ToString()}});
                return new ResultBase{IsSuccess = true};
            }
            catch (BusinessException exception)
            {
                return new ResultBase
                {
                    IsSuccess = false,
                    Message = new Message(exception.ErrorCode)
                };
            }
        }
        public ResultBase Update(Dictionary<string, object> configs)
        {
            throw new NotImplementedException();
        }

        #endregion
    }
}
