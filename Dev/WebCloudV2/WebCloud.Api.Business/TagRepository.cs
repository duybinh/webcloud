﻿using System;
using System.Collections.Generic;
using System.Linq;
using WebCloud.Api.DataAccess;
using WebCloud.Common;
using WebCloud.Common.Helper;
using WebCloud.Entity;

namespace WebCloud.Api.Business
{
    public interface ITagRepository : IContextRepository
    {
        ResultObject<List<Tag>> GetAll();
        ResultObject<List<string>> GetAllTagValues();
    }

    public class TagRepository : ITagRepository
    {
        private readonly ITagDataRepository _dataRepository;
        public TagRepository(ITagDataRepository dataRepository)
        {
            _dataRepository = dataRepository;
        }

        public IContext Context { get; set; }

        public ResultObject<List<Tag>> GetAll()
        {
            try
            {
                var result = _dataRepository.GetAll(Context.LanguageId);
                return new ResultObject<List<Tag>>
                {
                    IsSuccess = true,
                    Data = result
                };
            }
            catch (Exception exception)
            {
                return new ResultObject<List<Tag>> { IsSuccess = false, Message = MessageHelper.GenerateMessage(exception) };
            }
        }

        public ResultObject<List<string>> GetAllTagValues()
        {
            try
            {
                var result = _dataRepository.GetAll(Context.LanguageId).Select(obj => obj.Value).ToList();
                return new ResultObject<List<string>>
                {
                    IsSuccess = true,
                    Data = result
                };
            }
            catch (Exception exception)
            {
                return new ResultObject<List<string>> { IsSuccess = false, Message = MessageHelper.GenerateMessage(exception) };
            }
        }
    }
}
