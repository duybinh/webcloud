﻿using System.Threading;
using System.Web;
using WebCloud.Common.Auth;
using WebCloud.Common.Constant;

namespace WebCloud.Api.Business
{
    public interface IContext
    {
        string WebsiteId { get; }
        string LanguageId { get; }
        string ApplicationId { get; }
        IWebCloudPrincipal Principal { get; }
    }

    public class RequestContext : IContext
    {
        public RequestContext(string websiteId, string languageId, string applicationId, IWebCloudPrincipal principal)
        {
            Principal = principal;
            LanguageId = languageId;
            ApplicationId = applicationId;
            WebsiteId = websiteId;
        }

        public string WebsiteId { get; private set; }
        public string LanguageId { get; private set; }
        public string ApplicationId { get; private set; }
        public IWebCloudPrincipal Principal { get; private set; }

        public static RequestContext GetRequestContext(HttpRequest request)
        {
            string websiteId = request.Headers["WebsiteId"];
            string languageId = request.Headers["LanguageId"];
            string applicationId = request.Headers["ApplicationId"];
            //string applicationSecret = request.Headers["ApplicationSecret"];

            if (string.IsNullOrEmpty(websiteId)) websiteId = request["WebsiteId"];
            if (string.IsNullOrEmpty(languageId)) languageId = request["LanguageId"];

            if (string.IsNullOrEmpty(websiteId)) websiteId = Default.WebsiteId;
            if (string.IsNullOrEmpty(languageId)) languageId = Default.LanguageId;

            IWebCloudPrincipal principal = null;
            if (Thread.CurrentPrincipal != null && Thread.CurrentPrincipal.Identity.IsAuthenticated && Thread.CurrentPrincipal is IWebCloudPrincipal)
            {
                principal = (IWebCloudPrincipal)Thread.CurrentPrincipal;
            }
            else if (HttpContext.Current.User != null && HttpContext.Current.User.Identity.IsAuthenticated && HttpContext.Current.User is IWebCloudPrincipal)
            {
                principal = (IWebCloudPrincipal)HttpContext.Current.User;
            }

            if (principal != null)
            {
                websiteId = principal.TargetWebsiteID;
            }

            return new RequestContext(websiteId, languageId, applicationId, principal);
        }
    }
}
