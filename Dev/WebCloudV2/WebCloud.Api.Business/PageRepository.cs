﻿using System;
using System.Collections.Generic;
using System.Linq;
using DHSoft.Utility.Validation;
using WebCloud.Api.DataAccess;
using WebCloud.Common;
using WebCloud.Common.Constant;
using WebCloud.Common.Exception;
using WebCloud.Common.Helper;
using WebCloud.Entity;

namespace WebCloud.Api.Business
{
    public interface IPageRepository : IContextRepository
    {
        ResultCollection<Page> GetAll();
        ResultCollection<Page> GetDeleted();
        ResultObject<Page> GetById(string id);
        ResultBase Add(Page obj);
        ResultBase Edit(Page obj);
        ResultBase Delete(string id);
        ResultBase Install(string pageId);
        void Validate(Page obj, bool isNew = false);

        ResultCollection<Page> GetForSite();
    }

    public class PageRepository : IPageRepository
    {
        public IContext Context { get; set; }

        private readonly IPageDataRepository _pageDataRepository;

        public PageRepository(IPageDataRepository pagePageDataRepository)
        {
            _pageDataRepository = pagePageDataRepository;
        }

        public ResultCollection<Page> GetAll()
        {
            try
            {
                var websitePages = _pageDataRepository.GetAll(Context.WebsiteId, false)
                    //.OrderBy(o => o.Name)
                    .ToDictionary(o => o.PageID, o => o);

                if (Context.WebsiteId != Default.WebsiteId) // customer site
                {
                    var systemPages = _pageDataRepository.GetAll(Default.WebsiteId, false);
                    // add missing system page
                    foreach (var sysPage in systemPages)
                    {
                        if (!websitePages.ContainsKey(sysPage.PageID))
                        {
                            sysPage.IsSystemPage = true;
                            sysPage.Installable = true;
                            websitePages.Add(sysPage.PageID, sysPage);
                        }
                        else
                        {
                            websitePages[sysPage.PageID].IsSystemPage = true;
                        }
                    }
                }
                return new ResultCollection<Page> { IsSuccess = true, Data = websitePages.Values.ToList() };
            }
            catch (Exception exception)
            {
                return new ResultCollection<Page> { IsSuccess = false, Message = MessageHelper.GenerateMessage(exception) };
            }
        }

        public ResultCollection<Page> GetForSite()
        {
            try
            {
                //var websitePages = _pageDataRepository.GetAll(Context.WebsiteId, false, true, true).ToDictionary(o => o.PageID, o => o);
                //if (Context.WebsiteId != Default.WebsiteId) // customer site
                //{
                //    var systemPages = _pageDataRepository.GetAll(Default.WebsiteId, false, true, true);
                //    // add missing system page
                //    foreach (var sysPage in systemPages)
                //    {
                //        if (!websitePages.ContainsKey(sysPage.PageID))
                //        {
                //            sysPage.IsSystemPage = true;
                //            websitePages.Add(sysPage.PageID, sysPage);
                //        }
                //        else
                //        {
                //            websitePages[sysPage.PageID].IsSystemPage = true;
                //        }
                //    }
                //}

                //return new ResultCollection<Page> { IsSuccess = true, Data = websitePages.Values.ToList() };

                var websitePages = _pageDataRepository.GetAll(Context.WebsiteId, false, true, true);
                return new ResultCollection<Page> { IsSuccess = true, Data = websitePages };
            }
            catch (Exception exception)
            {
                return new ResultCollection<Page> { IsSuccess = false, Message = MessageHelper.GenerateMessage(exception) };
            }
        }

        public ResultCollection<Page> GetDeleted()
        {
            try
            {
                var deletedPages = _pageDataRepository.GetAll(Context.WebsiteId, true);
                return new ResultCollection<Page> { IsSuccess = true, Data = deletedPages };
            }
            catch (Exception exception)
            {
                return new ResultCollection<Page> { IsSuccess = false, Message = MessageHelper.GenerateMessage(exception) };
            }
        }

        public ResultObject<Page> GetById(string id)
        {
            try
            {
                if (string.IsNullOrWhiteSpace(id))
                    throw new BusinessException(MessageCode.Id_Required);

                var result = _pageDataRepository.GetById(Context.WebsiteId, id, Context.LanguageId);

                if (result == null && Context.WebsiteId != Default.WebsiteId)
                    result = _pageDataRepository.GetById(Default.WebsiteId, id, Context.LanguageId);

                if (result == null)
                    throw new BusinessException(MessageCode.Page_IdNotExist);

                return new ResultObject<Page> { IsSuccess = true, Data = result };
            }
            catch (Exception exception)
            {
                return new ResultObject<Page> { IsSuccess = false, Message = MessageHelper.GenerateMessage(exception) };
            }
        }

        public ResultBase Add(Page obj)
        {
            try
            {
                obj.WebsiteID = Context.WebsiteId;

                ValidatePermission();
                Validate(obj, true);

                if (!_pageDataRepository.Add(obj))
                    throw new BusinessException(MessageCode.Unknow);

                return new ResultBase { IsSuccess = true };
            }
            catch (Exception exception)
            {
                return new ResultBase { IsSuccess = false, Message = MessageHelper.GenerateMessage(exception) };
            }
        }

        public ResultBase Edit(Page obj)
        {
            try
            {
                obj.WebsiteID = Context.WebsiteId;

                ValidatePermission();
                Validate(obj, false);

                // check id is exist
                if (obj.WebsiteID == Default.WebsiteId)
                {
                    if (!_pageDataRepository.IsExist(Default.WebsiteId, obj.PageID))
                        throw new BusinessException(MessageCode.Page_IdNotExist);
                }
                else if (!_pageDataRepository.IsExist(obj.WebsiteID, obj.PageID))
                {
                    if (_pageDataRepository.IsExist(Default.WebsiteId, obj.PageID))
                        // add clone of system page to current site
                        return Add(obj);

                    throw new BusinessException(MessageCode.Page_IdNotExist);
                }

                if (!_pageDataRepository.Edit(obj, Context.LanguageId))
                    throw new BusinessException(MessageCode.Unknow);

                return new ResultBase { IsSuccess = true };
            }
            catch (Exception exception)
            {
                return new ResultBase { IsSuccess = false, Message = MessageHelper.GenerateMessage(exception) };
            }
        }

        public ResultBase Delete(string id)
        {
            try
            {
                ValidatePermission();

                if (string.IsNullOrWhiteSpace(id))
                    throw new BusinessException(MessageCode.Id_Required);

                if (_pageDataRepository.Delete(Context.WebsiteId, id))
                    return new ResultBase { IsSuccess = true };

                throw new BusinessException(MessageCode.Page_IdNotExist);
            }
            catch (Exception exception)
            {
                return new ResultBase { IsSuccess = false, Message = MessageHelper.GenerateMessage(exception) };
            }
        }

        public ResultBase Install(string pageId)
        {
            try
            {
                if (!_pageDataRepository.IsExist(Default.WebsiteId, pageId))
                    throw new BusinessException(MessageCode.Page_IdNotExist);

                if (_pageDataRepository.IsExist(Context.WebsiteId, pageId))
                    throw new BusinessException(MessageCode.Page_IdExist);

                if (_pageDataRepository.Install(Default.WebsiteId, pageId, Context.WebsiteId))
                    return new ResultBase { IsSuccess = true };

                throw new BusinessException(MessageCode.Unknow);
            }
            catch (Exception exception)
            {
                return new ResultBase { IsSuccess = false, Message = MessageHelper.GenerateMessage(exception) };
            }
        }

        public void Validate(Page obj, bool isNew = false)
        {
            if (isNew)
            {
                if (string.IsNullOrEmpty(obj.PageID))
                    obj.PageID = IdGenerator.GenerateId();
                else
                {
                    if (!obj.PageID.IsValidId())
                        throw new BusinessException(MessageCode.Id_Invalid);

                    // check id is not exist
                    if (_pageDataRepository.IsExist(obj.WebsiteID, obj.PageID))
                        throw new BusinessException(MessageCode.Page_IdExist);

                    if (obj.WebsiteID != Default.WebsiteId)
                    {
                        if (_pageDataRepository.IsExist(Default.WebsiteId, obj.PageID))
                            throw new BusinessException(MessageCode.Page_IdExist);
                    }
                }
            }
            else
            {
                if (string.IsNullOrEmpty(obj.PageID))
                    throw new BusinessException(MessageCode.Id_Required);
            }

            if (string.IsNullOrEmpty(obj.Name))
                throw new BusinessException(MessageCode.Name_Required);

            if (!obj.Name.IsValidLength(1, 250))
                throw new BusinessException(MessageCode.Name_Length_Invalid);

            if (string.IsNullOrEmpty(obj.LayoutID))
                throw new BusinessException(MessageCode.Page_LayoutIdRequired);

            if (!obj.MetaTitle.IsValidLength())
                throw new BusinessException(MessageCode.MetaTitle_Length_Invalid);

            if (!obj.MetaKeyword.IsValidLength(0, 500))
                throw new BusinessException(MessageCode.MetaKeyword_Length_Invalid);

            if (!obj.MetaDescription.IsValidLength(0, 500))
                throw new BusinessException(MessageCode.MetaDescription_Length_Invalid);
        }

        private void ValidatePermission()
        {
            // check permission
            if (Context.WebsiteId == Default.WebsiteId)
            {
                if (!Context.Principal.IsInRoles(RoleValue.RootAdmin, RoleValue.SetupAdmin))
                    throw new BusinessException(MessageCode.Permission_RoleRequired);
            }
        }


    }
}
