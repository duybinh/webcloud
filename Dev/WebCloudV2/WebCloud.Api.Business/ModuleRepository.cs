﻿using System;
using System.Collections.Generic;
using System.Linq;
using DHSoft.Utility.Validation;
using WebCloud.Api.DataAccess;
using WebCloud.Common;
using WebCloud.Common.Constant;
using WebCloud.Common.Exception;
using WebCloud.Common.Helper;
using WebCloud.Entity;

namespace WebCloud.Api.Business
{
    public interface IModuleRepository : IContextRepository
    {
        ResultCollection<Module> GetModuleInPage(string pageId);
        ResultObject<Module> GetById(string id);
        ResultBase Add(Module obj);
        ResultBase Edit(Module obj);
        ResultBase Delete(string id);
        ResultBase UpdatePosition(string moduleId, string section, string belowTo);
        ResultCollection<Module> GetForSite();
        ResultBase UpdateFields(string id, Dictionary<string, object> fields);

    }

    public class ModuleRepository : IModuleRepository
    {
        public IContext Context { get; set; }

        private readonly IModuleDataRepository _moduleDataRepository;

        public ModuleRepository(IModuleDataRepository moduleModuleDataRepository)
        {
            _moduleDataRepository = moduleModuleDataRepository;
        }

        public ResultCollection<Module> GetModuleInPage(string pageId)
        {
            try
            {
                var result = _moduleDataRepository.GetModuleInPage(Context.WebsiteId, pageId);
                return new ResultCollection<Module> { IsSuccess = true, Data = result };
            }
            catch (Exception exception)
            {
                return new ResultCollection<Module> { IsSuccess = false, Message = MessageHelper.GenerateMessage(exception) };
            }
        }

        public ResultObject<Module> GetById(string id)
        {
            try
            {
                if (string.IsNullOrWhiteSpace(id))
                    throw new BusinessException(MessageCode.Id_Required);

                var result = _moduleDataRepository.GetById(Context.WebsiteId, id, true, Context.LanguageId);
                if (result == null)
                    throw new BusinessException(MessageCode.Module_IdNotExist);

                return new ResultObject<Module> { IsSuccess = true, Data = result };
            }
            catch (Exception exception)
            {
                return new ResultObject<Module> { IsSuccess = false, Message = MessageHelper.GenerateMessage(exception) };
            }
        }

        public ResultBase Add(Module obj)
        {
            try
            {
                obj.WebsiteID = Context.WebsiteId;
                if (obj.ModuleConfigs != null)
                {
                    foreach (var config in obj.ModuleConfigs)
                    {
                        config.WebsiteID = Context.WebsiteId;
                    }
                }

                Validate(obj, true);

                if (!_moduleDataRepository.Add(obj)) throw new BusinessException(MessageCode.Unknow);

                return new ResultBase { IsSuccess = true };
            }
            catch (Exception exception)
            {
                return new ResultBase { IsSuccess = false, Message = MessageHelper.GenerateMessage(exception) };
            }
        }

        public ResultBase Edit(Module obj)
        {
            try
            {
                // set website has this layout
                obj.WebsiteID = Context.WebsiteId;

                Validate(obj, false);

                if (!_moduleDataRepository.Edit(obj, Context.LanguageId))
                    throw new BusinessException(MessageCode.Unknow);

                return new ResultBase { IsSuccess = true };
            }
            catch (Exception exception)
            {
                return new ResultBase { IsSuccess = false, Message = MessageHelper.GenerateMessage(exception) };
            }
        }

        public ResultBase Delete(string id)
        {
            try
            {
                if (string.IsNullOrWhiteSpace(id))
                    throw new BusinessException(MessageCode.Id_Required);

                if (_moduleDataRepository.Delete(Context.WebsiteId, id))
                    return new ResultBase { IsSuccess = true };


                throw new BusinessException(MessageCode.Module_IdNotExist);
            }
            catch (Exception exception)
            {
                return new ResultBase { IsSuccess = false, Message = MessageHelper.GenerateMessage(exception) };
            }
        }

        public ResultBase UpdatePosition(string moduleId, string section, string belowTo)
        {
            try
            {
                if (string.IsNullOrWhiteSpace(moduleId))
                    throw new BusinessException(MessageCode.Id_Required);

                _moduleDataRepository.UpdatePosition(Context.WebsiteId, moduleId, section, belowTo);
                return new ResultBase { IsSuccess = true };

            }
            catch (Exception exception)
            {
                return new ResultBase { IsSuccess = false, Message = MessageHelper.GenerateMessage(exception) };
            }
        }

        public void Validate(Module obj, bool isNew = false)
        {
            if (obj.WebsiteID == Default.WebsiteId)
            {
                ValidatePermission();
            }

            if (isNew)
            {
                obj.ModuleID = IdGenerator.GenerateId();
            }
            else
            {
                if (string.IsNullOrEmpty(obj.ModuleID))
                    throw new BusinessException(MessageCode.Id_Required);

                if (!_moduleDataRepository.IsExist(obj.WebsiteID, obj.ModuleID))
                    throw new BusinessException(MessageCode.Module_IdNotExist);
            }

            if (string.IsNullOrEmpty(obj.Name))
                throw new BusinessException(MessageCode.Name_Required);

            if (!obj.Name.IsValidLength(1, 250))
                throw new BusinessException(MessageCode.Name_Length_Invalid);

        }

        private void ValidatePermission()
        {
            // check permission
            if (!Context.Principal.IsInRoles(RoleValue.RootAdmin, RoleValue.SetupAdmin))
                throw new BusinessException(MessageCode.Permission_RoleRequired);
        }

        public ResultCollection<Module> GetForSite()
        {
            try
            {
                var result = _moduleDataRepository.GetAll(Context.WebsiteId, true, true);
                return new ResultCollection<Module> { IsSuccess = true, Data = result };
            }
            catch (Exception exception)
            {
                return new ResultCollection<Module> { IsSuccess = false, Message = MessageHelper.GenerateMessage(exception) };
            }
        }

        public ResultBase UpdateFields(string id, Dictionary<string, object> fields)
        {
            try
            {
                if (_moduleDataRepository.UpdateFields(Context.WebsiteId, id, Context.LanguageId, fields))
                    return new ResultBase { IsSuccess = true };

                throw new BusinessException(MessageCode.Module_IdNotExist);
            }
            catch (Exception exception)
            {
                return new ResultBase { IsSuccess = false, Message = MessageHelper.GenerateMessage(exception) };
            }
        }
    }
}
