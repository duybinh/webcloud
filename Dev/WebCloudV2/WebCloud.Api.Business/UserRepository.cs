﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using WebCloud.Common;
using WebCloud.Api.DataAccess;
using WebCloud.Common.Exception;
using WebCloud.Entity;

namespace WebCloud.Api.Business
{
    public interface IUserRepository : IContextRepository
    {
        ResultBase RegisterUser(User user);
        ResultBase CreateUser(User user);
        ResultBase EditUser(User user, bool checkCreator);
        ResultBase DeleteUser(User user, bool checkCreator);
        ResultBase DeleteUserDirectly(User user, bool checkCreator);
        ResultObject<User> GetUserById(string userId);
        ResultObject<User> GetUserByGlobalId(long globalId);
        ResultObject<User> GetUserByEmail(string email);
        ResultCollection<User> GetAllUser();
        ResultCollection<User> GetUsersInRole(string role);
        ResultCollection<User> GetUsersInGroup(string groupId);
        ResultBase Authenticate(string user, string password);
        ResultBase ChangePassword(string userId, string oldPassword, string newPassword);
        ResultObject<string> ResetPassword(string userId, bool notifyViaEmail);
    }

    public class UserRepository : ContextRepository, IUserRepository
    {
        private readonly IUserDataRepository _userDataRepository;

        public UserRepository(IUserDataRepository userDataRepository)
        {
            _userDataRepository = userDataRepository;
        }

        #region Implementation of IUserRepository

        public ResultBase RegisterUser(User user)
        {
            throw new NotImplementedException();
        }

        public ResultBase CreateUser(User user)
        {
            throw new NotImplementedException();
        }

        public ResultBase EditUser(User user, bool checkCreator)
        {
            throw new NotImplementedException();
        }

        public ResultBase DeleteUser(User user, bool checkCreator)
        {
            throw new NotImplementedException();
        }

        public ResultBase DeleteUserDirectly(User user, bool checkCreator)
        {
            throw new NotImplementedException();
        }

        public ResultObject<User> GetUserById(string userId)
        {
            throw new NotImplementedException();
        }

        public ResultObject<User> GetUserByGlobalId(long globalId)
        {
            try
            {
                var user = _userDataRepository.GetUserByGlobalId(globalId);
                if (user == null) throw new BusinessException("User_NotExist");
                var roles = _userDataRepository.GetRoleIdsOfUser(user);
                foreach (var role in roles)
                {
                    user.UserRoles.Add( new UserRole { UserRoleID = role });
                }
                return new ResultObject<User>
                       {
                           IsSuccess = true,
                           Data = user
                       };
            }
            catch (BusinessException exception)
            {
                return new ResultObject<User>
                       {
                           IsSuccess = false,
                           Message = new Message(exception.ErrorCode)
                       };
            }


            throw new NotImplementedException();
        }

        public ResultObject<User> GetUserByEmail(string email)
        {
            throw new NotImplementedException();
        }

        public ResultCollection<User> GetAllUser()
        {
            throw new NotImplementedException();
        }

        public ResultCollection<User> GetUsersInRole(string role)
        {
            throw new NotImplementedException();
        }

        public ResultCollection<User> GetUsersInGroup(string groupId)
        {
            throw new NotImplementedException();
        }

        public ResultBase Authenticate(string user, string password)
        {
            throw new NotImplementedException();
        }

        public ResultBase ChangePassword(string userId, string oldPassword, string newPassword)
        {
            throw new NotImplementedException();
        }

        public ResultObject<string> ResetPassword(string userId, bool notifyViaEmail)
        {
            throw new NotImplementedException();
        }

        #endregion
    }
}
