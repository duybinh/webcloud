﻿using System;
using System.Linq;
using DHSoft.Utility.Validation;
using WebCloud.Api.DataAccess;
using WebCloud.Common;
using WebCloud.Common.Constant;
using WebCloud.Common.Enums;
using WebCloud.Common.Exception;
using WebCloud.Common.Helper;
using WebCloud.Entity;

namespace WebCloud.Api.Business
{
    public interface ILayoutRepository : IContextRepository
    {
        ResultCollection<Layout> Query(int? type = null);
        ResultObject<Layout> GetById(string id);
        ResultObject<string> Add(Layout obj);
        ResultBase Edit(Layout obj);
        ResultBase Delete(string id);
        ResultObject<bool> IsExist(string id);
        ResultCollection<Layout> GetForSite();
        void Validate(Layout obj, bool isNew);
    }

    public class LayoutRepository : ILayoutRepository
    {
        private readonly ILayoutDataRepository _dataRepository;

        public LayoutRepository(ILayoutDataRepository layoutDataRepository)
        {
            _dataRepository = layoutDataRepository;
        }
        public IContext Context { get; set; }

        #region Layout
        public ResultCollection<Layout> Query(int? type = null)
        {
            try
            {
                var websiteLayouts = _dataRepository.Query(websiteId: Context.WebsiteId, type: type).ToDictionary(o => o.LayoutID, o => o);

                if (Context.WebsiteId == Default.WebsiteId)
                {
                    foreach (var layout in websiteLayouts.Values)
                    {
                        layout.SourceType = SourceType.Global;
                    }
                }
                else
                {
                    foreach (var layout in websiteLayouts.Values)
                    {
                        layout.SourceType = SourceType.Local;
                    }

                    var systemLayouts = _dataRepository.Query(Default.WebsiteId);
                    foreach (var layout in systemLayouts)
                    {
                        if (!websiteLayouts.ContainsKey(layout.LayoutID))
                        {
                            layout.SourceType = SourceType.Global;
                            websiteLayouts.Add(layout.LayoutID, layout);
                        }
                        else
                        {
                            websiteLayouts[layout.LayoutID].SourceType = SourceType.Overrided;
                        }
                    }
                }

                return new ResultCollection<Layout> { IsSuccess = true, Data = websiteLayouts.Values.ToList() };
            }
            catch (Exception exception)
            {
                return new ResultCollection<Layout> { IsSuccess = false, Message = MessageHelper.GenerateMessage(exception) };
            }
        }
        public ResultObject<Layout> GetById(string id)
        {
            try
            {
                if (string.IsNullOrWhiteSpace(id))
                    throw new BusinessException(MessageCode.Id_Required);

                var result = _dataRepository.GetById(Context.WebsiteId, id);

                if (result == null)
                {
                    if (Context.WebsiteId != Default.WebsiteId)
                    {
                        result = _dataRepository.GetById(Default.WebsiteId, id);
                        if (result != null)
                            result.SourceType = SourceType.Global;
                    }
                }
                else
                {
                    if (Context.WebsiteId == Default.WebsiteId)
                        result.SourceType = SourceType.Global;
                    else
                        result.SourceType = _dataRepository.IsExist(Default.WebsiteId, id) ? SourceType.Overrided : SourceType.Local;
                }


                if (result == null)
                    throw new BusinessException(MessageCode.Layout_IdNotExist);

                return new ResultObject<Layout> { IsSuccess = true, Data = result };
            }
            catch (Exception exception)
            {
                return new ResultObject<Layout> { IsSuccess = false, Message = MessageHelper.GenerateMessage(exception) };
            }
        }
        public ResultObject<string> Add(Layout obj)
        {
            try
            {
                obj.WebsiteID = Context.WebsiteId;

                ValidatePermission();
                Validate(obj, true);

                if (_dataRepository.Add(obj))
                {
                    return new ResultObject<string> { IsSuccess = true, Data = obj.LayoutID };
                }

                throw new BusinessException(MessageCode.Unknow);
            }
            catch (Exception exception)
            {
                return new ResultObject<string> { IsSuccess = false, Message = MessageHelper.GenerateMessage(exception) };
            }
        }
        public ResultBase Edit(Layout obj)
        {
            try
            {
                obj.WebsiteID = Context.WebsiteId;

                ValidatePermission();
                Validate(obj, false);

                // check id is exist
                if (obj.WebsiteID == Default.WebsiteId)
                {
                    if (!_dataRepository.IsExist(Default.WebsiteId, obj.LayoutID))
                        throw new BusinessException(MessageCode.Layout_IdNotExist);
                }
                else if (!_dataRepository.IsExist(obj.WebsiteID, obj.LayoutID))
                {
                    if (!_dataRepository.IsExist(Default.WebsiteId, obj.LayoutID))
                        throw new BusinessException(MessageCode.Layout_IdNotExist);

                    // add clone of system page to current site
                    if (_dataRepository.Add(obj))
                        return new ResultBase { IsSuccess = true };

                    throw new BusinessException(MessageCode.Unknow);
                }

                if (_dataRepository.Edit(obj))
                    return new ResultBase { IsSuccess = true };

                throw new BusinessException(MessageCode.Unknow);
            }
            catch (Exception exception)
            {
                return new ResultBase { IsSuccess = false, Message = MessageHelper.GenerateMessage(exception) };
            }
        }
        public ResultBase Delete(string id)
        {
            try
            {
                if (string.IsNullOrWhiteSpace(id))
                    throw new BusinessException(MessageCode.Id_Required);

                ValidatePermission();

                if (_dataRepository.Delete(Context.WebsiteId, id))
                    return new ResultBase { IsSuccess = true };

                throw new BusinessException(MessageCode.Layout_IdNotExist);
            }
            catch (Exception exception)
            {
                return new ResultBase { IsSuccess = false, Message = MessageHelper.GenerateMessage(exception) };
            }
        }
        public ResultObject<bool> IsExist(string id)
        {
            throw new NotImplementedException();
        }

        public ResultCollection<Layout> GetForSite()
        {
            try
            {
                var websiteLayouts = _dataRepository.Query(Context.WebsiteId, true).ToList();
                return new ResultCollection<Layout> { IsSuccess = true, Data = websiteLayouts };
            }
            catch (Exception exception)
            {
                return new ResultCollection<Layout> { IsSuccess = false, Message = MessageHelper.GenerateMessage(exception) };
            }
        }

        #endregion

        #region Validate
        public void Validate(Layout obj, bool isNew)
        {
            if (isNew)
            {
                if (string.IsNullOrEmpty(obj.LayoutID))
                    obj.LayoutID = IdGenerator.GenerateId();
                else
                {
                    if (!obj.LayoutID.IsValidId())
                        throw new BusinessException(MessageCode.Id_Invalid);

                    // check id is not exist
                    if (_dataRepository.IsExist(obj.WebsiteID, obj.LayoutID))
                        throw new BusinessException(MessageCode.Layout_IdExist);
                }
            }
            else
            {
                if (string.IsNullOrEmpty(obj.LayoutID))
                    throw new BusinessException(MessageCode.Id_Required);
            }

            if (string.IsNullOrEmpty(obj.Name))
                throw new BusinessException(MessageCode.Name_Required);

            if (!obj.Name.IsValidLength(1, 250))
                throw new BusinessException(MessageCode.Name_Length_Invalid);

            if (!obj.Description.IsValidLength(0, 500))
                throw new BusinessException(MessageCode.Description_Length_Invalid);

            if (string.IsNullOrWhiteSpace(obj.Content))
                throw new BusinessException(MessageCode.Content_Required);
        }

        private void ValidatePermission()
        {
            // check permission
            if (!Context.Principal.IsInRoles(RoleValue.RootAdmin, RoleValue.SetupAdmin))
                throw new BusinessException(MessageCode.Permission_RoleRequired);
        }
        #endregion

    }
}
