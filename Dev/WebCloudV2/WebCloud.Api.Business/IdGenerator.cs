﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WebCloud.Api.Business
{
    public class IdGenerator
    {
        private static readonly Random rand = new Random();
        private const String array = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";
        private static readonly int arrayLength = array.Length;

        public static string GenerateId(int length = 9)
        {
            var str = new StringBuilder();
            for (int i = 0; i < length; i++)
            {
                str.Append(array[rand.Next(arrayLength)]);
            }
            return str.ToString();
        }
    }
}
