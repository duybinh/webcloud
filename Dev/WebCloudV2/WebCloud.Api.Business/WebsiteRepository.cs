﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using WebCloud.Common;
using WebCloud.Api.DataAccess;
using WebCloud.Entity;

namespace WebCloud.Api.Business
{
    public interface IWebsiteRepository
    {
        ResultObject<String> Create(Website obj);
        ResultBase Edit(Website obj);
        ResultBase Delete(Website obj);
        ResultObject<Website> GetById(String id);
        ResultCollection<Website> GetAll();
    }
    public class WebsiteRepository
    {
        private readonly IWebsiteDataRepository _websiteDataRepository;

        public WebsiteRepository(IWebsiteDataRepository websiteDataRepository)
        {
            _websiteDataRepository = websiteDataRepository;
        }
    }
}
