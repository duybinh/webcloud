﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using WebCloud.Common.Constant;
using WebCloud.Common.Exception;

namespace WebCloud.Api.Business
{
    public interface IContextRepository
    {
        IContext Context { set; get; }
    }

    public class ContextRepository : IContextRepository
    {
        protected List<IContextRepository> Dependencies = new List<IContextRepository>();

        private IContext _context;
        public IContext Context
        {
            get
            {
                if (_context == null)
                {
                    throw new Exception("Context is null");
                }
                return _context;
            }
            set
            {
                if (_context != null && _context != value)
                {
                    throw new Exception("Context has already setted");
                }
                _context = value;
                foreach (var contextRepository in Dependencies)
                {
                    contextRepository.Context = value;
                }
            }
        }

        public void RequirePrincipal()
        {
            if (Context.Principal == null)
                throw new BusinessException(MessageCode.Permission_PrincipalRequired);
        }

        public void RequireRoles(bool all = false, params string[] role)
        {
            RequirePrincipal();
            if(all)
            {
                if(!Context.Principal.IsInAllRoles(role))
                    throw new BusinessException(MessageCode.Auth_NotAuthorize);
            }
            else
            {
                if (!Context.Principal.IsInRoles(role))
                    throw new BusinessException(MessageCode.Auth_NotAuthorize);
            }
        }
    }
}
