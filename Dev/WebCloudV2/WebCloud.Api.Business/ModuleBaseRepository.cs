﻿using System;
using System.Collections.Generic;
using System.Linq;
using DHSoft.Utility.Validation;
using WebCloud.Api.DataAccess;
using WebCloud.Common;
using WebCloud.Common.Constant;
using WebCloud.Common.Exception;
using WebCloud.Common.Helper;
using WebCloud.Entity;

namespace WebCloud.Api.Business
{
    public interface IModuleBaseRepository : IContextRepository
    {
        #region Management
        ResultCollection<ModuleBase> GetAll();
        ResultObject<ModuleBase> GetById(string id);
        ResultBase Add(ModuleBase obj);
        ResultBase Edit(ModuleBase obj);
        ResultBase Delete(string id);
        ResultObject<bool> IsExist(string id);
        void Validate(ModuleBase obj, bool isNew);
        #endregion

        #region Admin
        ResultCollection<ModuleBase> GetForAdmin(bool? installed = null);
        ResultBase InstallModule(string moduleId);
        ResultBase UninstallModule(string moduleId);
        #endregion

        // site
        ResultCollection<ModuleBase> GetForSite();

    }

    public class ModuleBaseRepository : IModuleBaseRepository
    {
        private readonly IModuleBaseDataRepository _dataRepository;

        public ModuleBaseRepository(IModuleBaseDataRepository moduleBaseDataRepository)
        {
            _dataRepository = moduleBaseDataRepository;
        }

        public IContext Context { get; set; }

        #region Management
        public ResultCollection<ModuleBase> GetAll()
        {
            try
            {
                var result = _dataRepository.GetAll();
                return new ResultCollection<ModuleBase> { IsSuccess = true, Data = result };
            }
            catch (Exception exception)
            {
                return new ResultCollection<ModuleBase> { IsSuccess = false, Message = MessageHelper.GenerateMessage(exception) };
            }
        }
        public ResultObject<ModuleBase> GetById(string id)
        {
            try
            {
                if (string.IsNullOrWhiteSpace(id))
                    throw new BusinessException(MessageCode.Id_Required);

                var result = _dataRepository.GetById(id, true, Context.LanguageId);
                if (result == null)
                    throw new BusinessException(MessageCode.ModuleBase_IdNotExist);

                return new ResultObject<ModuleBase> { IsSuccess = true, Data = result };
            }
            catch (Exception exception)
            {
                return new ResultObject<ModuleBase> { IsSuccess = false, Message = MessageHelper.GenerateMessage(exception) };
            }
        }
        public ResultBase Add(ModuleBase obj)
        {
            try
            {
                Validate(obj, true);

                if (!_dataRepository.Add(obj))
                    throw new BusinessException(MessageCode.Unknow);

                return new ResultObject<string> { IsSuccess = true, Data = obj.ModuleBaseID };
            }
            catch (Exception exception)
            {
                return new ResultObject<string> { IsSuccess = false, Message = MessageHelper.GenerateMessage(exception) };
            }
        }
        public ResultBase Edit(ModuleBase obj)
        {
            try
            {
                Validate(obj, false);

                if (!_dataRepository.Edit(obj, Context.LanguageId))
                    throw new BusinessException(MessageCode.Unknow);

                return new ResultBase { IsSuccess = true };
            }
            catch (Exception exception)
            {
                return new ResultBase { IsSuccess = false, Message = MessageHelper.GenerateMessage(exception) };
            }
        }
        public ResultBase Delete(string id)
        {
            try
            {
                if (string.IsNullOrWhiteSpace(id))
                    throw new BusinessException(MessageCode.Id_Required);

                if (!_dataRepository.Delete(id))
                    throw new BusinessException(MessageCode.ModuleBase_IdNotExist);

                return new ResultBase { IsSuccess = true };
            }
            catch (Exception exception)
            {
                return new ResultBase { IsSuccess = false, Message = MessageHelper.GenerateMessage(exception) };
            }
        }
        public ResultObject<bool> IsExist(string id)
        {
            return new ResultObject<bool> { IsSuccess = true, Data = _dataRepository.IsExist(id) };
        }
        public void Validate(ModuleBase obj, bool isNew)
        {
            if (isNew)
            {
                if (!obj.ModuleBaseID.IsValidId())
                    throw new BusinessException(MessageCode.Id_Invalid);

                if (_dataRepository.IsExist(obj.ModuleBaseID))
                    throw new BusinessException(MessageCode.ModuleBase_IdExist);
            }
            else
            {
                if (string.IsNullOrEmpty(obj.ModuleBaseID))
                    throw new BusinessException(MessageCode.Id_Required);

                if (!_dataRepository.IsExist(obj.ModuleBaseID))
                    throw new BusinessException(MessageCode.ModuleBase_IdNotExist);
            }

            if (string.IsNullOrEmpty(obj.Name))
                throw new BusinessException(MessageCode.Name_Required);

            if (!obj.Name.IsValidLength(1))
                throw new BusinessException(MessageCode.Name_Length_Invalid);

            if (!obj.Description.IsValidLength(0, 500))
                throw new BusinessException(MessageCode.Description_Length_Invalid);
        }
        #endregion

        #region Admin
        public ResultCollection<ModuleBase> GetForAdmin(bool? installed = null)
        {
            try
            {
                var allModules = _dataRepository.GetAll(isEnabled: true, includeConfig: false);
                var installedModules = _dataRepository.GetInstalled(Context.WebsiteId);
                var modulesForWebsite = new List<ModuleBase>();

                foreach (var module in allModules)
                {
                    if (Context.Principal.HasPermission(module.RolesToView))
                    {
                        module.Installed = installedModules.Any(o => o.ModuleBaseID == module.ModuleBaseID);
                        if (installed == null || module.Installed == installed)
                        {
                            module.AllowInstall = Context.Principal.HasPermission(module.RolesToInstall);
                            modulesForWebsite.Add(module);
                        }
                    }
                }
                return new ResultCollection<ModuleBase> { IsSuccess = true, Data = modulesForWebsite };
            }
            catch (Exception exception)
            {
                return new ResultCollection<ModuleBase> { IsSuccess = false, Message = MessageHelper.GenerateMessage(exception) };
            }
        }

        public ResultBase InstallModule(string moduleId)
        {
            try
            {
                var obj = new ModuleBaseInWebsite();
                obj.WebsiteID = Context.WebsiteId;
                obj.ModuleBaseID = moduleId;
                obj.InstalledDate = DateTime.Now;
                obj.InstalledUserID = Context.Principal.GlobalID;
                obj.InstalledUserName = Context.Principal.UserID;

                if (!_dataRepository.Install(obj))
                    throw new BusinessException(MessageCode.Unknow);

                return new ResultObject<string> { IsSuccess = true, Data = obj.ModuleBaseID };
            }
            catch (Exception exception)
            {
                return new ResultObject<string> { IsSuccess = false, Message = MessageHelper.GenerateMessage(exception) };
            }
        }

        public ResultBase UninstallModule(string moduleId)
        {
            try
            {
                if (string.IsNullOrWhiteSpace(moduleId))
                    throw new BusinessException(MessageCode.Id_Required);

                if (!_dataRepository.Uninstall(Context.WebsiteId, moduleId))
                    throw new BusinessException(MessageCode.ModuleBase_IdNotExist);

                return new ResultBase { IsSuccess = true };
            }
            catch (Exception exception)
            {
                return new ResultBase { IsSuccess = false, Message = MessageHelper.GenerateMessage(exception) };
            }
        }

        #endregion

        public ResultCollection<ModuleBase> GetForSite()
        {
            try
            {
                var result = _dataRepository.GetAll(isEnabled: true, includeConfig: true);
                return new ResultCollection<ModuleBase> { IsSuccess = true, Data = result };
            }
            catch (Exception exception)
            {
                return new ResultCollection<ModuleBase> { IsSuccess = false, Message = MessageHelper.GenerateMessage(exception) };
            }
        }
    }
}
