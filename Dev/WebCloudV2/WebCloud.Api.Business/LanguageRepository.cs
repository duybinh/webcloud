﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WebCloud.Api.DataAccess;
using WebCloud.Common;
using WebCloud.Common.Constant;
using WebCloud.Common.Exception;
using WebCloud.Common.Helper;
using WebCloud.Entity;

namespace WebCloud.Api.Business
{
    public interface ILanguageRepository
    {
        ResultObject<List<Language>> GetAll();
        ResultObject<List<Language>> GetLanguagesInWebsite();
        ResultObject<bool> IsExist(string langId);
        ResultBase Add(Language lang);
        ResultBase Edit(Language lang);
        ResultBase Delete(string langId);
        ResultObject<Language> GetById(string langId);
        void Validate(Language lang, bool isNew);
    }

    public class LanguageRepository : ILanguageRepository
    {
        private readonly ILanguageDataRepository _langDataRepository;

        public LanguageRepository(ILanguageDataRepository langDataRepository)
        {
            _langDataRepository = langDataRepository;
        }

        public ResultObject<List<Language>> GetAll()
        {
            try
            {
                var result = _langDataRepository.GetAll();
                return new ResultObject<List<Language>> { IsSuccess = true, Data = result };
            }
            catch (Exception exception)
            {
                return new ResultObject<List<Language>> { IsSuccess = false, Message = MessageHelper.GenerateMessage(exception) };
            }
        }

        public ResultObject<List<Language>> GetLanguagesInWebsite()
        {
            throw new NotImplementedException();
        }

        public ResultObject<bool> IsExist(string langId)
        {
            try
            {
                var result = _langDataRepository.IsExist(langId);
                return new ResultObject<bool> { IsSuccess = true, Data = result };
            }
            catch (Exception exception)
            {
                return new ResultObject<bool> { IsSuccess = false, Message = MessageHelper.GenerateMessage(exception) };
            }
        }

        public ResultBase Add(Language lang)
        {
            try
            {
                Validate(lang, true);
                if (_langDataRepository.Add(lang))
                {
                    return new ResultBase { IsSuccess = true };
                }

                throw new BusinessException(MessageCode.Unknow);
            }
            catch (Exception exception)
            {
                return new ResultBase { IsSuccess = false, Message = MessageHelper.GenerateMessage(exception) };
            }
        }

        public ResultBase Edit(Language lang)
        {
            try
            {
                Validate(lang, false);
                if (_langDataRepository.Edit(lang))
                {
                    return new ResultBase { IsSuccess = true };
                }

                throw new BusinessException(MessageCode.Unknow);
            }
            catch (Exception exception)
            {
                return new ResultBase { IsSuccess = false, Message = MessageHelper.GenerateMessage(exception) };
            }
        }

        public ResultBase Delete(string langId)
        {
            try
            {
                if (!_langDataRepository.IsExist(langId))
                {
                    throw new BusinessException(MessageCode.Language_IdNotExist);
                }

                if (_langDataRepository.Delete(langId))
                {
                    return new ResultBase { IsSuccess = true };
                }

                throw new BusinessException(MessageCode.Unknow);
            }
            catch (Exception exception)
            {
                return new ResultBase { IsSuccess = false, Message = MessageHelper.GenerateMessage(exception) };
            }
        }

        public ResultObject<Language> GetById(string langId)
        {
            try
            {
                var result = _langDataRepository.GetById(langId);
                if (result == null)
                {
                    throw new BusinessException(MessageCode.Language_IdNotExist);
                }

                return new ResultObject<Language> { IsSuccess = true, Data = result };
            }
            catch (Exception exception)
            {
                return new ResultObject<Language> { IsSuccess = false, Message = MessageHelper.GenerateMessage(exception) };
            }
        }

        public void Validate(Language lang, bool isNew)
        {
            if (isNew && _langDataRepository.IsExist(lang.LanguageID))
            {
                throw new BusinessException(MessageCode.Language_IdExist);
            }
        }
    }
}
