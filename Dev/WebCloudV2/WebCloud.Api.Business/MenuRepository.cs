﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using WebCloud.Common;
using WebCloud.Common.Helper;
using WebCloud.Entity;

namespace WebCloud.Api.Business
{
    public interface IMenuRepository : IContextRepository
    {
        ResultObject<List<MenuItem>> GetAll(string defaultLangId = null, bool isDeleted = false);
        ResultObject<MenuItem> GetById(string defaultLangId = null);
        ResultObject<string> Add(MenuItem item);
        ResultBase Edit(MenuItem item);
        ResultBase EditContent(MenuItemContent content);
        ResultBase DeleteContent(MenuItemContent content);
        ResultBase Delete(string menuItemId);
        ResultBase UnDelete(string menuItemId);
        ResultObject<MenuItem> Remove(string menuItemId, bool removeChilds);
    }

    public class MenuRepository : ContextRepository, IMenuRepository
    {
        #region Implementation of IMenuRepository

        public ResultObject<List<MenuItem>> GetAll(string defaultLangId = null, bool isDeleted = false)
        {
            throw new NotImplementedException();
        }

        public ResultObject<MenuItem> GetById(string defaultLangId = null)
        {
            throw new NotImplementedException();
        }

        public ResultObject<string> Add(MenuItem item)
        {
            item.MenuItemID = DataHelper.GenerateId();
            return new ResultObject<string>()
                       {
                           IsSuccess = true,
                           Data = item.MenuItemID
                       };
        }

        public ResultBase Edit(MenuItem item)
        {
            throw new NotImplementedException();
        }

        public ResultBase EditContent(MenuItemContent content)
        {
            throw new NotImplementedException();
        }

        public ResultBase DeleteContent(MenuItemContent content)
        {
            throw new NotImplementedException();
        }

        public ResultBase Delete(string menuItemId)
        {
            throw new NotImplementedException();
        }

        public ResultBase UnDelete(string menuItemId)
        {
            throw new NotImplementedException();
        }

        public ResultObject<MenuItem> Remove(string menuItemId, bool removeChilds)
        {
            throw new NotImplementedException();
        }

        #endregion
    }
}
