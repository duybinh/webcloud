﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using DHSoft.Utility.Validation;
using WebCloud.Common;
using WebCloud.Common.Constant;
using WebCloud.Api.DataAccess;
using WebCloud.Common.Exception;
using WebCloud.Common.Helper;
using WebCloud.Entity;

namespace WebCloud.Api.Business
{
   
    public interface IResourceRepository : IContextRepository
    {
        ResultCollection<Resource> GetResources();
        ResultCollection<Resource> GetResourcesInLang(string languageId);
        ResultObject<Resource> GetResourceById(string id);
        ResultBase AddResource(Resource obj);
        ResultBase EditResource(Resource obj);
        ResultBase DeleteResource(string id);
        ResultBase UpdateResourceValue(string resourceId, string languageId, string value);
        ResultBase UpdateResources(Dictionary<string, string> values);
        ResultObject<Dictionary<string, LocalizationString>> GetForSite();
    }

    public class ResourceRepository : ContextRepository, IResourceRepository
    {
        private readonly IResourceDataRepository _resourceDataRepository;

        public ResourceRepository(IResourceDataRepository resourceDataRepository)
        {
            _resourceDataRepository = resourceDataRepository;
        }

        #region Admin

        public ResultCollection<Resource> GetResources()
        {
            try
            {
                List<Resource> resources;
                if (Context.WebsiteId == Default.WebsiteId)
                {
                    resources = _resourceDataRepository.GetAll(Default.WebsiteId);
                    var resourceValues = _resourceDataRepository.GetValues(Default.WebsiteId, null);
                    foreach (var resource in resources)
                    {
                        if (resourceValues.ContainsKey(resource.ResourceID))
                            resource.Value = resourceValues[resource.ResourceID];
                    }
                }
                else
                {
                    // system resources
                    var sysResources = _resourceDataRepository.GetAll(Default.WebsiteId, true);
                    var sysResourceValues = _resourceDataRepository.GetValues(Default.WebsiteId, null);
                    var resourceValuesInSite = _resourceDataRepository.GetValues(Context.WebsiteId, null);

                    // website custom resources
                    resources = _resourceDataRepository.GetAll(Context.WebsiteId);
                    foreach (var resource in resources)
                    {
                        if (resourceValuesInSite.ContainsKey(resource.ResourceID))
                            resource.Value = resourceValuesInSite[resource.ResourceID];
                    }

                    foreach (var sysResource in sysResources)
                    {
                        // priority: Site Default Value > System Default Value
                        if (resourceValuesInSite.ContainsKey(sysResource.ResourceID))
                            sysResource.Value = resourceValuesInSite[sysResource.ResourceID];
                        else if (sysResourceValues.ContainsKey(sysResource.ResourceID))
                            sysResource.Value = sysResourceValues[sysResource.ResourceID];

                        sysResource.IsSystemResource = true;
                        resources.Add(sysResource);
                    }
                }

                return new ResultCollection<Resource> { IsSuccess = true, Data = resources };
            }
            catch (Exception exception)
            {
                return new ResultCollection<Resource> { IsSuccess = false, Message = MessageHelper.GenerateMessage(exception) };
            }
        }

        public ResultCollection<Resource> GetResourcesInLang(string languageId)
        {
            try
            {
                List<Resource> resources;
                if (Context.WebsiteId == Default.WebsiteId)
                {
                    resources = _resourceDataRepository.GetAll(Default.WebsiteId);
                    var resourceValues = _resourceDataRepository.GetValues(Default.WebsiteId, null);
                    var resourceValuesInLang = _resourceDataRepository.GetValues(Default.WebsiteId, languageId);

                    foreach (var resource in resources)
                    {
                        if (resourceValuesInLang.ContainsKey(resource.ResourceID))
                            resource.Value = resourceValuesInLang[resource.ResourceID];
                        else if (resourceValues.ContainsKey(resource.ResourceID))
                            resource.Value = resourceValues[resource.ResourceID];
                    }
                }
                else
                {
                    // priority: Site Lang Value > System Lang Value > Site Default Value > System Default Value
                    // system resources
                    var sysResources = _resourceDataRepository.GetAll(Default.WebsiteId, true);
                    var sysResourceValues = _resourceDataRepository.GetValues(Default.WebsiteId, null);
                    var sysResourceValuesInLang = _resourceDataRepository.GetValues(Default.WebsiteId, languageId);
                    var resourceValuesInSite = _resourceDataRepository.GetValues(Context.WebsiteId, null);
                    var resourceValuesInSiteLang = _resourceDataRepository.GetValues(Context.WebsiteId, languageId);

                    // website custom resources
                    resources = _resourceDataRepository.GetAll(Context.WebsiteId);
                    foreach (var resource in resources)
                    {
                        if (resourceValuesInSiteLang.ContainsKey(resource.ResourceID))
                            resource.Value = resourceValuesInSiteLang[resource.ResourceID];
                        else if (resourceValuesInSite.ContainsKey(resource.ResourceID))
                            resource.Value = resourceValuesInSite[resource.ResourceID];
                    }

                    foreach (var sysResource in sysResources)
                    {
                        if (resourceValuesInSiteLang.ContainsKey(sysResource.ResourceID))
                            sysResource.Value = resourceValuesInSiteLang[sysResource.ResourceID];
                        else if (sysResourceValuesInLang.ContainsKey(sysResource.ResourceID))
                            sysResource.Value = sysResourceValuesInLang[sysResource.ResourceID];
                        else if (resourceValuesInSite.ContainsKey(sysResource.ResourceID))
                            sysResource.Value = resourceValuesInSite[sysResource.ResourceID];
                        else if (sysResourceValues.ContainsKey(sysResource.ResourceID))
                            sysResource.Value = sysResourceValues[sysResource.ResourceID];

                        sysResource.IsSystemResource = true;
                        resources.Add(sysResource);
                    }
                }

                return new ResultCollection<Resource> { IsSuccess = true, Data = resources };
            }
            catch (Exception exception)
            {
                return new ResultCollection<Resource> { IsSuccess = false, Message = MessageHelper.GenerateMessage(exception) };
            }
        }

        public ResultObject<Resource> GetResourceById(string id)
        {
            try
            {
                Resource resource;
                if (Context.WebsiteId == Default.WebsiteId)
                {
                    resource = _resourceDataRepository.GetById(Context.WebsiteId, id);
                    if (resource != null)
                        resource.Value = _resourceDataRepository.GetValue(id, Context.WebsiteId, null);
                }
                else
                {
                    resource = _resourceDataRepository.GetById(Context.WebsiteId, id);
                    if (resource != null)
                        resource.Value = _resourceDataRepository.GetValue(id, Context.WebsiteId, null);
                    else
                    {
                        resource = _resourceDataRepository.GetById(Default.WebsiteId, id);
                        if (resource != null)
                        {
                            resource.IsSystemResource = true;
                            resource.Value = _resourceDataRepository.GetValue(id, Context.WebsiteId, null);
                            if (resource.Value == null)
                                resource.Value = _resourceDataRepository.GetValue(id, Default.WebsiteId, null);
                        }
                    }
                }

                if (resource == null)
                    throw new BusinessException(MessageCode.TextResource_IdNotExist);


                return new ResultObject<Resource> { IsSuccess = true, Data = resource };
            }
            catch (Exception exception)
            {
                return new ResultObject<Resource> { IsSuccess = false, Message = MessageHelper.GenerateMessage(exception) };
            }
        }

        public ResultBase AddResource(Resource obj)
        {
            try
            {
                obj.WebsiteID = Context.WebsiteId;

                ValidatePermission();
                Validate(obj, true);

                if (_resourceDataRepository.Add(obj))
                {
                    // add default value
                    _resourceDataRepository.UpdateValue(obj.ResourceID, Context.WebsiteId, null, obj.Value);
                }

                return new ResultBase { IsSuccess = true };
            }
            catch (Exception exception)
            {
                return new ResultBase { IsSuccess = false, Message = MessageHelper.GenerateMessage(exception) };
            }
        }

        public ResultBase EditResource(Resource obj)
        {
            try
            {
                obj.WebsiteID = Context.WebsiteId;

                ValidatePermission();
                Validate(obj, false);

                if (_resourceDataRepository.Edit(obj))
                {
                    // update default value
                    _resourceDataRepository.UpdateValue(obj.ResourceID, Context.WebsiteId, null, obj.Value);
                }

                return new ResultBase { IsSuccess = true };
            }
            catch (Exception exception)
            {
                return new ResultBase { IsSuccess = false, Message = MessageHelper.GenerateMessage(exception) };
            }
        }

        public ResultBase UpdateResourceValue(string resourceId, string languageId, string value)
        {
            try
            {
                ValidatePermission();
                _resourceDataRepository.UpdateValue(resourceId, Context.WebsiteId, languageId, value);

                return new ResultBase { IsSuccess = true };
            }
            catch (Exception exception)
            {
                return new ResultBase { IsSuccess = false, Message = MessageHelper.GenerateMessage(exception) };
            }
        }

        public ResultBase DeleteResource(string id)
        {
            try
            {
                if (string.IsNullOrWhiteSpace(id))
                    throw new BusinessException(MessageCode.Id_Required);

                ValidatePermission();

                if (!_resourceDataRepository.Delete(Context.WebsiteId, id))
                    throw new BusinessException(MessageCode.TextResource_IdNotExist);

                // delete values
                if (Context.WebsiteId == Default.WebsiteId)
                    _resourceDataRepository.DeleteValueByCondition(id, null, null);
                else
                    _resourceDataRepository.DeleteValueByCondition(id, Context.WebsiteId, null);

                return new ResultBase { IsSuccess = true };
            }
            catch (Exception exception)
            {
                return new ResultBase { IsSuccess = false, Message = MessageHelper.GenerateMessage(exception) };
            }
        }


        public ResultBase UpdateResources(Dictionary<string, string> values)
        {
            throw new NotImplementedException();
        }

        #endregion

        #region Site
        public ResultObject<Dictionary<string, LocalizationString>> GetForSite()
        {
            try
            {
                var resourceValues = _resourceDataRepository.GetResourceValues(Context.WebsiteId);
                var dict = new Dictionary<string, LocalizationString>();
                foreach (var resourceValue in resourceValues.Where(o => o.WebsiteID == Default.WebsiteId))
                {
                    AddToDictionary(dict, resourceValue);
                }

                foreach (var resourceValue in resourceValues.Where(o => o.WebsiteID != Default.WebsiteId))
                {
                    AddToDictionary(dict, resourceValue);
                }

                return new ResultObject<Dictionary<string, LocalizationString>>
                {
                    IsSuccess = true,
                    Data = dict
                };
            }
            catch (Exception exception)
            {
                return new ResultObject<Dictionary<string, LocalizationString>> { IsSuccess = false, Message = MessageHelper.GenerateMessage(exception) };
            }
        }

        private void AddToDictionary(Dictionary<string, LocalizationString> dict, ResourceValue resourceValue)
        {
            LocalizationString lString;
            if (!dict.ContainsKey(resourceValue.ResourceID))
            {
                lString = new LocalizationString();
                dict.Add(resourceValue.ResourceID, lString);
            }
            else
            {
                lString = dict[resourceValue.ResourceID];
            }
            lString.SetValue(resourceValue.Value, resourceValue.LanguageID);
        }
        #endregion

        public void Validate(Resource obj, bool isNew)
        {
            if (string.IsNullOrEmpty(obj.ResourceID))
                throw new BusinessException(MessageCode.Id_Required);

            if (isNew)
            {
                if (!obj.ResourceID.IsValidId())
                    throw new BusinessException(MessageCode.Id_Invalid);

                // check id is not exist
                if (_resourceDataRepository.IsExist(obj.WebsiteID, obj.ResourceID))
                    throw new BusinessException(MessageCode.Resource_IdExist);

                if (obj.WebsiteID != Default.WebsiteId)
                {
                    if (_resourceDataRepository.IsExist(Default.WebsiteId, obj.ResourceID))
                        throw new BusinessException(MessageCode.Resource_IdExist);
                }
            }


            if (string.IsNullOrEmpty(obj.Name))
                throw new BusinessException(MessageCode.Name_Required);

            if (!obj.Name.IsValidLength(1, 250))
                throw new BusinessException(MessageCode.Name_Length_Invalid);

            if (!obj.Description.IsValidLength(0, 500))
                throw new BusinessException(MessageCode.Description_Length_Invalid);

            if (!obj.RolesToView.IsValidLength(0, 250))
                throw new BusinessException(MessageCode.Resource_RolesToView_Length_Invalid);

            if (!obj.RolesToEdit.IsValidLength(0, 250))
                throw new BusinessException(MessageCode.Resource_RolesToEdit_Length_Invalid);

            if (!obj.Group.IsValidLength(0, 250))
                throw new BusinessException(MessageCode.Resource_Type_Length_Invalid);

            if (string.IsNullOrEmpty(obj.Value))
                throw new BusinessException(MessageCode.Resource_DefaultValue_Required);
        }
        private void ValidatePermission()
        {
            // check permission
            if (!Context.Principal.IsInRoles(RoleValue.RootAdmin, RoleValue.SetupAdmin))
                throw new BusinessException(MessageCode.Permission_RoleRequired);
        }

    }

}
