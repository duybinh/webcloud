﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WebCloud.Api.DataAccess;
using WebCloud.Common;
using WebCloud.Common.Constant;
using WebCloud.Common.Exception;
using WebCloud.Common.Helper;
using WebCloud.Entity;

namespace WebCloud.Api.Business
{
    public interface IRoleRepository
    {
        ResultObject<List<UserRole>> GetAll();
        ResultObject<List<UserRole>> GetRolesInWebsite();
        ResultObject<bool> IsExist(string roleId);
        ResultBase Add(UserRole role);
        ResultBase Edit(UserRole role);
        ResultBase Delete(string roleId);
        ResultObject<UserRole> GetById(string roleId);
        void Validate(UserRole role, bool isNew);
    }

    public class RoleRepository : IRoleRepository
    {
        private readonly IRoleDataRepository _roleDataRepository;

        public RoleRepository(IRoleDataRepository roleDataRepository)
        {
            _roleDataRepository = roleDataRepository;
        }

        public ResultObject<List<UserRole>> GetAll()
        {
            try
            {
                var result = _roleDataRepository.GetAll();
                return new ResultObject<List<UserRole>> { IsSuccess = true, Data = result };
            }
            catch (Exception exception)
            {
                return new ResultObject<List<UserRole>> { IsSuccess = false, Message = MessageHelper.GenerateMessage(exception) };
            }
        }

        public ResultObject<List<UserRole>> GetRolesInWebsite()
        {
            throw new NotImplementedException();
        }

        public ResultObject<bool> IsExist(string roleId)
        {
            try
            {
                var result = _roleDataRepository.IsExist(roleId);
                return new ResultObject<bool> { IsSuccess = true, Data = result };
            }
            catch (Exception exception)
            {
                return new ResultObject<bool> { IsSuccess = false, Message = MessageHelper.GenerateMessage(exception) };
            }
        }

        public ResultBase Add(UserRole role)
        {
            try
            {
                Validate(role, true);
                if (_roleDataRepository.Add(role))
                {
                    return new ResultBase { IsSuccess = true };
                }

                throw new BusinessException(MessageCode.Unknow);
            }
            catch (Exception exception)
            {
                return new ResultBase { IsSuccess = false, Message = MessageHelper.GenerateMessage(exception) };
            }
        }

        public ResultBase Edit(UserRole role)
        {
            try
            {
                Validate(role, false);
                if (_roleDataRepository.Edit(role))
                {
                    return new ResultBase { IsSuccess = true };
                }

                throw new BusinessException(MessageCode.Role_IdNotExist);
            }
            catch (Exception exception)
            {
                return new ResultBase { IsSuccess = false, Message = MessageHelper.GenerateMessage(exception) };
            }
        }

        public ResultBase Delete(string roleId)
        {
            try
            {
                if (!_roleDataRepository.IsExist(roleId))
                {
                    throw new BusinessException(MessageCode.Role_IdNotExist);
                }

                if (_roleDataRepository.Delete(roleId))
                {
                    return new ResultBase { IsSuccess = true };
                }

                throw new BusinessException(MessageCode.Unknow);
            }
            catch (Exception exception)
            {
                return new ResultBase { IsSuccess = false, Message = MessageHelper.GenerateMessage(exception) };
            }
        }

        public ResultObject<UserRole> GetById(string roleId)
        {
            try
            {
                var result = _roleDataRepository.GetById(roleId);
                if (result == null)
                {
                    throw new BusinessException(MessageCode.Role_IdNotExist);
                }

                return new ResultObject<UserRole> { IsSuccess = true, Data = result };
            }
            catch (Exception exception)
            {
                return new ResultObject<UserRole> { IsSuccess = false, Message = MessageHelper.GenerateMessage(exception) };
            }
        }

        public void Validate(UserRole role, bool isNew)
        {
            if (isNew && _roleDataRepository.IsExist(role.UserRoleID))
            {
                throw new BusinessException(MessageCode.Role_IdExist);
            }
        }
    }
}
