﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DHSoft.Utility.Validation;
using WebCloud.Api.DataAccess;
using WebCloud.Common;
using WebCloud.Common.Constant;
using WebCloud.Common.Exception;
using WebCloud.Common.Helper;
using WebCloud.Entity;

namespace WebCloud.Api.Business
{
    public interface IArticleCategoryRepository : IContextRepository
    {
        ResultObject<List<ArticleCategory>> GetAll();
        ResultObject<List<ArticleCategory>> Trash();
        ResultObject<ArticleCategory> GetById(string id);
        ResultObject<bool> IsExist(string id);
        ResultObject<string> Add(ArticleCategory obj);
        ResultBase Edit(ArticleCategory obj);
        ResultBase Delete(string id, bool deleteChilds = false);
        ResultBase Remove(string id);
        ResultBase Restore(string id);
        ResultBase EmptyTrash();
        ResultBase DeleteInLanguage(string id);
        void Validate(ArticleCategory obj, bool isNew);
    }

    public class ArticleCategoryRepository : IArticleCategoryRepository
    {
        private readonly IArticleCategoryDataRepository _categoryDataRepository;
        public ArticleCategoryRepository(IArticleCategoryDataRepository categoryDataRepository)
        {
            _categoryDataRepository = categoryDataRepository;
        }

        public IContext Context { get; set; }
        public ResultObject<List<ArticleCategory>> GetAll()
        {
            try
            {
                var result = _categoryDataRepository.GetAll(Context.WebsiteId, Context.LanguageId);
                return new ResultObject<List<ArticleCategory>>
                {
                    IsSuccess = true,
                    Data = result
                };
            }
            catch (Exception exception)
            {
                return new ResultObject<List<ArticleCategory>> { IsSuccess = false, Message = MessageHelper.GenerateMessage(exception) };
            }
        }

        public ResultObject<List<ArticleCategory>> Trash()
        {
            try
            {
                var result = _categoryDataRepository.GetAll(Context.WebsiteId, Context.LanguageId, Default.LanguageId, true);
                return new ResultObject<List<ArticleCategory>>
                {
                    IsSuccess = true,
                    Data = result
                };
            }
            catch (Exception exception)
            {
                return new ResultObject<List<ArticleCategory>> { IsSuccess = false, Message = MessageHelper.GenerateMessage(exception) };
            }
        }

        public ResultObject<ArticleCategory> GetById(string id)
        {
            try
            {
                if (string.IsNullOrWhiteSpace(id))
                    throw new BusinessException(MessageCode.Id_Required);

                var result = _categoryDataRepository.GetById(Context.WebsiteId, id, Context.LanguageId);

                if (result == null)
                    throw new BusinessException(MessageCode.ArticleCategory_IdNotExist);

                return new ResultObject<ArticleCategory> { IsSuccess = true, Data = result };
            }
            catch (Exception exception)
            {
                return new ResultObject<ArticleCategory> { IsSuccess = false, Message = MessageHelper.GenerateMessage(exception) };
            }
        }

        public ResultObject<bool> IsExist(string id)
        {
            throw new NotImplementedException();
        }

        public ResultObject<string> Add(ArticleCategory obj)
        {
            try
            {
                Validate(obj, true);

                obj.ArticleCategoryID = IdGenerator.GenerateId();
                SetInsertInfo(obj);

                if (_categoryDataRepository.Add(obj))
                {
                    return new ResultObject<string> { IsSuccess = true, Data = obj.ArticleCategoryID };
                }

                throw new BusinessException(MessageCode.Unknow);
            }
            catch (Exception exception)
            {
                return new ResultObject<string> { IsSuccess = false, Message = MessageHelper.GenerateMessage(exception) };
            }
        }

        public ResultBase Edit(ArticleCategory obj)
        {
            try
            {
                Validate(obj, false);

                obj.Content.LanguageID = Context.LanguageId;
                SetUpdateInfo(obj);

                if (_categoryDataRepository.Edit(obj))
                {
                    return new ResultBase { IsSuccess = true };
                }
                throw new BusinessException(MessageCode.ArticleCategory_IdNotExist);
            }
            catch (Exception exception)
            {
                return new ResultBase { IsSuccess = false, Message = MessageHelper.GenerateMessage(exception) };
            }
        }

        public ResultBase Delete(string id, bool deleteChilds)
        {
            try
            {
                var obj = new ArticleCategory { ArticleCategoryID = id };
                SetUpdateInfo(obj);

                if (_categoryDataRepository.Delete(obj, deleteChilds))
                    return new ResultBase { IsSuccess = true };

                throw new BusinessException(MessageCode.ArticleCategory_IdNotExist);
            }
            catch (Exception exception)
            {
                return new ResultBase { IsSuccess = false, Message = MessageHelper.GenerateMessage(exception) };
            }
        }

        public ResultBase Remove(string id)
        {
            try
            {
                var obj = new ArticleCategory { ArticleCategoryID = id };
                SetUpdateInfo(obj);

                if (_categoryDataRepository.Remove(obj))
                    return new ResultBase { IsSuccess = true };

                throw new BusinessException(MessageCode.ArticleCategory_IdNotExist);
            }
            catch (Exception exception)
            {
                return new ResultBase { IsSuccess = false, Message = MessageHelper.GenerateMessage(exception) };
            }
        }

        public ResultBase Restore(string id)
        {
            try
            {
                var obj = new ArticleCategory { ArticleCategoryID = id };
                SetUpdateInfo(obj);

                if (_categoryDataRepository.Restore(obj))
                    return new ResultBase { IsSuccess = true };

                throw new BusinessException(MessageCode.ArticleCategory_IdNotExist);
            }
            catch (Exception exception)
            {
                return new ResultBase { IsSuccess = false, Message = MessageHelper.GenerateMessage(exception) };
            }
        }

        public ResultBase EmptyTrash()
        {
            try
            {
                if (_categoryDataRepository.EmptyTrash(Context.WebsiteId))
                    return new ResultBase { IsSuccess = true };

                throw new BusinessException(MessageCode.Unknow);
            }
            catch (Exception exception)
            {
                return new ResultBase { IsSuccess = false, Message = MessageHelper.GenerateMessage(exception) };
            }
        }

        public ResultBase DeleteInLanguage(string id)
        {
            try
            {
                var obj = new ArticleCategory()
                {
                    ArticleCategoryID = id,
                    Content = new ArticleCategoryContent()
                    {
                        LanguageID = Context.LanguageId
                    }
                };
                SetUpdateInfo(obj);

                if (_categoryDataRepository.Delete(obj))
                    return new ResultBase { IsSuccess = true };

                throw new BusinessException(MessageCode.ArticleCategory_IdNotExist);
            }
            catch (Exception exception)
            {
                return new ResultBase { IsSuccess = false, Message = MessageHelper.GenerateMessage(exception) };
            }
        }

        public void Validate(ArticleCategory obj, bool isNew)
        {
            if (!isNew)
            {
                if (string.IsNullOrEmpty(obj.ArticleCategoryID))
                    throw new BusinessException(MessageCode.Id_Required);

                if (obj.ParentID != null)
                {
                    if (obj.ArticleCategoryID.Equals(obj.ParentID, StringComparison.InvariantCultureIgnoreCase)
                        || _categoryDataRepository.HasChild(Context.WebsiteId, obj.ArticleCategoryID, obj.ParentID))
                    {
                        throw new BusinessException(MessageCode.ArticleCategory_ParentIdInvalid);
                    }
                }
            }

            if (obj.Content == null)
                throw new BusinessException(MessageCode.Name_Required);

            if (string.IsNullOrEmpty(obj.Content.Name))
                throw new BusinessException(MessageCode.Name_Required);
            if (!obj.Content.Name.IsValidLength(1))
                throw new BusinessException(MessageCode.Name_Length_Invalid);

            if (!obj.Content.Description.IsValidLength())
                throw new BusinessException(MessageCode.Description_Length_Invalid);

            if (!obj.Content.MetaDescription.IsValidLength())
                throw new BusinessException(MessageCode.MetaDescription_Length_Invalid);

            if (!obj.Content.MetaKeyword.IsValidLength())
                throw new BusinessException(MessageCode.MetaKeyword_Length_Invalid);

            if (!obj.Content.MetaTitle.IsValidLength())
                throw new BusinessException(MessageCode.MetaTitle_Length_Invalid);

            if (!obj.ThumbImage.IsValidLength())
                throw new BusinessException(MessageCode.ThumbImage_Length_Invalid);
        }

        #region Private methods

        private void SetUpdateInfo(ArticleCategory obj)
        {
            obj.WebsiteID = Context.WebsiteId;
            obj.LastEditedUserName = Context.Principal.UserID;
            obj.LastEditedUserID = Context.Principal.GlobalID;
            obj.LastEditedDatetime = DateTime.Now;
        }
        private void SetInsertInfo(ArticleCategory obj)
        {
            obj.WebsiteID = Context.WebsiteId;
            obj.Content.LanguageID = Context.LanguageId;
            obj.CreatedUserName = obj.LastEditedUserName = Context.Principal.UserID;
            obj.CreatedUserID = obj.LastEditedUserID = Context.Principal.GlobalID;
            obj.CreatedDatetime = obj.LastEditedDatetime = DateTime.Now;
        }

        #endregion
    }
}
