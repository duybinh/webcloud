﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace DHSoft.Utility.Common
{
    public static class TextUtils
    {
        public static string ToStringRejexMarks(this string accented)
        {
            Regex regex = new Regex(@"\p{IsCombiningDiacriticalMarks}+");
            string strFormD = accented.Normalize(System.Text.NormalizationForm.FormD);
            return regex.Replace(strFormD, String.Empty).Replace("Đ", "D").Replace("đ", "d").ToLower();
        }


        public static string ToSeoString(this string inputStr)
        {
            if (string.IsNullOrEmpty(inputStr)) return null;

            inputStr = inputStr.Trim().ToStringRejexMarks().Replace(" ", "-");
            Regex re = new Regex("[^A-Za-z0-9\\-]");
            inputStr = re.Replace(inputStr, "");
            while (inputStr.Contains("--"))
            {
                inputStr = inputStr.Replace("--", "-");
            }
            if (inputStr.EndsWith("-"))
            {
                inputStr = inputStr.Substring(0, inputStr.Length - 1);
            }
            return inputStr;
        }

        public static List<string> SplitToList(this string input, string splitter)
        {
            if (string.IsNullOrEmpty(input)) 
                return new List<string>();
            return input.Split(new[] { splitter }, StringSplitOptions.RemoveEmptyEntries).ToList();
        }

        public static string[] SplitToArray(this string input, string splitter)
        {
            if (string.IsNullOrEmpty(input)) 
                return new string[]{};
            return input.Split(new[] { splitter }, StringSplitOptions.RemoveEmptyEntries);
        }
    }
}
