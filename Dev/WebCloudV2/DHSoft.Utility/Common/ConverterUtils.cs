﻿using System;
using System.Globalization;

namespace DHSoft.Utility.Common
{
    public static class ConverterUtils
    {
        public static int ToInt32TryParse(this object input, int catchValue = 0)
        {
            try
            {
                return Convert.ToInt32(input);
            }
            catch { return catchValue; }
        }

        public static long ToInt64TryParse(this object input, long catchValue = 0)
        {
            try
            {
                return Convert.ToInt64(input);
            }
            catch { return catchValue; }
        }

        public static double ToDoubleTryParse(this object input, double catchValue = 0)
        {
            try
            {
                return Convert.ToDouble(input);
            }
            catch { return catchValue; }
        }

        public static float ToFloatTryParse(this object input, string culture = "en-US", float catchValue = 0)
        {
            try
            {
                return float.Parse(input.ToString(), NumberStyles.Float, new CultureInfo(culture));
            }
            catch { return catchValue; }
        }

        public static Decimal ToDecimalTryParse(this object input, string culture = "en-US", long catchValue = 0)
        {
            try
            {
                return Convert.ToDecimal(input, new CultureInfo(culture));
            }
            catch { return catchValue; }
        }

        public static DateTime? ToDateTimeTryParse(this object input, string culture = "vi-VN", DateTime? catchValue = null)
        {
            try
            {
                return Convert.ToDateTime(input, new CultureInfo(culture));
            }
            catch { return catchValue; }
        }

        public static TimeSpan? ToTimeTryParse(this object input, string culture = "vi-VN", TimeSpan? catchValue = null)
        {
            try
            {
                TimeSpan value;
                if (TimeSpan.TryParse(input.ToString(), out value))
                    return value;

                return catchValue;
            }
            catch { return catchValue; }
        }

        public static Boolean ToBooleanTryParse(this object input, Boolean catchValue = true)
        {
            try
            {
                return Convert.ToBoolean(input);
            }
            catch { return catchValue; }
        }

        public static object ToCorrectType(this object input, string dstType)
        {
            if (input == null) return null;
            switch (dstType)
            {
                case "String":
                case "Html":
                    return input.ToString();

                case "Number":
                    if (input.ToString().Length <= 9)
                        return input.ToInt32TryParse();
                    return input.ToInt64TryParse();

                case "Decimal":
                    return input.ToDecimalTryParse();

                case "Boolean":
                    return input.ToBooleanTryParse();

                case "DateTime":
                    return input.ToDateTimeTryParse();

                case "Date":
                    return input.ToDateTimeTryParse();

                case "Time":
                    return input.ToTimeTryParse();
            }
            return null;
        }
    }
}
