﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DHSoft.Utility.Common
{
    public static class RoleUtils
    {
        public static bool IsContainRole(this string str, string role)
        {
            if (string.IsNullOrEmpty(str) || string.IsNullOrWhiteSpace(str))
                return false;
            
            if (str == "*")
                return true;

            var roles = str.Split(',');
            return roles.Length > 0 && roles.Contains(role);
        }

        public static bool IsContainRole(this string str, string[] roles)
        {
            if (string.IsNullOrEmpty(str) || string.IsNullOrWhiteSpace(str))
                return false;

            if (str == "*")
                return true;

            var strs = str.Split(',');
            if (strs.Length == 0) return false;

            foreach (var role in roles)
            {
                if (strs.Contains(role)) return true;
            }

            return false;
        }
    }
}
