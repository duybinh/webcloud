﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DHSoft.Utility.Sql
{
    public static class SqlDataReaderHelper
    {
        public static string ReadString(this SqlDataReader reader, string column)
        {
            var obj = reader[column];
            if (obj == DBNull.Value)
            {
                return null;
            }
            return (string)obj;
        }

        public static T? ReadNullable<T>(this SqlDataReader reader, string column) where T : struct
        {
            var obj = reader[column];
            if (obj == DBNull.Value)
            {
                return null;
            }
            return (T)obj;
        }

        public static SqlCommand CreateProcedureCommand(this SqlConnection sqlConnection,
                                                        string storedProcedure, Dictionary<string, object> inParameters = null)
        {
            var command = new SqlCommand(storedProcedure, sqlConnection)
            {
                CommandType = CommandType.StoredProcedure
            };

            // set input parameters
            if (inParameters != null && inParameters.Count > 0)
            {
                foreach (var pair in inParameters)
                {
                    command.Parameters.AddWithValue(pair.Key, pair.Value ?? DBNull.Value);
                }
            }

            return command;
        }

        public static DataTable ToListStringTable(this List<string> list)
        {
            DataTable table = new DataTable();
            table.Columns.Add("Value", typeof (string));
            foreach (var item in list)
            {
                table.Rows.Add(item);
            }
            return table;
        }
       
    }
}
