﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace DHSoft.Utility.Validation
{
    public static class ValidationUtils
    {
        /// <summary>
        /// Check valid user name
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        public static bool IsValidUsername(this string input)
        {
            Regex r = new Regex("^[a-zA-Z0-9_]{4,20}$");
            return r.IsMatch(input);
        }

        /// <summary>
        /// Check valid user name
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        public static bool IsValidId(this string input)
        {
            Regex r = new Regex("^[a-zA-Z0-9_]{1,50}$");
            return r.IsMatch(input);
        }

        /// <summary>
        /// Check valid email
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        public static bool IsValidEmail(this string input)
        {
            Regex r = new Regex(@"^\w+[\w-\.]*\@\w+((-\w+)|(\w*))\.[a-z]{2,3}$");
            return r.IsMatch(input);
        }

        /// <summary>
        /// Check valid length
        /// </summary>
        /// <param name="input"></param>
        /// <param name="min"></param>
        /// <param name="max"></param>
        /// <returns></returns>
        public static bool IsValidLength(this string input, int min = 0, int max = 250)
        {
            if (input == null) return min <= 0;
            return (input.Length >= min) && (input.Length <= max);
        }

        /// <summary>
        /// Check valid Folder name
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        public static bool IsValidFolderName(this string input)
        {
            if (!input.IsValidLength(1, 253)) return false;
            foreach (var ch in Path.GetInvalidFileNameChars())
            {
                if (input.Contains(ch)) return false;
            }
            return true;
        }

        /// <summary>
        /// Check valid File name
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        public static bool IsValidFileName(this string input)
        {
            if (!input.IsValidLength(1, 253)) return false;
            foreach (var ch in Path.GetInvalidFileNameChars())
            {
                if (input.Contains(ch)) return false;
            }
            return true;
        }


        /// <summary>
        /// Check valid value for a type
        /// </summary>
        /// <param name="input"></param>
        /// <param name="type"></param>
        /// <returns></returns>
        public static bool IsValidValueType(this string input, string type)
        {
            switch (type)
            {
                case "String":
                    return true;
                case "Html":
                    return true;
                case "Number":
                    return IsNumber(input);
                case "Decimal":
                    return IsDecimalNumber(input);
                case "Boolean":
                    return IsBoolean(input);
                case "DateTime":
                    return IsDateTime(input);
                case "Date":
                    return IsDateTime(input);
                case "Time":
                    return IsTime(input);
            }
            return false;
        }
        public static bool GetValidValueType(ref string input, string type)
        {
            switch (type)
            {
                case "String":
                    return true;
                case "Html":
                    return true;
                case "Number":
                    if (IsNumber(input.Replace(",", "")))
                    {
                        input = input.Replace(",", "");
                        return true;
                    }
                    return false;
                case "Decimal":
                    if (IsDecimalNumber(input.Replace(",","")))
                    {
                        input = input.Replace(",", "");
                        return true;
                    }
                    return false;
                case "Boolean":
                    if (IsBoolean(input))
                    {
                        input = input.ToLower();
                        return true;
                    }
                    return false;
                case "DateTime":
                    return IsDateTime(input);
                case "Date":
                    return IsDateTime(input);
                case "Time":
                    return IsTime(input);
            }
            return false;
        }

        public static bool IsNumber(this string input)
        {
            int value;
            return Int32.TryParse(input, out value);
        }

        public static bool IsDecimalNumber(this string input)
        {
            decimal value;
            return Decimal.TryParse(input, out value);
        }

        public static bool IsBoolean(this string input)
        {
            bool value;
            return Boolean.TryParse(input, out value);
        }

        public static bool IsDateTime(this string input)
        {
            DateTime value;
            return DateTime.TryParse(input, out value);
        }

        public static bool IsTime(this string input)
        {
            TimeSpan value;
            return TimeSpan.TryParse(input, out value);
        }
    }
}
