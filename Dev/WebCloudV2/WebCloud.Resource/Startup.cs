﻿using System.Web.Http;
using Microsoft.Owin;
using Microsoft.Owin.Security;
using Microsoft.Owin.Security.DataHandler.Encoder;
using Microsoft.Owin.Security.Jwt;
using Owin;
using WebCloud.Resource;
using WebCloud.Resource.Infrastructure.Wrapper;

[assembly: OwinStartup(typeof(Startup))]
namespace WebCloud.Resource
{
    public class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureOAuth(app);

            var config = new HttpConfiguration();
            WebApiConfig.Register(config);
            // Web API routes
            //config.MapHttpAttributeRoutes();
            
            app.UseCors(Microsoft.Owin.Cors.CorsOptions.AllowAll);
            app.UseWebApi(config);
        }

        private void ConfigureOAuth(IAppBuilder app)
        {
            // Api controllers with an [Authorize] attribute will be validated with JWT
            app.UseJwtBearerAuthentication(
                new JwtBearerAuthenticationOptions
                {
                    AuthenticationMode = AuthenticationMode.Active,
                    AllowedAudiences = new[] { WebCloudResourceConfiguration.AuthAudience },
                    IssuerSecurityTokenProviders = new IIssuerSecurityTokenProvider[]
                    {
                        new SymmetricKeyIssuerSecurityTokenProvider(WebCloudResourceConfiguration.AuthIssuer, 
                            TextEncodings.Base64Url.Decode(WebCloudResourceConfiguration.AuthAudienceSecret))
                    }
                });
        }
    }
}