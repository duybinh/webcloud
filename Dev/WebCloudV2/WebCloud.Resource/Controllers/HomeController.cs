﻿using System.Collections;
using System.Linq;
using System.Security.Claims;
using System.Web.Http;

namespace WebCloud.Resource.Controllers
{

    public class HomeController : ApiController
    {
        [Route("")]
        [HttpGet]
        public string Index()
        {
            return "Welcome to WebCloud.Resource! Copyright @2015 by Dong Hanh.";
        }

        [Authorize]
        [HttpGet]
        [Route("api/testauth")]
        public IEnumerable TestAuth()
        {
            var identity = User.Identity as ClaimsIdentity;

            return identity.Claims.Select(c => new
            {
                Type = c.Type,
                Value = c.Value
            });
        }

      
    }
}