﻿using System.Configuration;

namespace WebCloud.Resource.Infrastructure.Wrapper
{
    public class WebCloudResourceConfiguration
    {
        private static readonly WebCloudResourceConfiguration Instance = new WebCloudResourceConfiguration();
        private readonly string _authIssuer;
        private readonly string _authAudience;
        private readonly string _authAudienceSecret;

        private WebCloudResourceConfiguration()
        {
            _authIssuer = ConfigurationManager.AppSettings["Auth_Issuer"];
            _authAudience = ConfigurationManager.AppSettings["Auth_Audience"];
            _authAudienceSecret = ConfigurationManager.AppSettings["Auth_AudienceSecret"];
        }
        public static string AuthIssuer
        {
            get { return Instance._authIssuer; }
        }

        public static string AuthAudience
        {
            get { return Instance._authAudience; }
        }
        public static string AuthAudienceSecret
        {
            get { return Instance._authAudienceSecret; }
        }
    }
}
