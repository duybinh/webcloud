﻿using System;
using System.Security.Claims;
using System.Web;
using WebCloud.Common.Auth;

namespace WebCloud.Resource
{
    public class Global : System.Web.HttpApplication
    {

        protected void Application_Start(object sender, EventArgs e)
        {

        }

        protected void Session_Start(object sender, EventArgs e)
        {

        }

        protected void Application_BeginRequest(object sender, EventArgs e)
        {

        }

        protected void Application_AuthenticateRequest(object sender, EventArgs e)
        {
            //if (HttpContext.Current.User.Identity.IsAuthenticated)
            //{
            //    var webCloudPrincipal = new WebCloudPrincipal((ClaimsPrincipal)HttpContext.Current.User);
            //    HttpContext.Current.User = webCloudPrincipal;
            //}
            var a = HttpContext.Current;
        }

        protected void Application_Error(object sender, EventArgs e)
        {

        }

        protected void Session_End(object sender, EventArgs e)
        {

        }

        protected void Application_End(object sender, EventArgs e)
        {

        }

        protected void Application_PostAuthenticateRequest(Object sender, EventArgs e)
        {
            if (HttpContext.Current.User.Identity.IsAuthenticated)
            {
                var webCloudPrincipal = new WebCloudPrincipal((ClaimsPrincipal) HttpContext.Current.User);
                HttpContext.Current.User = webCloudPrincipal;
            }
        }

    }
}