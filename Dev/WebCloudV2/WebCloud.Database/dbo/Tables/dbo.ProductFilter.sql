﻿CREATE TABLE [dbo].[ProductFilter] (
    [WebsiteID]         VARCHAR (50) NOT NULL,
    [ProductFilterID]   VARCHAR (50) NOT NULL,
    [ProductPropertyID] VARCHAR (50) NOT NULL,
    [AllowMultyChoice]  BIT          CONSTRAINT [DF_ProductFilter_AllowMultyChoice] DEFAULT ((1)) NOT NULL,
    [SortOrder]         INT          CONSTRAINT [DF_ProductFilter_SortOrder] DEFAULT ((0)) NOT NULL,
    [IsEnabled]         BIT          CONSTRAINT [DF_ProductFilter_IsEnabled] DEFAULT ((0)) NOT NULL,
    [IsDeleted]         BIT          CONSTRAINT [DF_ProductFilter_IsDeleted] DEFAULT ((0)) NOT NULL,
    CONSTRAINT [PK_ProductFilter] PRIMARY KEY CLUSTERED ([WebsiteID] ASC, [ProductFilterID] ASC),
    CONSTRAINT [FK_ProductFilter_Website] FOREIGN KEY ([WebsiteID]) REFERENCES [dbo].[Website] ([WebsiteID])
);

