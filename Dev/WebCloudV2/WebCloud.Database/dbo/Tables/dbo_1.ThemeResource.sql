﻿CREATE TABLE [dbo].[ThemeResource] (
    [ThemeResourceID] BIGINT         IDENTITY (1, 1) NOT NULL,
    [Name]            NVARCHAR (250) NOT NULL,
    [Content]         NVARCHAR (MAX) NOT NULL,
    [Type]            INT            CONSTRAINT [DF_ThemeResource_Type] DEFAULT ((0)) NOT NULL,
    [VirtualPath]     NVARCHAR (250) NULL,
    [IsBase]          BIT            CONSTRAINT [DF_ThemeResource_IsBase] DEFAULT ((0)) NOT NULL,
    [ThemeID]         VARCHAR (50)   NOT NULL,
    [WebsiteID]       VARCHAR (50)   NULL,
    CONSTRAINT [PK_ThemeResource] PRIMARY KEY CLUSTERED ([ThemeResourceID] ASC),
    CONSTRAINT [FK_ThemeResource_Theme] FOREIGN KEY ([ThemeID]) REFERENCES [dbo].[Theme] ([ThemeID]),
    CONSTRAINT [FK_ThemeResource_Website] FOREIGN KEY ([WebsiteID]) REFERENCES [dbo].[Website] ([WebsiteID]) ON DELETE CASCADE ON UPDATE CASCADE
);


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'0:css ; 1:js', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ThemeResource', @level2type = N'COLUMN', @level2name = N'Type';

