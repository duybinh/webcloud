﻿CREATE TABLE [dbo].[TagInArticle] (
    [WebsiteID] VARCHAR (50) NOT NULL,
    [ArticleID] VARCHAR (50) NOT NULL,
    [TagID]     BIGINT       NOT NULL,
    CONSTRAINT [PK_TagInArticle] PRIMARY KEY CLUSTERED ([WebsiteID] ASC, [ArticleID] ASC, [TagID] ASC),
    CONSTRAINT [FK_TagInArticle_Article] FOREIGN KEY ([WebsiteID], [ArticleID]) REFERENCES [dbo].[Article] ([WebsiteID], [ArticleID]) ON DELETE CASCADE ON UPDATE CASCADE,
    CONSTRAINT [FK_TagInArticle_Tag] FOREIGN KEY ([TagID]) REFERENCES [dbo].[Tag] ([TagID]) ON DELETE CASCADE ON UPDATE CASCADE
);

