﻿CREATE TABLE [dbo].[ModuleBase] (
    [ModuleBaseID]   VARCHAR (50)   NOT NULL,
    [DisplayControl] NVARCHAR (500) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
    [ManageControl]  NVARCHAR (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
    [Name]           NVARCHAR (200) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
    [Description]    NVARCHAR (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
    [RolesToInstall] VARCHAR (500)  CONSTRAINT [DF_Module_RolesToInstall] DEFAULT ('*') NULL,
    [RolesToCreate]  VARCHAR (500)  CONSTRAINT [DF_Module_RolesToCreate] DEFAULT ('*') NULL,
    [RolesToEdit]    VARCHAR (500)  CONSTRAINT [DF_Module_RolesToEdit] DEFAULT ('*') NULL,
    [RolesToView]    VARCHAR (500)  CONSTRAINT [DF_Module_RolesToView] DEFAULT ('*') NULL,
    [Group]          NVARCHAR (200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
    [Thumbnail]      NVARCHAR (200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
    [Version]        VARCHAR (10)   NULL,
    [Author]         NVARCHAR (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
    [SortOrder]      INT            CONSTRAINT [DF_ModuleBase_SortOrder] DEFAULT ((0)) NOT NULL,
    [IsEnabled]      BIT            CONSTRAINT [DF_ModuleBase_IsEnabled] DEFAULT ((1)) NOT NULL,
    CONSTRAINT [PK_ModuleBase] PRIMARY KEY CLUSTERED ([ModuleBaseID] ASC)
);

