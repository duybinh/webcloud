﻿CREATE TABLE [dbo].[PageContent] (
    [WebsiteID]       VARCHAR (50)   NOT NULL,
    [LanguageID]      VARCHAR (10)   NOT NULL,
    [PageID]          VARCHAR (50)   NOT NULL,
    [Name]            NVARCHAR (250) NOT NULL,
    [SeoName]         NVARCHAR (250) NOT NULL,
    [MetaTitle]       NVARCHAR (250) NULL,
    [MetaKeyword]     NVARCHAR (500) NULL,
    [MetaDescription] NVARCHAR (500) NULL,
    CONSTRAINT [PK_PageContent] PRIMARY KEY CLUSTERED ([WebsiteID] ASC, [LanguageID] ASC, [PageID] ASC),
    CONSTRAINT [FK_PageContent_Language] FOREIGN KEY ([LanguageID]) REFERENCES [dbo].[Language] ([LanguageID]),
    CONSTRAINT [FK_PageContent_Page] FOREIGN KEY ([WebsiteID], [PageID]) REFERENCES [dbo].[Page] ([WebsiteID], [PageID]) ON DELETE CASCADE ON UPDATE CASCADE
);

