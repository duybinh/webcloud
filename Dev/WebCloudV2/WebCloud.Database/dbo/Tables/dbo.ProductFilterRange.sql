﻿CREATE TABLE [dbo].[ProductFilterRange] (
    [WebsiteID]            VARCHAR (50)    NOT NULL,
    [ProductFilterRangeID] VARCHAR (50)    NOT NULL,
    [ProductFilterID]      VARCHAR (50)    NOT NULL,
    [TextValueEqual]       NVARCHAR (500)  NULL,
    [NumberValueEqual]     DECIMAL (18, 6) NULL,
    [BooleanValueEqual]    BIT             NULL,
    [DatetimeValueEqual]   DATETIME2 (7)   NULL,
    [NumberValueMin]       DECIMAL (18, 6) NULL,
    [DatetimeValueMin]     DATETIME2 (7)   NULL,
    [NumberValueMax]       DECIMAL (18, 6) NULL,
    [DatetimeValueMax]     DATETIME2 (7)   NULL,
    [SortOrder]            INT             CONSTRAINT [DF_ProductFilterRange_SortOrder] DEFAULT ((0)) NOT NULL,
    CONSTRAINT [PK_ProductFilterRange] PRIMARY KEY CLUSTERED ([WebsiteID] ASC, [ProductFilterRangeID] ASC),
    CONSTRAINT [FK_ProductFilterRange_ProductFilter] FOREIGN KEY ([WebsiteID], [ProductFilterID]) REFERENCES [dbo].[ProductFilter] ([WebsiteID], [ProductFilterID]) ON DELETE CASCADE ON UPDATE CASCADE
);

