﻿CREATE TABLE [dbo].[UserInGroup] (
    [WebsiteID]   VARCHAR (50) NOT NULL,
    [UserGroupID] VARCHAR (50) NOT NULL,
    [UserID]      VARCHAR (50) NOT NULL,
    CONSTRAINT [PK_UserInGroup] PRIMARY KEY CLUSTERED ([WebsiteID] ASC, [UserGroupID] ASC, [UserID] ASC),
    CONSTRAINT [FK_UserInGroup_User] FOREIGN KEY ([WebsiteID], [UserID]) REFERENCES [dbo].[User] ([WebsiteID], [UserID]) ON DELETE CASCADE ON UPDATE CASCADE,
    CONSTRAINT [FK_UserInGroup_UserGroup] FOREIGN KEY ([WebsiteID], [UserGroupID]) REFERENCES [dbo].[UserGroup] ([WebsiteID], [UserGroupID]) ON DELETE CASCADE ON UPDATE CASCADE
);

