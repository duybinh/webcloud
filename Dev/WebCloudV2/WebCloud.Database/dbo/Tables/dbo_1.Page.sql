﻿CREATE TABLE [dbo].[Page] (
    [WebsiteID]  VARCHAR (50) NOT NULL,
    [PageID]     VARCHAR (50) NOT NULL,
    [ParentID]   VARCHAR (50) NULL,
    [LayoutID]   VARCHAR (50) NOT NULL,
    [SortOrder]  INT          CONSTRAINT [DF_Page_SortOrder] DEFAULT ((0)) NOT NULL,
    [IsBasePage] BIT          CONSTRAINT [DF_Page_IsBasePage] DEFAULT ((1)) NOT NULL,
    [IsEnabled]  BIT          CONSTRAINT [DF_Page_IsEnabled] DEFAULT ((1)) NOT NULL,
    [IsDeleted]  BIT          CONSTRAINT [DF_Page_IsDeleted] DEFAULT ((0)) NOT NULL,
    [IsGroup]    BIT          CONSTRAINT [DF_Page_IsGroup] DEFAULT ((0)) NOT NULL,
    CONSTRAINT [PK_Page] PRIMARY KEY CLUSTERED ([WebsiteID] ASC, [PageID] ASC),
    CONSTRAINT [FK_Page_Layout] FOREIGN KEY ([WebsiteID], [LayoutID]) REFERENCES [dbo].[Layout] ([WebsiteID], [LayoutID]),
    CONSTRAINT [FK_Page_Website] FOREIGN KEY ([WebsiteID]) REFERENCES [dbo].[Website] ([WebsiteID])
);

