﻿CREATE TABLE [dbo].[ProductPropertyValue] (
    [WebsiteID]         VARCHAR (50)    NOT NULL,
    [LanguageID]        VARCHAR (10)    NOT NULL,
    [ProductID]         VARCHAR (50)    NOT NULL,
    [ProductPropertyID] VARCHAR (50)    NOT NULL,
    [TextValue]         NVARCHAR (500)  NULL,
    [NumberValue]       DECIMAL (18, 6) NULL,
    [BooleanValue]      BIT             NULL,
    [DatetimeValue]     DATETIME2 (7)   NULL,
    CONSTRAINT [PK_ProductPropertyValue] PRIMARY KEY CLUSTERED ([WebsiteID] ASC, [LanguageID] ASC, [ProductID] ASC, [ProductPropertyID] ASC),
    CONSTRAINT [FK_ProductPropertyValue_Language] FOREIGN KEY ([LanguageID]) REFERENCES [dbo].[Language] ([LanguageID]),
    CONSTRAINT [FK_ProductPropertyValue_Product] FOREIGN KEY ([WebsiteID], [ProductID]) REFERENCES [dbo].[Product] ([WebsiteID], [ProductID]) ON DELETE CASCADE ON UPDATE CASCADE,
    CONSTRAINT [FK_ProductPropertyValue_ProductProperty] FOREIGN KEY ([WebsiteID], [ProductPropertyID]) REFERENCES [dbo].[ProductProperty] ([WebsiteID], [ProductPropertyID]) ON DELETE CASCADE ON UPDATE CASCADE
);

