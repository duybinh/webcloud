﻿CREATE TABLE [dbo].[RoleBaseInWebsite] (
    [RoleBaseID] INT          NOT NULL,
    [WebsiteKey] VARCHAR (50) NOT NULL,
    [SortOrder]  INT          NOT NULL,
    CONSTRAINT [PK_RoleInWebsite] PRIMARY KEY CLUSTERED ([RoleBaseID] ASC, [WebsiteKey] ASC),
    CONSTRAINT [FK_RoleBaseInWebsite_RoleBase] FOREIGN KEY ([RoleBaseID]) REFERENCES [dbo].[RoleBase] ([ID]) ON DELETE CASCADE ON UPDATE CASCADE,
    CONSTRAINT [FK_RoleBaseInWebsite_Website] FOREIGN KEY ([WebsiteKey]) REFERENCES [dbo].[Website] ([WebsiteKey]) ON DELETE CASCADE ON UPDATE CASCADE
);

