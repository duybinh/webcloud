﻿CREATE TABLE [dbo].[ArticleCategoryContent] (
    [WebsiteID]         VARCHAR (50)   NOT NULL,
    [ArticleCategoryID] VARCHAR (50)   NOT NULL,
    [LanguageID]        VARCHAR (10)   NOT NULL,
    [Name]              NVARCHAR (250) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
    [SeoName]           NVARCHAR (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
    [Description]       NVARCHAR (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
    [MetaTitle]         NVARCHAR (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
    [MetaKeyword]       NVARCHAR (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
    [MetaDescription]   NVARCHAR (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
    CONSTRAINT [PK_ArticleCategoryContent] PRIMARY KEY CLUSTERED ([WebsiteID] ASC, [ArticleCategoryID] ASC, [LanguageID] ASC),
    CONSTRAINT [FK_ArticleCategoryContent_ArticleCategory] FOREIGN KEY ([WebsiteID], [ArticleCategoryID]) REFERENCES [dbo].[ArticleCategory] ([WebsiteID], [ArticleCategoryID]) ON DELETE CASCADE ON UPDATE CASCADE,
    CONSTRAINT [FK_ArticleCategoryContent_Language] FOREIGN KEY ([LanguageID]) REFERENCES [dbo].[Language] ([LanguageID])
);

