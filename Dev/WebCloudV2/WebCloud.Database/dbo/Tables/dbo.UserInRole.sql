﻿CREATE TABLE [dbo].[UserInRole] (
    [UserID]     BIGINT       NOT NULL,
    [RoleKey]    VARCHAR (50) NOT NULL,
    [WebsiteKey] VARCHAR (50) NOT NULL,
    CONSTRAINT [PK_UserInRole] PRIMARY KEY CLUSTERED ([UserID] ASC, [RoleKey] ASC, [WebsiteKey] ASC),
    CONSTRAINT [FK_UserInRole_Role] FOREIGN KEY ([RoleKey], [WebsiteKey]) REFERENCES [dbo].[Role] ([RoleKey], [WebsiteKey]) ON DELETE CASCADE ON UPDATE CASCADE,
    CONSTRAINT [FK_UserInRole_User] FOREIGN KEY ([UserID]) REFERENCES [dbo].[User] ([UserID]) ON DELETE CASCADE ON UPDATE CASCADE
);

