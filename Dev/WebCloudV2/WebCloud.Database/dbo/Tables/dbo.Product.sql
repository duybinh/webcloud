﻿CREATE TABLE [dbo].[Product] (
    [WebsiteID]          VARCHAR (50)    NOT NULL,
    [ProductID]          VARCHAR (50)    NOT NULL,
    [GlobalID]           BIGINT          IDENTITY (100000, 1) NOT NULL,
    [ThumbImage]         NVARCHAR (300)  NOT NULL,
    [Price]              DECIMAL (18, 6) CONSTRAINT [DF_Product_Price] DEFAULT ((0)) NOT NULL,
    [OldPrice]           DECIMAL (18, 6) CONSTRAINT [DF_Product_OldPrice] DEFAULT ((0)) NOT NULL,
    [IsNew]              BIT             CONSTRAINT [DF_Product_IsNew] DEFAULT ((0)) NOT NULL,
    [IsHot]              BIT             CONSTRAINT [DF_Product_IsHot] DEFAULT ((0)) NOT NULL,
    [IsDiscount]         BIT             CONSTRAINT [DF_Product_IsDiscount] DEFAULT ((0)) NOT NULL,
    [IsAvailable]        BIT             CONSTRAINT [DF_Table_1_Avaiabled] DEFAULT ((1)) NOT NULL,
    [AllowOrder]         BIT             CONSTRAINT [DF_Product_AllowOrder] DEFAULT ((1)) NOT NULL,
    [AllowComment]       BIT             CONSTRAINT [DF_Product_AllowComment] DEFAULT ((1)) NOT NULL,
    [CallToPrice]        BIT             CONSTRAINT [DF_Product_CallToPrice] DEFAULT ((0)) NOT NULL,
    [DisplayDatetime]    DATETIME2 (7)   NULL,
    [SortOrder]          BIGINT          CONSTRAINT [DF_Product_SortOrder] DEFAULT ((0)) NOT NULL,
    [IsEnabled]          BIT             CONSTRAINT [DF_Product_IsEnabled] DEFAULT ((0)) NOT NULL,
    [IsDeleted]          BIT             CONSTRAINT [DF_Product_IsDeleted] DEFAULT ((0)) NOT NULL,
    [DiscussionGroupID]  BIGINT          NULL,
    [StatisticID]        BIGINT          NULL,
    [LogGroupID]         BIGINT          NULL,
    [CreatedDatetime]    DATETIME2 (7)   NOT NULL,
    [LastEditedDatetime] DATETIME2 (7)   NOT NULL,
    [CreatedUserName]    VARCHAR (50)    NULL,
    [LastEditedUserName] VARCHAR (50)    NULL,
    [CreatedUserID]      BIGINT          NULL,
    [LastEditedUserID]   BIGINT          NULL,
    CONSTRAINT [PK_Product] PRIMARY KEY CLUSTERED ([WebsiteID] ASC, [ProductID] ASC),
    CONSTRAINT [FK_Product_DiscussionGroup] FOREIGN KEY ([DiscussionGroupID]) REFERENCES [dbo].[DiscussionGroup] ([DiscussionGroupID]),
    CONSTRAINT [FK_Product_LogGroup] FOREIGN KEY ([LogGroupID]) REFERENCES [dbo].[LogGroup] ([LogGroupID]),
    CONSTRAINT [FK_Product_Statistic] FOREIGN KEY ([StatisticID]) REFERENCES [dbo].[Statistic] ([StatisticID]),
    CONSTRAINT [FK_Product_Website] FOREIGN KEY ([WebsiteID]) REFERENCES [dbo].[Website] ([WebsiteID])
);


GO
-- =============================================
-- Author:		BinhND
-- Create date: 13/09/2013
-- Description:	TRIGGER FOR INSERT ACTICLE
-- =============================================
CREATE TRIGGER [dbo].[TR_PRODUCT_INSERT]
   ON  dbo.Product 
   INSTEAD OF INSERT
AS 
BEGIN
	
	SET NOCOUNT ON;

    ------------ TRIGGER BODY ------------
	BEGIN
		
		DECLARE @ReferenceTable VARCHAR(100) = 'PRODUCT'
		DECLARE @Action VARCHAR(50)= 'INSERT'
		DECLARE @LogGroupTbl TABLE (ProductID VARCHAR(50),LogGroupID BIGINT)
		DECLARE @StatisticTbl TABLE (ProductID VARCHAR(50),StatisticID BIGINT)
		DECLARE @DiscussionGroupTbl TABLE (ProductID VARCHAR(50),DiscussionGroupID BIGINT)
		
		-- insert LogGroup
		INSERT INTO dbo.LogGroup
		        ( ReferenceTable ,
		          ItemID ,
		          WebsiteID
		        )
		        OUTPUT INSERTED.ItemID, INSERTED.LogGroupID INTO @LogGroupTbl
				SELECT @ReferenceTable AS ReferenceTable,
						INSERTED.ProductID AS ItemID,
						INSERTED.WebsiteID AS WebsiteID
				FROM	INSERTED

		-- insert Log
		INSERT INTO dbo.Log
		        ( LogGroupID ,
		          Action ,
		          ActionDetail ,
		          Actor
		        )
				SELECT lg.LogGroupID AS LogGroupID, 
						@Action AS ACTION,
						NULL,
						i.LastEditedUserID AS Actor						
				FROM INSERTED i
				JOIN @LogGroupTbl lg ON i.ProductID = lg.ProductID
		
		-- insert DiscussionGroup
		INSERT INTO dbo.DiscussionGroup
		        ( ReferenceTable ,
		          ItemID ,
		          WebsiteID
		        )
				OUTPUT INSERTED.ItemID, INSERTED.DiscussionGroupID INTO @DiscussionGroupTbl
				SELECT @ReferenceTable AS ReferenceTable,
						INSERTED.ProductID AS ItemID,
						INSERTED.WebsiteID AS WebsiteID
				FROM	INSERTED

		-- insert Statistic
		INSERT INTO dbo.Statistic
		        ( ReferenceTable ,
		          ItemID ,
		          WebsiteID
		        )
				OUTPUT INSERTED.ItemID, INSERTED.StatisticID INTO @StatisticTbl
				SELECT @ReferenceTable AS ReferenceTable,
						INSERTED.ProductID AS ItemID,
						INSERTED.WebsiteID AS WebsiteID
				FROM	INSERTED
		
		-- insert Product
		INSERT INTO dbo.Product
		SELECT    WebsiteID,
		          i.ProductID ,		         
		          ThumbImage ,		          
		          Price ,
		          OldPrice ,
		          IsNew ,
		          IsHot ,
		          IsDiscount ,
		          IsAvailable ,
		          AllowOrder ,
		          AllowComment ,
		          CallToPrice ,
		          DisplayDatetime ,
		          SortOrder ,
		          IsEnabled ,
		          IsDeleted ,
				  dg.DiscussionGroupID AS DiscussionGroupID ,
		          s.StatisticID AS StatisticID ,
		          lg.LogGroupID AS LogGroupID , 
		          CreatedDatetime ,
		          LastEditedDatetime ,
		          CreatedUserName ,
		          LastEditedUserName ,
		          CreatedUserID ,
		          LastEditedUserID
		 FROM	  INSERTED i
		          JOIN @LogGroupTbl AS lg ON lg.ProductID = i.ProductID
		          JOIN @DiscussionGroupTbl AS dg ON dg.ProductID = i.ProductID
		          JOIN @StatisticTbl AS s ON s.ProductID = i.ProductID
		  
	END
END
