﻿CREATE TABLE [dbo].[ArticleInCategory] (
    [ArticleCategoryID] VARCHAR (50) NOT NULL,
    [ArticleID]         VARCHAR (50) NOT NULL,
    [WebsiteKey]        VARCHAR (50) NOT NULL,
    [LanguageKey]       VARCHAR (10) NOT NULL,
    CONSTRAINT [PK_ArticleInCategory] PRIMARY KEY CLUSTERED ([ArticleID] ASC, [ArticleCategoryID] ASC, [WebsiteKey] ASC, [LanguageKey] ASC),
    CONSTRAINT [FK_ArticleInCategory_Article] FOREIGN KEY ([ArticleID], [WebsiteKey], [LanguageKey]) REFERENCES [dbo].[Article] ([ID], [WebsiteKey], [LanguageKey]) ON DELETE CASCADE ON UPDATE CASCADE,
    CONSTRAINT [FK_ArticleInCategory_ArticleCategory] FOREIGN KEY ([ArticleCategoryID], [WebsiteKey], [LanguageKey]) REFERENCES [dbo].[ArticleCategory] ([ID], [WebsiteKey], [LanguageKey]) ON DELETE CASCADE ON UPDATE CASCADE
);

