﻿CREATE TABLE [dbo].[Module] (
    [WebsiteID]    VARCHAR (50)   NOT NULL,
    [ModuleID]     VARCHAR (50)   NOT NULL,
    [ModuleBaseID] VARCHAR (50)   NOT NULL,
    [PageID]       VARCHAR (50)   NOT NULL,
    [Name]         NVARCHAR (250) NOT NULL,
    [Section]      NVARCHAR (100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
    [OnAllPage]    BIT            CONSTRAINT [DF_Module_OnAllPage] DEFAULT ((0)) NOT NULL,
    [IsBasic]      BIT            CONSTRAINT [DF_Module_IsBasic] DEFAULT ((0)) NOT NULL,
    [IsEnabled]    BIT            CONSTRAINT [DF_Module_IsEnabled] DEFAULT ((1)) NOT NULL,
    [SortOrder]    INT            CONSTRAINT [DF_Module_SortOrder] DEFAULT ((0)) NOT NULL,
    CONSTRAINT [PK_Module] PRIMARY KEY CLUSTERED ([WebsiteID] ASC, [ModuleID] ASC),
    CONSTRAINT [FK_Module_ModuleBase] FOREIGN KEY ([ModuleBaseID]) REFERENCES [dbo].[ModuleBase] ([ModuleBaseID]),
    CONSTRAINT [FK_Module_Page] FOREIGN KEY ([WebsiteID], [PageID]) REFERENCES [dbo].[Page] ([WebsiteID], [PageID]) ON DELETE CASCADE ON UPDATE CASCADE,
    CONSTRAINT [FK_Module_Website] FOREIGN KEY ([WebsiteID]) REFERENCES [dbo].[Website] ([WebsiteID])
);

