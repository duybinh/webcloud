﻿CREATE TABLE [dbo].[Text] (
    [WebsiteID]  VARCHAR (50)   NOT NULL,
    [TextID]     VARCHAR (50)   NOT NULL,
    [LanguageID] VARCHAR (10)   NOT NULL,
    [Value]      NVARCHAR (MAX) NULL,
    CONSTRAINT [PK_Text] PRIMARY KEY CLUSTERED ([WebsiteID] ASC, [TextID] ASC, [LanguageID] ASC),
    CONSTRAINT [FK_Text_Language] FOREIGN KEY ([LanguageID]) REFERENCES [dbo].[Language] ([LanguageID]) ON DELETE CASCADE ON UPDATE CASCADE,
    CONSTRAINT [FK_Text_Website] FOREIGN KEY ([WebsiteID]) REFERENCES [dbo].[Website] ([WebsiteID]) ON DELETE CASCADE ON UPDATE CASCADE
);

