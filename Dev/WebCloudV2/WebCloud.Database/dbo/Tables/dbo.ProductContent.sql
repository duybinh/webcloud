﻿CREATE TABLE [dbo].[ProductContent] (
    [WebsiteID]       VARCHAR (50)   NOT NULL,
    [LanguageID]      VARCHAR (10)   NOT NULL,
    [ProductID]       VARCHAR (50)   NOT NULL,
    [Name]            NVARCHAR (250) NOT NULL,
    [SeoName]         NVARCHAR (250) NOT NULL,
    [Description]     NVARCHAR (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
    [Detail]          NVARCHAR (MAX) NULL,
    [MetaTitle]       NVARCHAR (250) NULL,
    [MetaKeyword]     NVARCHAR (500) NULL,
    [MetaDescription] NVARCHAR (500) NULL,
    CONSTRAINT [PK_ProductContent] PRIMARY KEY CLUSTERED ([WebsiteID] ASC, [LanguageID] ASC, [ProductID] ASC),
    CONSTRAINT [FK_ProductContent_Language] FOREIGN KEY ([LanguageID]) REFERENCES [dbo].[Language] ([LanguageID]),
    CONSTRAINT [FK_ProductContent_Product] FOREIGN KEY ([WebsiteID], [ProductID]) REFERENCES [dbo].[Product] ([WebsiteID], [ProductID]) ON DELETE CASCADE ON UPDATE CASCADE
);

