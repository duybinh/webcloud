﻿CREATE TABLE [dbo].[TextResourceInLanguage] (
    [LanguageID]     VARCHAR (10)   NOT NULL,
    [TextResourceID] VARCHAR (50)   NOT NULL,
    [Value]          NVARCHAR (MAX) NULL,
    CONSTRAINT [PK_TextResourceInLanguage] PRIMARY KEY CLUSTERED ([LanguageID] ASC, [TextResourceID] ASC),
    CONSTRAINT [FK_TextResourceInLanguage_Language] FOREIGN KEY ([LanguageID]) REFERENCES [dbo].[Language] ([LanguageID]) ON DELETE CASCADE ON UPDATE CASCADE,
    CONSTRAINT [FK_TextResourceInLanguage_TextResource] FOREIGN KEY ([TextResourceID]) REFERENCES [dbo].[TextResource] ([TextResourceID]) ON DELETE CASCADE ON UPDATE CASCADE
);

