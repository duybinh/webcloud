﻿CREATE TABLE [dbo].[LogGroup] (
    [ID]              BIGINT        IDENTITY (1, 1) NOT NULL,
    [CreatedDatetime] DATETIME2 (7) NOT NULL,
    [ReferenceTable]  VARCHAR (100) NULL,
    [ItemID]          VARCHAR (50)  NULL,
    [WebsiteKey]      VARCHAR (50)  NULL,
    [LanguageKey]     VARCHAR (10)  NULL,
    CONSTRAINT [PK_LogGroup] PRIMARY KEY CLUSTERED ([ID] ASC)
);

