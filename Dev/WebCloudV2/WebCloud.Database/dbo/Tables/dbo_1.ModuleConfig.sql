﻿CREATE TABLE [dbo].[ModuleConfig] (
    [WebsiteID]          VARCHAR (50)   NOT NULL,
    [LanguageID]         VARCHAR (10)   NOT NULL,
    [ModuleBaseConfigID] VARCHAR (50)   NOT NULL,
    [ModuleID]           VARCHAR (50)   NOT NULL,
    [Value]              NVARCHAR (MAX) NOT NULL,
    CONSTRAINT [PK_ModuleConfig] PRIMARY KEY CLUSTERED ([WebsiteID] ASC, [LanguageID] ASC, [ModuleBaseConfigID] ASC, [ModuleID] ASC),
    CONSTRAINT [FK_ModuleConfig_Language] FOREIGN KEY ([LanguageID]) REFERENCES [dbo].[Language] ([LanguageID]),
    CONSTRAINT [FK_ModuleConfig_Module] FOREIGN KEY ([WebsiteID], [ModuleID]) REFERENCES [dbo].[Module] ([WebsiteID], [ModuleID]) ON DELETE CASCADE ON UPDATE CASCADE,
    CONSTRAINT [FK_ModuleConfig_ModuleBaseConfig] FOREIGN KEY ([ModuleBaseConfigID]) REFERENCES [dbo].[ModuleBaseConfig] ([ModuleBaseConfigID]) ON DELETE CASCADE ON UPDATE CASCADE
);

