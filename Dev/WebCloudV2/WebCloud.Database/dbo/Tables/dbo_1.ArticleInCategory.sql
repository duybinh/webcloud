﻿CREATE TABLE [dbo].[ArticleInCategory] (
    [WebsiteID]         VARCHAR (50) NOT NULL,
    [ArticleCategoryID] VARCHAR (50) NOT NULL,
    [ArticleID]         VARCHAR (50) NOT NULL,
    CONSTRAINT [PK_ArticleInCategory_1] PRIMARY KEY CLUSTERED ([WebsiteID] ASC, [ArticleCategoryID] ASC, [ArticleID] ASC),
    CONSTRAINT [FK_ArticleInCategory_Article] FOREIGN KEY ([WebsiteID], [ArticleID]) REFERENCES [dbo].[Article] ([WebsiteID], [ArticleID]) ON DELETE CASCADE ON UPDATE CASCADE,
    CONSTRAINT [FK_ArticleInCategory_ArticleCategory] FOREIGN KEY ([WebsiteID], [ArticleCategoryID]) REFERENCES [dbo].[ArticleCategory] ([WebsiteID], [ArticleCategoryID]) ON DELETE CASCADE ON UPDATE CASCADE
);

