﻿CREATE TABLE [dbo].[DiscussionGroups] (
    [ID]              BIGINT          IDENTITY (1, 1) NOT NULL,
    [DatetimeCreated] DATETIME        NOT NULL,
    [DiscussionCount] BIGINT          CONSTRAINT [DF_DiscussionGroups_DiscussionCount] DEFAULT ((0)) NOT NULL,
    [TotalRateValue]  BIGINT          CONSTRAINT [DF_DiscussionGroups_TotalRateValue] DEFAULT ((0)) NOT NULL,
    [RateCount]       BIGINT          CONSTRAINT [DF_DiscussionGroups_RateCount] DEFAULT ((0)) NOT NULL,
    [RateAverage]     DECIMAL (18, 2) CONSTRAINT [DF_DiscussionGroups_RateAverage] DEFAULT ((0)) NOT NULL,
    [IsClosed]        BIT             CONSTRAINT [DF_DiscussionGroups_IsClosed] DEFAULT ((0)) NOT NULL,
    [DatetimeClosed]  DATETIME        NULL,
    [ReferenceTable]  VARCHAR (100)   NULL,
    [ItemID]          VARCHAR (50)    NULL,
    [WebsiteKey]      VARCHAR (50)    NULL,
    [LanguageKey]     VARCHAR (10)    NULL,
    CONSTRAINT [PK_DiscussionGroups] PRIMARY KEY CLUSTERED ([ID] ASC)
);

