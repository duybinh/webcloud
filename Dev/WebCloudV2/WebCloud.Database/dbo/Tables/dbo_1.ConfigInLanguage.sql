﻿CREATE TABLE [dbo].[ConfigInLanguage] (
    [WebsiteID]  VARCHAR (50)   NOT NULL,
    [LanguageID] VARCHAR (10)   NOT NULL,
    [ConfigID]   VARCHAR (50)   NOT NULL,
    [Value]      NVARCHAR (MAX) NOT NULL,
    CONSTRAINT [PK_ConfigInLanguage] PRIMARY KEY CLUSTERED ([WebsiteID] ASC, [LanguageID] ASC, [ConfigID] ASC),
    CONSTRAINT [FK_ConfigInLanguage_Config] FOREIGN KEY ([ConfigID]) REFERENCES [dbo].[Config] ([ConfigID]) ON DELETE CASCADE ON UPDATE CASCADE,
    CONSTRAINT [FK_ConfigInLanguage_Language] FOREIGN KEY ([LanguageID]) REFERENCES [dbo].[Language] ([LanguageID]) ON DELETE CASCADE ON UPDATE CASCADE,
    CONSTRAINT [FK_ConfigInLanguage_Website] FOREIGN KEY ([WebsiteID]) REFERENCES [dbo].[Website] ([WebsiteID]) ON DELETE CASCADE ON UPDATE CASCADE
);

