﻿CREATE TABLE [dbo].[Layout] (
    [LayoutKey]    VARCHAR (50)   NOT NULL,
    [WebsiteKey]   VARCHAR (50)   NOT NULL,
    [Name]         NVARCHAR (250) NOT NULL,
    [Description]  NVARCHAR (500) NULL,
    [Content]      NVARCHAR (MAX) NOT NULL,
    [SortOrder]    INT            CONSTRAINT [DF_Layout_SortOrder] DEFAULT ((0)) NOT NULL,
    [IsBaseLayout] BIT            CONSTRAINT [DF_Layout_IsBaseLayout] DEFAULT ((0)) NOT NULL,
    [IsEnabled]    BIT            CONSTRAINT [DF_Layout_IsEnabled] DEFAULT ((1)) NOT NULL,
    [IsDefault]    BIT            CONSTRAINT [DF_Layout_IsDefault] DEFAULT ((0)) NOT NULL,
    CONSTRAINT [PK_Layout] PRIMARY KEY CLUSTERED ([LayoutKey] ASC, [WebsiteKey] ASC),
    CONSTRAINT [FK_Layout_LayoutBase] FOREIGN KEY ([LayoutKey]) REFERENCES [dbo].[LayoutBase] ([LayoutKey])
);

