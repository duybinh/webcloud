﻿CREATE TABLE [dbo].[Discussion] (
    [DiscussionID]       BIGINT         IDENTITY (1, 1) NOT NULL,
    [DiscussionGroupID]  BIGINT         NOT NULL,
    [Title]              NVARCHAR (200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
    [Content]            NVARCHAR (MAX) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
    [Avarta]             NVARCHAR (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
    [Name]               NVARCHAR (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
    [Email]              NVARCHAR (200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
    [IsReplyOf]          BIGINT         NULL,
    [Rate]               INT            NULL,
    [LikeCount]          INT            CONSTRAINT [DF_Discussion_LikeCount] DEFAULT ((0)) NOT NULL,
    [DislikeCount]       INT            CONSTRAINT [DF_Discussion_DislikeCount] DEFAULT ((0)) NOT NULL,
    [IsApproved]         BIT            CONSTRAINT [DF_Discussion_IsApproved] DEFAULT ((0)) NOT NULL,
    [IsEnabled]          BIT            CONSTRAINT [DF_Discussion_IsEnabled] DEFAULT ((1)) NOT NULL,
    [IsDeleted]          BIT            CONSTRAINT [DF_Discussion_IsDeleted] DEFAULT ((0)) NOT NULL,
    [CreatedDatetime]    DATETIME2 (7)  NOT NULL,
    [LastEditedDatetime] DATETIME2 (7)  NOT NULL,
    [CreatingUser]       BIGINT         NULL,
    [LastEditedUser]     BIGINT         NULL,
    CONSTRAINT [PK_Discussion] PRIMARY KEY CLUSTERED ([DiscussionID] ASC),
    CONSTRAINT [FK_Discussion_DiscussionGroups] FOREIGN KEY ([DiscussionGroupID]) REFERENCES [dbo].[DiscussionGroups] ([ID]) ON DELETE CASCADE ON UPDATE CASCADE
);

