﻿CREATE TABLE [dbo].[Module] (
    [ModuleKey]     VARCHAR (50)   NOT NULL,
    [ModuleBaseKey] VARCHAR (50)   NOT NULL,
    [PageKey]       VARCHAR (50)   NOT NULL,
    [WebsiteKey]    VARCHAR (50)   NOT NULL,
    [LanguageKey]   VARCHAR (10)   NOT NULL,
    [Name]          NVARCHAR (250) NOT NULL,
    [Section]       NVARCHAR (100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
    [OnAllPage]     BIT            CONSTRAINT [DF_Module_OnAllPage] DEFAULT ((0)) NOT NULL,
    [IsBasic]       BIT            CONSTRAINT [DF_Module_IsBasic] DEFAULT ((0)) NOT NULL,
    [IsEnabled]     BIT            CONSTRAINT [DF_Module_IsEnabled] DEFAULT ((1)) NOT NULL,
    [SortOrder]     INT            CONSTRAINT [DF_Module_SortOrder] DEFAULT ((0)) NOT NULL,
    CONSTRAINT [PK_Module_1] PRIMARY KEY CLUSTERED ([ModuleKey] ASC),
    CONSTRAINT [FK_Module_ModuleBase] FOREIGN KEY ([ModuleBaseKey]) REFERENCES [dbo].[ModuleBase] ([ModuleKey]),
    CONSTRAINT [FK_Module_Page] FOREIGN KEY ([PageKey], [WebsiteKey], [LanguageKey]) REFERENCES [dbo].[Page] ([PageKey], [WebsiteKey], [LanguageKey]) ON DELETE CASCADE ON UPDATE CASCADE
);

