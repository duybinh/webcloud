﻿CREATE TABLE [dbo].[Language] (
    [LanguageID]    VARCHAR (10)   NOT NULL,
    [TranslateCode] VARCHAR (10)   NOT NULL,
    [Name]          NVARCHAR (50)  NOT NULL,
    [DisplayName]   NVARCHAR (50)  NOT NULL,
    [Image]         NVARCHAR (250) NULL,
    [IsEnabled]     BIT            CONSTRAINT [DF_Language_IsEnabled] DEFAULT ((1)) NOT NULL,
    [SortOrder]     INT            CONSTRAINT [DF_Language_SortOrder] DEFAULT ((0)) NOT NULL,
    CONSTRAINT [PK_Language] PRIMARY KEY CLUSTERED ([LanguageID] ASC)
);

