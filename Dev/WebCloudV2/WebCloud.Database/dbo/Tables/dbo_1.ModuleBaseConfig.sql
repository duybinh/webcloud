﻿CREATE TABLE [dbo].[ModuleBaseConfig] (
    [ModuleBaseConfigID] VARCHAR (50)   NOT NULL,
    [ModuleBaseID]       VARCHAR (50)   NOT NULL,
    [Name]               NVARCHAR (250) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
    [Description]        NVARCHAR (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
    [Hint]               NVARCHAR (MAX) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
    [DataType]           VARCHAR (20)   NOT NULL,
    [DefaultValue]       NVARCHAR (MAX) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
    [RolesToConfig]      VARCHAR (500)  CONSTRAINT [DF_ModuleBaseConfig_RolesToConfig] DEFAULT ('*') NULL,
    [SortOrder]          INT            CONSTRAINT [DF_ModuleBaseConfig_SortOrder] DEFAULT ((0)) NOT NULL,
    [IsEnabled]          BIT            CONSTRAINT [DF_ModuleBaseConfig_IsEnabled] DEFAULT ((1)) NOT NULL,
    [AllLanguage]        BIT            CONSTRAINT [DF_ModuleBaseConfig_AllLanguage] DEFAULT ((1)) NOT NULL,
    CONSTRAINT [PK_ModuleBaseConfig] PRIMARY KEY CLUSTERED ([ModuleBaseConfigID] ASC),
    CONSTRAINT [FK_ModuleBaseConfig_ModuleBase] FOREIGN KEY ([ModuleBaseID]) REFERENCES [dbo].[ModuleBase] ([ModuleBaseID]) ON DELETE CASCADE ON UPDATE CASCADE
);

