﻿CREATE TABLE [dbo].[LogGroup] (
    [LogGroupID]     BIGINT        IDENTITY (1, 1) NOT NULL,
    [ReferenceTable] VARCHAR (100) NULL,
    [ItemID]         VARCHAR (50)  NULL,
    [WebsiteID]      VARCHAR (50)  NULL,
    CONSTRAINT [PK_LogGroup] PRIMARY KEY CLUSTERED ([LogGroupID] ASC)
);

