﻿CREATE TABLE [dbo].[ProductInCategory] (
    [WebsiteID]         VARCHAR (50) NOT NULL,
    [ProductCategoryID] VARCHAR (50) NOT NULL,
    [ProductID]         VARCHAR (50) NOT NULL,
    CONSTRAINT [PK_ProductInCategory] PRIMARY KEY CLUSTERED ([WebsiteID] ASC, [ProductCategoryID] ASC, [ProductID] ASC),
    CONSTRAINT [FK_ProductInCategory_Product] FOREIGN KEY ([WebsiteID], [ProductID]) REFERENCES [dbo].[Product] ([WebsiteID], [ProductID]) ON DELETE CASCADE ON UPDATE CASCADE,
    CONSTRAINT [FK_ProductInCategory_ProductCategory] FOREIGN KEY ([WebsiteID], [ProductCategoryID]) REFERENCES [dbo].[ProductCategory] ([WebsiteID], [ProductCategoryID]) ON DELETE CASCADE ON UPDATE CASCADE
);

