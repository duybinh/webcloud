﻿CREATE TABLE [dbo].[ArticleCategory] (
    [WebsiteID]           VARCHAR (50)   NOT NULL,
    [ArticleCategoryID]   VARCHAR (50)   NOT NULL,
    [GlobalID]            BIGINT         IDENTITY (1, 1) NOT NULL,
    [ParentID]            VARCHAR (50)   COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
    [ThumbImage]          NVARCHAR (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
    [DisplayArticleThumb] BIT            CONSTRAINT [DF_ArticleCategory_DisplayArticleThumb] DEFAULT ((1)) NOT NULL,
    [IsHot]               BIT            CONSTRAINT [DF_ArticleCategory_IsHot] DEFAULT ((0)) NOT NULL,
    [AllowComment]        BIT            CONSTRAINT [DF_ArticleCategory_AllowComment] DEFAULT ((1)) NOT NULL,
    [IsEnabled]           BIT            CONSTRAINT [DF_ArticleCategory_IsEnabled] DEFAULT ((1)) NOT NULL,
    [IsDeleted]           BIT            CONSTRAINT [DF_ArticleCategory_IsDeleted] DEFAULT ((0)) NOT NULL,
    [SortOrder]           INT            CONSTRAINT [DF_ArticleCategory_SortOrder] DEFAULT ((0)) NOT NULL,
    [IsLeaf]              BIT            CONSTRAINT [DF_ArticleCategory_IsLeaf] DEFAULT ((1)) NOT NULL,
    [LogGroupID]          BIGINT         NULL,
    [CreatedDatetime]     DATETIME2 (7)  NOT NULL,
    [LastEditedDatetime]  DATETIME2 (7)  NOT NULL,
    [CreatedUserName]     VARCHAR (50)   NULL,
    [LastEditedUserName]  VARCHAR (50)   NULL,
    [CreatedUserID]       BIGINT         NULL,
    [LastEditedUserID]    BIGINT         NULL,
    CONSTRAINT [PK_ArticleCategory] PRIMARY KEY CLUSTERED ([WebsiteID] ASC, [ArticleCategoryID] ASC),
    CONSTRAINT [FK_ArticleCategory_LogGroup] FOREIGN KEY ([LogGroupID]) REFERENCES [dbo].[LogGroup] ([LogGroupID]),
    CONSTRAINT [FK_ArticleCategory_Website] FOREIGN KEY ([WebsiteID]) REFERENCES [dbo].[Website] ([WebsiteID])
);


GO
-- =============================================
-- Author:		BinhND
-- Create date: 13/09/2013
-- Description:	TRIGGER FOR UPDATE ACTICLE
-- =============================================
CREATE TRIGGER TR_ARTICLE_CATEGORY_UPDATE ON dbo.ArticleCategory
    AFTER UPDATE
AS
    IF @@ROWCOUNT = 1
    BEGIN
	
        SET NOCOUNT ON;

		------------ TRIGGER BODY ------------
    
        DECLARE @ArticleCategoryID VARCHAR(50)
        DECLARE @Actor BIGINT
        DECLARE @LogGroupID BIGINT
        DECLARE @IsDeleted BIT 
        DECLARE @Action VARCHAR(50)
	
        SELECT  @ArticleCategoryID = inserted.ArticleCategoryID ,
                @Actor = inserted.LastEditedUserID ,
                @LogGroupID = inserted.LogGroupID ,
                @IsDeleted = inserted.IsDeleted
        FROM    inserted

        IF UPDATE(IsDeleted) 
            BEGIN
                IF @IsDeleted = 1 
                    BEGIN
                        SET @Action = 'DELETE'
                    END 
                ELSE 
                    BEGIN
                        SET @Action = 'RESTORE'
                    END 
            END 
        ELSE 
            BEGIN
                SET @Action = 'UPDATE'
            END 


        INSERT  INTO dbo.Log
                ( LogGroupID ,
                  Action ,
                  ActionDetail ,
                  Actor			        
                )
        VALUES  ( @LogGroupID , -- LogGroupID - bigint
                  @Action , -- Action - varchar(50)
                  NULL , -- ActionDetail - nvarchar(250)
                  @Actor  -- Actor - bigint			        
                )

    END

GO
-- =============================================
-- Author:		BinhND
-- Create date: 17/05/2014
-- Description:	TRIGGER FOR DELETE ACTICLE CATEGORY
-- =============================================
CREATE TRIGGER [dbo].[TR_ARTICLE_CATEGORY_DELETE] ON [dbo].[ArticleCategory]
    AFTER DELETE
AS
    BEGIN
	
        SET NOCOUNT ON;

		------------ TRIGGER BODY ------------
		-- delete log
    	DELETE dbo.LogGroup
		FROM DELETED
		WHERE LogGroup.LogGroupID = DELETED.LogGroupID
		
		-- delete relationship
		DELETE dbo.ArticleCategoryRelationship
		FROM DELETED	
		WHERE	ArticleCategoryRelationship.WebsiteID = DELETED.WebsiteID 
			AND	ArticleCategoryRelationship.ChildID	= DELETED.ArticleCategoryID
    END

GO
-- =============================================
-- Author:		BinhND
-- Create date: 15/05/2013
-- Description:	TRIGGER FOR INSERT ARTICLE CATEGORY
-- =============================================
CREATE TRIGGER [dbo].[TR_ARTICLE_CATEGORY_INSERT]
   ON  [dbo].[ArticleCategory] 
   INSTEAD OF INSERT
AS 
BEGIN
	
	SET NOCOUNT ON;

    ------------ TRIGGER BODY ------------
	BEGIN
		
		DECLARE @ReferenceTable VARCHAR(100) = 'ArticleCategory'
		DECLARE @Action VARCHAR(50)= 'INSERT'
		DECLARE @LogGroupTbl TABLE (ArticleCategoryID VARCHAR(50),LogGroupID BIGINT)		
		
		-- insert LogGroup
		INSERT INTO dbo.LogGroup
		        ( ReferenceTable ,
		          ItemID ,
		          WebsiteID
		        )
		        OUTPUT INSERTED.ItemID, INSERTED.LogGroupID INTO @LogGroupTbl
				SELECT @ReferenceTable AS ReferenceTable,
						INSERTED.ArticleCategoryID AS ItemID,
						INSERTED.WebsiteID AS WebsiteID
				FROM	INSERTED
		
		-- insert Log
		INSERT INTO dbo.Log
		        ( LogGroupID ,
		          Action ,
		          ActionDetail ,
		          Actor
		        )
				SELECT lg.LogGroupID AS LogGroupID, 
						@Action AS ACTION,
						NULL,
						i.CreatedUserID AS Actor		
										
				FROM INSERTED i
				JOIN @LogGroupTbl lg ON i.ArticleCategoryID = lg.ArticleCategoryID

		-- insert Article Category	

		INSERT INTO dbo.ArticleCategory
		        ( WebsiteID ,
		          ArticleCategoryID ,
		          ParentID ,
		          ThumbImage ,
		          DisplayArticleThumb ,
		          IsHot ,
		          AllowComment ,
		          IsEnabled ,
		          IsDeleted ,
		          SortOrder ,
		          LogGroupID ,
		          CreatedDatetime ,
		          LastEditedDatetime ,
		          CreatedUserName ,
		          LastEditedUserName ,
		          CreatedUserID ,
		          LastEditedUserID
		        )		
		SELECT    WebsiteID ,
		          i.ArticleCategoryID ,
		          ParentID ,
		          ThumbImage ,
		          DisplayArticleThumb ,
		          IsHot ,
		          AllowComment ,
		          IsEnabled ,
		          IsDeleted ,
		          SortOrder ,
		          lg.LogGroupID ,
		          CreatedDatetime ,
		          LastEditedDatetime ,
		          CreatedUserName ,
		          LastEditedUserName ,
		          CreatedUserID ,
		          LastEditedUserID
		        
		 FROM	  INSERTED i
		          JOIN @LogGroupTbl AS lg ON lg.ArticleCategoryID = i.ArticleCategoryID		        
		  
	END
END
