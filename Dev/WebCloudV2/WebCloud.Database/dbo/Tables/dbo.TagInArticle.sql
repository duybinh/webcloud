﻿CREATE TABLE [dbo].[TagInArticle] (
    [TagID]       BIGINT       NOT NULL,
    [ArticleID]   VARCHAR (50) NOT NULL,
    [WebsiteKey]  VARCHAR (50) NOT NULL,
    [LanguageKey] VARCHAR (10) NOT NULL,
    CONSTRAINT [PK_TagInArticle] PRIMARY KEY CLUSTERED ([TagID] ASC, [ArticleID] ASC, [WebsiteKey] ASC, [LanguageKey] ASC),
    CONSTRAINT [FK_TagInArticle_Article] FOREIGN KEY ([ArticleID], [WebsiteKey], [LanguageKey]) REFERENCES [dbo].[Article] ([ID], [WebsiteKey], [LanguageKey]) ON DELETE CASCADE ON UPDATE CASCADE,
    CONSTRAINT [FK_TagInArticle_Tag] FOREIGN KEY ([TagID]) REFERENCES [dbo].[Tag] ([ID]) ON DELETE CASCADE ON UPDATE CASCADE
);

