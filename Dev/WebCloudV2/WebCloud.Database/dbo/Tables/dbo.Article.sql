﻿CREATE TABLE [dbo].[Article] (
    [ID]                 VARCHAR (50)   NOT NULL,
    [WebsiteKey]         VARCHAR (50)   NOT NULL,
    [LanguageKey]        VARCHAR (10)   NOT NULL,
    [Title]              NVARCHAR (250) NOT NULL,
    [SeoTitle]           NVARCHAR (250) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
    [Description]        NVARCHAR (500) NULL,
    [Content]            NVARCHAR (MAX) NULL,
    [ThumbImage]         NVARCHAR (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
    [MetaTitle]          NVARCHAR (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
    [MetaKeyword]        NVARCHAR (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
    [MetaDescription]    NVARCHAR (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
    [SortOrder]          BIGINT         CONSTRAINT [DF_Article_SortOrder] DEFAULT ((0)) NOT NULL,
    [HaveVideo]          BIT            CONSTRAINT [DF_Article_HaveVideo] DEFAULT ((0)) NOT NULL,
    [HavePhoto]          BIT            CONSTRAINT [DF_Article_HavePhoto] DEFAULT ((0)) NOT NULL,
    [HaveAttachFile]     BIT            CONSTRAINT [DF_Article_HaveAttachFile] DEFAULT ((0)) NOT NULL,
    [IsHot]              BIT            CONSTRAINT [DF_Article_IsHot] DEFAULT ((0)) NOT NULL,
    [IsApproved]         BIT            CONSTRAINT [DF_Article_IsApproved] DEFAULT ((1)) NOT NULL,
    [AllowComment]       BIT            CONSTRAINT [DF_Article_AllowComment] DEFAULT ((1)) NOT NULL,
    [DisplayDatetime]    DATETIME2 (7)  NOT NULL,
    [IsEnabled]          BIT            CONSTRAINT [DF_Article_IsEnabled] DEFAULT ((1)) NOT NULL,
    [IsDeleted]          BIT            CONSTRAINT [DF_Article_IsDeleted] DEFAULT ((0)) NOT NULL,
    [DiscussionGroupID]  BIGINT         NULL,
    [StatisticID]        BIGINT         NULL,
    [LogGroupID]         BIGINT         NULL,
    [CreatedDatetime]    DATETIME2 (7)  NOT NULL,
    [LastEditedDatetime] DATETIME2 (7)  NOT NULL,
    [CreatingUser]       BIGINT         NULL,
    [LastEditedUser]     BIGINT         NULL,
    CONSTRAINT [PK_Article] PRIMARY KEY CLUSTERED ([ID] ASC, [WebsiteKey] ASC, [LanguageKey] ASC),
    CONSTRAINT [FK_Article_DiscussionGroups] FOREIGN KEY ([DiscussionGroupID]) REFERENCES [dbo].[DiscussionGroups] ([ID]),
    CONSTRAINT [FK_Article_Language] FOREIGN KEY ([LanguageKey]) REFERENCES [dbo].[Language] ([LanguageKey]),
    CONSTRAINT [FK_Article_LogGroup] FOREIGN KEY ([LogGroupID]) REFERENCES [dbo].[LogGroup] ([ID]),
    CONSTRAINT [FK_Article_Website] FOREIGN KEY ([WebsiteKey]) REFERENCES [dbo].[Website] ([WebsiteKey])
);


GO
CREATE NONCLUSTERED INDEX [IN_Article_SeoTitle]
    ON [dbo].[Article]([SeoTitle] ASC);

