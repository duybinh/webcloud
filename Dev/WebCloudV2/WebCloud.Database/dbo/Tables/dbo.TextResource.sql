﻿CREATE TABLE [dbo].[TextResource] (
    [TextResourceID] VARCHAR (50)   NOT NULL,
    [Value]          NVARCHAR (MAX) NOT NULL,
    [DisplayAsHtml]  BIT            CONSTRAINT [DF_TextResource_DisplayAsHtml] DEFAULT ((0)) NOT NULL,
    CONSTRAINT [PK_TextResource] PRIMARY KEY CLUSTERED ([TextResourceID] ASC)
);

