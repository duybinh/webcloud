﻿CREATE TABLE [dbo].[User] (
    [UserID]                   BIGINT         IDENTITY (1000000000, 1) NOT NULL,
    [Account]                  NVARCHAR (50)  NOT NULL,
    [DisplayName]              NVARCHAR (250) NULL,
    [Email]                    NVARCHAR (250) NOT NULL,
    [FirstName]                NVARCHAR (100) NULL,
    [LastName]                 NVARCHAR (100) NULL,
    [Birthday]                 DATETIME2 (7)  NULL,
    [Gender]                   BIT            NULL,
    [Avatar]                   NVARCHAR (255) NULL,
    [EmailVerificationKey]     NVARCHAR (50)  NOT NULL,
    [IsVerifiedEmail]          BIT            CONSTRAINT [DF_User_IsVerifiedEmail] DEFAULT ((0)) NOT NULL,
    [EmailVerificationExpried] DATETIME2 (7)  NULL,
    [IsLocked]                 BIT            CONSTRAINT [DF_User_IsLocked] DEFAULT ((0)) NOT NULL,
    [LockExpried]              DATETIME2 (7)  NULL,
    [IsDeleted]                BIT            CONSTRAINT [DF_User_IsDeleted] DEFAULT ((0)) NOT NULL,
    [LanguageKey]              VARCHAR (10)   NULL,
    [WebsiteKey]               VARCHAR (50)   NOT NULL,
    CONSTRAINT [PK_User] PRIMARY KEY CLUSTERED ([UserID] ASC),
    CONSTRAINT [FK_User_Language] FOREIGN KEY ([LanguageKey]) REFERENCES [dbo].[Language] ([LanguageKey]) ON DELETE SET NULL ON UPDATE SET NULL,
    CONSTRAINT [FK_User_Website] FOREIGN KEY ([WebsiteKey]) REFERENCES [dbo].[Website] ([WebsiteKey]) ON DELETE CASCADE ON UPDATE CASCADE
);

