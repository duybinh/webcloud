﻿CREATE TABLE [dbo].[UserOAuthMembership] (
    [ID]              BIGINT         IDENTITY (1, 1) NOT NULL,
    [WebsiteID]       VARCHAR (50)   NOT NULL,
    [UserID]          VARCHAR (50)   NOT NULL,
    [OAuthProviderID] INT            NOT NULL,
    [ProviderUserID]  NVARCHAR (250) NOT NULL,
    CONSTRAINT [PK_UserOAuthMembership] PRIMARY KEY CLUSTERED ([ID] ASC),
    CONSTRAINT [FK_UserOAuthMembership_OAuthProvider] FOREIGN KEY ([OAuthProviderID]) REFERENCES [dbo].[OAuthProvider] ([OAuthProviderID]) ON DELETE CASCADE ON UPDATE CASCADE,
    CONSTRAINT [FK_UserOAuthMembership_User] FOREIGN KEY ([WebsiteID], [UserID]) REFERENCES [dbo].[User] ([WebsiteID], [UserID]) ON DELETE CASCADE ON UPDATE CASCADE
);

