﻿CREATE TABLE [dbo].[ProductCategoryContent] (
    [WebsiteID]         VARCHAR (50)   NOT NULL,
    [LanguageID]        VARCHAR (10)   NOT NULL,
    [ProductCategoryID] VARCHAR (50)   NOT NULL,
    [Name]              NVARCHAR (250) NOT NULL,
    [SeoName]           NVARCHAR (250) NULL,
    [Description]       NVARCHAR (500) NULL,
    [MetaTitle]         NVARCHAR (250) NULL,
    [MetaKeyword]       NVARCHAR (500) NULL,
    [MetaDescription]   NVARCHAR (500) NULL,
    CONSTRAINT [PK_ProductCategoryContent] PRIMARY KEY CLUSTERED ([WebsiteID] ASC, [LanguageID] ASC, [ProductCategoryID] ASC),
    CONSTRAINT [FK_ProductCategoryContent_Language] FOREIGN KEY ([LanguageID]) REFERENCES [dbo].[Language] ([LanguageID]),
    CONSTRAINT [FK_ProductCategoryContent_ProductCategory] FOREIGN KEY ([WebsiteID], [ProductCategoryID]) REFERENCES [dbo].[ProductCategory] ([WebsiteID], [ProductCategoryID]) ON DELETE CASCADE ON UPDATE CASCADE
);

