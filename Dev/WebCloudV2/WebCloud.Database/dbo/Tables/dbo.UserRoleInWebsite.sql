﻿CREATE TABLE [dbo].[UserRoleInWebsite] (
    [WebsiteID]  VARCHAR (50) NOT NULL,
    [UserRoleID] VARCHAR (50) NOT NULL,
    CONSTRAINT [PK_UserRoleInWebsite] PRIMARY KEY CLUSTERED ([WebsiteID] ASC, [UserRoleID] ASC),
    CONSTRAINT [FK_UserRoleInWebsite_UserRole] FOREIGN KEY ([UserRoleID]) REFERENCES [dbo].[UserRole] ([UserRoleID]) ON DELETE CASCADE ON UPDATE CASCADE,
    CONSTRAINT [FK_UserRoleInWebsite_Website] FOREIGN KEY ([WebsiteID]) REFERENCES [dbo].[Website] ([WebsiteID]) ON DELETE CASCADE ON UPDATE CASCADE
);

