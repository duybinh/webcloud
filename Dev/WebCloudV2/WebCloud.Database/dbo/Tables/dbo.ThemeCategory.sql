﻿CREATE TABLE [dbo].[ThemeCategory] (
    [ID]          INT            NOT NULL,
    [Name]        NVARCHAR (200) NOT NULL,
    [Description] NVARCHAR (500) NULL,
    [IsEnable]    BIT            CONSTRAINT [DF_ThemeCategory_IsEnable] DEFAULT ((1)) NOT NULL,
    [SortOrder]   INT            CONSTRAINT [DF_ThemeCategory_SortOrder] DEFAULT ((0)) NOT NULL,
    CONSTRAINT [PK_ThemeCategory] PRIMARY KEY CLUSTERED ([ID] ASC)
);

