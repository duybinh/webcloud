﻿CREATE TABLE [dbo].[OAuthProvider] (
    [OAuthProviderID] INT            NOT NULL,
    [Name]            NVARCHAR (250) NOT NULL,
    [Description]     NVARCHAR (500) NULL,
    [Website]         NVARCHAR (250) NULL,
    CONSTRAINT [PK_OAuthProvider] PRIMARY KEY CLUSTERED ([OAuthProviderID] ASC)
);

