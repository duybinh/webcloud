﻿CREATE TABLE [dbo].[WebPlan] (
    [WebPlanID]        INT            NOT NULL,
    [Name]             NVARCHAR (200) NOT NULL,
    [Description]      NVARCHAR (500) NOT NULL,
    [Name_Lang]        VARCHAR (50)   NOT NULL,
    [Description_Lang] VARCHAR (50)   NOT NULL,
    [IsEnabled]        BIT            CONSTRAINT [DF_WebPlan_IsEnabled] DEFAULT ((1)) NOT NULL,
    [SortOrder]        INT            CONSTRAINT [DF_WebPlan_SortOrder] DEFAULT ((0)) NOT NULL,
    CONSTRAINT [PK_WebPlan] PRIMARY KEY CLUSTERED ([WebPlanID] ASC)
);

