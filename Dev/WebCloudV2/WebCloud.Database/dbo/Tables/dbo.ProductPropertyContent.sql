﻿CREATE TABLE [dbo].[ProductPropertyContent] (
    [WebsiteID]         VARCHAR (50)   NOT NULL,
    [LanguageID]        VARCHAR (10)   NOT NULL,
    [ProductPropertyID] VARCHAR (50)   NOT NULL,
    [Name]              NVARCHAR (250) NOT NULL,
    [Unit]              NVARCHAR (100) NULL,
    [DefaultTextValue]  NVARCHAR (500) NULL,
    [ValueRange]        NVARCHAR (MAX) NULL,
    CONSTRAINT [PK_ProductPropertyContent] PRIMARY KEY CLUSTERED ([WebsiteID] ASC, [LanguageID] ASC, [ProductPropertyID] ASC),
    CONSTRAINT [FK_ProductPropertyContent_Language] FOREIGN KEY ([LanguageID]) REFERENCES [dbo].[Language] ([LanguageID]),
    CONSTRAINT [FK_ProductPropertyContent_ProductProperty] FOREIGN KEY ([WebsiteID], [ProductPropertyID]) REFERENCES [dbo].[ProductProperty] ([WebsiteID], [ProductPropertyID]) ON DELETE CASCADE ON UPDATE CASCADE
);

