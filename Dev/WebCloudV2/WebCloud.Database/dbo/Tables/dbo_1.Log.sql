﻿CREATE TABLE [dbo].[Log] (
    [LogID]          BIGINT         IDENTITY (1, 1) NOT NULL,
    [LogGroupID]     BIGINT         NOT NULL,
    [ActionDatetime] DATETIME2 (7)  CONSTRAINT [DF_Log_ActionDatetime] DEFAULT (getdate()) NOT NULL,
    [Action]         VARCHAR (50)   NOT NULL,
    [ActionDetail]   NVARCHAR (250) NULL,
    [Actor]          BIGINT         NULL,
    CONSTRAINT [PK_Log] PRIMARY KEY CLUSTERED ([LogID] ASC),
    CONSTRAINT [FK_Log_LogGroup] FOREIGN KEY ([LogGroupID]) REFERENCES [dbo].[LogGroup] ([LogGroupID]) ON DELETE CASCADE ON UPDATE CASCADE
);

