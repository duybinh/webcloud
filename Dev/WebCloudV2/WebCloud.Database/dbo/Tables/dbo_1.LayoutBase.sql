﻿CREATE TABLE [dbo].[LayoutBase] (
    [LayoutBaseID] VARCHAR (50)   NOT NULL,
    [Name]         NVARCHAR (250) NOT NULL,
    [Description]  NVARCHAR (500) NULL,
    [Content]      NVARCHAR (MAX) NOT NULL,
    [SortOrder]    INT            CONSTRAINT [DF_LayoutBase_SortOrder] DEFAULT ((0)) NOT NULL,
    [IsEnabled]    BIT            CONSTRAINT [DF_LayoutBase_IsEnabled] DEFAULT ((1)) NOT NULL,
    [IsDefault]    BIT            CONSTRAINT [DF_LayoutBase_IsDefault] DEFAULT ((0)) NOT NULL,
    CONSTRAINT [PK_LayoutBase] PRIMARY KEY CLUSTERED ([LayoutBaseID] ASC)
);

