﻿CREATE TABLE [dbo].[ConfigInWebsite] (
    [ConfigKey]  VARCHAR (50)   NOT NULL,
    [WebsiteKey] VARCHAR (50)   NOT NULL,
    [Value]      NVARCHAR (MAX) NOT NULL,
    CONSTRAINT [PK_ConfigInWebsite] PRIMARY KEY CLUSTERED ([ConfigKey] ASC, [WebsiteKey] ASC),
    CONSTRAINT [FK_ConfigInWebsite_Config] FOREIGN KEY ([ConfigKey]) REFERENCES [dbo].[Config] ([ConfigKey]) ON DELETE CASCADE ON UPDATE CASCADE,
    CONSTRAINT [FK_ConfigInWebsite_Website] FOREIGN KEY ([WebsiteKey]) REFERENCES [dbo].[Website] ([WebsiteKey]) ON DELETE CASCADE ON UPDATE CASCADE
);

