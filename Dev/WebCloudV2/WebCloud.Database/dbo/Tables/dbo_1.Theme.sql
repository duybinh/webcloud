﻿CREATE TABLE [dbo].[Theme] (
    [ThemeID]         VARCHAR (50)   NOT NULL,
    [Name]            NVARCHAR (250) NOT NULL,
    [Description]     NVARCHAR (MAX) NULL,
    [PreviewImage]    NVARCHAR (250) NULL,
    [ThemeCategoryID] INT            NULL,
    [Author]          NVARCHAR (250) NULL,
    [RolesToView]     NVARCHAR (MAX) NULL,
    [RolesToInstall]  NVARCHAR (MAX) NULL,
    [IsEnabled]       BIT            CONSTRAINT [DF_Theme_IsEnabled] DEFAULT ((1)) NOT NULL,
    [SortOrder]       INT            CONSTRAINT [DF_Theme_SortOrder] DEFAULT ((0)) NOT NULL,
    CONSTRAINT [PK_Theme] PRIMARY KEY CLUSTERED ([ThemeID] ASC),
    CONSTRAINT [FK_Theme_ThemeCategory] FOREIGN KEY ([ThemeCategoryID]) REFERENCES [dbo].[ThemeCategory] ([ThemeCategoryID])
);

