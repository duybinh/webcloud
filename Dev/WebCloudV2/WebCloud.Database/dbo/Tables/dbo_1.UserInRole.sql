﻿CREATE TABLE [dbo].[UserInRole] (
    [WebsiteID]  VARCHAR (50) NOT NULL,
    [UserID]     VARCHAR (50) NOT NULL,
    [UserRoleID] VARCHAR (50) NOT NULL,
    CONSTRAINT [PK_UserInRole] PRIMARY KEY CLUSTERED ([WebsiteID] ASC, [UserID] ASC, [UserRoleID] ASC),
    CONSTRAINT [FK_UserInRole_User] FOREIGN KEY ([WebsiteID], [UserID]) REFERENCES [dbo].[User] ([WebsiteID], [UserID]) ON DELETE CASCADE ON UPDATE CASCADE,
    CONSTRAINT [FK_UserInRole_UserRole] FOREIGN KEY ([UserRoleID]) REFERENCES [dbo].[UserRole] ([UserRoleID]) ON DELETE CASCADE ON UPDATE CASCADE
);

