﻿CREATE TABLE [dbo].[ArticleCategoryRelationship] (
    [WebsiteID] VARCHAR (50) NOT NULL,
    [ParentID]  VARCHAR (50) NOT NULL,
    [ChildID]   VARCHAR (50) NOT NULL,
    [Level]     INT          CONSTRAINT [DF_ArticleCategoryRelationship_Level] DEFAULT ((1)) NOT NULL,
    CONSTRAINT [PK_ArticleCategoryRelationship] PRIMARY KEY CLUSTERED ([WebsiteID] ASC, [ParentID] ASC, [ChildID] ASC),
    CONSTRAINT [FK_ArticleCategoryRelationship_ArticleCategory] FOREIGN KEY ([WebsiteID], [ParentID]) REFERENCES [dbo].[ArticleCategory] ([WebsiteID], [ArticleCategoryID]) ON DELETE CASCADE ON UPDATE CASCADE
);

