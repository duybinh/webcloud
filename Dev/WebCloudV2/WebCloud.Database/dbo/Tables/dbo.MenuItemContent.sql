﻿CREATE TABLE [dbo].[MenuItemContent] (
    [WebsiteID]  VARCHAR (50)   NOT NULL,
    [LanguageID] VARCHAR (10)   NOT NULL,
    [MenuItemID] VARCHAR (50)   NOT NULL,
    [Title]      NVARCHAR (250) NOT NULL,
    [Content]    NVARCHAR (MAX) NULL,
    CONSTRAINT [PK_MenuItemContent] PRIMARY KEY CLUSTERED ([WebsiteID] ASC, [LanguageID] ASC, [MenuItemID] ASC),
    CONSTRAINT [FK_MenuItemContent_Language] FOREIGN KEY ([LanguageID]) REFERENCES [dbo].[Language] ([LanguageID]) ON DELETE CASCADE ON UPDATE CASCADE,
    CONSTRAINT [FK_MenuItemContent_MenuItem] FOREIGN KEY ([WebsiteID], [MenuItemID]) REFERENCES [dbo].[MenuItem] ([WebsiteID], [MenuItemID]) ON DELETE CASCADE ON UPDATE CASCADE
);

