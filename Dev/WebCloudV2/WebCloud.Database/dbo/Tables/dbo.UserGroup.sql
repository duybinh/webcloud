﻿CREATE TABLE [dbo].[UserGroup] (
    [WebsiteID]   VARCHAR (50)   NOT NULL,
    [UserGroupID] VARCHAR (50)   NOT NULL,
    [Name]        NVARCHAR (250) NOT NULL,
    [Description] NVARCHAR (500) NULL,
    [IsEnabled]   BIT            CONSTRAINT [DF_Role_IsEnabled] DEFAULT ((1)) NOT NULL,
    [IsDeleted]   BIT            CONSTRAINT [DF_Role_IsDeleted] DEFAULT ((0)) NOT NULL,
    CONSTRAINT [PK_UserGroup] PRIMARY KEY CLUSTERED ([WebsiteID] ASC, [UserGroupID] ASC),
    CONSTRAINT [FK_UserGroup_Website] FOREIGN KEY ([WebsiteID]) REFERENCES [dbo].[Website] ([WebsiteID])
);

