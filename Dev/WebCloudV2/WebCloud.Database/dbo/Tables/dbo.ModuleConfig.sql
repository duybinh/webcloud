﻿CREATE TABLE [dbo].[ModuleConfig] (
    [ConfigBaseKey] VARCHAR (50)   NOT NULL,
    [ModuleKey]     VARCHAR (50)   NOT NULL,
    [Value]         NVARCHAR (MAX) NOT NULL,
    CONSTRAINT [PK_ModuleConfig] PRIMARY KEY CLUSTERED ([ConfigBaseKey] ASC, [ModuleKey] ASC),
    CONSTRAINT [FK_ModuleConfig_Module] FOREIGN KEY ([ModuleKey]) REFERENCES [dbo].[Module] ([ModuleKey]) ON DELETE CASCADE ON UPDATE CASCADE,
    CONSTRAINT [FK_ModuleConfig_ModuleBaseConfig] FOREIGN KEY ([ConfigBaseKey]) REFERENCES [dbo].[ModuleBaseConfig] ([ConfigKey]) ON DELETE CASCADE ON UPDATE CASCADE
);

