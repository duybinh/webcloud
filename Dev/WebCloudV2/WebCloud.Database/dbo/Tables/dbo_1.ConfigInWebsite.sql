﻿CREATE TABLE [dbo].[ConfigInWebsite] (
    [WebsiteID] VARCHAR (50)   NOT NULL,
    [ConfigID]  VARCHAR (50)   NOT NULL,
    [Value]     NVARCHAR (MAX) NOT NULL,
    CONSTRAINT [PK_ConfigInWebsite] PRIMARY KEY CLUSTERED ([WebsiteID] ASC, [ConfigID] ASC),
    CONSTRAINT [FK_ConfigInWebsite_Config] FOREIGN KEY ([ConfigID]) REFERENCES [dbo].[Config] ([ConfigID]) ON DELETE CASCADE ON UPDATE CASCADE,
    CONSTRAINT [FK_ConfigInWebsite_Website] FOREIGN KEY ([WebsiteID]) REFERENCES [dbo].[Website] ([WebsiteID]) ON DELETE CASCADE ON UPDATE CASCADE
);

