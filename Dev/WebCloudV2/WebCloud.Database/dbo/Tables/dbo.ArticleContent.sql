﻿CREATE TABLE [dbo].[ArticleContent] (
    [WebsiteID]       VARCHAR (50)   NOT NULL,
    [ArticleID]       VARCHAR (50)   NOT NULL,
    [LanguageID]      VARCHAR (10)   NOT NULL,
    [Title]           NVARCHAR (250) NOT NULL,
    [SeoTitle]        NVARCHAR (250) NOT NULL,
    [Description]     NVARCHAR (500) NULL,
    [Content]         NVARCHAR (MAX) NULL,
    [MetaTitle]       NVARCHAR (250) NULL,
    [MetaKeyword]     NVARCHAR (500) NULL,
    [MetaDescription] NVARCHAR (500) NULL,
    CONSTRAINT [PK_ArticleContent] PRIMARY KEY CLUSTERED ([WebsiteID] ASC, [ArticleID] ASC, [LanguageID] ASC),
    CONSTRAINT [FK_ArticleContent_Article] FOREIGN KEY ([WebsiteID], [ArticleID]) REFERENCES [dbo].[Article] ([WebsiteID], [ArticleID]) ON DELETE CASCADE ON UPDATE CASCADE,
    CONSTRAINT [FK_ArticleContent_Language] FOREIGN KEY ([LanguageID]) REFERENCES [dbo].[Language] ([LanguageID])
);

