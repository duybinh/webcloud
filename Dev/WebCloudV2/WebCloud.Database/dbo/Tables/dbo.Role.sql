﻿CREATE TABLE [dbo].[Role] (
    [RoleKey]     VARCHAR (50)   NOT NULL,
    [WebsiteKey]  VARCHAR (50)   NOT NULL,
    [Name]        NVARCHAR (250) NOT NULL,
    [Description] NVARCHAR (500) NULL,
    [IsEnabled]   BIT            CONSTRAINT [DF_Role_IsEnabled] DEFAULT ((1)) NOT NULL,
    [IsDeleted]   BIT            CONSTRAINT [DF_Role_IsDeleted] DEFAULT ((0)) NOT NULL,
    CONSTRAINT [PK_Role] PRIMARY KEY CLUSTERED ([RoleKey] ASC, [WebsiteKey] ASC)
);

