﻿CREATE TABLE [dbo].[ConfigInLanguage] (
    [ConfigKey]   VARCHAR (50)   NOT NULL,
    [WebsiteKey]  VARCHAR (50)   NOT NULL,
    [LanguageKey] VARCHAR (10)   NOT NULL,
    [Value]       NVARCHAR (MAX) NOT NULL,
    CONSTRAINT [PK_ConfigInLanguage] PRIMARY KEY CLUSTERED ([ConfigKey] ASC, [WebsiteKey] ASC, [LanguageKey] ASC),
    CONSTRAINT [FK_ConfigInLanguage_Config] FOREIGN KEY ([ConfigKey]) REFERENCES [dbo].[Config] ([ConfigKey]) ON DELETE CASCADE ON UPDATE CASCADE,
    CONSTRAINT [FK_ConfigInLanguage_Language] FOREIGN KEY ([LanguageKey]) REFERENCES [dbo].[Language] ([LanguageKey]) ON DELETE CASCADE ON UPDATE CASCADE,
    CONSTRAINT [FK_ConfigInLanguage_Website] FOREIGN KEY ([WebsiteKey]) REFERENCES [dbo].[Website] ([WebsiteKey]) ON DELETE CASCADE ON UPDATE CASCADE
);

