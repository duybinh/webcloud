﻿CREATE TABLE [dbo].[TagInProduct] (
    [WebsiteID] VARCHAR (50) NOT NULL,
    [ProductID] VARCHAR (50) NOT NULL,
    [TagID]     BIGINT       NOT NULL,
    CONSTRAINT [PK_TagInProduct] PRIMARY KEY CLUSTERED ([WebsiteID] ASC, [ProductID] ASC, [TagID] ASC),
    CONSTRAINT [FK_TagInProduct_Product] FOREIGN KEY ([WebsiteID], [ProductID]) REFERENCES [dbo].[Product] ([WebsiteID], [ProductID]) ON DELETE CASCADE ON UPDATE CASCADE,
    CONSTRAINT [FK_TagInProduct_Tag] FOREIGN KEY ([TagID]) REFERENCES [dbo].[Tag] ([TagID]) ON DELETE CASCADE ON UPDATE CASCADE
);

