﻿CREATE TABLE [dbo].[Tag] (
    [ID]                 BIGINT         IDENTITY (1, 1) NOT NULL,
    [Value]              NVARCHAR (250) NOT NULL,
    [SeoValue]           NVARCHAR (250) NOT NULL,
    [SearchCount]        BIGINT         CONSTRAINT [DF_Tag_SearchCount] DEFAULT ((0)) NOT NULL,
    [LastSearchDatetime] DATETIME2 (7)  NULL,
    [SortOrder]          BIGINT         CONSTRAINT [DF_Tag_SortOrder] DEFAULT ((0)) NOT NULL,
    [LanguageKey]        VARCHAR (10)   NOT NULL,
    CONSTRAINT [PK_Tag] PRIMARY KEY CLUSTERED ([ID] ASC),
    CONSTRAINT [FK_Tag_Language] FOREIGN KEY ([LanguageKey]) REFERENCES [dbo].[Language] ([LanguageKey])
);

