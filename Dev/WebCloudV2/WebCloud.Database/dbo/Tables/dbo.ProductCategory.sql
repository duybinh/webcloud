﻿CREATE TABLE [dbo].[ProductCategory] (
    [WebsiteID]           VARCHAR (50)   NOT NULL,
    [ProductCategoryID]   VARCHAR (50)   NOT NULL,
    [ParentID]            VARCHAR (50)   NULL,
    [ThumbImage]          NVARCHAR (250) NULL,
    [DisplayProductThumb] BIT            CONSTRAINT [DF_ProductCategory_DisplayArticleThumb] DEFAULT ((1)) NOT NULL,
    [IsHot]               BIT            CONSTRAINT [DF_ProductCategory_IsHot] DEFAULT ((0)) NOT NULL,
    [AllowComment]        BIT            CONSTRAINT [DF_ProductCategory_AllowComment] DEFAULT ((1)) NOT NULL,
    [IsEnabled]           BIT            CONSTRAINT [DF_ProductCategory_IsEnabled] DEFAULT ((1)) NOT NULL,
    [IsDeleted]           BIT            CONSTRAINT [DF_ProductCategory_IsDeleted] DEFAULT ((0)) NOT NULL,
    [SortOrder]           INT            CONSTRAINT [DF_ProductCategory_SortOrder] DEFAULT ((0)) NOT NULL,
    [IsLeaf]              BIT            CONSTRAINT [DF_ProductCategory_IsLeaf] DEFAULT ((1)) NOT NULL,
    [LogGroupID]          BIGINT         NULL,
    [CreatedDatetime]     DATETIME2 (7)  NOT NULL,
    [LastEditedDatetime]  DATETIME2 (7)  NOT NULL,
    [CreatingUser]        VARCHAR (50)   NULL,
    [LastEditedUser]      VARCHAR (50)   NULL,
    CONSTRAINT [PK_ProductCategory] PRIMARY KEY CLUSTERED ([WebsiteID] ASC, [ProductCategoryID] ASC),
    CONSTRAINT [FK_ProductCategory_Website] FOREIGN KEY ([WebsiteID]) REFERENCES [dbo].[Website] ([WebsiteID])
);

