﻿CREATE TABLE [dbo].[Page] (
    [PageKey]         VARCHAR (50)   NOT NULL,
    [WebsiteKey]      VARCHAR (50)   NOT NULL,
    [LanguageKey]     VARCHAR (10)   NOT NULL,
    [Name]            NVARCHAR (250) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
    [SeoName]         NVARCHAR (250) NOT NULL,
    [MetaTitle]       NVARCHAR (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
    [MetaKeyword]     NVARCHAR (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
    [MetaDescription] NVARCHAR (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
    [ParentKey]       VARCHAR (50)   COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
    [LayoutKey]       VARCHAR (50)   NOT NULL,
    [SortOrder]       INT            CONSTRAINT [DF_Page_SortOrder] DEFAULT ((0)) NOT NULL,
    [IsBasePage]      BIT            CONSTRAINT [DF_Page_IsBasePage] DEFAULT ((1)) NOT NULL,
    [IsEnabled]       BIT            CONSTRAINT [DF_Page_IsEnabled] DEFAULT ((1)) NOT NULL,
    [IsDeleted]       BIT            CONSTRAINT [DF_Page_IsDeleted] DEFAULT ((0)) NOT NULL,
    [IsGroup]         BIT            CONSTRAINT [DF_Page_IsGroup] DEFAULT ((0)) NOT NULL,
    CONSTRAINT [PK_Page] PRIMARY KEY CLUSTERED ([PageKey] ASC, [WebsiteKey] ASC, [LanguageKey] ASC),
    CONSTRAINT [FK_Page_Layout] FOREIGN KEY ([LayoutKey], [WebsiteKey]) REFERENCES [dbo].[Layout] ([LayoutKey], [WebsiteKey])
);

