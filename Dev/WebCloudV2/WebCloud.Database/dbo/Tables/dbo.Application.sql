﻿CREATE TABLE [dbo].[Application] (
    [ApplicationId] VARCHAR (50)   NOT NULL,
    [Name]          NVARCHAR (200) NOT NULL,
    [Url]           NVARCHAR (200) NULL,
    [SecretKey]     VARCHAR (50)   NOT NULL,
    CONSTRAINT [PK_Application] PRIMARY KEY CLUSTERED ([ApplicationId] ASC)
);

