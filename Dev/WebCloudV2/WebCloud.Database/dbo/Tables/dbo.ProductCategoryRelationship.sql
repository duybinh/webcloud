﻿CREATE TABLE [dbo].[ProductCategoryRelationship] (
    [WebsiteID] VARCHAR (50) NOT NULL,
    [ParentID]  VARCHAR (50) NOT NULL,
    [ChildID]   VARCHAR (50) NOT NULL,
    [Level]     INT          CONSTRAINT [DF_ProductCategoryRelationship_Level] DEFAULT ((1)) NOT NULL,
    CONSTRAINT [PK_ProductCategoryRelationship] PRIMARY KEY CLUSTERED ([WebsiteID] ASC, [ParentID] ASC),
    CONSTRAINT [FK_ProductCategoryRelationship_ProductCategory] FOREIGN KEY ([WebsiteID], [ParentID]) REFERENCES [dbo].[ProductCategory] ([WebsiteID], [ProductCategoryID]) ON DELETE CASCADE ON UPDATE CASCADE
);

