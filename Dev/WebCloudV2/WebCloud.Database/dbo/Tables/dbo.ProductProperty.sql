﻿CREATE TABLE [dbo].[ProductProperty] (
    [WebsiteID]            VARCHAR (50)    NOT NULL,
    [ProductPropertyID]    VARCHAR (50)    NOT NULL,
    [DataType]             VARCHAR (50)    NOT NULL,
    [DefaultNumberValue]   DECIMAL (18, 6) NULL,
    [DefaultBooleanValue]  BIT             NULL,
    [DefaultDatetimeValue] DATETIME2 (7)   NULL,
    [AppliedCategoryIDs]   VARCHAR (250)   NULL,
    [ApplyToAllProduct]    BIT             CONSTRAINT [DF_ProductProperty_ApplyToAllProduct] DEFAULT ((0)) NOT NULL,
    [AllowAdvanceSearch]   BIT             CONSTRAINT [DF_ProductProperty_AllowAdvanceSearch] DEFAULT ((0)) NOT NULL,
    [AllowFilter]          BIT             CONSTRAINT [DF_ProductProperty_AllowFilter] DEFAULT ((0)) NOT NULL,
    [SortOrder]            INT             CONSTRAINT [DF_ProductProperty_SortOrder] DEFAULT ((0)) NOT NULL,
    [IsEnabled]            BIT             CONSTRAINT [DF_ProductProperty_IsEnabled] DEFAULT ((1)) NOT NULL,
    [AllLanguage]          BIT             CONSTRAINT [DF_ProductProperty_AllLanguage] DEFAULT ((1)) NOT NULL,
    CONSTRAINT [PK_ProductProperty] PRIMARY KEY CLUSTERED ([WebsiteID] ASC, [ProductPropertyID] ASC),
    CONSTRAINT [FK_ProductProperty_Website] FOREIGN KEY ([WebsiteID]) REFERENCES [dbo].[Website] ([WebsiteID])
);

