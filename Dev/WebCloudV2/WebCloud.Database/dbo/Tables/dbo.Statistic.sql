﻿CREATE TABLE [dbo].[Statistic] (
    [StatisticID]         BIGINT         IDENTITY (1, 1) NOT NULL,
    [ViewCount]           BIGINT         CONSTRAINT [DF_Statistic_ViewCount] DEFAULT ((0)) NOT NULL,
    [LikeCount]           BIGINT         CONSTRAINT [DF_Statistic_LikeCount] DEFAULT ((0)) NOT NULL,
    [DislikeCount]        BIGINT         CONSTRAINT [DF_Statistic_DislikeCount] DEFAULT ((0)) NOT NULL,
    [LastUpdatedDatetime] DATETIME2 (7)  NULL,
    [LikeUserIDs]         NVARCHAR (MAX) NULL,
    [DislikeUserIDs]      NVARCHAR (MAX) NULL,
    [ViewUserIDs]         NVARCHAR (MAX) NULL,
    [ViewUserIPs]         NVARCHAR (MAX) NULL,
    [ReferenceTable]      VARCHAR (100)  NULL,
    [ItemID]              VARCHAR (50)   NULL,
    [WebsiteID]           VARCHAR (50)   NULL,
    CONSTRAINT [PK_Statistic] PRIMARY KEY CLUSTERED ([StatisticID] ASC)
);

