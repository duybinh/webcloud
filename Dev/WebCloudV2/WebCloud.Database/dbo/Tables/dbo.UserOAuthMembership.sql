﻿CREATE TABLE [dbo].[UserOAuthMembership] (
    [ID]             BIGINT         IDENTITY (1, 1) NOT NULL,
    [UserID]         BIGINT         NOT NULL,
    [ProviderID]     INT            NOT NULL,
    [ProviderUserID] NVARCHAR (250) NOT NULL,
    CONSTRAINT [PK_UserOAuthMembership] PRIMARY KEY CLUSTERED ([ID] ASC),
    CONSTRAINT [FK_UserOAuthMembership_OAuthProvider] FOREIGN KEY ([ProviderID]) REFERENCES [dbo].[OAuthProvider] ([ID]) ON DELETE CASCADE ON UPDATE CASCADE,
    CONSTRAINT [FK_UserOAuthMembership_User] FOREIGN KEY ([UserID]) REFERENCES [dbo].[User] ([UserID]) ON DELETE CASCADE ON UPDATE CASCADE
);

