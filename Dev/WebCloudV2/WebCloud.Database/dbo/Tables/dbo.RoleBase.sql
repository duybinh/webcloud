﻿CREATE TABLE [dbo].[RoleBase] (
    [ID]          INT            NOT NULL,
    [Name]        NVARCHAR (250) NOT NULL,
    [Description] NVARCHAR (500) NOT NULL,
    [SortOrder]   INT            CONSTRAINT [DF_RoleBase_SortOrder] DEFAULT ((0)) NOT NULL,
    [IsEnabled]   BIT            CONSTRAINT [DF_RoleBase_IsEnabled] DEFAULT ((1)) NOT NULL,
    CONSTRAINT [PK_RoleBase] PRIMARY KEY CLUSTERED ([ID] ASC)
);

