﻿CREATE TABLE [dbo].[MenuItem] (
    [WebsiteID]    VARCHAR (50)   NOT NULL,
    [MenuItemID]   VARCHAR (50)   NOT NULL,
    [ParentID]     VARCHAR (50)   NULL,
    [Icon]         NVARCHAR (200) NULL,
    [TargetUrl]    NVARCHAR (300) NULL,
    [OpenMode]     INT            CONSTRAINT [DF_MenuItem_OpenMode] DEFAULT ((0)) NOT NULL,
    [SortOrder]    INT            CONSTRAINT [DF_MenuItem_SortOrder] DEFAULT ((0)) NOT NULL,
    [IsEnabled]    BIT            CONSTRAINT [DF_MenuItem_IsEnabled] DEFAULT ((1)) NOT NULL,
    [SpecialClass] VARCHAR (50)   NULL,
    [IsDeleted]    BIT            CONSTRAINT [DF_MenuItem_IsDeleted] DEFAULT ((0)) NOT NULL,
    CONSTRAINT [PK_MenuItem] PRIMARY KEY CLUSTERED ([WebsiteID] ASC, [MenuItemID] ASC),
    CONSTRAINT [FK_MenuItem_Website] FOREIGN KEY ([WebsiteID]) REFERENCES [dbo].[Website] ([WebsiteID])
);

