﻿CREATE TABLE [dbo].[TextResourceInWebsite] (
    [WebsiteID]      VARCHAR (50)   NOT NULL,
    [LanguageID]     VARCHAR (10)   NOT NULL,
    [TextResourceID] VARCHAR (50)   NOT NULL,
    [Value]          NVARCHAR (MAX) NULL,
    CONSTRAINT [PK_TextResourceInWebsite] PRIMARY KEY CLUSTERED ([WebsiteID] ASC, [LanguageID] ASC, [TextResourceID] ASC),
    CONSTRAINT [FK_TextResourceInWebsite_TextResourceInLanguage] FOREIGN KEY ([LanguageID], [TextResourceID]) REFERENCES [dbo].[TextResourceInLanguage] ([LanguageID], [TextResourceID]) ON DELETE CASCADE ON UPDATE CASCADE,
    CONSTRAINT [FK_TextResourceInWebsite_Website] FOREIGN KEY ([WebsiteID]) REFERENCES [dbo].[Website] ([WebsiteID]) ON DELETE CASCADE ON UPDATE CASCADE
);

