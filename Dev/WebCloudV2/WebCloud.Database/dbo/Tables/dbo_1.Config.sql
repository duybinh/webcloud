﻿CREATE TABLE [dbo].[Config] (
    [ConfigID]       VARCHAR (50)   NOT NULL,
    [Name]           NVARCHAR (250) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
    [Description]    NVARCHAR (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
    [DataType]       VARCHAR (20)   COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
    [DefaultValue]   NVARCHAR (MAX) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
    [SortOrder]      INT            NULL,
    [Group]          NVARCHAR (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
    [IsGlobalConfig] BIT            CONSTRAINT [DF_WebConfig_IsGlobalConfig] DEFAULT ((1)) NOT NULL,
    [AllowDelete]    BIT            CONSTRAINT [DF_WebConfig_AllowDelete] DEFAULT ((0)) NOT NULL,
    [AllowEdit]      BIT            CONSTRAINT [DF_WebConfig_AllowEdit] DEFAULT ((1)) NOT NULL,
    [RolesToView]    VARCHAR (MAX)  COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
    [RolesToEdit]    VARCHAR (MAX)  COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
    [EditingControl] VARCHAR (MAX)  NULL,
    CONSTRAINT [PK_Config] PRIMARY KEY CLUSTERED ([ConfigID] ASC)
);

