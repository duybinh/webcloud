﻿-- =============================================
-- Author:		BinhND
-- Create date: 17/05/2014
-- Description:	Get menu items
-- =============================================   
      
-------------------------------------------DECLARATIONS HERE-----------------------------------  
CREATE PROCEDURE [dbo].[ArticleCategory_Insert]
    (	@WebsiteID varchar(50),
		@ArticleCategoryID varchar(50),
		@LanguageID varchar(10),
		@Name nvarchar(250),
		@SeoName nvarchar(250),
		@Description nvarchar(500),
		@MetaTitle nvarchar(250),
		@MetaKeyword nvarchar(250),
		@MetaDescription nvarchar(500),
		@ParentID varchar(50),
		@ThumbImage  nvarchar(250),
		@DisplayArticleThumb bit,
		@IsHot bit,
		@AllowComment bit,
		@IsEnabled bit,
		@SortOrder int,
		@CreatedUserName varchar(50),
		@LastEditedUserName varchar(50),
		@CreatedUserID bigint,
		@LastEditedUserID bigint
     )
AS 
    SET QUOTED_IDENTIFIER ON	-- IDENTIFIERS IS DELIMITED BY DOUBLE QUOTATION MARKS, LITERALS MUST BE DELIMITED BY SINGLE QUOTATION MARKS
    SET NOCOUNT ON				-- DO NOT DISPLAY OUTPUTS TO SCREEN
    SET ANSI_NULLS ON			-- FOR HETROGENOUS DATA SOURCE
    SET ANSI_WARNINGS ON		-- TURNS NULL WARNINGS ETC ON
    SET XACT_ABORT ON			-- ENTIRE DISTRIBUTED TRANSACTION ROLLED BACK
	
	-------------------------------------MAIN STORED PROCEDURE BODY HERE-----------------------
    BEGIN TRY
        SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
        BEGIN TRAN
		
        INSERT  INTO dbo.ArticleCategory
                ( WebsiteID ,
                  ArticleCategoryID ,
                  ParentID ,
                  ThumbImage ,
                  DisplayArticleThumb ,
                  IsHot ,
                  AllowComment ,
                  IsEnabled ,
                  SortOrder ,
				  CreatedDatetime ,
				  LastEditedDatetime,                  
                  CreatedUserName ,
                  LastEditedUserName ,
                  CreatedUserID ,
                  LastEditedUserID
                )
        VALUES  ( @WebsiteID , -- WebsiteID - varchar(50)
                  @ArticleCategoryID , -- ArticleCategoryID - varchar(50)
                  @ParentID , -- ParentID - varchar(50)
                  @ThumbImage , -- ThumbImage - nvarchar(250)
                  @DisplayArticleThumb , -- DisplayArticleThumb - bit
                  @IsHot , -- IsHot - bit
                  @AllowComment , -- AllowComment - bit
                  @IsEnabled , -- IsEnabled - bit
                  @SortOrder , -- SortOrder - int
				  GETDATE() , -- CreatedDatetime - datetime2
				  GETDATE() , -- LastEditedDatetime - datetime2
                  @CreatedUserName , -- CreatedUserName - varchar(50)
                  @LastEditedUserName , -- LastEditedUserName - varchar(50)
                  @CreatedUserID , -- CreatedUserID - bigint
                  @LastEditedUserID  -- LastEditedUserID - bigint
                )
        
		INSERT INTO dbo.ArticleCategoryContent
		        ( WebsiteID ,
		          ArticleCategoryID ,
		          LanguageID ,
		          Name ,
		          SeoName ,
		          Description ,
		          MetaTitle ,
		          MetaKeyword ,
		          MetaDescription
		        )
		VALUES  ( @WebsiteID , -- WebsiteID - varchar(50)
		          @ArticleCategoryID , -- ArticleCategoryID - varchar(50)
		          @LanguageID , -- LanguageID - varchar(10)
		          @Name , -- Name - nvarchar(250)
		          @SeoName , -- SeoName - nvarchar(250)
		          @Description , -- Description - nvarchar(500)
		          @MetaTitle , -- MetaTitle - nvarchar(250)
		          @MetaKeyword , -- MetaKeyword - nvarchar(500)
		          @MetaDescription  -- MetaDescription - nvarchar(500)
		        )

		IF @ParentID IS NOT NULL
		BEGIN
			EXEC dbo.ArticleCategoryRelationship_Add @WebsiteID, @ParentID, @ArticleCategoryID
		END 
		      

        IF @@ERROR <> 0 
            ROLLBACK TRAN  
        COMMIT TRAN  
    END TRY
    BEGIN CATCH
        IF @@TRANCOUNT > 0 
            ROLLBACK TRAN  
        EXEC RethrowError
    END CATCH
	
	
	------------------------------------------- CLOSE SET STATEMENTS ---------------------------
    SET NOCOUNT OFF
    SET ANSI_NULLS OFF
    SET ANSI_WARNINGS OFF
    SET XACT_ABORT OFF
    SET QUOTED_IDENTIFIER OFF
