﻿-- =============================================
-- Author:		BinhND
-- Create date: 17/05/2014
-- Description:	Get menu items
-- =============================================
CREATE PROCEDURE [dbo].[ArticleCategory_GetAll]
    @WebsiteId VARCHAR(50) ,
    @LanguageId VARCHAR(10) ,
    @DefaultLanguageId VARCHAR(10) ,
    @IsDeleted BIT = 0
AS 
    BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
        SET NOCOUNT ON;

    -- Procedure body
        IF ( @LanguageId = @DefaultLanguageId
             OR @DefaultLanguageId IS NULL
           ) 
            BEGIN	
                SELECT  t.* ,
                        c.LanguageID ,
                        c.Name ,
						c.SeoName ,
						c.Description ,
						c.MetaTitle ,
						c.MetaKeyword ,
						c.MetaDescription
                FROM    ( SELECT    *
                          FROM      dbo.ArticleCategory
                          WHERE     WebsiteID = @WebsiteId AND IsDeleted = @IsDeleted						  
                        ) AS t
                        JOIN dbo.ArticleCategoryContent AS c ON ( t.WebsiteID = c.WebsiteID
                                                              AND t.ArticleCategoryID = c.ArticleCategoryID
                                                              AND c.LanguageID = @LanguageId
                                                              )
						ORDER BY t.GlobalID
		
            END
        ELSE 
            BEGIN	
                SELECT  t.* ,
                        c.ArticleCategoryID AS Flag ,
                        c2.LanguageID ,
                        c2.Name ,
						c2.SeoName ,
						c2.Description ,
						c2.MetaTitle ,
						c2.MetaKeyword ,
						c2.MetaDescription
                FROM    ( SELECT    *
                          FROM      dbo.ArticleCategory
                          WHERE     WebsiteID = @WebsiteId AND IsDeleted = @IsDeleted						  
                        ) AS t
                        LEFT JOIN dbo.ArticleCategoryContent AS c ON ( t.WebsiteID = c.WebsiteID
                                                              AND t.ArticleCategoryID = c.ArticleCategoryID
                                                              AND c.LanguageID = @LanguageId
                                                              )
                        LEFT JOIN dbo.ArticleCategoryContent AS c2 ON ( t.WebsiteID = c2.WebsiteID
                                                              AND t.ArticleCategoryID = c2.ArticleCategoryID
                                                              AND ( c2.LanguageID = @LanguageId
                                                              OR ( c.ArticleCategoryID IS NULL
                                                              AND c2.LanguageID = @DefaultLanguageId
                                                              )
                                                              )
                                                              ) 
						ORDER BY t.GlobalID
            END
    END
