﻿-- =============================================
-- Author:		BinhND
-- Create date: 21/05/2014
-- Description:	RethrowError
-- =============================================
          
-------------------------------------------Declarations here-----------------------------------    
CREATE PROCEDURE dbo.RethrowError  
AS 
    SET QUOTED_IDENTIFIER ON	-- IDENTIFIERS IS DELIMITED BY DOUBLE QUOTATION MARKS, LITERALS MUST BE DELIMITED BY SINGLE QUOTATION MARKS
    SET NOCOUNT ON				-- DO NOT DISPLAY OUTPUTS TO SCREEN
    SET ANSI_NULLS ON			-- FOR HETROGENOUS DATA SOURCE
    SET ANSI_WARNINGS ON		-- TURNS NULL WARNINGS ETC ON
    SET XACT_ABORT ON			-- ENTIRE DISTRIBUTED TRANSACTION ROLLED BACK
;
-------------------------------------------MAIN STORED PROCEDURE BODY HERE-------------------- 
    IF ERROR_NUMBER() IS NULL 
        RETURN ;
    
    DECLARE @ErrorMessage NVARCHAR(4000) ,
        @ErrorNumber INT ,
        @ErrorSeverity INT ,
        @ErrorState INT ,
        @ErrorLine INT ,
        @ErrorProcedure NVARCHAR(200)
		
    SELECT  @ErrorNumber = ERROR_NUMBER() ,
            @ErrorSeverity = ERROR_SEVERITY() ,
            @ErrorState = ERROR_STATE() ,
            @ErrorLine = ERROR_LINE() ,
            @ErrorProcedure = ISNULL(ERROR_PROCEDURE(), '-') ;

-- Building the message string that will contain original
-- error information.
    SELECT  @ErrorMessage = N'Error %d, Level %d, State %d, Procedure %s, Line %d, '
            + 'Message: ' + ERROR_MESSAGE() ;

-- Raise an error: msg_str parameter of RAISERROR will contain
-- the original error information.
    RAISERROR ( @ErrorMessage, @ErrorSeverity, 1, @ErrorNumber, -- parameter: original error number.
    @ErrorSeverity, -- parameter: original error severity.
    @ErrorState, -- parameter: original error state.
    @ErrorProcedure, -- parameter: original error procedure name.
    @ErrorLine -- parameter: original error line number.
) ;

-------------------------------------------CLOSE SET STATEMEMTS HERE---------------------------  
    SET NOCOUNT OFF  
    SET ANSI_NULLS OFF  
    SET ANSI_WARNINGS OFF  
    SET XACT_ABORT OFF  
    SET QUOTED_IdENTIFIER OFF