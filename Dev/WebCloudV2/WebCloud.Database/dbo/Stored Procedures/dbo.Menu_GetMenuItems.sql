﻿-- =============================================
-- Author:		BinhND
-- Create date: 24/11/2013
-- Description:	Get menu items
-- =============================================
CREATE PROCEDURE [dbo].[Menu_GetMenuItems]
    @WebsiteId VARCHAR(50) ,
    @LanguageId VARCHAR(10) ,
    @DefaultLanguageId VARCHAR(10) ,
    @IsDeleted BIT = 0
AS 
    BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
        SET NOCOUNT ON;

    -- Procedure body
        IF ( @LanguageId = @DefaultLanguageId
             OR @DefaultLanguageId IS NULL
           ) 
            BEGIN	
                SELECT  mi.* ,
                        mic.LanguageID ,
                        mic.Title ,
                        mic.Content
                FROM    ( SELECT    *
                          FROM      dbo.MenuItem
                          WHERE     WebsiteID = @WebsiteId AND IsDeleted = @IsDeleted
                        ) AS mi
                        JOIN dbo.MenuItemContent AS mic ON ( mi.WebsiteID = mic.WebsiteID
                                                              AND mi.MenuItemID = mic.MenuItemID
                                                              AND mic.LanguageID = @LanguageId
                                                              )
		
            END
        ELSE 
            BEGIN	
                SELECT  mi.* ,
                        mic.MenuItemID AS Flag ,
                        mic2.LanguageID ,
                        mic2.Title ,
                        mic2.Content
                FROM    ( SELECT    *
                          FROM      dbo.MenuItem
                          WHERE     WebsiteID = @WebsiteId AND IsDeleted = @IsDeleted
                        ) AS mi
                        LEFT JOIN dbo.MenuItemContent AS mic ON ( mi.WebsiteID = mic.WebsiteID
                                                              AND mi.MenuItemID = mic.MenuItemID
                                                              AND mic.LanguageID = @LanguageId
                                                              )
                        LEFT JOIN dbo.MenuItemContent AS mic2 ON ( mi.WebsiteID = mic2.WebsiteID
                                                              AND mi.MenuItemID = mic2.MenuItemID
                                                              AND ( mic2.LanguageID = @LanguageId
                                                              OR ( mic.MenuItemID IS NULL
                                                              AND mic2.LanguageID = @DefaultLanguageId
                                                              )
                                                              )
                                                              ) 
            END
    END
