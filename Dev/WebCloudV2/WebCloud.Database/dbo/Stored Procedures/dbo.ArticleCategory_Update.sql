﻿-- =============================================
-- Author:		BinhND
-- Create date: 17/05/2014
-- Description:	Get menu items
-- =============================================   
      
-------------------------------------------DECLARATIONS HERE-----------------------------------  
CREATE PROCEDURE [dbo].[ArticleCategory_Update]
    (	@WebsiteID varchar(50),
		@ArticleCategoryID varchar(50),
		@LanguageID varchar(10),
		@Name nvarchar(250),
		@SeoName nvarchar(250),
		@Description nvarchar(500),
		@MetaTitle nvarchar(250),
		@MetaKeyword nvarchar(250),
		@MetaDescription nvarchar(500),
		@ParentID varchar(50),
		@ThumbImage  nvarchar(250),
		@DisplayArticleThumb bit,
		@IsHot bit,
		@AllowComment bit,
		@IsEnabled bit,
		@SortOrder int,
		@LastEditedUserName varchar(50),
		@LastEditedUserID bigint
     )
AS 
    SET QUOTED_IDENTIFIER ON	-- IDENTIFIERS IS DELIMITED BY DOUBLE QUOTATION MARKS, LITERALS MUST BE DELIMITED BY SINGLE QUOTATION MARKS
    SET NOCOUNT ON				-- DO NOT DISPLAY OUTPUTS TO SCREEN
    SET ANSI_NULLS ON			-- FOR HETROGENOUS DATA SOURCE
    SET ANSI_WARNINGS ON		-- TURNS NULL WARNINGS ETC ON
    SET XACT_ABORT ON			-- ENTIRE DISTRIBUTED TRANSACTION ROLLED BACK
	
	-------------------------------------MAIN STORED PROCEDURE BODY HERE-----------------------
    BEGIN TRY
        SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
        BEGIN TRAN
		
		DECLARE @OldParentId varchar(50)

		SELECT	@OldParentId = ParentID
		FROM	dbo.ArticleCategory
		WHERE	WebsiteID = @WebsiteID AND ArticleCategoryID = @ArticleCategoryID

		UPDATE dbo.ArticleCategory SET 
				 ParentID				=  @ParentID					,
				 ThumbImage				=  @ThumbImage					,
				 DisplayArticleThumb	=  @DisplayArticleThumb			,
				 IsHot					=  @IsHot						,
				 AllowComment			=  @AllowComment				,
				 IsEnabled				=  @IsEnabled					,
				 SortOrder				=  @SortOrder					,				 
				 LastEditedDatetime		=  GETDATE()					,                  
				 LastEditedUserName		=  @LastEditedUserName			,
				 LastEditedUserID		=  @LastEditedUserID
        WHERE    WebsiteID = @WebsiteID AND ArticleCategoryID = @ArticleCategoryID
        
		UPDATE dbo.ArticleCategoryContent SET
				Name					= @Name							,
				SeoName					= @SeoName						,
				Description				= @Description					,
				MetaTitle				= @MetaTitle					,
				MetaKeyword				= @MetaKeyword					,
				MetaDescription			= @MetaDescription
		WHERE   WebsiteID = @WebsiteID AND ArticleCategoryID = @ArticleCategoryID AND LanguageID = @LanguageID      
		

		IF @OldParentId <> @ParentID
		BEGIN
        
			IF @OldParentId IS NOT NULL
			BEGIN
				EXEC dbo.ArticleCategoryRelationship_Move @WebsiteID, @OldParentId, @ArticleCategoryID				 
			END	
			      
			IF @ParentID IS NOT NULL
			BEGIN
				EXEC dbo.ArticleCategoryRelationship_Add @WebsiteID, @ParentID, @ArticleCategoryID				 
			END	
	
		END 
		      

        IF @@ERROR <> 0 
            ROLLBACK TRAN  
        COMMIT TRAN  
    END TRY
    BEGIN CATCH
        IF @@TRANCOUNT > 0 
            ROLLBACK TRAN  
        EXEC RethrowError
    END CATCH
	
	
	------------------------------------------- CLOSE SET STATEMENTS ---------------------------
    SET NOCOUNT OFF
    SET ANSI_NULLS OFF
    SET ANSI_WARNINGS OFF
    SET XACT_ABORT OFF
    SET QUOTED_IDENTIFIER OFF
