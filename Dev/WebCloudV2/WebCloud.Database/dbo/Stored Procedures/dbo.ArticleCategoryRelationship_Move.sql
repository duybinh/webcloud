﻿-- =============================================
-- Author:		BinhND
-- Create date: 17/05/2014
-- Description:	ArticleCategoryRelationship Move
-- =============================================   
      
-------------------------------------------DECLARATIONS HERE-----------------------------------  
CREATE PROCEDURE [dbo].[ArticleCategoryRelationship_Move]
    (	@WebsiteID varchar(50),
		@ParentID varchar(50),
		@ChildID varchar(50)		
     )
AS 
    SET QUOTED_IDENTIFIER ON	-- IDENTIFIERS IS DELIMITED BY DOUBLE QUOTATION MARKS, LITERALS MUST BE DELIMITED BY SINGLE QUOTATION MARKS
    SET NOCOUNT ON				-- DO NOT DISPLAY OUTPUTS TO SCREEN
    SET ANSI_NULLS ON			-- FOR HETROGENOUS DATA SOURCE
    SET ANSI_WARNINGS ON		-- TURNS NULL WARNINGS ETC ON
    SET XACT_ABORT ON			-- ENTIRE DISTRIBUTED TRANSACTION ROLLED BACK
	
	-------------------------------------MAIN STORED PROCEDURE BODY HERE-----------------------
    BEGIN TRY
        SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
        BEGIN TRAN
		
		DELETE	dbo.ArticleCategoryRelationship
		WHERE	WebsiteID = @WebsiteID
			AND (ParentID = @ParentID OR ParentID IN (SELECT acr1.ParentID FROM dbo.ArticleCategoryRelationship AS acr1 WHERE acr1.ChildID = @ParentID))
			AND	(ChildID = @ChildID OR ChildID IN (SELECT acr2.ChildID FROM dbo.ArticleCategoryRelationship AS acr2 WHERE acr2.ParentID = @ChildID))

        IF @@ERROR <> 0 
            ROLLBACK TRAN  
        COMMIT TRAN  
    END TRY
    BEGIN CATCH
        IF @@TRANCOUNT > 0 
            ROLLBACK TRAN  
        EXEC RethrowError
    END CATCH
	
	
	------------------------------------------- CLOSE SET STATEMENTS ---------------------------
    SET NOCOUNT OFF
    SET ANSI_NULLS OFF
    SET ANSI_WARNINGS OFF
    SET XACT_ABORT OFF
    SET QUOTED_IDENTIFIER OFF
