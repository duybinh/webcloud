﻿-- =============================================
-- Author:		BinhND
-- Create date: 22/05/2014
-- Description:	ArticleCategoryRelationship Add
-- =============================================   
      
-------------------------------------------DECLARATIONS HERE-----------------------------------  
CREATE PROCEDURE [dbo].[ArticleCategoryRelationship_Add]
    (	@WebsiteID varchar(50),
		@ParentID varchar(50),
		@ChildID varchar(50)		
     )
AS 
    SET QUOTED_IDENTIFIER ON	-- IDENTIFIERS IS DELIMITED BY DOUBLE QUOTATION MARKS, LITERALS MUST BE DELIMITED BY SINGLE QUOTATION MARKS
    SET NOCOUNT ON				-- DO NOT DISPLAY OUTPUTS TO SCREEN
    SET ANSI_NULLS ON			-- FOR HETROGENOUS DATA SOURCE
    SET ANSI_WARNINGS ON		-- TURNS NULL WARNINGS ETC ON
    SET XACT_ABORT ON			-- ENTIRE DISTRIBUTED TRANSACTION ROLLED BACK
	
	-------------------------------------MAIN STORED PROCEDURE BODY HERE-----------------------
    BEGIN TRY
        SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
        BEGIN TRAN
		
			INSERT INTO dbo.ArticleCategoryRelationship
					( WebsiteID, ParentID, ChildID, Level )
			VALUES  ( @WebsiteID, -- WebsiteID - varchar(50)
					  @ParentID, -- ParentID - varchar(50)
					  @ChildID, -- ChildID - varchar(50)
					  1  -- Level - int
					  )
			

			INSERT INTO dbo.ArticleCategoryRelationship
			        ( WebsiteID, ParentID, ChildID, Level )
			SELECT acr.WebsiteID,
				   acr.ParentID,
				   @ChildID AS ChildID,
				   (acr.Level + 1) AS Level					
			FROM dbo.ArticleCategoryRelationship AS acr
			WHERE (acr.WebsiteID = @WebsiteID AND acr.ChildID = @ParentID)


			INSERT INTO dbo.ArticleCategoryRelationship
			        ( WebsiteID, ParentID, ChildID, Level )
			SELECT acr1.WebsiteID, acr1.ParentID, acr2.ChildID, (acr1.Level + acr2.Level) AS Level
			FROM	dbo.ArticleCategoryRelationship AS acr1,
					dbo.ArticleCategoryRelationship AS acr2
			WHERE (acr1.WebsiteID = @WebsiteID AND acr1.ChildID = @ChildID) 
			AND   (acr2.WebsiteID = @WebsiteID AND acr2.ParentID = @ChildID)
		      

        IF @@ERROR <> 0 
            ROLLBACK TRAN  
        COMMIT TRAN  
    END TRY
    BEGIN CATCH
        IF @@TRANCOUNT > 0 
            ROLLBACK TRAN  
        EXEC RethrowError
    END CATCH
	
	
	------------------------------------------- CLOSE SET STATEMENTS ---------------------------
    SET NOCOUNT OFF
    SET ANSI_NULLS OFF
    SET ANSI_WARNINGS OFF
    SET XACT_ABORT OFF
    SET QUOTED_IDENTIFIER OFF
