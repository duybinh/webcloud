﻿using System.Linq;
using System.Net.Http.Formatting;
using System.Web.Http;
using Microsoft.Practices.Unity;
using Newtonsoft.Json.Serialization;
using WebCloud.Api.Business;
using WebCloud.Api.DataAccess;
using WebCloud.Api.Infrastructure;

namespace WebCloud.Api
{
    public static class WebApiConfig
    {
        public static void Register(HttpConfiguration config)
        {
            // Web API routes
            config.MapHttpAttributeRoutes();

            //// Json formart
            //var jsonFormatter = config.Formatters.OfType<JsonMediaTypeFormatter>().First();
            //jsonFormatter.SerializerSettings.ContractResolver = new CamelCasePropertyNamesContractResolver();

            // Dependency injection config
            var container = new UnityContainer();
            container.RegisterType<ICacheRepository, CacheRepository>(new HierarchicalLifetimeManager());
            container.RegisterType<IContextRepository, ContextRepository>(new HierarchicalLifetimeManager());

            container.RegisterType<IConfigurationRepository, ConfigurationRepository>(new HierarchicalLifetimeManager());
            container.RegisterType<IConfigurationDataRepository, ConfigurationDataRepository>(new HierarchicalLifetimeManager());

            container.RegisterType<IUserRepository, UserRepository>(new HierarchicalLifetimeManager());
            container.RegisterType<IUserDataRepository, UserDataRepository>(new HierarchicalLifetimeManager());

            container.RegisterType<IApplicationRepository, ApplicationRepository>(new HierarchicalLifetimeManager());
            container.RegisterType<IApplicationDataRepository, ApplicationDataRepository>(new HierarchicalLifetimeManager());

            container.RegisterType<IResourceDataRepository, ResourceDataRepository>(new HierarchicalLifetimeManager());
            container.RegisterType<IResourceRepository, ResourceRepository>(new HierarchicalLifetimeManager());

            container.RegisterType<IRoleDataRepository, RoleDataRepository>(new HierarchicalLifetimeManager());
            container.RegisterType<IRoleRepository, RoleRepository>(new HierarchicalLifetimeManager());

            container.RegisterType<ILanguageDataRepository, LanguageDataRepository>(new HierarchicalLifetimeManager());
            container.RegisterType<ILanguageRepository, LanguageRepository>(new HierarchicalLifetimeManager());

            container.RegisterType<IArticleCategoryDataRepository, ArticleCategoryDataRepository>(new HierarchicalLifetimeManager());
            container.RegisterType<IArticleCategoryRepository, ArticleCategoryRepository>(new HierarchicalLifetimeManager());

            container.RegisterType<ITagDataRepository, TagDataRepository>(new HierarchicalLifetimeManager());
            container.RegisterType<ITagRepository, TagRepository>(new HierarchicalLifetimeManager());

            container.RegisterType<ILayoutDataRepository, LayoutDataRepository>(new HierarchicalLifetimeManager());
            container.RegisterType<ILayoutRepository, LayoutRepository>(new HierarchicalLifetimeManager());

            container.RegisterType<IThemeDataRepository, ThemeDataRepository>(new HierarchicalLifetimeManager());
            container.RegisterType<IThemeRepository, ThemeRepository>(new HierarchicalLifetimeManager());

            container.RegisterType<ITextDataRepository, TextDataRepository>(new HierarchicalLifetimeManager());

            container.RegisterType<IPageDataRepository, PageDataRepository>(new HierarchicalLifetimeManager());
            container.RegisterType<IPageRepository, PageRepository>(new HierarchicalLifetimeManager());

            container.RegisterType<IModuleBaseDataRepository, ModuleBaseDataRepository>(new HierarchicalLifetimeManager());
            container.RegisterType<IModuleBaseRepository, ModuleBaseRepository>(new HierarchicalLifetimeManager());

            container.RegisterType<IModuleDataRepository, ModuleDataRepository>(new HierarchicalLifetimeManager());
            container.RegisterType<IModuleRepository, ModuleRepository>(new HierarchicalLifetimeManager());

            config.DependencyResolver = new WebCloudDependencyResolver(container);
        }
    }
}
