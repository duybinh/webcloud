﻿using System.Collections.Generic;
using System.Web.Http;
using WebCloud.Api.Core.Helper;
using WebCloud.Api.Infrastructure;
using WebCloud.Api.Infrastructure.Filters;
using WebCloud.Api.Core.Models;
using WebCloud.Api.Business;
using WebCloud.Common;
using WebCloud.Common.Constant;
using WebCloud.Entity;

namespace WebCloud.Api.Controllers
{
    public class ResourceController : BaseApiController
    {
        private readonly IResourceRepository _resourceRepository;

        public ResourceController(IResourceRepository resourceRepository)
        {
            _resourceRepository = resourceRepository;
            _resourceRepository.Context = this.ApiContext;
        }


        [Route(ApiHelper.ResourceGetAllUrl)]
        [ApiAuthorize(Roles = RoleValue.Admin)]
        [HttpGet]
        public ResultCollection<Resource> GetAllResources()
        {
            var result = _resourceRepository.GetResources();
            return result;
        }

        [Route(ApiHelper.ResourceGetAllUrlInLang)]
        [HttpGet]
        [HttpPost]
        public ResultCollection<Resource> GetAllResourcesInLang(string id)
        {
            var result = _resourceRepository.GetResourcesInLang(id);
            return result;
        }

        [Route(ApiHelper.ResourceGetByIdUrl)]
        [ApiAuthorize(Roles = RoleValue.Admin)]
        [HttpGet]
        public ResultObject<Resource> GetResourceById(string id)
        {
            var result = _resourceRepository.GetResourceById(id);
            return result;
        }

        [Route(ApiHelper.ResourceAddUrl)]
        [ApiAuthorize(Roles = RoleValue.Admin)]
        [HttpPost]
        public ResultBase AddResource(Resource obj)
        {
            var result = _resourceRepository.AddResource(obj);
            return result;
        }

        [Route(ApiHelper.ResourceEditUrl)]
        [ApiAuthorize(Roles = RoleValue.Admin)]
        [HttpPost]
        public ResultBase EditResource(Resource obj)
        {
            var result = _resourceRepository.EditResource(obj);
            return result;
        }

        [Route(ApiHelper.ResourceUpdateUrl)]
        [ApiAuthorize(Roles = RoleValue.Admin)]
        [HttpPost]
        public ResultBase UpdateResource(ResourceUpdateModel obj)
        {
            var result = _resourceRepository.UpdateResourceValue(obj.ResourceId, obj.LanguageId, obj.Value);
            return result;
        }

        [Route(ApiHelper.ResourceDeleteUrl)]
        [ApiAuthorize(Roles = RoleValue.Admin)]
        [HttpGet]
        [HttpDelete]
        public ResultBase DeleteResource(string id)
        {
            var result = _resourceRepository.DeleteResource(id);
            return result;
        }

        [Route(ApiHelper.ResourceGetForSiteUrl)]
        [HttpGet]
        public ResultObject<Dictionary<string, LocalizationString>> GetForSite()
        {
            var result = _resourceRepository.GetForSite();
            return result;
        }
    }
}