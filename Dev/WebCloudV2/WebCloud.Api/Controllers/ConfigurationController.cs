﻿using System.Collections.Generic;
using System.Web.Http;
using WebCloud.Api.Core.Helper;
using WebCloud.Api.Infrastructure;
using WebCloud.Api.Infrastructure.Filters;
using WebCloud.Api.Core.Models;
using WebCloud.Api.Business;
using WebCloud.Common;
using WebCloud.Common.Constant;
using WebCloud.Entity;

namespace WebCloud.Api.Controllers
{
    public class ConfigurationController : BaseApiController
    {
        private readonly ConfigurationRepository _configurationRepository;

        public ConfigurationController(ConfigurationRepository configurationRepository)
        {
            _configurationRepository = configurationRepository;
            _configurationRepository.Context = this.ApiContext;
        }

        #region Management Api
        [Route(ApiHelper.ConfigGetAllUrl)]
        [ApiAuthorize(Roles = RoleValue.SetupAdmin)]
        [HttpGet]
        public ResultCollection<Config> GetAll()
        {
            var result = _configurationRepository.GetAll();
            return result;
        }

        [Route(ApiHelper.ConfigGetByIdUrl)]
        [ApiAuthorize(Roles = RoleValue.SetupAdmin)]
        [HttpGet]
        public ResultObject<Config> GetById(string id)
        {
            var result = _configurationRepository.GetById(id);
            return result;
        }

        [Route(ApiHelper.ConfigAddUrl)]
        [ApiAuthorize(Roles = RoleValue.SetupAdmin)]
        [HttpPost]
        public ResultBase Add(Config configuration)
        {
            var result = _configurationRepository.Add(configuration);
            return result;
        }

        [Route(ApiHelper.ConfigEditUrl)]
        [ApiAuthorize(Roles = RoleValue.SetupAdmin)]
        [HttpPost]
        public ResultBase Edit(Config configuration)
        {
            var result = _configurationRepository.Edit(configuration);
            return result;
        }

        [Route(ApiHelper.ConfigDeleteUrl)]
        [ApiAuthorize(Roles = RoleValue.SetupAdmin)]
        [HttpGet]
        [HttpDelete]
        public ResultBase Delete(string id)
        {
            var result = _configurationRepository.Delete(id);
            return result;
        }
        #endregion

        [Route(ApiHelper.ConfigGetForSiteUrl)]
        [HttpGet]
        public ResultObject<Dictionary<string, object>> GetForSite()
        {
            var result = _configurationRepository.GetForSite();
            return result;
        }

        [Route(ApiHelper.ConfigUpdateUrl)]
        [ApiAuthorize(Roles = RoleValue.Admin)]
        [HttpPost]
        public ResultBase Update(KeyValueModel obj)
        {
            var result = _configurationRepository.Update(obj.Key, obj.Value);
            return result;
        }

    }
}