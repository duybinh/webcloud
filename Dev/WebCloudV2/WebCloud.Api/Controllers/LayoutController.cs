﻿using System.Web.Http;
using WebCloud.Api.Business;
using WebCloud.Api.Core.Helper;
using WebCloud.Api.Infrastructure;
using WebCloud.Api.Infrastructure.Filters;
using WebCloud.Common;
using WebCloud.Common.Constant;
using WebCloud.Entity;

namespace WebCloud.Api.Controllers
{
    public class LayoutController : BaseApiController
    {
        private readonly ILayoutRepository _layoutRepository;

        public LayoutController(ILayoutRepository layoutRepository)
        {
            _layoutRepository = layoutRepository;
            _layoutRepository.Context = this.ApiContext;
        }

        #region Management
        [Route(ApiHelper.LayoutUrl)]
        [ApiAuthorize(Roles = RoleValue.Admin)]
        [HttpGet]
        public ResultCollection<Layout> GetAll()
        {
            var result = _layoutRepository.Query();
            return result;
        }

        [Route(ApiHelper.LayoutByTypeUrl)]
        [ApiAuthorize(Roles = RoleValue.Admin)]
        [HttpGet]
        public ResultCollection<Layout> GetByType(int id)
        {
            var result = _layoutRepository.Query(id);
            return result;
        }

        [Route(ApiHelper.LayoutGetByIdUrl)]
        [ApiAuthorize(Roles = RoleValue.Admin)]
        [HttpGet]
        public ResultObject<Layout> GetById(string id)
        {
            var result = _layoutRepository.GetById(id);
            return result;
        }

        [Route(ApiHelper.LayoutAddUrl)]
        [ApiAuthorize(Roles = RoleValue.Admin)]
        [HttpPost]
        public ResultObject<string> Add(Layout obj)
        {
            var result = _layoutRepository.Add(obj);
            return result;
        }

        [Route(ApiHelper.LayoutEditUrl)]
        [ApiAuthorize(Roles = RoleValue.Admin)]
        [HttpPost]
        public ResultBase Edit(Layout obj)
        {
            var result = _layoutRepository.Edit(obj);
            return result;
        }

        [Route(ApiHelper.LayoutDeleteUrl)]
        [ApiAuthorize(Roles = RoleValue.Admin)]
        [HttpGet]
        [HttpDelete]
        public ResultBase Delete(string id)
        {
            var result = _layoutRepository.Delete(id);
            return result;
        }
        #endregion

        #region Frontend Site
        [Route(ApiHelper.LayoutForSiteUrl)]
        [HttpGet]
        public ResultCollection<Layout> GetForSite()
        {
            var result = _layoutRepository.GetForSite();
            return result;
        }
        #endregion
    }
}