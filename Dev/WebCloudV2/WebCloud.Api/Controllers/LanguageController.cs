﻿using System.Collections.Generic;
using System.Web.Http;
using WebCloud.Api.Business;
using WebCloud.Api.Core.Helper;
using WebCloud.Api.Infrastructure;
using WebCloud.Api.Infrastructure.Filters;
using WebCloud.Common;
using WebCloud.Common.Constant;
using WebCloud.Entity;

namespace WebCloud.Api.Controllers
{
    public class LanguageController : BaseApiController
    {
        private readonly ILanguageRepository _langRepository;

        public LanguageController(ILanguageRepository langRepository)
        {
            _langRepository = langRepository;
        }

        [Route(ApiHelper.LanguageGetAllUrl)]
        [HttpGet]
        public ResultObject<List<Language>> GetAll()
        {
            var result = _langRepository.GetAll();
            return result;
        }

        [Route(ApiHelper.LanguageGetByIdUrl)]
        [HttpGet]
        public ResultObject<Language> GetById(string id)
        {
            var result = _langRepository.GetById(id);
            return result;
        }

        [Route(ApiHelper.LanguageAddUrl)]
        [ApiAuthorize(Roles = RoleValue.SetupAdmin)]
        [HttpPost]
        public ResultBase Add(Language lang)
        {
            var result = _langRepository.Add(lang);
            return result;
        }

        [Route(ApiHelper.LanguageEditUrl)]
        [ApiAuthorize(Roles = RoleValue.SetupAdmin)]
        [HttpPost]
        public ResultBase Edit(Language lang)
        {
            var result = _langRepository.Edit(lang);
            return result;
        }

        [Route(ApiHelper.LanguageDeleteUrl)]
        [ApiAuthorize(Roles = RoleValue.SetupAdmin)]
        [HttpGet]
        [HttpDelete]
        public ResultBase Delete(string id)
        {
            var result = _langRepository.Delete(id);
            return result;
        }
    }
}