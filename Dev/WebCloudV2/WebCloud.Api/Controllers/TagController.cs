﻿using System.Collections.Generic;
using System.Web.Http;
using WebCloud.Api.Business;
using WebCloud.Api.Core.Helper;
using WebCloud.Api.Infrastructure;
using WebCloud.Common;

namespace WebCloud.Api.Controllers
{
    public class TagController : BaseApiController
    {
        private readonly ITagRepository _articleRepository;

        public TagController(ITagRepository articleRepository)
        {
            _articleRepository = articleRepository;
            _articleRepository.Context = this.ApiContext;
        }

        [Route(ApiHelper.TagUrl)]
        [HttpGet]
        public ResultObject<List<string>> GetAll()
        {
            var result = _articleRepository.GetAllTagValues();
            return result;
        }

        
    }
}