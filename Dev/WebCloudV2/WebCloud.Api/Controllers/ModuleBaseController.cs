﻿using System.Web.Http;
using WebCloud.Api.Business;
using WebCloud.Api.Core.Helper;
using WebCloud.Api.Infrastructure;
using WebCloud.Api.Infrastructure.Filters;
using WebCloud.Common;
using WebCloud.Common.Constant;
using WebCloud.Entity;

namespace WebCloud.Api.Controllers
{
    public class ModuleBaseController : BaseApiController
    {
        private readonly IModuleBaseRepository _moduleBaseRepository;

        public ModuleBaseController(IModuleBaseRepository moduleBaseRepository)
        {
            _moduleBaseRepository = moduleBaseRepository;
            _moduleBaseRepository.Context = this.ApiContext;
        }

        #region Management
        [Route(ApiHelper.ModuleBaseUrl)]
        [ApiAuthorize(Roles = RoleValue.RootAdmin)]
        [HttpGet]
        public ResultCollection<ModuleBase> GetAll()
        {
            var result = _moduleBaseRepository.GetAll();
            return result;
        }

        [Route(ApiHelper.ModuleBaseGetByIdUrl)]
        [HttpGet]
        public ResultObject<ModuleBase> GetById(string id)
        {
            var result = _moduleBaseRepository.GetById(id);
            return result;
        }

        [Route(ApiHelper.ModuleBaseAddUrl)]
        [ApiAuthorize(Roles = RoleValue.RootAdmin)]
        [HttpPost]
        public ResultBase Add(ModuleBase obj)
        {
            var result = _moduleBaseRepository.Add(obj);
            return result;
        }

        [Route(ApiHelper.ModuleBaseEditUrl)]
        [ApiAuthorize(Roles = RoleValue.RootAdmin)]
        [HttpPost]
        [HttpPut]
        public ResultBase Edit(ModuleBase obj)
        {
            var result = _moduleBaseRepository.Edit(obj);
            return result;
        }

        [Route(ApiHelper.ModuleBaseDeleteUrl)]
        [ApiAuthorize(Roles = RoleValue.RootAdmin)]
        [HttpGet]
        [HttpDelete]
        public ResultBase Delete(string id)
        {
            var result = _moduleBaseRepository.Delete(id);
            return result;
        }
        #endregion


        #region Admin

        [Route(ApiHelper.ModuleBaseForAdminUrl)]
        [ApiAuthorize(Roles = RoleValue.Admin)]
        [HttpGet]
        public ResultCollection<ModuleBase> GetForAdmin()
        {
            var result = _moduleBaseRepository.GetForAdmin();
            return result;
        }

        [Route(ApiHelper.ModuleBaseGetInstalledUrl)]
        [ApiAuthorize(Roles = RoleValue.Admin)]
        [HttpGet]
        public ResultCollection<ModuleBase> GetInstalled()
        {
            var result = _moduleBaseRepository.GetForAdmin(true);
            return result;
        }

        [Route(ApiHelper.ModuleBaseInstallUrl)]
        [ApiAuthorize(Roles = RoleValue.Admin)]
        [HttpPost]
        [HttpGet]
        public ResultBase Install(string id)
        {
            var result = _moduleBaseRepository.InstallModule(id);
            return result;
        }

        [Route(ApiHelper.ModuleBaseUninstallUrl)]
        [ApiAuthorize(Roles = RoleValue.Admin)]
        [HttpPost]
        [HttpGet]
        public ResultBase Uninstall(string id)
        {
            var result = _moduleBaseRepository.UninstallModule(id);
            return result;
        }

        #endregion

        [Route(ApiHelper.ModuleBaseForSiteUrl)]
        [HttpGet]
        public ResultCollection<ModuleBase> GetForSite()
        {
            var result = _moduleBaseRepository.GetForSite();
            return result;
        }
    }
}