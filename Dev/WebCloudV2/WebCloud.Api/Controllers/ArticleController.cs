﻿using System.Collections.Generic;
using System.Web.Http;
using WebCloud.Api.Business;
using WebCloud.Api.Core.Helper;
using WebCloud.Api.Infrastructure;
using WebCloud.Api.Infrastructure.Filters;
using WebCloud.Common;
using WebCloud.Common.Constant;
using WebCloud.Entity;

namespace WebCloud.Api.Controllers
{
    public class ArticleController : BaseApiController
    {
        private readonly IArticleRepository _articleRepository;

        public ArticleController(IArticleRepository articleRepository)
        {
            _articleRepository = articleRepository;
            _articleRepository.Context = this.ApiContext;
        }

        [Route(ApiHelper.ArticleUrl)]
        [HttpGet]
        public ResultObject<List<Article>> GetAll()
        {
            var result = _articleRepository.GetAll();
            return result;
        }

        [Route(ApiHelper.ArticleQueryUrl)]
        [HttpGet]
        [HttpPost]
        public ResultPagging<Article> Query(ArticleQuery query)
        {
            var result = _articleRepository.Query(query);
            return result;
        }

        [Route(ApiHelper.ArticleGetByIdUrl)]
        [HttpGet]
        public ResultObject<Article> GetById(string id)
        {
            var result = _articleRepository.GetById(id);
            return result;
        }

        [Route(ApiHelper.ArticleAddUrl)]
        [ApiAuthorize(Roles = RoleValue.Admin)]
        [HttpPost]
        public ResultObject<string> Add(Article obj)
        {
            var result = _articleRepository.Add(obj);
            return result;
        }

        [Route(ApiHelper.ArticleEditUrl)]
        [ApiAuthorize(Roles = RoleValue.Admin)]
        [HttpPost]
        public ResultBase Edit(Article obj)
        {
            var result = _articleRepository.Edit(obj);
            return result;
        }

        [Route(ApiHelper.ArticleDeleteUrl)]
        [ApiAuthorize(Roles = RoleValue.Admin)]
        [HttpGet]
        [HttpDelete]
        public ResultBase Delete(string id)
        {
            var result = _articleRepository.Delete(id);
            return result;
        }

        [Route(ApiHelper.ArticleTrashUrl)]
        [HttpGet]
        public ResultObject<List<Article>> Trash()
        {
            var result = _articleRepository.Trash();
            return result;
        }

        [Route(ApiHelper.ArticleRemoveUrl)]
        [ApiAuthorize(Roles = RoleValue.Admin)]
        [HttpGet]
        [HttpDelete]
        public ResultBase Remove(string id)
        {
            var result = _articleRepository.Remove(id);
            return result;
        }

        [Route(ApiHelper.ArticleRestoreUrl)]
        [ApiAuthorize(Roles = RoleValue.Admin)]
        [HttpGet]
        public ResultBase Restore(string id)
        {
            var result = _articleRepository.Restore(id);
            return result;
        }

        [Route(ApiHelper.ArticleEmptyTrashUrl)]
        [ApiAuthorize(Roles = RoleValue.Admin)]
        [HttpGet]
        public ResultBase EmptyTrash()
        {
            var result = _articleRepository.EmptyTrash();
            return result;
        }

    }
}