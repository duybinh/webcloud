﻿using System.Web.Http;
using WebCloud.Api.Infrastructure;
using WebCloud.Api.Infrastructure.Filters;
using WebCloud.Common;
using WebCloud.Common.Auth;

namespace WebCloud.Api.Controllers
{
    [RoutePrefix("api/values")]
    public class ValuesController : BaseApiController
    {
        [ApiAuthorize(Roles = "Admin")]
        [HttpGet]
        [Route("test")]
        public ResultObject<BasePrincipalModel> Test()
        {
            var principalModel = new BasePrincipalModel()
            {
                Email = this.ApiContext.Principal.Email,
                GlobalID = this.ApiContext.Principal.GlobalID,
                WebsiteID = this.ApiContext.Principal.WebsiteID,
                UserID = this.ApiContext.Principal.UserID,
                LanguageID = this.ApiContext.Principal.LanguageID,
                IsVerifiedEmail = this.ApiContext.Principal.IsVerifiedEmail,
                Roles = this.ApiContext.Principal.Roles,
            };

            return new ResultObject<BasePrincipalModel>()
            {
                IsSuccess = true,
                Data = principalModel
            };
        }

        [ApiAuthorize(Roles = "RootAdmin")]
        [HttpGet]
        [Route("testrole")]
        public ResultObject<BasePrincipalModel> TestRole()
        {
            var principalModel = new BasePrincipalModel()
            {
                Email = this.ApiContext.Principal.Email,
                GlobalID = this.ApiContext.Principal.GlobalID,
                WebsiteID = this.ApiContext.Principal.WebsiteID,
                UserID = this.ApiContext.Principal.UserID,
                LanguageID = this.ApiContext.Principal.LanguageID,
                IsVerifiedEmail = this.ApiContext.Principal.IsVerifiedEmail,
                Roles = this.ApiContext.Principal.Roles,
            };

            return new ResultObject<BasePrincipalModel>()
            {
                IsSuccess = true,
                Data = principalModel
            };
        }
        
        [AuthenticatedApp]
        [HttpGet]
        [Route("testauthapp")]
        public string TestAuthenticatedApp()
        {
            return "OK";
        }
    }
}