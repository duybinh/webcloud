﻿using System.Web.Http;
using WebCloud.Api.Business;
using WebCloud.Api.Core.Helper;
using WebCloud.Api.Infrastructure;
using WebCloud.Api.Infrastructure.Filters;
using WebCloud.Common;
using WebCloud.Common.Constant;
using WebCloud.Entity;

namespace WebCloud.Api.Controllers
{
    public class PageController : BaseApiController
    {
        private readonly IPageRepository _pageRepository;

        public PageController(IPageRepository pageRepository)
        {
            _pageRepository = pageRepository;
            _pageRepository.Context = this.ApiContext;
        }

        #region Page
        [Route(ApiHelper.PageUrl)]
        [ApiAuthorize(Roles = RoleValue.Admin)]
        [HttpGet]
        public ResultCollection<Page> GetAll()
        {
            var result = _pageRepository.GetAll();
            return result;
        }

        [Route(ApiHelper.PageGetByIdUrl)]
        [ApiAuthorize(Roles = RoleValue.Admin)]
        [HttpGet]
        public ResultObject<Page> GetById(string id)
        {
            var result = _pageRepository.GetById(id);
            return result;
        }

        [Route(ApiHelper.PageAddUrl)]
        [ApiAuthorize(Roles = RoleValue.Admin)]
        [HttpPost]
        public ResultBase Add(Page obj)
        {
            var result = _pageRepository.Add(obj);
            return result;
        }

        [Route(ApiHelper.PageEditUrl)]
        [ApiAuthorize(Roles = RoleValue.Admin)]
        [HttpPost]
        [HttpPut]
        public ResultBase Edit(Page obj)
        {
            var result = _pageRepository.Edit(obj);
            return result;
        }

        [Route(ApiHelper.PageDeleteUrl)]
        [ApiAuthorize(Roles = RoleValue.Admin)]
        [HttpGet]
        [HttpDelete]
        public ResultBase Delete(string id)
        {
            var result = _pageRepository.Delete(id);
            return result;
        }

        [Route(ApiHelper.PageInstallUrl)]
        [ApiAuthorize(Roles = RoleValue.Admin)]
        [HttpGet]
        public ResultBase Install(string id)
        {
            var result = _pageRepository.Install(id);
            return result;
        }

        [Route(ApiHelper.PageForSiteUrl)]
        [HttpGet]
        public ResultCollection<Page> GetForSite()
        {
            var result = _pageRepository.GetForSite();
            return result;
        }
        #endregion
    }
}