﻿using System.Web.Http;
using WebCloud.Api.Business;
using WebCloud.Api.Core.Helper;
using WebCloud.Api.Core.Models;
using WebCloud.Api.Infrastructure;
using WebCloud.Api.Infrastructure.Filters;
using WebCloud.Common;
using WebCloud.Common.Constant;
using WebCloud.Entity;

namespace WebCloud.Api.Controllers
{
    public class ModuleController : BaseApiController
    {
        private readonly IModuleRepository _moduleRepository;

        public ModuleController(IModuleRepository moduleRepository)
        {
            _moduleRepository = moduleRepository;
            _moduleRepository.Context = this.ApiContext;
        }

        #region Management
        [Route(ApiHelper.ModuleInPageUrl)]
        [HttpGet]
        public ResultCollection<Module> GetModuleInPage(string pageId)
        {
            var result = _moduleRepository.GetModuleInPage(pageId);
            return result;
        }

        [Route(ApiHelper.ModuleGetByIdUrl)]
        [HttpGet]
        public ResultObject<Module> GetById(string id)
        {
            var result = _moduleRepository.GetById(id);
            return result;
        }

        [Route(ApiHelper.ModuleAddUrl)]
        [ApiAuthorize(Roles = RoleValue.Admin)]
        [HttpPost]
        public ResultBase Add(Module obj)
        {
            var result = _moduleRepository.Add(obj);
            return result;
        }

        [Route(ApiHelper.ModuleEditUrl)]
        [ApiAuthorize(Roles = RoleValue.Admin)]
        [HttpPost]
        [HttpPut]
        public ResultBase Edit(Module obj)
        {
            var result = _moduleRepository.Edit(obj);
            return result;
        }

        [Route(ApiHelper.ModuleUpdatePositionUrl)]
        [ApiAuthorize(Roles = RoleValue.Admin)]
        [HttpPost]
        [HttpPut]
        public ResultBase UpdatePosition(UpdateModulePositionModel model)
        {
            var result = _moduleRepository.UpdatePosition(model.ModuleId, model.Section, model.BelowTo);
            return result;
        }

        [Route(ApiHelper.ModuleDeleteUrl)]
        [ApiAuthorize(Roles = RoleValue.Admin)]
        [HttpGet]
        [HttpDelete]
        public ResultBase Delete(string id)
        {
            var result = _moduleRepository.Delete(id);
            return result;
        }

       
        #endregion

        // get all modules for site
        [Route(ApiHelper.ModuleForSite)]
        [HttpGet]
        public ResultCollection<Module> GetForSite()
        {
            var result = _moduleRepository.GetForSite();
            return result;
        }

    }
}