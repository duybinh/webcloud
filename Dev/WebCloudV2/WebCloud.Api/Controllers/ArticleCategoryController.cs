﻿using System.Collections.Generic;
using System.Web.Http;
using WebCloud.Api.Business;
using WebCloud.Api.Core.Helper;
using WebCloud.Api.Infrastructure;
using WebCloud.Api.Infrastructure.Filters;
using WebCloud.Common;
using WebCloud.Common.Constant;
using WebCloud.Entity;

namespace WebCloud.Api.Controllers
{
    public class ArticleCategoryController : BaseApiController
    {
        private readonly IArticleCategoryRepository _articleCategoryRepository;

        public ArticleCategoryController(IArticleCategoryRepository articleCategoryRepository)
        {
            _articleCategoryRepository = articleCategoryRepository;
            _articleCategoryRepository.Context = this.ApiContext;
        }

        [Route(ApiHelper.ArticleCategoryUrl)]
        [HttpGet]
        public ResultObject<List<ArticleCategory>> GetAll()
        {
            var result = _articleCategoryRepository.GetAll();
            return result;
        }

        [Route(ApiHelper.ArticleCategoryGetByIdUrl)]
        [HttpGet]
        public ResultObject<ArticleCategory> GetById(string id)
        {
            var result = _articleCategoryRepository.GetById(id);
            return result;
        }

        [Route(ApiHelper.ArticleCategoryAddUrl)]
        [ApiAuthorize(Roles = RoleValue.Admin)]
        [HttpPost]
        public ResultObject<string> Add(ArticleCategory obj)
        {
            var result = _articleCategoryRepository.Add(obj);
            return result;
        }

        [Route(ApiHelper.ArticleCategoryEditUrl)]
        [ApiAuthorize(Roles = RoleValue.Admin)]
        [HttpPost]
        public ResultBase Edit(ArticleCategory obj)
        {
            var result = _articleCategoryRepository.Edit(obj);
            return result;
        }

        [Route(ApiHelper.ArticleCategoryDeleteUrl)]
        [ApiAuthorize(Roles = RoleValue.Admin)]
        [HttpGet]
        [HttpDelete]
        public ResultBase Delete(string id)
        {
            var result = _articleCategoryRepository.Delete(id, false);
            return result;
        }

        [Route(ApiHelper.ArticleCategoryDeleteWithChildsUrl)]
        [ApiAuthorize(Roles = RoleValue.Admin)]
        [HttpGet]
        [HttpDelete]
        public ResultBase DeleteWithChilds(string id)
        {
            var result = _articleCategoryRepository.Delete(id, true);
            return result;
        }

        [Route(ApiHelper.ArticleCategoryTrashUrl)]
        [HttpGet]
        public ResultObject<List<ArticleCategory>> Trash()
        {
            var result = _articleCategoryRepository.Trash();
            return result;
        }

        [Route(ApiHelper.ArticleCategoryRemoveUrl)]
        [ApiAuthorize(Roles = RoleValue.Admin)]
        [HttpGet]
        [HttpDelete]
        public ResultBase Remove(string id)
        {
            var result = _articleCategoryRepository.Remove(id);
            return result;
        }

        [Route(ApiHelper.ArticleCategoryRestoreUrl)]
        [ApiAuthorize(Roles = RoleValue.Admin)]
        [HttpGet]
        public ResultBase Restore(string id)
        {
            var result = _articleCategoryRepository.Restore(id);
            return result;
        }

        [Route(ApiHelper.ArticleCategoryEmptyTrashUrl)]
        [ApiAuthorize(Roles = RoleValue.Admin)]
        [HttpGet]
        public ResultBase EmptyTrash()
        {
            var result = _articleCategoryRepository.EmptyTrash();
            return result;
        }

    }
}