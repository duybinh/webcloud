﻿using System.Collections.Generic;
using System.Web.Http;
using WebCloud.Api.Business;
using WebCloud.Api.Core.Helper;
using WebCloud.Api.Infrastructure;
using WebCloud.Api.Infrastructure.Filters;
using WebCloud.Common;
using WebCloud.Common.Constant;
using WebCloud.Entity;

namespace WebCloud.Api.Controllers
{
    public class ThemeController : BaseApiController
    {
        private readonly IThemeRepository _themeRepository;

        public ThemeController(IThemeRepository themeRepository)
        {
            _themeRepository = themeRepository;
            _themeRepository.Context = this.ApiContext;
        }

        [Route(ApiHelper.ThemeGetAllUrl)]
        [ApiAuthorize]
        [HttpGet]
        public ResultObject<List<Theme>> GetAll()
        {
            var result = _themeRepository.GetAll();
            return result;
        }

        [Route(ApiHelper.ThemeGetByIdUrl)]
        [ApiAuthorize]
        [HttpGet]
        public ResultObject<Theme> GetById(string id)
        {
            var result = _themeRepository.GetById(id);
            return result;
        }

        [Route(ApiHelper.ThemeAddUrl)]
        [ApiAuthorize(Roles = RoleValue.SetupAdmin)]
        [HttpPost]
        public ResultBase Add(Theme theme)
        {
            var result = _themeRepository.Add(theme);
            return result;
        }

        [Route(ApiHelper.ThemeEditUrl)]
        [ApiAuthorize(Roles = RoleValue.SetupAdmin)]
        [HttpPost]
        public ResultBase Edit(Theme theme)
        {
            var result = _themeRepository.Edit(theme);
            return result;
        }

        [Route(ApiHelper.ThemeDeleteUrl)]
        [ApiAuthorize(Roles = RoleValue.SetupAdmin)]
        [HttpGet]
        [HttpDelete]
        public ResultBase Delete(string id)
        {
            var result = _themeRepository.Delete(id);
            return result;
        }

    }
}