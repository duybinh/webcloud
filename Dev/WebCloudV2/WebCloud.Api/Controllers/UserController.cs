﻿using System.Linq;
using System.Web.Http;
using WebCloud.Api.Business;
using WebCloud.Api.Core.Helper;
using WebCloud.Api.Core.Models;
using WebCloud.Api.Infrastructure;
using WebCloud.Api.Infrastructure.Filters;
using WebCloud.Common;
using System.Data.SqlClient;

namespace WebCloud.Api.Controllers
{
    public class UserController : BaseApiController
    {
        private readonly IUserRepository _userRepository;

        public UserController(IUserRepository userRepository)
        {
            _userRepository = userRepository;
            _userRepository.Context = this.ApiContext;
        }

        [ApiAuthorize]
        [Route(ApiHelper.UserGetMyProfileUrl)]
        public ResultObject<UserProfileModel> GetMyProfile()
        {
            var result = _userRepository.GetUserByGlobalId(ApiContext.Principal.GlobalID);
            if (result.IsSuccess)
            {
                var userProfileModel = new UserProfileModel
                                       {
                                           WebsiteID = result.Data.WebsiteID,
                                           UserID = result.Data.UserID,
                                           GlobalID = result.Data.GlobalID,
                                           LanguageID = result.Data.LanguageID,
                                           Email = result.Data.Email,
                                           DisplayName = result.Data.DisplayName,
                                           Avatar = result.Data.Avatar,
                                           IsVerifiedEmail = result.Data.IsVerifiedEmail,
                                           Roles = result.Data.UserRoles.Select(obj => obj.UserRoleID).ToArray(),
                                       };

                return new ResultObject<UserProfileModel>
                {
                    IsSuccess = true,
                    Message = result.Message,
                    Data = userProfileModel
                };
            }

            return new ResultObject<UserProfileModel>
                   {
                       IsSuccess = false,
                       Message = result.Message
                   };
        }
    }
}