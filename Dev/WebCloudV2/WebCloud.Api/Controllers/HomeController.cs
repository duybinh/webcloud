﻿using System.Collections;
using System.Linq;
using System.Security.Claims;
using System.Web.Http;
using WebCloud.Api.Infrastructure;
using WebCloud.Api.Infrastructure.Filters;

namespace WebCloud.Api.Controllers
{
   
    public class HomeController : BaseApiController
    {
        [Route("")]
        [HttpGet]
        public string Index()
        {
            return "Welcome to WebCloud.Api! Copyright @2015 by Dong Hanh.";
        }

        [Authorize]
        [HttpGet]
        [Route("api/testauth")]
        public IEnumerable TestAuth()
        {
            var identity = User.Identity as ClaimsIdentity;

            return identity.Claims.Select(c => new
            {
                Type = c.Type,
                Value = c.Value
            });
        }

        [ApiAuthorize(Roles = "Any")]
        [HttpGet]
        [Route("api/testauth2")]
        public dynamic TestAuth2()
        {
            return new
            {
                ApiContext.WebsiteId,
                ApiContext.LanguageId,
                ApiContext.ApplicationId,

                ApiContext.Principal.GlobalID,
                ApiContext.Principal.UserID,
                ApiContext.Principal.WebsiteID,
                ApiContext.Principal.TargetWebsiteID,
                ApiContext.Principal.Email,
                ApiContext.Principal.IsVerifiedEmail,
                ApiContext.Principal.Roles,
                ApiContext.Principal.Groups,
            };
        }
    }
}