﻿using System.Collections.Generic;
using System.Web.Http;
using WebCloud.Api.Business;
using WebCloud.Api.Core.Helper;
using WebCloud.Api.Infrastructure;
using WebCloud.Api.Infrastructure.Filters;
using WebCloud.Common;
using WebCloud.Common.Constant;
using WebCloud.Entity;

namespace WebCloud.Api.Controllers
{
    public class RoleController : BaseApiController
    {
        private readonly IRoleRepository _roleRepository;

        public RoleController(IRoleRepository roleRepository)
        {
            _roleRepository = roleRepository;
        }

        [Route(ApiHelper.RoleGetAllUrl)]
        [ApiAuthorize]
        [HttpGet]
        public ResultObject<List<UserRole>> GetAll()
        {
            var result = _roleRepository.GetAll();
            return result;
        }

        [Route(ApiHelper.RoleGetByIdUrl)]
        [ApiAuthorize]
        [HttpGet]
        public ResultObject<UserRole> GetById(string id)
        {
            var result = _roleRepository.GetById(id);
            return result;
        }

        [Route(ApiHelper.RoleAddUrl)]
        [ApiAuthorize(Roles = RoleValue.SetupAdmin)]
        [HttpPost]
        public ResultBase Add(UserRole role)
        {
            var result = _roleRepository.Add(role);
            return result;
        }

        [Route(ApiHelper.RoleEditUrl)]
        [ApiAuthorize(Roles = RoleValue.SetupAdmin)]
        [HttpPost]
        public ResultBase Edit(UserRole role)
        {
            var result = _roleRepository.Edit(role);
            return result;
        }

        [Route(ApiHelper.RoleDeleteUrl)]
        [ApiAuthorize(Roles = RoleValue.SetupAdmin)]
        [HttpGet]
        [HttpDelete]
        public ResultBase Delete(string id)
        {
            var result = _roleRepository.Delete(id);
            return result;
        }

    }
}