﻿using System.Web.Http;
using WebCloud.Api.Business;
using WebCloud.Api.Core.Helper;
using WebCloud.Api.Core.Models;
using WebCloud.Api.Infrastructure;
using WebCloud.Common;

namespace WebCloud.Api.Controllers
{
    public class DataUpdateController : BaseApiController
    {
        private readonly IArticleRepository _articleRepository;
        private readonly IModuleRepository _moduleRepository;

        public DataUpdateController(IArticleRepository articleRepository, IModuleRepository moduleRepository)
        {
            _articleRepository = articleRepository;
            _moduleRepository = moduleRepository;
            _articleRepository.Context = _moduleRepository.Context = this.ApiContext;
        }

        [Route(ApiHelper.DataUpdateUrl)]
        [HttpPost]
        public ResultBase Update(DataUpdateModel model)
        {
            switch (model.Obj)
            {
                case "Article":
                    return _articleRepository.UpdateFields(model.Key, model.Fields);

                case "Module":
                    return _moduleRepository.UpdateFields(model.Key, model.Fields);

                default:
                    return new ResultBase()
                    {
                        IsSuccess = false,
                        Message = new Message("Update_Object_Invalid")
                    };
            }
        }
       

    }
}