﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Web;
using WebCloud.Api.Infrastructure;
using WebCloud.Common.Constant;

namespace WebCloud.Api.Handlers
{
    public class ResourceAccessHandler : BaseHttpHandler
    {
        private string _websitePath;
        private string _defaultPath;
        public override bool IsReusable
        {
            // Return false in case your Managed Handler cannot be reused for another request.
            // Usually this would be false in case you have some state information preserved per request.
            get { return true; }
        }

        public override void ProcessRequest(HttpContext context)
        {
            base.ProcessRequest(context);

            _websitePath = context.Server.MapPath("~/Resources/Websites/");
            string relativePath = context.Request.Url.LocalPath.Substring(11).Replace("/", "\\"); // "/resources/..." --> "..."
            string absolutedPath = Path.Combine(_websitePath, HandlerContext.WebsiteId, relativePath);

            if (!File.Exists(absolutedPath))
            {
                if (relativePath.StartsWith("themes", StringComparison.InvariantCultureIgnoreCase))
                {
                    absolutedPath = DefaultThemeFile(relativePath);
                }
                else
                {
                    absolutedPath = DefaultFile(relativePath);
                }

                if (absolutedPath == null)
                {
                    context.Response.StatusCode = 404;
                    context.Response.Write("File not exist: " + relativePath);
                    return;
                }
            }

            switch (Path.GetExtension(relativePath).ToLower())
            {
                case ".css":
                    context.Response.ContentType = "text/css";
                    break;
                case ".js":
                    context.Response.ContentType = "text/javascript";
                    break;
                case ".woff":
                case ".woff2":
                case ".ttf":
                    context.Response.ContentType = "application/font-woff2";
                    break;
            }

            context.Response.WriteFile(absolutedPath);
        }

        private string DefaultThemeFile(string filePath)
        {
            string[] subs = filePath.Split(new[] { '\\' }, 3);
            if (subs.Length < 3)
                return null;

            string theme = subs[1];
            string file = subs[2];

            if (HandlerContext.WebsiteId == Default.WebsiteId &&
                theme.Equals(Default.ThemeId, StringComparison.InvariantCultureIgnoreCase))
            {
                return null;
            }

            var handles = new List<string[]>();
            if (HandlerContext.WebsiteId != Default.WebsiteId)
            {
                handles.Add(new[] { Default.WebsiteId, Default.ThemeId });
            }
            handles.Add(new[] { HandlerContext.WebsiteId, Default.ThemeId });
            if (theme.Contains("-"))
            {
                string[] themes = theme.Split('-');
                string handleTheme = "";
                for (int i = 0; i < themes.Length - 1; i++)
                {
                    handleTheme += themes[i];
                    if (HandlerContext.WebsiteId != Default.WebsiteId)
                    {
                        handles.Add(new[] { Default.WebsiteId, handleTheme });
                    }
                    handles.Add(new[] { HandlerContext.WebsiteId, handleTheme });
                }
            }
            if (HandlerContext.WebsiteId != Default.WebsiteId)
            {
                handles.Add(new[] { Default.WebsiteId, theme });
            }

            if (handles.Count > 1)
                handles.Reverse();

            foreach (var handle in handles)
            {
                string defaultFilePath = Path.Combine(_websitePath, handle[0], "themes", handle[1], file);
                if (File.Exists(defaultFilePath))
                    return defaultFilePath;
            }
            return null;
        }

        private string DefaultFile(string filePath)
        {
            if (HandlerContext.WebsiteId == Default.WebsiteId)
                return null;

            string defaultFilePath = Path.Combine(_websitePath, Default.WebsiteId, filePath);
            if (File.Exists(defaultFilePath))
                return defaultFilePath;

            return null;
        }

    }
}
