﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using DHSoft.Utility.Common;
using DHSoft.Utility.Validation;
using Newtonsoft.Json;
using WebCloud.Api.Infrastructure;
using WebCloud.Common.Constant;
using WebCloud.Common.Enums;

namespace WebCloud.Api.Handlers
{
    //public enum ResourceLocation
    //{
    //    Themes,
    //    Websites
    //}

    public enum ResourceAction
    {
        ListDir,
        MkDir,
        RenameDir,
        DeleteDir,
        GetFile,
        Upload,
        AddFile,
        RenameFile,
        EditFile,
        DeleteFile,
        Thumb,
        AccessFile
    }

    public enum ResourceAuthorizeRule
    {
        Anonymous,
        AnyRole,
        AnyOfRoles,
        AllOfRoles
    }

    public class ResourceAuthorizeSetting
    {
        public ResourceAuthorizeRule Type { set; get; }
        public string[] Roles { set; get; }
    }

    public class ResourceHandler : BaseHttpHandler
    {
        public ResourceAction CurrentResourceAction { private set; get; }
        private string _websitePath;
        private string _defaultPath;
        public override void ProcessRequest(HttpContext context)
        {
            base.ProcessRequest(context);
            Authorize(context);
            HandleRequest(context);
        }

        public override bool IsReusable
        {
            get
            {
                return true;
            }
        }
        protected override void InitHandlerContext(HttpContext context)
        {
            base.InitHandlerContext(context);

            _websitePath = context.Server.MapPath("~/Resources/websites/" + HandlerContext.WebsiteId);
            _defaultPath = context.Server.MapPath("~/Resources/websites/" + Default.WebsiteId);

            switch (context.Request["action"])
            {
                case "listdir":
                    CurrentResourceAction = ResourceAction.ListDir;
                    break;
                case "mkdir":
                    CurrentResourceAction = ResourceAction.MkDir;
                    break;
                case "renamedir":
                    CurrentResourceAction = ResourceAction.RenameDir;
                    break;
                case "deletedir":
                    CurrentResourceAction = ResourceAction.DeleteDir;
                    break;
                case "upload":
                    CurrentResourceAction = ResourceAction.Upload;
                    break;
                case "getfile":
                    CurrentResourceAction = ResourceAction.GetFile;
                    break;
                case "addfile":
                    CurrentResourceAction = ResourceAction.AddFile;
                    break;
                case "editfile":
                    CurrentResourceAction = ResourceAction.EditFile;
                    break;
                case "renamefile":
                    CurrentResourceAction = ResourceAction.RenameFile;
                    break;
                case "deletefile":
                    CurrentResourceAction = ResourceAction.DeleteFile;
                    break;
                case "thumb":
                    CurrentResourceAction = ResourceAction.Thumb;
                    break;
                default:
                    CurrentResourceAction = ResourceAction.AccessFile;
                    break;
            }
        }
        public static Dictionary<ResourceAction, ResourceAuthorizeSetting> AuthorizeMapSystem = new Dictionary<ResourceAction, ResourceAuthorizeSetting>
                    {
                        {ResourceAction.ListDir, new ResourceAuthorizeSetting{Type = ResourceAuthorizeRule.AnyOfRoles, Roles = new []{RoleValue.RootAdmin,RoleValue.SetupAdmin}}},
                        {ResourceAction.MkDir, new ResourceAuthorizeSetting{Type = ResourceAuthorizeRule.AnyOfRoles, Roles = new []{RoleValue.RootAdmin,RoleValue.SetupAdmin}}},
                        {ResourceAction.RenameDir, new ResourceAuthorizeSetting{Type = ResourceAuthorizeRule.AnyOfRoles, Roles = new []{RoleValue.RootAdmin,RoleValue.SetupAdmin}}},
                        {ResourceAction.DeleteDir, new ResourceAuthorizeSetting{Type = ResourceAuthorizeRule.AnyOfRoles, Roles = new []{RoleValue.RootAdmin,RoleValue.SetupAdmin}}},
                        {ResourceAction.GetFile, new ResourceAuthorizeSetting{Type = ResourceAuthorizeRule.Anonymous}},
                        {ResourceAction.Upload, new ResourceAuthorizeSetting{Type = ResourceAuthorizeRule.AnyOfRoles, Roles = new []{RoleValue.RootAdmin,RoleValue.SetupAdmin}}},
                        {ResourceAction.AddFile, new ResourceAuthorizeSetting{Type = ResourceAuthorizeRule.AnyOfRoles, Roles = new []{RoleValue.RootAdmin,RoleValue.SetupAdmin}}},
                        {ResourceAction.EditFile, new ResourceAuthorizeSetting{Type = ResourceAuthorizeRule.AnyOfRoles, Roles = new []{RoleValue.RootAdmin,RoleValue.SetupAdmin}}},
                        {ResourceAction.RenameFile, new ResourceAuthorizeSetting{Type = ResourceAuthorizeRule.AnyOfRoles, Roles = new []{RoleValue.RootAdmin,RoleValue.SetupAdmin}}},
                        {ResourceAction.DeleteFile, new ResourceAuthorizeSetting{Type = ResourceAuthorizeRule.AnyOfRoles, Roles = new []{RoleValue.RootAdmin,RoleValue.SetupAdmin}}},
                        {ResourceAction.Thumb, new ResourceAuthorizeSetting{Type = ResourceAuthorizeRule.Anonymous}},
                        {ResourceAction.AccessFile, new ResourceAuthorizeSetting{Type = ResourceAuthorizeRule.Anonymous}},
                    };

        public static Dictionary<ResourceAction, ResourceAuthorizeSetting> AuthorizeMap = new Dictionary<ResourceAction, ResourceAuthorizeSetting>
                    {
                        {ResourceAction.ListDir, new ResourceAuthorizeSetting{Type = ResourceAuthorizeRule.AnyOfRoles, Roles = new []{RoleValue.RootAdmin,RoleValue.SetupAdmin}}},
                        {ResourceAction.MkDir, new ResourceAuthorizeSetting{Type = ResourceAuthorizeRule.AnyOfRoles, Roles = new []{RoleValue.RootAdmin,RoleValue.SetupAdmin}}},
                        {ResourceAction.RenameDir, new ResourceAuthorizeSetting{Type = ResourceAuthorizeRule.AnyOfRoles, Roles = new []{RoleValue.RootAdmin,RoleValue.SetupAdmin}}},
                        {ResourceAction.DeleteDir, new ResourceAuthorizeSetting{Type = ResourceAuthorizeRule.AnyOfRoles, Roles = new []{RoleValue.RootAdmin,RoleValue.SetupAdmin}}},
                        {ResourceAction.GetFile, new ResourceAuthorizeSetting{Type = ResourceAuthorizeRule.Anonymous}},
                        {ResourceAction.Upload, new ResourceAuthorizeSetting{Type = ResourceAuthorizeRule.AnyOfRoles, Roles = new []{RoleValue.RootAdmin,RoleValue.SetupAdmin}}},
                        {ResourceAction.AddFile, new ResourceAuthorizeSetting{Type = ResourceAuthorizeRule.AnyOfRoles, Roles = new []{RoleValue.RootAdmin,RoleValue.SetupAdmin}}},
                        {ResourceAction.EditFile, new ResourceAuthorizeSetting{Type = ResourceAuthorizeRule.AnyOfRoles, Roles = new []{RoleValue.RootAdmin,RoleValue.SetupAdmin}}},
                        {ResourceAction.RenameFile, new ResourceAuthorizeSetting{Type = ResourceAuthorizeRule.AnyOfRoles, Roles = new []{RoleValue.RootAdmin,RoleValue.SetupAdmin}}},
                        {ResourceAction.DeleteFile, new ResourceAuthorizeSetting{Type = ResourceAuthorizeRule.AnyOfRoles, Roles = new []{RoleValue.RootAdmin,RoleValue.SetupAdmin}}},
                        {ResourceAction.Thumb, new ResourceAuthorizeSetting{Type = ResourceAuthorizeRule.Anonymous}},
                        {ResourceAction.AccessFile, new ResourceAuthorizeSetting{Type = ResourceAuthorizeRule.Anonymous}},
                    };

        protected void Authorize(HttpContext context)
        {
            var setting = HandlerContext.WebsiteId == Default.WebsiteId ? AuthorizeMapSystem[CurrentResourceAction] : AuthorizeMap[CurrentResourceAction];

            if (setting.Type == ResourceAuthorizeRule.Anonymous) return;

            if (HandlerContext.Principal != null)
            {
                switch (setting.Type)
                {
                    case ResourceAuthorizeRule.AnyRole: return;
                    case ResourceAuthorizeRule.AnyOfRoles:
                        if (HandlerContext.Principal.IsInRoles(setting.Roles)) return;
                        break;
                    case ResourceAuthorizeRule.AllOfRoles:
                        if (HandlerContext.Principal.IsInAllRoles(setting.Roles)) return;
                        break;
                }
            }

            // response not authorized
            ResponseError401(context);
        }

        protected void HandleRequest(HttpContext context)
        {
            switch (CurrentResourceAction)
            {
                case ResourceAction.ListDir:
                    ListDir(context);
                    break;
                case ResourceAction.DeleteDir:
                    DeleteDir(context);
                    break;
                case ResourceAction.RenameDir:
                    RenameDir(context);
                    break;
                case ResourceAction.MkDir:
                    MkDir(context);
                    break;
                case ResourceAction.DeleteFile:
                    DeleteFile(context);
                    break;
                case ResourceAction.RenameFile:
                    RenameFile(context);
                    break;
                case ResourceAction.Upload:
                    Upload(context);
                    break;
                case ResourceAction.Thumb:
                    Thumb(context);
                    break;
                case ResourceAction.GetFile:
                    GetFile(context);
                    break;
                case ResourceAction.EditFile:
                    EditFile(context);
                    break;
                case ResourceAction.AddFile:
                    AddFile(context);
                    break;
                case ResourceAction.AccessFile:
                    AccessFile(context);
                    break;
            }
        }

        #region Manage Dir
        private void ListDir(HttpContext context)
        {
            string folder = context.Request["path"];
            if (HandlerContext.WebsiteId == Default.WebsiteId)
            {
                var items = ListDirItems(folder, true);
                if (items == null)
                {
                    ResponseError404(context);
                    return;
                }

                ResponseSuccess(context, items);
            }
            else
            {
                var localItems = ListDirItems(folder, false);
                var systemItems = ListDirItems(folder, true);
                if (localItems == null && systemItems == null)
                {
                    ResponseError404(context);
                    return;
                }

                ResponseSuccess(context, MergeItems(localItems, systemItems));
            }

        }

        private List<FileItem> ListDirItems(string folder, bool systemSite)
        {
            string path = Path.Combine(systemSite ? _defaultPath : _websitePath, folder.Replace("/", "\\"));
            SourceType sourceType = systemSite ? SourceType.Global : SourceType.Local;

            if (!Directory.Exists(path))
                return null;

            var dir = new DirectoryInfo(path);

            var files = dir.GetFiles("*", SearchOption.TopDirectoryOnly)
                           .Where(f => !f.Attributes.HasFlag(FileAttributes.Hidden))
                           .Select(f => new FileItem(f, folder, sourceType)).OrderBy(f => f.Name)
                           .ToList();

            var subDirs = dir.GetDirectories("*", SearchOption.TopDirectoryOnly)
                           .Where(d => !d.Attributes.HasFlag(FileAttributes.Hidden))
                           .Select(d => new FileItem(d, folder, sourceType)).OrderBy(f => f.Name)
                           .ToList();
            return subDirs.Union(files).OrderByDescending(o => o.IsDirectory).ThenBy(o => o.Name).ToList();
        }

        private List<FileItem> MergeItems(List<FileItem> local, List<FileItem> system)
        {
            if (local == null) return system;
            if (system == null) return local;

            var localDict = local.ToDictionary(o => o.Path.ToLower(), o => o);
            var systemDict = system.ToDictionary(o => o.Path.ToLower(), o => o);

            foreach (var p in systemDict)
            {
                if (localDict.ContainsKey(p.Key))
                {
                    localDict[p.Key].SourceType = SourceType.Overrided;
                }
                else
                {
                    p.Value.SourceType = SourceType.Global;
                    localDict.Add(p.Key, p.Value);
                }
            }

            return localDict.Values.OrderByDescending(o => o.IsDirectory).ThenBy(o => o.Name).ToList();
        }

        private void DeleteDir(HttpContext context)
        {
            string path = context.Request["path"];
            if (string.IsNullOrWhiteSpace(path))
            {
                ResponseError404(context);
                return;
            }

            string absolutedPath = _websitePath + "\\" + path.Replace("/", "\\");
            if (!Directory.Exists(absolutedPath))
            {
                ResponseError404(context);
                return;
            }

            Directory.Delete(absolutedPath, true);

            ResponseSuccess(context, path);
        }

        private void RenameDir(HttpContext context)
        {
            string path = context.Request["path"];
            if (string.IsNullOrWhiteSpace(path))
            {
                ResponseError404(context);
                return;
            }

            string absolutePath = _websitePath + "\\" + path.Replace("/", "\\");
            string newName = context.Request["newname"];
            string newPath = absolutePath.Substring(0, absolutePath.LastIndexOf("\\", StringComparison.Ordinal)) + "\\" + newName;

            if (!Directory.Exists(absolutePath))
            {
                ResponseError404(context);
                return;
            }

            if (!newName.IsValidFolderName())
            {
                ResponseError(context, MessageCode.Resource_FolderNameInvalid);
                return;
            }

            if (Directory.Exists(newPath) || File.Exists(newPath))
            {
                ResponseError(context, MessageCode.Resource_NewNameExist);
                return;
            }

            Directory.Move(absolutePath, newPath);

            ResponseSuccess(context, newName);
        }

        private void MkDir(HttpContext context)
        {
            string dir = context.Request["path"];
            if (string.IsNullOrWhiteSpace(dir))
            {
                ResponseError(context, MessageCode.Resource_FolderNameInvalid);
                return;
            }

            string newDirPath = Path.Combine(_websitePath, dir.Replace("/", "\\"));
            if (Directory.Exists(newDirPath) || File.Exists(newDirPath))
            {
                ResponseError(context, MessageCode.Resource_NewNameExist);
                return;
            }
            try
            {
                Directory.CreateDirectory(newDirPath);
            }
            catch (UnauthorizedAccessException)
            {
                ResponseError(context, MessageCode.Resource_NoPermissionToAccess);
                return;
            }
            catch (Exception)
            {
                ResponseError(context, MessageCode.Resource_FolderNameInvalid);
                return;
            }

            ResponseSuccess(context, dir);
        }
        #endregion

        #region Manage File
        private void DeleteFile(HttpContext context)
        {
            string path = context.Request["path"];
            if (string.IsNullOrWhiteSpace(path))
            {
                ResponseError404(context);
                return;
            }

            string absolutedPath = Path.Combine(_websitePath, path.Replace("/", "\\"));
            if (!File.Exists(absolutedPath))
            {
                ResponseError404(context);
                return;
            }

            File.Delete(absolutedPath);

            ResponseSuccess(context, path);
        }

        private void RenameFile(HttpContext context)
        {
            string path = context.Request["path"];
            if (string.IsNullOrWhiteSpace(path))
            {
                ResponseError404(context);
            }

            string absolutePath = Path.Combine(_websitePath, path.Replace("/", "\\"));
            string newName = context.Request["newname"] + Path.GetExtension(absolutePath);
            string newPath = absolutePath.Substring(0, absolutePath.LastIndexOf("\\", StringComparison.Ordinal)) + "\\" + newName;

            if (!File.Exists(absolutePath))
            {
                ResponseError404(context);
                return;
            }

            if (!newName.IsValidFileName())
            {
                ResponseError(context, MessageCode.Resource_FileNameInvalid);
            }

            if (Directory.Exists(newPath) || File.Exists(newPath))
            {
                ResponseError(context, MessageCode.Resource_NewNameExist);
                return;
            }

            File.Move(absolutePath, newPath);

            ResponseSuccess(context, newName);
        }

        private void GetFile(HttpContext context)
        {
            string path = context.Request["path"];
            if (string.IsNullOrWhiteSpace(path))
            {
                ResponseError404(context);
                return;
            }


            string filePath = Path.Combine(_websitePath, path.Replace("/", "\\"));

            if (!File.Exists(filePath))
            {
                if (HandlerContext.WebsiteId == Default.WebsiteId)
                {
                    ResponseError404(context);
                    return;
                }

                filePath = Path.Combine(_defaultPath, path.Replace("/", "\\"));
                if (!File.Exists(filePath))
                {
                    ResponseError404(context);
                    return;
                }
            }

            string content = File.ReadAllText(filePath, Encoding.UTF8);

            ResponseSuccess(context, content);
        }

        public class PostedFileContent
        {
            public string Name { set; get; }
            public string Content { set; get; }
        }
        private void EditFile(HttpContext context)
        {
            string dir = context.Request["dir"];
            PostedFileContent file = GetPostedFileContent(context);
            if (file == null)
            {
                ResponseError(context, MessageCode.Resource_FileContentInvalid);
                return;
            }
            string folderPath = Path.Combine(_websitePath, dir.Replace("/", "\\"));
            string filePath = Path.Combine(folderPath, file.Name);

            if (!File.Exists(filePath))
            {
                if (HandlerContext.WebsiteId == Default.WebsiteId)
                {
                    ResponseError404(context);
                    return;
                }

                string systemFilePath = Path.Combine(_defaultPath, dir.Replace("/", "\\"), file.Name);
                if (!File.Exists(systemFilePath))
                {
                    ResponseError404(context);
                    return;
                }
            }
            if (!Directory.Exists(folderPath))
            {
                Directory.CreateDirectory(folderPath);
            }
            File.WriteAllText(filePath, file.Content, Encoding.UTF8);

            ResponseSuccess(context, null);
        }

        private void AddFile(HttpContext context)
        {
            string dir = context.Request["dir"];
            if (string.IsNullOrWhiteSpace(dir))
            {
                ResponseError404(context);
                return;
            }

            string folderPath = Path.Combine(_websitePath, dir.Replace("/", "\\"));

            if (!Directory.Exists(folderPath))
            {
                if (HandlerContext.WebsiteId == Default.WebsiteId)
                {
                    ResponseError404(context);
                    return;
                }

                string systemFolderPath = Path.Combine(_defaultPath, dir.Replace("/", "\\"));
                if (!Directory.Exists(systemFolderPath))
                {
                    ResponseError404(context);
                    return;
                }

                Directory.CreateDirectory(folderPath);
            }

            PostedFileContent file = GetPostedFileContent(context);
            if (file == null)
            {
                ResponseError(context, MessageCode.Resource_FileContentInvalid);
                return;
            }

            string filePath = Path.Combine(folderPath, file.Name);
            if (File.Exists(filePath))
            {
                ResponseError(context, MessageCode.Resource_NewNameExist);
                return;
            }

            File.WriteAllText(filePath, file.Content, Encoding.UTF8);

            ResponseSuccess(context, new FileItem(new FileInfo(filePath), dir,
                                                  HandlerContext.WebsiteId == Default.WebsiteId ? SourceType.Global : SourceType.Local));
        }

        private PostedFileContent GetPostedFileContent(HttpContext context)
        {
            try
            {
                context.Request.InputStream.Position = 0;
                using (var inputStream = new StreamReader(context.Request.InputStream))
                {
                    string jsonString = inputStream.ReadToEnd();
                    return JsonConvert.DeserializeObject<PostedFileContent>(jsonString);
                }
            }
            catch
            {
                return null;
            }
        }
        #endregion

        #region Upload
        private void Upload(HttpContext context)
        {
            if (context.Request.Files.Count == 0)
                ResponseError(context, MessageCode.Resource_UploadFileInvalid);

            string dir = context.Request["dir"];
            string folderPath = _websitePath;
            if (!string.IsNullOrEmpty(dir))
                folderPath += "\\" + dir.Replace("/", "\\");

            if (!Directory.Exists(folderPath))
                Directory.CreateDirectory(folderPath);

            var uploadFiles = new List<FileItem>();
            for (int i = 0; i < context.Request.Files.Count; i++)
            {
                var file = context.Request.Files[i];
                var fileName = GenerateFileName(folderPath, file.FileName);
                file.SaveAs(Path.Combine(folderPath, fileName));
                uploadFiles.Add(new FileItem
                {
                    FileType = file.ContentType,
                    Name = fileName,
                    Size = file.ContentLength,
                    IsDirectory = false
                });
            }

            ResponseSuccess(context, uploadFiles);
        }

        private string GenerateFileName(string folderPath, string fileName)
        {
            string oldName = Path.GetFileNameWithoutExtension(fileName);
            string newName = oldName;
            string fileExt = Path.GetExtension(fileName);

            int count = 0;
            while (File.Exists(Path.Combine(folderPath, newName + fileExt)))
            {
                count++;
                newName = oldName + "-" + count;
            }

            return newName + fileExt;
        }
        #endregion

        #region Thumb
        const string DefaultImg = "files/images/no_image.jpg";

        private void Thumb(HttpContext context)
        {
            context.Response.ContentType = "image/png";
            bool fileExist = GetThumb(context, _websitePath); ;
            if (!fileExist && HandlerContext.WebsiteId != Default.WebsiteId)
            {
                fileExist = GetThumb(context, _defaultPath);
            }
            if (!fileExist)
            {
                string photoFile = context.Request.QueryString["p"];
                if (photoFile == DefaultImg)
                {
                    ResponseError404(context);
                }
                else
                {
                    //if image is not exist return no_image.jpg
                    context.Response.Redirect(context.Request.Url.PathAndQuery.Replace("p=" + photoFile, "p=" + DefaultImg));
                }
            }
        }
        private bool GetThumb(HttpContext context, string websitePath)
        {
            string thumbPath = Path.Combine(websitePath, GetThumbPath(context));
            if (File.Exists(thumbPath))
            {
                context.Response.WriteFile(thumbPath);
                context.Response.End();
                return true;
            }

            //Get path
            string photoFile = context.Request.QueryString["p"];
            string photoPath = Path.Combine(websitePath, photoFile.Replace("/", "\\"));
            Bitmap photo = null;
            try
            {
                photo = new Bitmap(photoPath);
            }
            catch (ArgumentException)
            {
                return false;
            }

            //Get size
            int width = context.Request["w"].ToInt32TryParse();
            int height = context.Request["h"].ToInt32TryParse();
            if (context.Request["relative"] == "1")
            {
                width = photo.Width * width / 100;
                height = photo.Height * height / 100;
            }
            if (width > 0 && height == 0)
            {
                height = photo.Height * width / photo.Width;
            }
            else if (width == 0 && height > 0)
            {
                width = photo.Width * height / photo.Height;
            }
            else if (width == 0 && height == 0)
            {
                // default size
                width = 120;
                height = 100;
            }
            Bitmap target = new Bitmap(width, height);
            using (Graphics graphics = Graphics.FromImage(target))
            {
                graphics.CompositingQuality = CompositingQuality.HighSpeed;
                graphics.InterpolationMode = InterpolationMode.HighQualityBicubic;
                graphics.CompositingMode = CompositingMode.SourceCopy;
                graphics.DrawImage(photo, 0, 0, width, height);

                // save thumbnail to disk
                target.Save(thumbPath, ImageFormat.Png);
                target.Save(context.Response.OutputStream, ImageFormat.Png);
                target.Dispose();
                photo.Dispose();
                context.Response.End();
            }
            return true;
        }
        private string GetThumbPath(HttpContext context)
        {
            var fileName = context.Request.Url.Query.GetHashCode();
            return "_thumbnails" + "\\" + fileName + ".jpg";
        }

        #endregion

        #region Access file
        private void AccessFile(HttpContext context)
        {
            string path = context.Request["path"];
            if (string.IsNullOrWhiteSpace(path))
            {
                ResponseError404(context);
                return;
            }

            string absolutePath = _websitePath + "\\" + path.Replace("/", "\\");

            if (!File.Exists(absolutePath))
            {
                ResponseError404(context);
                return;
            }

            switch (Path.GetExtension(path).ToLower())
            {
                case ".css":
                    context.Response.ContentType = "text/css";
                    break;
                case ".js":
                    context.Response.ContentType = "text/javascript";
                    break;
                case ".woff":
                    context.Response.ContentType = "application/x-font-woff";
                    break;
            }
            context.Response.WriteFile(absolutePath);
        }
        #endregion
    }
}