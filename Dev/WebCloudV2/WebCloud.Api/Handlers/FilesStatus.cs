﻿using System;
using System.Collections.Generic;
using System.IO;
using WebCloud.Common.Enums;

namespace WebCloud.Api.Handlers
{
    public class FilesStatus
    {
        public bool IsSuccess { get; set; }
        public string Name { get; set; }
        public string Type { get; set; }
        public int Size { get; set; }
        public string Url { get; set; }
        public string RelativePath { get; set; }
        public string Error { get; set; }
    }

    public class FileItem
    {
        public string Name { get; set; }
        public string Path { get; set; }
        public string FileType { get; set; }
        public long Size { get; set; }
        public bool IsDirectory { get; set; }
        public SourceType SourceType { set; get; }

        public FileItem()
        {

        }

        public FileItem(FileInfo file, string folder, SourceType sourceType)
        {
            Name = file.Name;
            Path = folder + "/" + file.Name;
            FileType = file.Extension;
            Size = file.Length;
            IsDirectory = false;
            SourceType = sourceType;
        }

        public FileItem(DirectoryInfo dir, string folder, SourceType sourceType)
        {
            Name = dir.Name;
            Path =  folder + "/" + dir.Name;
            FileType = null;
            Size = 0;
            IsDirectory = true;
            SourceType = sourceType;
        }


        // Custom comparer for the Product class
        public class FileItemComparer : IEqualityComparer<FileItem>
        {
            // Products are equal if their names and product numbers are equal.
            public bool Equals(FileItem x, FileItem y)
            {
                if (Object.ReferenceEquals(x, y)) return true;

                if (Object.ReferenceEquals(x, null) || Object.ReferenceEquals(y, null))
                    return false;

                return string.Equals(x.Path, y.Path, StringComparison.InvariantCultureIgnoreCase);
            }

            // If Equals() returns true for a pair of objects 
            // then GetHashCode() must return the same value for these objects.

            public int GetHashCode(FileItem item)
            {
                //Check whether the object is null
                if (Object.ReferenceEquals(item, null)) return 0;

                //Get hash code for the Name field if it is not null.
                return item.Path == null ? 0 : item.Path.GetHashCode();
            }
        }
    }


}