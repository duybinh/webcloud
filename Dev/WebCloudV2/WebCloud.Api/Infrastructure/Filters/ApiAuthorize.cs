﻿using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using WebCloud.Common;
using WebCloud.Common.Constant;

namespace WebCloud.Api.Infrastructure.Filters
{
    public class ApiAuthorizeAttribute : AuthorizeAttribute
    {
        protected override void HandleUnauthorizedRequest(System.Web.Http.Controllers.HttpActionContext actionContext)
        {
            base.HandleUnauthorizedRequest(actionContext);

            //if (HttpContext.Current == null)
            //{
            //    base.HandleUnauthorizedRequest(actionContext);
            //    return;
            //}

            //string authResult = HttpContext.Current.Response.Headers["AuthResult"];
            //if (authResult == MessageCode.Auth_Success)
            //{
            //    authResult = MessageCode.Auth_NotAuthorize;
            //}

            //actionContext.Response = actionContext.Request.CreateResponse(HttpStatusCode.Unauthorized,
            //    new ResultBase
            //    {
            //        IsSuccess = false,
            //        Message = new Message(authResult)
            //    });
        }
    }
}