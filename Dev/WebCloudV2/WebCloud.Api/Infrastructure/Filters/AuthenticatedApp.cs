﻿using System.Net;
using System.Net.Http;
using System.Web;
using WebCloud.Api.Business;
using WebCloud.Api.DataAccess;
using WebCloud.Common;
using WebCloud.Common.Constant;
using ActionFilterAttribute = System.Web.Http.Filters.ActionFilterAttribute;

namespace WebCloud.Api.Infrastructure.Filters
{
    public class AuthenticatedAppAttribute : ActionFilterAttribute
    {
        private readonly IApplicationRepository _applicationRepository = new ApplicationRepository(new ApplicationDataRepository(), new CacheRepository());

        public override void OnActionExecuting(System.Web.Http.Controllers.HttpActionContext actionContext)
        {
            base.OnActionExecuting(actionContext);
            if (HttpContext.Current != null)
            {
                string applicationId = HttpContext.Current.Request.Headers["ApplicationId"];
                string applicationSecret = HttpContext.Current.Request.Headers["ApplicationSecret"];
                bool validApplication = false;
                if (!string.IsNullOrEmpty(applicationId) && !string.IsNullOrEmpty(applicationSecret))
                {
                    if (_applicationRepository.ValidateApplicationSecret(applicationId, applicationSecret))
                    {
                        validApplication = true;
                    }
                }
                if (!validApplication)
                {
                    actionContext.Response = actionContext.Request.CreateResponse(
                                                HttpStatusCode.Unauthorized,
                                                new ResultBase
                                                {
                                                    IsSuccess = false,
                                                    Message = new Message(MessageCode.Auth_UnAuthenticatedApplication)
                                                });
                }
            }
        }
    }
}