﻿using System.Configuration;
using DHSoft.Utility.Common;

namespace WebCloud.Api.Infrastructure.Wrapper
{
    public class WebCloudApiConfiguration
    {
        private static readonly WebCloudApiConfiguration Instance = new WebCloudApiConfiguration();
        private readonly string _authIssuer;
        private readonly string _authAudience;
        private readonly string _authAudienceSecret;

        private WebCloudApiConfiguration()
        {
            _authIssuer = ConfigurationManager.AppSettings["Auth_Issuer"];
            _authAudience = ConfigurationManager.AppSettings["Auth_Audience"];
            _authAudienceSecret = ConfigurationManager.AppSettings["Auth_AudienceSecret"];
        }
        public static string AuthIssuer
        {
            get { return Instance._authIssuer; }
        }

        public static string AuthAudience
        {
            get { return Instance._authAudience; }
        }
        public static string AuthAudienceSecret
        {
            get { return Instance._authAudienceSecret; }
        }
    }
}
