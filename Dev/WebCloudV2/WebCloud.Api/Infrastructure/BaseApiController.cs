﻿using System.Web;
using System.Web.Http;
using WebCloud.Api.Business;

namespace WebCloud.Api.Infrastructure
{
    public class BaseApiController : ApiController
    {
        protected IContext ApiContext { set; get; }

        public BaseApiController()
        {
            if (HttpContext.Current == null) return;
            ApiContext = Business.RequestContext.GetRequestContext(HttpContext.Current.Request);
        }

    }
}