﻿using System;
using System.Web;
using Newtonsoft.Json;
using WebCloud.Common;
using WebCloud.Api.Business;
using WebCloud.Common.Constant;

namespace WebCloud.Api.Infrastructure
{
    public class BaseHttpHandler : IHttpHandler
    {
        protected IContext HandlerContext { set; get; }

        protected virtual void InitHandlerContext(HttpContext context)
        {
            if (context == null) return;
            HandlerContext = RequestContext.GetRequestContext(context.Request);
        }

        protected void ResponseJson(HttpContext context, object data)
        {
            context.Response.ContentType = "application/json";
            context.Response.Write(JsonConvert.SerializeObject(data));
            context.Response.End();
        }

        protected void ResponseError(HttpContext context, string msg, int statusCode = 400)
        {
            context.Response.StatusCode = statusCode;
            ResponseJson(context, new ResultBase { IsSuccess = false, Message = new Message(msg) });
        }

        protected void ResponseError404(HttpContext context)
        {
            ResponseError(context, MessageCode.Resource_NotFound, 404);
        }

        protected void ResponseError401(HttpContext context)
        {
            ResponseError(context, MessageCode.Resource_NoPermissionToAccess, 401);
        }

        protected void ResponseSuccess(HttpContext context, object data)
        {
            context.Response.StatusCode = 200;
            ResponseJson(context, new ResultObject<Object> { IsSuccess = true, Data = data });
        }

        public virtual void ProcessRequest(HttpContext context)
        {
            // add headers
            context.Response.AddHeader("Pragma", "no-cache");
            context.Response.AddHeader("Cache-Control", "private, no-cache");
            context.Response.AddHeader("Access-Control-Allow-Origin", "*");

            if (context.Request.HttpMethod == "OPTIONS")
            {
                context.Response.AddHeader("Allow", "DELETE,GET,HEAD,POST,PUT,OPTIONS");
                context.Response.AddHeader("Access-Control-Allow-Methods", "DELETE,GET,HEAD,POST,PUT,OPTIONS");
                context.Response.AddHeader("Access-Control-Allow-Headers", "authtoken,languageid,websiteid, origin, x-file-size, x-file-name, content-type, accept, x-file-type, content-disposition");
                context.Response.StatusCode = 200;
                context.Response.End();
                return;
            }

            InitHandlerContext(context);
        }

        public virtual bool IsReusable { get; private set; }
    }
}