﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net.Http;
using System.Security.Cryptography;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
using DotLiquid;
using DotLiquid.NamingConventions;
using Microsoft.Owin.Security.DataHandler.Encoder;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using WebCloud.Entity;

namespace WebCloud.Site.Tests
{
    [TestClass]
    public class UnitTest1
    {

        [TestMethod]
        public void GenSecret()
        {
            var clientId = Guid.NewGuid().ToString("N");
            var key = new byte[32];
            RNGCryptoServiceProvider.Create().GetBytes(key);
            var base64Secret = TextEncodings.Base64Url.Encode(key);
            Console.WriteLine(clientId);
            Console.WriteLine(base64Secret);
        }


        [TestMethod]
        public void TestAsyncAwait()
        {
            TestAsync();
        }


        public async void TestAsync()
        {
            Log("TestAsync: 1");
            string text = await WorkAsync();
            Log("TestAsync: 2");
            Log("TestAsync: " + text);
            Log("TestAsync: 3");
        }

        public async Task<string> WorkAsync()
        {
            Log("WorkAsync: 1");
            await Task.Run(new Action(LongDelay));
            Log("WorkAsync: 2");
            Log("WorkAsync: 3");
            return "Finish async ";
        }

        public static void LongDelay()
        {
            Thread.Sleep(2000);
        }
        public void Log(string text)
        {
            Console.WriteLine(DateTime.Now.ToLongTimeString() + ":" + text);
        }

        [TestMethod]
        public void TestPath()
        {
            string p1 = "C://folder";
            string p2 = "file/file.txt";
            Console.WriteLine(Path.Combine(p1, p2));
        }

        [TestMethod]
        public void TestSplit()
        {
            string str = "theme\\default\\css\\site.css";
            string[] subs = str.Split(new[] { '\\' }, 3);
            foreach (var sub in subs)
            {
                Console.WriteLine(sub);
            }
        }




        [TestMethod]
        public void TestSectionRegex()
        {
            string testString = "saff  {% section top %} eff {% section left %} ggg";
            Regex r = new Regex(@"\{\%\s*section\s+(?<section>\w+)\s*\%\}");
            var matches = r.Matches(testString);
            Console.WriteLine(matches.Count);
            foreach (Match match in matches)
            {
                Console.WriteLine(match);
                foreach (Group group in match.Groups)
                {
                    Console.WriteLine(group.Value);
                }
                Console.WriteLine(match.Groups["section"]);
                Console.WriteLine(match.Groups["section1"].Value);
            }
        }
        [TestMethod]
        public void TestDictionary()
        {
            var dict = new Dictionary<string, string>();
            dict["a"] = "a1";
            Console.WriteLine(dict["a"]);
            Console.WriteLine(dict["b"]);
        }

        [TestMethod]
        public void RenderString()
        {
            Template template = Template.Parse("hello {{name}}");
            //string str = template.Render(Hash.FromAnonymousObject(new { name = "binh" }));
            string str = template.Render(Hash.FromDictionary(new Dictionary<string, object>()
            {
                {"name","binh"}
            }));
            Console.WriteLine(str);
        }

        [TestMethod]
        public void RenderObject()
        {
            Template.NamingConvention = new CSharpNamingConvention();
            //Template.RegisterSafeType(typeof(User), new[] { "UserName", "Email","Gender" });
            //Template template = Template.Parse("username: {{user_UserName}}, email: {{user.Email}}, Gender: {% if user.Gender == true %} Male {% else %} Female {% endif %}");
            //string str = template.Render(Hash.FromAnonymousObject(
            //                            new
            //                            {
            //                                user = new User
            //                                    {
            //                                        UserName = "binhpro",
            //                                        Email = "binhpro@gmail.com",
            //                                        Gender = true
            //                                    }
            //                            }));

            //string str = template.Render(Hash.FromDictionary(new Dictionary<string, object>()
            //{
            //    {"user_UserName", "binhpro"},
            //    {"user.Email", "binhpro@gmail.com"},
            //    {"user.Gender", true},
            //}));

            Template.RegisterSafeType(typeof(Article), new[] { "Content" });
            Template.RegisterSafeType(typeof(ArticleContent), new[] { "Title", "Description", "Content" });
            Template template = Template.Parse("title: {{CurrentArticle.Content.Title}}, description:  {{CurrentArticle.Content.Description}}, content:  {{CurrentArticle.Content.Content}} ");
            string str = template.Render(Hash.FromAnonymousObject(
                                        new
                                        {
                                            CurrentArticle = new Article
                                                {
                                                    Content = new ArticleContent()
                                                    {
                                                        Title = "Test Title",
                                                        Description = "Test Description",
                                                        Content = "Test Content"
                                                    }
                                                }
                                        }));

            Console.WriteLine(str);
        }

        public class ConfigDrop : Drop
        {
            private static readonly Dictionary<string, object> Values = new Dictionary<string, object>
            {
                {"Theme", "DefaultTheme"},
                {"Lang", "vi"},
                {"IsDemo", true},
            };

            public override object BeforeMethod(string method)
            {
                Console.WriteLine("BeforeMethod: " + method);

                if (Values.ContainsKey(method))
                    return Values[method];
                return "unknown";
            }
        }

        [TestMethod]
        public void RenderConfig()
        {
            Template template = Template.Parse("Theme: {{config.Theme}}, Lang: {{config.Lang}}, Is demo: {% if config.IsDemo == true %} Yes {% else %} No {% endif %}, {{config.ok}}");
            string str = template.Render(Hash.FromAnonymousObject(new { config = new ConfigDrop() }));
            Console.WriteLine(str);
        }

        public class User
        {
            public string UserName { set; get; }
            public string Email { set; get; }
            public bool Gender { set; get; }
        }
    }
}
