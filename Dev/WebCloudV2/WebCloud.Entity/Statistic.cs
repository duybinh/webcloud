using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace WebCloud.Entity
{
    [Serializable]
    [DataContract(IsReference = true)]
    public partial class Statistic
    {
        public Statistic()
        {
            this.Articles = new List<Article>();
            this.Products = new List<Product>();
        }

        [DataMember]
        public long StatisticID { get; set; }
        [DataMember]
        public long ViewCount { get; set; }
        [DataMember]
        public long LikeCount { get; set; }
        [DataMember]
        public long DislikeCount { get; set; }
        [DataMember]
        public Nullable<System.DateTime> LastUpdatedDatetime { get; set; }
        [DataMember]
        public string LikeUserIDs { get; set; }
        [DataMember]
        public string DislikeUserIDs { get; set; }
        [DataMember]
        public string ViewUserIDs { get; set; }
        [DataMember]
        public string ViewUserIPs { get; set; }
        [DataMember]
        public string ReferenceTable { get; set; }
        [DataMember]
        public string ItemID { get; set; }
        [DataMember]
        public string WebsiteID { get; set; }
        public virtual ICollection<Article> Articles { get; set; }
        public virtual ICollection<Product> Products { get; set; }
    }
}
