using System;
using System.Collections.Generic;

namespace WebCloud.Entity
{
    public partial class Discussion
    {
        public long DiscussionID { get; set; }
        public long DiscussionGroupID { get; set; }
        public string Title { get; set; }
        public string Content { get; set; }
        public string Avarta { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
        public Nullable<long> IsReplyOf { get; set; }
        public Nullable<int> Rate { get; set; }
        public int LikeCount { get; set; }
        public int DislikeCount { get; set; }
        public bool IsApproved { get; set; }
        public bool IsEnabled { get; set; }
        public bool IsDeleted { get; set; }
        public System.DateTime CreatedDatetime { get; set; }
        public System.DateTime LastEditedDatetime { get; set; }
        public string CreatingUser { get; set; }
        public string LastEditedUser { get; set; }
        public virtual DiscussionGroup DiscussionGroup { get; set; }
    }
}
