using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace WebCloud.Entity
{
    [Serializable]
    [DataContract]
    public partial class Language
    {
        public Language()
        {
            this.ArticleCategoryContents = new List<ArticleCategoryContent>();
            this.ArticleContents = new List<ArticleContent>();
            this.LanguageInWebsites = new List<LanguageInWebsite>();
            this.MenuItemContents = new List<MenuItemContent>();
            this.ProductCategoryContents = new List<ProductCategoryContent>();
            this.ProductContents = new List<ProductContent>();
            this.ProductPropertyContents = new List<ProductPropertyContent>();
            this.ProductPropertyValues = new List<ProductPropertyValue>();
            this.ResourceValues = new List<ResourceValue>();
            this.Tags = new List<Tag>();
            this.Texts = new List<Text>();
            this.Users = new List<User>();
        }

        [DataMember]
        public string LanguageID { get; set; }
        
        [DataMember]
        public string TranslateCode { get; set; }

        [DataMember]
        public string Name { get; set; }

        [DataMember]
        public string DisplayName { get; set; }

        [DataMember]
        public string Image { get; set; }

        [DataMember]
        public bool IsEnabled { get; set; }

        [DataMember]
        public int SortOrder { get; set; }
        public virtual ICollection<ArticleCategoryContent> ArticleCategoryContents { get; set; }
        public virtual ICollection<ArticleContent> ArticleContents { get; set; }
        public virtual ICollection<LanguageInWebsite> LanguageInWebsites { get; set; }

        public virtual ICollection<MenuItemContent> MenuItemContents { get; set; }
        public virtual ICollection<ProductCategoryContent> ProductCategoryContents { get; set; }
        public virtual ICollection<ProductContent> ProductContents { get; set; }
        public virtual ICollection<ProductPropertyContent> ProductPropertyContents { get; set; }
        public virtual ICollection<ProductPropertyValue> ProductPropertyValues { get; set; }
        public virtual ICollection<ResourceValue> ResourceValues { get; set; }
        public virtual ICollection<Tag> Tags { get; set; }
        public virtual ICollection<Text> Texts { get; set; }
        public virtual ICollection<User> Users { get; set; }
    }
}
