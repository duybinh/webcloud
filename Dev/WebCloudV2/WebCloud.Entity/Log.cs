using System;
using System.Collections.Generic;

namespace WebCloud.Entity
{
    public partial class Log
    {
        public long LogID { get; set; }
        public long LogGroupID { get; set; }
        public System.DateTime ActionDatetime { get; set; }
        public string Action { get; set; }
        public string ActionDetail { get; set; }
        public Nullable<long> Actor { get; set; }
        public virtual LogGroup LogGroup { get; set; }
    }
}
