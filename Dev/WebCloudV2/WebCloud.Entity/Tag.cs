using System;
using System.Collections.Generic;

namespace WebCloud.Entity
{
    public partial class Tag
    {
        public Tag()
        {
            this.Articles = new List<Article>();
            this.Products = new List<Product>();
        }

        public long TagID { get; set; }
        public string Value { get; set; }
        public string SeoValue { get; set; }
        public long SearchCount { get; set; }
        public Nullable<System.DateTime> LastSearchDatetime { get; set; }
        public long SortOrder { get; set; }
        public string LanguageID { get; set; }
        public virtual Language Language { get; set; }
        public virtual ICollection<Article> Articles { get; set; }
        public virtual ICollection<Product> Products { get; set; }
    }
}
