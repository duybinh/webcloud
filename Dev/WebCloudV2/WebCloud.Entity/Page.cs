using System;
using System.Collections.Generic;

namespace WebCloud.Entity
{
    public partial class Page
    {
        public Page()
        {
            this.Modules = new List<Module>();
        }

        public string PageID { get; set; }
        public string WebsiteID { get; set; }
        public string ParentID { get; set; }
        public string LayoutID { get; set; }
        public string Group { get; set; }

        [LocalizationField]
        public string Name { get; set; }

        [LocalizationField]
        public string SeoName { get; set; }
        
        [LocalizationField]
        public string MetaTitle { get; set; }

        [LocalizationField]
        public string MetaKeyword { get; set; }
        
        [LocalizationField]
        public string MetaDescription { get; set; }
        public int SortOrder { get; set; }
        public bool IsEnabled { get; set; }
        public bool IsGroup { get; set; }
        public bool IsDeleted { get; set; }
        public virtual ICollection<Module> Modules { get; set; }
        public virtual Website Website { get; set; }
    }
}
