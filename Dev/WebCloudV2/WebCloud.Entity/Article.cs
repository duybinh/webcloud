using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace WebCloud.Entity
{
    [Serializable]
    [DataContract(IsReference = true)]
    public partial class Article
    {
        public Article()
        {
            this.ArticleContents = new List<ArticleContent>();
            this.ArticleInCategories = new List<ArticleInCategory>();
            this.Tags = new List<Tag>();
            this.CategoryIds = new List<string>();
            this.TagValues = new List<string>();
        }
        [DataMember]
        public string WebsiteID { get; set; }
        [DataMember]
        public string ArticleID { get; set; }
        [DataMember]
        public long GlobalID { get; set; }
        [DataMember]
        public string ThumbImage { get; set; }
        [DataMember]
        public long SortOrder { get; set; }
        [DataMember]
        public bool HaveVideo { get; set; }
        [DataMember]
        public bool HavePhoto { get; set; }
        [DataMember]
        public bool HaveAttachFile { get; set; }
        [DataMember]
        public bool IsHot { get; set; }
        [DataMember]
        public bool IsApproved { get; set; }
        [DataMember]
        public bool AllowComment { get; set; }
        [DataMember]
        public System.DateTime DisplayDatetime { get; set; }
        [DataMember]
        public bool IsGlobalDisplay { get; set; }
        [DataMember]
        public bool IsEnabled { get; set; }
        [DataMember]
        public bool IsDeleted { get; set; }
        [DataMember]
        public Nullable<long> DiscussionGroupID { get; set; }
        [DataMember]
        public Nullable<long> StatisticID { get; set; }
        [DataMember]
        public Nullable<long> LogGroupID { get; set; }
        [DataMember]
        public System.DateTime CreatedDatetime { get; set; }
        [DataMember]
        public System.DateTime LastEditedDatetime { get; set; }
        [DataMember]
        public string CreatedUserName { get; set; }
        [DataMember]
        public string LastEditedUserName { get; set; }
        [DataMember]
        public Nullable<long> CreatedUserID { get; set; }
        [DataMember]
        public Nullable<long> LastEditedUserID { get; set; }

        [DataMember]
        public  DiscussionGroup DiscussionGroup { get; set; }
        public virtual LogGroup LogGroup { get; set; }
        
        [DataMember]
        public virtual Statistic Statistic { get; set; }
        public virtual Website Website { get; set; }
        public virtual ICollection<ArticleContent> ArticleContents { get; set; }
        public virtual ICollection<ArticleInCategory> ArticleInCategories { get; set; }
        public virtual ICollection<Tag> Tags { get; set; }
    }
}
