using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace WebCloud.Entity
{
    [Serializable]
    [DataContract(IsReference = true)]
    public partial class ModuleBaseConfig
    {
        public ModuleBaseConfig()
        {
            this.ModuleConfigs = new List<ModuleConfig>();
        }

        [DataMember]
        public string ModuleBaseConfigID { get; set; }

        [DataMember]
        public string ModuleBaseID { get; set; }

        [DataMember]
        public string Name { get; set; }

        [DataMember]
        public string Description { get; set; }

        [DataMember]
        public string Hint { get; set; }

        [DataMember]
        public string DataType { get; set; }

        [DataMember]
        public string DefaultValue { get; set; }

        [DataMember]
        public string RolesToConfig { get; set; }

        [DataMember]
        public int SortOrder { get; set; }

        [DataMember]
        public bool IsEnabled { get; set; }

        [DataMember]
        public bool AllLanguage { get; set; }
        
        public virtual ModuleBase ModuleBase { get; set; }
        public virtual ICollection<ModuleConfig> ModuleConfigs { get; set; }
    }
}
