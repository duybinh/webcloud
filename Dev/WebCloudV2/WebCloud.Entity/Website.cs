using System;
using System.Collections.Generic;

namespace WebCloud.Entity
{
    public partial class Website
    {
        public Website()
        {
            this.Articles = new List<Article>();
            this.ArticleCategories = new List<ArticleCategory>();
            this.ConfigInWebsites = new List<ConfigInWebsite>();
            this.LanguageInWebsites = new List<LanguageInWebsite>();
            this.Layouts = new List<Layout>();
            this.MenuItems = new List<MenuItem>();
            this.Modules = new List<Module>();
            this.ModuleBaseInWebsites = new List<ModuleBaseInWebsite>();
            this.Pages = new List<Page>();
            this.Products = new List<Product>();
            this.ProductCategories = new List<ProductCategory>();
            this.ProductFilters = new List<ProductFilter>();
            this.ProductProperties = new List<ProductProperty>();
            this.Resources = new List<Resource>();
            this.Texts = new List<Text>();
            this.Users = new List<User>();
            this.UserGroups = new List<UserGroup>();
            this.UserRoles = new List<UserRole>();
        }

        public string WebsiteID { get; set; }
        public Nullable<int> PlanID { get; set; }
        public Nullable<int> GroupID { get; set; }
        public string WebsiteName { get; set; }
        public string Description { get; set; }
        public string AdminEmail { get; set; }
        public string DefaultTheme { get; set; }
        public string DefaultLanguage { get; set; }
        public string ContactName { get; set; }
        public string ContactPhone { get; set; }
        public string City { get; set; }
        public string Address { get; set; }
        public bool IsApproved { get; set; }
        public bool IsEnabled { get; set; }
        public bool IsDeleted { get; set; }
        public bool IsDemo { get; set; }
        public bool IsTrial { get; set; }
        public Nullable<System.DateTime> ExpiredDatetime { get; set; }
        public System.DateTime CreatedDatetime { get; set; }
        public virtual ICollection<Article> Articles { get; set; }
        public virtual ICollection<ArticleCategory> ArticleCategories { get; set; }
        public virtual ICollection<ConfigInWebsite> ConfigInWebsites { get; set; }
        public virtual ICollection<LanguageInWebsite> LanguageInWebsites { get; set; }
        public virtual ICollection<Layout> Layouts { get; set; }
        public virtual ICollection<MenuItem> MenuItems { get; set; }
        public virtual ICollection<Module> Modules { get; set; }
        public virtual ICollection<ModuleBaseInWebsite> ModuleBaseInWebsites { get; set; }
        public virtual ICollection<Page> Pages { get; set; }
        public virtual ICollection<Product> Products { get; set; }
        public virtual ICollection<ProductCategory> ProductCategories { get; set; }
        public virtual ICollection<ProductFilter> ProductFilters { get; set; }
        public virtual ICollection<ProductProperty> ProductProperties { get; set; }
        public virtual ICollection<Resource> Resources { get; set; }
        public virtual ICollection<Text> Texts { get; set; }
        public virtual ICollection<User> Users { get; set; }
        public virtual ICollection<UserGroup> UserGroups { get; set; }
        public virtual WebGroup WebGroup { get; set; }
        public virtual WebPlan WebPlan { get; set; }
        public virtual ICollection<UserRole> UserRoles { get; set; }
    }
}
