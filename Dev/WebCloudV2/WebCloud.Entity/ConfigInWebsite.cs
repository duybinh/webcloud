using System;
using System.Collections.Generic;

namespace WebCloud.Entity
{
    public partial class ConfigInWebsite
    {
        public string WebsiteID { get; set; }
        public string ConfigID { get; set; }
        public string Value { get; set; }
        public virtual Config Config { get; set; }
        public virtual Website Website { get; set; }
    }
}
