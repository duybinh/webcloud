using System;
using System.Collections.Generic;

namespace WebCloud.Entity
{
    public partial class LanguageInWebsite
    {
        public string LanguageID { get; set; }
        public string WebsiteID { get; set; }
        public bool IsEnabled { get; set; }
        public int SortOrder { get; set; }
        public Nullable<System.DateTime> InstalledDate { get; set; }
        public string InstalledUserName { get; set; }
        public Nullable<long> InstalledUserID { get; set; }
        public virtual Language Language { get; set; }
        public virtual Website Website { get; set; }
    }
}
