using System;
using System.Collections.Generic;

namespace WebCloud.Entity
{
    public partial class ProductCategoryContent
    {
        public string WebsiteID { get; set; }
        public string LanguageID { get; set; }
        public string ProductCategoryID { get; set; }
        public string Name { get; set; }
        public string SeoName { get; set; }
        public string Description { get; set; }
        public string MetaTitle { get; set; }
        public string MetaKeyword { get; set; }
        public string MetaDescription { get; set; }
        public virtual Language Language { get; set; }
        public virtual ProductCategory ProductCategory { get; set; }
    }
}
