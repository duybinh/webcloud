using System;
using System.Collections.Generic;

namespace WebCloud.Entity
{
    public partial class UserGroup
    {
        public UserGroup()
        {
            this.UserInGroups = new List<UserInGroup>();
        }

        public string WebsiteID { get; set; }
        public string UserGroupID { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public bool IsEnabled { get; set; }
        public bool IsDeleted { get; set; }
        public virtual Website Website { get; set; }
        public virtual ICollection<UserInGroup> UserInGroups { get; set; }
    }
}
