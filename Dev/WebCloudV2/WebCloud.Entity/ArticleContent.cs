using System;
using System.Collections.Generic;

namespace WebCloud.Entity
{
    public partial class ArticleContent
    {
        public string WebsiteID { get; set; }
        public string ArticleID { get; set; }
        public string LanguageID { get; set; }
        public string Title { get; set; }
        public string SeoTitle { get; set; }
        public string Description { get; set; }
        public string Content { get; set; }
        public string MetaTitle { get; set; }
        public string MetaKeyword { get; set; }
        public string MetaDescription { get; set; }
        public virtual Article Article { get; set; }
        public virtual Language Language { get; set; }
    }
}
