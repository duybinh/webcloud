using System;
using System.Collections.Generic;

namespace WebCloud.Entity
{
    public partial class UserInGroup
    {
        public string WebsiteID { get; set; }
        public string UserGroupID { get; set; }
        public string UserID { get; set; }
        public virtual User User { get; set; }
        public virtual UserGroup UserGroup { get; set; }
    }
}
