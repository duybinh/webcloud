using System;
using System.Collections.Generic;

namespace WebCloud.Entity
{
    public partial class ProductPropertyContent
    {
        public string WebsiteID { get; set; }
        public string LanguageID { get; set; }
        public string ProductPropertyID { get; set; }
        public string Name { get; set; }
        public string Unit { get; set; }
        public string DefaultTextValue { get; set; }
        public string ValueRange { get; set; }
        public virtual Language Language { get; set; }
        public virtual ProductProperty ProductProperty { get; set; }
    }
}
