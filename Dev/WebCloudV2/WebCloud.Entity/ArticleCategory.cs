using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace WebCloud.Entity
{
    [Serializable]
    [DataContract(IsReference = true)]
    public partial class ArticleCategory
    {
        public ArticleCategory()
        {
            this.ArticleCategoryContents = new List<ArticleCategoryContent>();
            this.ArticleInCategories = new List<ArticleInCategory>();
        }
        [DataMember]
        public string WebsiteID { get; set; }
        [DataMember]
        public string ArticleCategoryID { get; set; }
        [DataMember]
        public long GlobalID { get; set; }
        [DataMember]
        public string ParentID { get; set; }
        [DataMember]
        public string ThumbImage { get; set; }
        [DataMember]
        public bool DisplayArticleThumb { get; set; }
        [DataMember]
        public bool IsHot { get; set; }
        [DataMember]
        public bool AllowComment { get; set; }
        [DataMember]
        public bool IsEnabled { get; set; }
        [DataMember]
        public bool IsDeleted { get; set; }
        [DataMember]
        public int SortOrder { get; set; }
        [DataMember]
        public bool IsLeaf { get; set; }
        [DataMember]
        public Nullable<long> LogGroupID { get; set; }
        [DataMember]
        public System.DateTime CreatedDatetime { get; set; }
        [DataMember]
        public System.DateTime LastEditedDatetime { get; set; }
        [DataMember]
        public string CreatedUserName { get; set; }
        [DataMember]
        public string LastEditedUserName { get; set; }
        [DataMember]
        public Nullable<long> CreatedUserID { get; set; }
        [DataMember]
        public Nullable<long> LastEditedUserID { get; set; }
        public virtual LogGroup LogGroup { get; set; }
        public virtual Website Website { get; set; }
        public virtual ICollection<ArticleCategoryContent> ArticleCategoryContents { get; set; }
        public virtual ArticleCategoryRelationship ArticleCategoryRelationship { get; set; }
        public virtual ICollection<ArticleInCategory> ArticleInCategories { get; set; }
    }
}
