using System;
using System.Collections.Generic;

namespace WebCloud.Entity
{
    public partial class MenuItemContent
    {
        public string WebsiteID { get; set; }
        public string LanguageID { get; set; }
        public string MenuItemID { get; set; }
        public string Title { get; set; }
        public string Content { get; set; }
        public virtual Language Language { get; set; }
        public virtual MenuItem MenuItem { get; set; }
    }
}
