using System;
using System.Collections.Generic;

namespace WebCloud.Entity
{
    public partial class Application
    {
        public string ApplicationId { get; set; }
        public string Name { get; set; }
        public string Url { get; set; }
        public string SecretKey { get; set; }
    }
}
