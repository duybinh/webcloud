using System;
using System.Collections.Generic;

namespace WebCloud.Entity
{
    public partial class User
    {
        public User()
        {
            this.UserInGroups = new List<UserInGroup>();
            this.UserOAuthMemberships = new List<UserOAuthMembership>();
            this.UserRoles = new List<UserRole>();
        }

        public string WebsiteID { get; set; }
        public string UserID { get; set; }
        public string LanguageID { get; set; }
        public long GlobalID { get; set; }
        public string Password { get; set; }
        public string Email { get; set; }
        public string DisplayName { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public Nullable<System.DateTime> Birthday { get; set; }
        public Nullable<bool> Gender { get; set; }
        public string Avatar { get; set; }
        public string EmailVerificationKey { get; set; }
        public bool IsVerifiedEmail { get; set; }
        public Nullable<System.DateTime> EmailVerificationExpried { get; set; }
        public bool IsLocked { get; set; }
        public Nullable<System.DateTime> LockExpried { get; set; }
        public bool IsDeleted { get; set; }
        public System.DateTime CreatedDateTime { get; set; }
        public virtual Language Language { get; set; }
        public virtual Website Website { get; set; }
        public virtual ICollection<UserInGroup> UserInGroups { get; set; }
        public virtual ICollection<UserOAuthMembership> UserOAuthMemberships { get; set; }
        public virtual ICollection<UserRole> UserRoles { get; set; }
    }
}
