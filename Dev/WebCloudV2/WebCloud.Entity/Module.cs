using System;
using System.Collections.Generic;

namespace WebCloud.Entity
{
    public partial class Module
    {
        public Module()
        {
            this.ModuleConfigs = new List<ModuleConfig>();
        }
        public string WebsiteID { get; set; }
        public string ModuleID { get; set; }
        public string ModuleBaseID { get; set; }
        public string PageID { get; set; }
        public string Section { get; set; }
        public string Name { get; set; }
        public bool OnAllPage { get; set; }
        public bool IsBasic { get; set; }
        public bool IsEnabled { get; set; }
        public int SortOrder { get; set; }
        public virtual ModuleBase ModuleBase { get; set; }
        public virtual Page Page { get; set; }
        public virtual Website Website { get; set; }
        public virtual ICollection<ModuleConfig> ModuleConfigs { get; set; }
    }
}
