namespace WebCloud.Entity
{
    public partial class ProductCategoryRelationship
    {
        public string WebsiteID { get; set; }
        public string ParentID { get; set; }
        public string ChildID { get; set; }
        public int Level { get; set; }
        public virtual ProductCategory ProductCategory { get; set; }
    }
}
