using System;
using System.Collections.Generic;

namespace WebCloud.Entity
{
    public partial class ResourceValue
    {
        public long ResourceValueID { get; set; }
        public string ResourceID { get; set; }
        public string WebsiteID { get; set; }
        public string LanguageID { get; set; }
        public string Value { get; set; }
        public virtual Language Language { get; set; }
    }
}
