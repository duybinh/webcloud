using System;
using System.Collections.Generic;

namespace WebCloud.Entity
{
    public partial class ProductProperty
    {
        public ProductProperty()
        {
            this.ProductPropertyContents = new List<ProductPropertyContent>();
            this.ProductPropertyValues = new List<ProductPropertyValue>();
        }

        public string WebsiteID { get; set; }
        public string ProductPropertyID { get; set; }
        public string DataType { get; set; }
        public Nullable<decimal> DefaultNumberValue { get; set; }
        public Nullable<bool> DefaultBooleanValue { get; set; }
        public Nullable<System.DateTime> DefaultDatetimeValue { get; set; }
        public string AppliedCategoryIDs { get; set; }
        public bool ApplyToAllProduct { get; set; }
        public bool AllowAdvanceSearch { get; set; }
        public bool AllowFilter { get; set; }
        public int SortOrder { get; set; }
        public bool IsEnabled { get; set; }
        public bool AllLanguage { get; set; }
        public virtual Website Website { get; set; }
        public virtual ICollection<ProductPropertyContent> ProductPropertyContents { get; set; }
        public virtual ICollection<ProductPropertyValue> ProductPropertyValues { get; set; }
    }
}
