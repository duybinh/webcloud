using System;
using System.Collections.Generic;

namespace WebCloud.Entity
{
    public partial class UserOAuthMembership
    {
        public long ID { get; set; }
        public string WebsiteID { get; set; }
        public string UserID { get; set; }
        public int OAuthProviderID { get; set; }
        public string ProviderUserID { get; set; }
        public virtual OAuthProvider OAuthProvider { get; set; }
        public virtual User User { get; set; }
    }
}
