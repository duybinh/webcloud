﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace WebCloud.Entity
{
    public partial class Page
    {

        [NotMapped]
        public bool IsSystemPage { set; get; }

        [NotMapped]
        public bool Installable { set; get; }

        [NotMapped]
        public int Level { set; get; }

        [NotMapped]
        public Page Parent { set; get; }

        [NotMapped]
        public List<Page> Childs { set; get; }

        [NotMapped]
        public LocalizationString NameString { set; get; }
        
        [NotMapped]
        public LocalizationString SeoNameString { get; set; }
        
        [NotMapped]
        public LocalizationString MetaTitleString { get; set; }

        [NotMapped]
        public LocalizationString MetaKeywordString { get; set; }

        [NotMapped]
        public LocalizationString MetaDescriptionString { get; set; }
    }
}
