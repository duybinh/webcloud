﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace WebCloud.Entity
{
    public partial class Resource
    {
        [NotMapped]
        public string Value { get; set; }

        [NotMapped]
        public Dictionary<string, string> ValueInLanguages { set; get; }

        [NotMapped]
        public bool IsSystemResource { set; get; }
    }
}
