﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Runtime.Serialization;

namespace WebCloud.Entity
{

    public partial class Module
    {
        public Module DeepCopy()
        {
            var copy = (Module)this.MemberwiseClone();
            if (this.ModuleBase != null)
                copy.ModuleBase = this.ModuleBase.ShallowCopy();
            return copy;
        }
    }

    public partial class ModuleBase
    {
        public ModuleBase ShallowCopy()
        {
            return (ModuleBase)this.MemberwiseClone();
        }

        [NotMapped]
        [DataMember]
        public bool Installed { set; get; }

        [NotMapped]
        [DataMember]
        public bool AllowInstall { set; get; }

    }
    public partial class ModuleBaseConfig
    {
        [NotMapped]
        public LocalizationString DefaultValueString { set; get; }
    }

    public partial class ModuleConfig
    {
        [NotMapped]
        public LocalizationString ValueString { set; get; }
    }

}
