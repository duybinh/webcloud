﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;
using System.Threading.Tasks;

namespace WebCloud.Entity
{
    public partial class MenuItem
    {
        [NotMapped]
        public MenuItemContent Content { set; get; }

        [NotMapped]
        public MenuItem Parent { set; get; }

        [NotMapped]
        public List<MenuItem> Childs { set; get; }
    }
}
