﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using WebCloud.Common.Enums;

namespace WebCloud.Entity
{

    public partial class Layout : OverridableResource
    {
    }

    [DataContract]
    [Serializable]
    public class OverridableResource
    {
        [NotMapped]
        [DataMember]
        public SourceType SourceType { set; get; }
    }
}
