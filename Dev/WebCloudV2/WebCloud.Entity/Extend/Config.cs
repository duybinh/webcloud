﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Runtime.Serialization;

namespace WebCloud.Entity
{
    public partial class Article
    {
        [DataMember]
        [NotMapped]
        public ArticleContent Content { set; get; }

        [DataMember]
        [NotMapped]
        public List<string> CategoryIds { set; get; }

        [DataMember]
        [NotMapped]
        public List<string> TagValues { set; get; }
    }

}
