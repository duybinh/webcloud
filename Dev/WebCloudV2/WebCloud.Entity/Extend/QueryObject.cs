﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace WebCloud.Entity
{
    public interface IUriParamter
    {

    }

    [Serializable]
    public class QueryObject : IUriParamter
    {
        public const string DESC = "DESC";
        public const string ASC = "ASC";

        public QueryObject()
        {
            PageSize = 10;
            Page = 1;
            SortDirection = ASC;
        }

        [DataMember]
        public string SortField { set; get; }

        [DataMember]
        public string SortDirection { set; get; }

        [DataMember]
        public int? PageSize { set; get; }

        [DataMember]
        public int? Page { set; get; }
    }


    public class ArticleQuery : QueryObject
    {
        public ArticleQuery()
        {
            Title = null;
            Category = null;
            IsEnabled = null;
            IsApproved = null;
            IsHot = null;
            IsGlobalDisplay = null;
            CategoryIds = new List<string>();
            SortField = "GlobalID";
            SortDirection = DESC;
        }

        [DataMember]
        public string Title { set; get; }

        [DataMember]
        public string Category { set; get; }

        [DataMember]
        public bool? IsEnabled { set; get; }

        [DataMember]
        public bool? IsApproved { set; get; }

        [DataMember]
        public bool? IsHot { set; get; }

        [DataMember]
        public bool? IsGlobalDisplay { set; get; }

        [DataMember]
        public List<string> CategoryIds { set; get; }
    }
}
