﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Runtime.Serialization;

namespace WebCloud.Entity
{
    public partial class Config
    {
        [DataMember]
        [NotMapped]
        public object Value { set; get; }

    }

}
