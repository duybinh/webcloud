﻿using System.ComponentModel.DataAnnotations.Schema;
using System.Runtime.Serialization;

namespace WebCloud.Entity
{
    public partial class ArticleCategory
    {
        [DataMember]
        [NotMapped]
        public ArticleCategoryContent Content { set; get; }

        [DataMember]
        [NotMapped]
        public int ArticleCount { set; get; }
    }
}
