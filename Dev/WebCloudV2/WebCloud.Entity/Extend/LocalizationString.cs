﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace WebCloud.Entity
{
    public class LocalizationString
    {
        public LocalizationString()
        {
            DefaultValue = "";
            ValueInLanguages = new Dictionary<string, string>();
        }

        public LocalizationString(string defaultValue)
        {
            DefaultValue = defaultValue;
            ValueInLanguages = new Dictionary<string, string>();
        }

        public LocalizationString(string defaultValue, Dictionary<string, string> valueInLanguages)
        {
            DefaultValue = defaultValue;
            ValueInLanguages = valueInLanguages;
        }

        public string DefaultValue { set; get; }
        public Dictionary<string, string> ValueInLanguages { set; get; }

        public void SetValue(string value, string languageId = null)
        {
            if (languageId == null)
                DefaultValue = value;
            else
            {
                ValueInLanguages[languageId] = value;
            }

        }

        public string GetValue(string languageId)
        {
            if (languageId != null && ValueInLanguages.ContainsKey(languageId))
                return ValueInLanguages[languageId];

            return DefaultValue;
        }
    }

    public class LocalizationField : Attribute
    {
    }
}
