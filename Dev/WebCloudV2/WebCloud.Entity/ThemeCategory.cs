using System;
using System.Collections.Generic;

namespace WebCloud.Entity
{
    public partial class ThemeCategory
    {
        public ThemeCategory()
        {
            this.Themes = new List<Theme>();
        }

        public int ThemeCategoryID { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public bool IsEnable { get; set; }
        public int SortOrder { get; set; }
        public virtual ICollection<Theme> Themes { get; set; }
    }
}
