using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace WebCloud.Entity
{
    [Serializable]
    [DataContract]
    public partial class Layout
    {
        [DataMember]
        public string LayoutID { get; set; }

        [DataMember]
        public string WebsiteID { get; set; }

        [DataMember]
        public string Name { get; set; }

        [DataMember]
        public string Description { get; set; }

        [DataMember]
        public string Content { get; set; }

        [DataMember]
        public int SortOrder { get; set; }

        [DataMember]
        public bool IsEnabled { get; set; }

        [DataMember]
        public int Type { get; set; }

        public virtual Website Website { get; set; }
    }
}
