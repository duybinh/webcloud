using System;
using System.Collections.Generic;

namespace WebCloud.Entity
{
    public partial class LogGroup
    {
        public LogGroup()
        {
            this.Articles = new List<Article>();
            this.ArticleCategories = new List<ArticleCategory>();
            this.Logs = new List<Log>();
            this.Products = new List<Product>();
        }

        public long LogGroupID { get; set; }
        public string ReferenceTable { get; set; }
        public string ItemID { get; set; }
        public string WebsiteID { get; set; }
        public virtual ICollection<Article> Articles { get; set; }
        public virtual ICollection<ArticleCategory> ArticleCategories { get; set; }
        public virtual ICollection<Log> Logs { get; set; }
        public virtual ICollection<Product> Products { get; set; }
    }
}
