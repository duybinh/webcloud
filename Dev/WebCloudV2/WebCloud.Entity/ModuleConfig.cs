using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace WebCloud.Entity
{
    [Serializable]
    [DataContract(IsReference = true)]

    public partial class ModuleConfig
    {
        [DataMember]
        public string WebsiteID { get; set; }
        
        [DataMember]
        public string ModuleID { get; set; }

        [DataMember]
        public string ModuleBaseConfigID { get; set; }

        [DataMember]
        public string ModuleBaseID { get; set; }

        [DataMember]
        public string Value { get; set; }
        
        public virtual Module Module { get; set; }
        public virtual ModuleBaseConfig ModuleBaseConfig { get; set; }
    }
}
