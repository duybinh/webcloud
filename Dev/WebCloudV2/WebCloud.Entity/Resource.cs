using System;
using System.Collections.Generic;

namespace WebCloud.Entity
{
    public partial class Resource
    {
        public string ResourceID { get; set; }
        public string WebsiteID { get; set; }
        public string Group { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public bool DisplayAsHtml { get; set; }
        public string RolesToView { get; set; }
        public string RolesToEdit { get; set; }
        public bool IsEnabled { get; set; }
        public int SortOrder { get; set; }
        public virtual Website Website { get; set; }
    }
}
