using System;
using System.Collections.Generic;

namespace WebCloud.Entity
{
    public partial class ProductFilterRange
    {
        public string WebsiteID { get; set; }
        public string ProductFilterRangeID { get; set; }
        public string ProductFilterID { get; set; }
        public string TextValueEqual { get; set; }
        public Nullable<decimal> NumberValueEqual { get; set; }
        public Nullable<bool> BooleanValueEqual { get; set; }
        public Nullable<System.DateTime> DatetimeValueEqual { get; set; }
        public Nullable<decimal> NumberValueMin { get; set; }
        public Nullable<System.DateTime> DatetimeValueMin { get; set; }
        public Nullable<decimal> NumberValueMax { get; set; }
        public Nullable<System.DateTime> DatetimeValueMax { get; set; }
        public int SortOrder { get; set; }
        public virtual ProductFilter ProductFilter { get; set; }
    }
}
