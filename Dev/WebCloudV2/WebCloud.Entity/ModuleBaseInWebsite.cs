using System;
using System.Collections.Generic;

namespace WebCloud.Entity
{
    public partial class ModuleBaseInWebsite
    {
        public string ModuleBaseID { get; set; }
        public string WebsiteID { get; set; }
        public Nullable<System.DateTime> InstalledDate { get; set; }
        public string InstalledUserName { get; set; }
        public Nullable<long> InstalledUserID { get; set; }
        public virtual ModuleBase ModuleBase { get; set; }
        public virtual Website Website { get; set; }
    }
}
