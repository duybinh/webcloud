using System;
using System.Collections.Generic;

namespace WebCloud.Entity
{
    public partial class ProductPropertyValue
    {
        public string WebsiteID { get; set; }
        public string LanguageID { get; set; }
        public string ProductID { get; set; }
        public string ProductPropertyID { get; set; }
        public string TextValue { get; set; }
        public Nullable<decimal> NumberValue { get; set; }
        public Nullable<bool> BooleanValue { get; set; }
        public Nullable<System.DateTime> DatetimeValue { get; set; }
        public virtual Language Language { get; set; }
        public virtual Product Product { get; set; }
        public virtual ProductProperty ProductProperty { get; set; }
    }
}
