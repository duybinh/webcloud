using System;
using System.Collections.Generic;

namespace WebCloud.Entity
{
    public partial class Config
    {
        public Config()
        {
            this.ConfigInWebsites = new List<ConfigInWebsite>();
        }

        public string ConfigID { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string DataType { get; set; }
        public string DefaultValue { get; set; }
        public string Group { get; set; }
        public string RolesToView { get; set; }
        public string RolesToEdit { get; set; }
        public string EditingControl { get; set; }
        public bool IsEnabled { get; set; }
        public int SortOrder { get; set; }
        public virtual ICollection<ConfigInWebsite> ConfigInWebsites { get; set; }
    }
}
