using System;
using System.Collections.Generic;

namespace WebCloud.Entity
{
    public partial class ProductInCategory
    {
        public string WebsiteID { get; set; }
        public string ProductCategoryID { get; set; }
        public string ProductID { get; set; }
        public virtual Product Product { get; set; }
        public virtual ProductCategory ProductCategory { get; set; }
    }
}
