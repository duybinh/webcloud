using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace WebCloud.Entity
{
    [Serializable]
    [DataContract(IsReference = true)]
    public partial class ModuleBase
    {
        public ModuleBase()
        {
            this.Modules = new List<Module>();
            this.ModuleBaseConfigs = new List<ModuleBaseConfig>(); 
            this.ModuleBaseInWebsites = new List<ModuleBaseInWebsite>();
        }

        [DataMember]
        public string ModuleBaseID { get; set; }

        [DataMember]
        public string DisplayControl { get; set; }

        [DataMember]
        public string ManageControl { get; set; }

        [DataMember]
        public string Name { get; set; }

        [DataMember]
        public string Description { get; set; }

        [DataMember]
        public string LayoutID { get; set; }

        [DataMember]
        public string ScriptID { get; set; }

        [DataMember]
        public string RolesToInstall { get; set; }

        [DataMember]
        public string RolesToCreate { get; set; }

        [DataMember]
        public string RolesToEdit { get; set; }

        [DataMember]
        public string RolesToView { get; set; }

        [DataMember]
        public string Group { get; set; }

        [DataMember]
        public string Thumbnail { get; set; }

        [DataMember]
        public string Version { get; set; }

        [DataMember]
        public string Author { get; set; }

        [DataMember]
        public int SortOrder { get; set; }

        [DataMember]
        public bool IsEnabled { get; set; }
        
        public virtual ICollection<Module> Modules { get; set; }
        
        [DataMember]
        public virtual ICollection<ModuleBaseConfig> ModuleBaseConfigs { get; set; }
        public virtual ICollection<ModuleBaseInWebsite> ModuleBaseInWebsites { get; set; }

    }
}
