using System;
using System.Collections.Generic;

namespace WebCloud.Entity
{
    public partial class Product
    {
        public Product()
        {
            this.ProductContents = new List<ProductContent>();
            this.ProductInCategories = new List<ProductInCategory>();
            this.ProductPropertyValues = new List<ProductPropertyValue>();
            this.Tags = new List<Tag>();
        }

        public string WebsiteID { get; set; }
        public string ProductID { get; set; }
        public long GlobalID { get; set; }
        public string ThumbImage { get; set; }
        public decimal Price { get; set; }
        public decimal OldPrice { get; set; }
        public bool IsNew { get; set; }
        public bool IsHot { get; set; }
        public bool IsDiscount { get; set; }
        public bool IsAvailable { get; set; }
        public bool AllowOrder { get; set; }
        public bool AllowComment { get; set; }
        public bool CallToPrice { get; set; }
        public Nullable<System.DateTime> DisplayDatetime { get; set; }
        public long SortOrder { get; set; }
        public bool IsEnabled { get; set; }
        public bool IsDeleted { get; set; }
        public Nullable<long> DiscussionGroupID { get; set; }
        public Nullable<long> StatisticID { get; set; }
        public Nullable<long> LogGroupID { get; set; }
        public System.DateTime CreatedDatetime { get; set; }
        public System.DateTime LastEditedDatetime { get; set; }
        public string CreatedUserName { get; set; }
        public string LastEditedUserName { get; set; }
        public Nullable<long> CreatedUserID { get; set; }
        public Nullable<long> LastEditedUserID { get; set; }
        public virtual DiscussionGroup DiscussionGroup { get; set; }
        public virtual LogGroup LogGroup { get; set; }
        public virtual Statistic Statistic { get; set; }
        public virtual Website Website { get; set; }
        public virtual ICollection<ProductContent> ProductContents { get; set; }
        public virtual ICollection<ProductInCategory> ProductInCategories { get; set; }
        public virtual ICollection<ProductPropertyValue> ProductPropertyValues { get; set; }
        public virtual ICollection<Tag> Tags { get; set; }
    }
}
