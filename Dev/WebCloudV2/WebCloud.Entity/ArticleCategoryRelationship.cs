namespace WebCloud.Entity
{
    public partial class ArticleCategoryRelationship
    {
        public string WebsiteID { get; set; }
        public string ParentID { get; set; }
        public string ChildID { get; set; }
        public int Level { get; set; }
        public virtual ArticleCategory ArticleCategory { get; set; }
    }
}
