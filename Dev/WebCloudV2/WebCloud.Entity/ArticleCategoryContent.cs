using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Runtime.Serialization;

namespace WebCloud.Entity
{
    [Serializable]
    [DataContract(IsReference = true)]
    public partial class ArticleCategoryContent
    {
        [DataMember]
        public string WebsiteID { get; set; }
        [DataMember]
        public string ArticleCategoryID { get; set; }
        [DataMember]
        public string LanguageID { get; set; }
        [DataMember]
        public string Name { get; set; }
        [DataMember]
        public string SeoName { get; set; }
        [DataMember]
        public string Description { get; set; }
        [DataMember]
        public string MetaTitle { get; set; }
        [DataMember]
        public string MetaKeyword { get; set; }
        [DataMember]
        public string MetaDescription { get; set; }

        public virtual ArticleCategory ArticleCategory { get; set; }
        public virtual Language Language { get; set; }
    }
}
