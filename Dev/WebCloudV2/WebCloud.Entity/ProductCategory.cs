using System;
using System.Collections.Generic;

namespace WebCloud.Entity
{
    public partial class ProductCategory
    {
        public ProductCategory()
        {
            this.ProductCategoryContents = new List<ProductCategoryContent>();
            this.ProductInCategories = new List<ProductInCategory>();
        }

        public string WebsiteID { get; set; }
        public string ProductCategoryID { get; set; }
        public string ParentID { get; set; }
        public string ThumbImage { get; set; }
        public bool DisplayProductThumb { get; set; }
        public bool IsHot { get; set; }
        public bool AllowComment { get; set; }
        public bool IsEnabled { get; set; }
        public bool IsDeleted { get; set; }
        public int SortOrder { get; set; }
        public bool IsLeaf { get; set; }
        public Nullable<long> LogGroupID { get; set; }
        public System.DateTime CreatedDatetime { get; set; }
        public System.DateTime LastEditedDatetime { get; set; }
        public string CreatingUser { get; set; }
        public string LastEditedUser { get; set; }
        public virtual Website Website { get; set; }
        public virtual ICollection<ProductCategoryContent> ProductCategoryContents { get; set; }
        public virtual ProductCategoryRelationship ProductCategoryRelationship { get; set; }
        public virtual ICollection<ProductInCategory> ProductInCategories { get; set; }
    }
}
