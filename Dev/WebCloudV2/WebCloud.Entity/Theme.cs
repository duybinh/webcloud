using System;
using System.Collections.Generic;

namespace WebCloud.Entity
{
    public partial class Theme
    {
        public string ThemeCode { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public Nullable<int> ThemeCategoryID { get; set; }
        public string BasedTheme { get; set; }
        public string Author { get; set; }
        public string RolesToView { get; set; }
        public string RolesToInstall { get; set; }
        public bool IsEnabled { get; set; }
        public int SortOrder { get; set; }
        public virtual ThemeCategory ThemeCategory { get; set; }
    }
}
