using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace WebCloud.Entity
{
    [Serializable]
    [DataContract]
    public partial class UserRole
    {
        public UserRole()
        {
            this.Users = new List<User>();
            this.Websites = new List<Website>();
        
        }
        [DataMember]
        public string UserRoleID { get; set; }
        
        [DataMember]
        public string Name { get; set; }
        
        [DataMember]
        public string Description { get; set; }
        
        [DataMember]
        public int SortOrder { get; set; }
        
        [DataMember]
        public bool IsEnabled { get; set; }
        public virtual ICollection<User> Users { get; set; }
        public virtual ICollection<Website> Websites { get; set; }
    }
}
