using System;
using System.Collections.Generic;

namespace WebCloud.Entity
{
    public partial class WebPlan
    {
        public WebPlan()
        {
            this.Websites = new List<Website>();
        }

        public int WebPlanID { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string Name_Lang { get; set; }
        public string Description_Lang { get; set; }
        public bool IsEnabled { get; set; }
        public int SortOrder { get; set; }
        public virtual ICollection<Website> Websites { get; set; }
    }
}
