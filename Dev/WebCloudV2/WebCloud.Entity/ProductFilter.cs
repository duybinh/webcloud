using System;
using System.Collections.Generic;

namespace WebCloud.Entity
{
    public partial class ProductFilter
    {
        public ProductFilter()
        {
            this.ProductFilterRanges = new List<ProductFilterRange>();
        }

        public string WebsiteID { get; set; }
        public string ProductFilterID { get; set; }
        public string ProductPropertyID { get; set; }
        public bool AllowMultyChoice { get; set; }
        public int SortOrder { get; set; }
        public bool IsEnabled { get; set; }
        public bool IsDeleted { get; set; }
        public virtual Website Website { get; set; }
        public virtual ICollection<ProductFilterRange> ProductFilterRanges { get; set; }
    }
}
