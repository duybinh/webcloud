using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace WebCloud.Entity
{
    [Serializable]
    [DataContract(IsReference = true)]
    public partial class DiscussionGroup
    {
        public DiscussionGroup()
        {
            this.Articles = new List<Article>();
            this.Discussions = new List<Discussion>();
            this.Products = new List<Product>();
        }

        [DataMember]
        public long DiscussionGroupID { get; set; }
        [DataMember]
        public System.DateTime DatetimeCreated { get; set; }
        [DataMember]
        public long DiscussionCount { get; set; }
        [DataMember]
        public long TotalRateValue { get; set; }
        [DataMember]
        public long RateCount { get; set; }
        [DataMember]
        public decimal RateAverage { get; set; }
        [DataMember]
        public bool IsClosed { get; set; }
        [DataMember]
        public Nullable<System.DateTime> DatetimeClosed { get; set; }
        [DataMember]
        public string ReferenceTable { get; set; }
        [DataMember]
        public string ItemID { get; set; }
        [DataMember]
        public string WebsiteID { get; set; }
        public virtual ICollection<Article> Articles { get; set; }
        public virtual ICollection<Discussion> Discussions { get; set; }
        public virtual ICollection<Product> Products { get; set; }
    }
}
