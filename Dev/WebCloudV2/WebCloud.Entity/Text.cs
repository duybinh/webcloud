using System;
using System.Collections.Generic;

namespace WebCloud.Entity
{
    public partial class Text
    {
        public string WebsiteID { get; set; }
        public string TextID { get; set; }
        public string LanguageID { get; set; }
        public string Value { get; set; }
        public string ReferenceTable { get; set; }
        public string ReferenceField { get; set; }
        public string ReferenceObject { get; set; }
        public virtual Language Language { get; set; }
        public virtual Website Website { get; set; }
    }
}
