using System;

namespace WebCloud.Entity
{
    public partial class RefreshToken
    {
        public string RefreshTokenID { get; set; }
        public string UserID { get; set; }
        public string WebsiteID { get; set; }
        public string Client { get; set; }
        public string Audience { get; set; }
        public System.DateTime IssuedUtc { get; set; }
        public Nullable<System.DateTime> ExpiredUtc { get; set; }
        public string ProtectedTicket { get; set; }
    }
}
