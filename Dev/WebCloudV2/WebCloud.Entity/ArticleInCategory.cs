using System;
using System.Collections.Generic;

namespace WebCloud.Entity
{
    public partial class ArticleInCategory
    {
        public string WebsiteID { get; set; }
        public string ArticleCategoryID { get; set; }
        public string ArticleID { get; set; }
        public virtual Article Article { get; set; }
        public virtual ArticleCategory ArticleCategory { get; set; }
    }
}
