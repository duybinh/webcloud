using System;
using System.Collections.Generic;

namespace WebCloud.Entity
{
    public partial class OAuthProvider
    {
        public OAuthProvider()
        {
            this.UserOAuthMemberships = new List<UserOAuthMembership>();
        }

        public int OAuthProviderID { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string Website { get; set; }
        public virtual ICollection<UserOAuthMembership> UserOAuthMemberships { get; set; }
    }
}
