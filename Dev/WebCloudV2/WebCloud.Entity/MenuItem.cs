using System;
using System.Collections.Generic;

namespace WebCloud.Entity
{
    public partial class MenuItem
    {
        public MenuItem()
        {
            this.MenuItemContents = new List<MenuItemContent>();
        }

        public string WebsiteID { get; set; }
        public string MenuItemID { get; set; }
        public string ParentID { get; set; }
        public string Icon { get; set; }
        public string TargetUrl { get; set; }
        public int OpenMode { get; set; }
        public int SortOrder { get; set; }
        public bool IsEnabled { get; set; }
        public string SpecialClass { get; set; }
        public bool IsDeleted { get; set; }
        public virtual Website Website { get; set; }
        public virtual ICollection<MenuItemContent> MenuItemContents { get; set; }
    }
}
